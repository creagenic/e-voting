/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.controlcomponent.commandmessaging.CommandEntity;
import ch.post.it.evoting.controlcomponent.commandmessaging.CommandId;
import ch.post.it.evoting.controlcomponent.commandmessaging.CommandService;

/**
 * Class for processing requests exactly once.
 */
@Service
public class ExactlyOnceCommandExecutor {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExactlyOnceCommandExecutor.class);

	private final CommandService commandService;

	@Value("${nodeID}")
	private int nodeId;

	public ExactlyOnceCommandExecutor(final CommandService commandService) {
		this.commandService = commandService;
	}

	/**
	 * Processes a given task exactly once.
	 * <ul>
	 * <li>If the same request (same ids, same request content) is already stored in the database, the response to the request is taken from the database and returned.</li>
	 * <li>If the same request ids but different request content, an IllegalStateException is thrown.</li>
	 * <li>Otherwise the request is processed and the response is persisted and returned.</li>
	 * </ul>
	 *
	 * @param exactlyOnceCommand The exactlyOnceCommand to be processed.
	 * @return the result of the processing as a byte array.
	 * @throws NullPointerException  if the exactlyOnceCommand is null.
	 * @throws IllegalStateException if the request could not be processed correctly.
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public <T> byte[] process(final ExactlyOnceCommand<T> exactlyOnceCommand) {
		checkNotNull(exactlyOnceCommand);
		final LocalDateTime requestDateTime = LocalDateTime.now();
		final String correlationId = exactlyOnceCommand.getCorrelationId();
		final String contextId = exactlyOnceCommand.getContextId();
		final String context = exactlyOnceCommand.getContext();
		final Callable<T> task = exactlyOnceCommand.getTask();
		final Function<T, byte[]> serializer = exactlyOnceCommand.getSerializer();
		final byte[] requestContent = exactlyOnceCommand.getRequestContent();

		final CommandId commandId = CommandId.builder()
				.contextId(contextId)
				.context(context)
				.correlationId(correlationId)
				.nodeId(nodeId)
				.build();
		final List<CommandEntity> identicalCommandEntity = commandService.findSemanticallyIdenticalCommand(commandId);
		checkState(identicalCommandEntity.size() <= 1, "There was a problem with exactly once processing, multiple semantically identical commands exist.");

		final byte[] result;
		if (identicalCommandEntity.size() == 1) {
			final byte[] requestPayload = identicalCommandEntity.get(0).getRequestPayload();
			// Check if the identical command request payload is the same as the new request payload.
			if (Arrays.equals(requestContent, requestPayload)) {
				LOGGER.warn("Request already processed, returning previously calculated payload. [correlationId: {}, contextId: {}, context: {}]",
						correlationId, contextId, context);
				return identicalCommandEntity.get(0).getResponsePayload();
			} else {
				final String errorMessage = String.format(
						"Similar request previously treated but for different request payload. [correlationId: %s, contextId: %s, context: %s, nodeId: %s]",
						correlationId, contextId, context, nodeId);
				throw new IllegalStateException(errorMessage);
			}
		} else {
			try {
				result = serializer.apply(task.call());
			} catch (final Exception e) {
				throw new IllegalStateException("Failed to execute exactly once command", e);
			}

			commandService.save(commandId, requestContent, requestDateTime, result, LocalDateTime.now());
			return result;
		}
	}
}
