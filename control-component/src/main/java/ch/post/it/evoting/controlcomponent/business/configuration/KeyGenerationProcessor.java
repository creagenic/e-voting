/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.business.configuration;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.SignatureException;
import java.time.LocalDateTime;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponent.service.ElectionContextService;
import ch.post.it.evoting.controlcomponent.service.ElectionEventService;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.configuration.ControlComponentPublicKeysPayload;
import ch.post.it.evoting.evotinglibraries.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionEventContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;

@Service
public class KeyGenerationProcessor {

	private static final Logger LOGGER = LoggerFactory.getLogger(KeyGenerationProcessor.class);

	private final ObjectMapper objectMapper;
	private final KeyGenerationService keyGenerationService;
	private final ElectionEventService electionEventService;
	private final ElectionContextService electionContextService;
	private final SignatureKeystore<Alias> signatureKeystoreService;

	@Value("${nodeID}")
	private int nodeId;

	KeyGenerationProcessor(final ObjectMapper objectMapper,
			final KeyGenerationService keyGenerationService,
			final ElectionEventService electionEventService,
			final ElectionContextService electionContextService,
			final SignatureKeystore<Alias> signatureKeystoreService) {
		this.objectMapper = objectMapper;
		this.keyGenerationService = keyGenerationService;
		this.electionEventService = electionEventService;
		this.electionContextService = electionContextService;
		this.signatureKeystoreService = signatureKeystoreService;
	}

	public ControlComponentPublicKeysPayload onRequest(final ElectionEventContextPayload electionEventContextPayload) {
		checkNotNull(electionEventContextPayload);
		final ElectionEventContext electionEventContext = electionEventContextPayload.getElectionEventContext();
		final String electionEventId = electionEventContext.electionEventId();

		checkArgument(electionEventContext.finishTime().isAfter(LocalDateTime.now()),
				"The election event period should not be finished yet. [electionEventId: %s, nodeId: %s]", electionEventId, nodeId);

		// Save the encryption group.
		final GqGroup encryptionGroup = electionEventContextPayload.getEncryptionGroup();
		electionEventService.save(electionEventId, encryptionGroup);
		LOGGER.info("Saved encryption parameters. [electionEventId: {}, nodeId: {}]", electionEventId, nodeId);

		// Save the election event context.
		electionContextService.save(electionEventContext);
		LOGGER.info("Saved election event context. [electionEventId: {}, nodeId: {}]", electionEventId, nodeId);

		// Generate ccrj and ccmj keys.
		final ControlComponentPublicKeys controlComponentPublicKeys = keyGenerationService.generateCCKeys(encryptionGroup, electionEventId,
				electionEventContext);

		// Create and sign payload.
		final ControlComponentPublicKeysPayload controlComponentPublicKeysPayload = new ControlComponentPublicKeysPayload(encryptionGroup,
				electionEventId, controlComponentPublicKeys);
		controlComponentPublicKeysPayload.setSignature(generateResponsePayloadSignature(controlComponentPublicKeysPayload));

		LOGGER.info("Successfully signed control component public keys payload. [electionEventId: {}, nodeId: {}]", electionEventId, nodeId);

		return controlComponentPublicKeysPayload;
	}

	public ElectionEventContextPayload deserializeRequest(final byte[] messageBytes) {
		checkNotNull(messageBytes);
		final byte[] messageBytesCopy = Arrays.copyOf(messageBytes, messageBytes.length);
		try {
			return objectMapper.readValue(messageBytesCopy, ElectionEventContextPayload.class);
		} catch (final IOException e) {
			throw new UncheckedIOException("Unable to deserialize Election Event Context payload", e);
		}
	}

	public byte[] serializeResponse(final ControlComponentPublicKeysPayload controlComponentPublicKeysPayload) {
		checkNotNull(controlComponentPublicKeysPayload);
		try {
			return objectMapper.writeValueAsBytes(controlComponentPublicKeysPayload);
		} catch (final IOException e) {
			throw new UncheckedIOException("Unable to serialize Control Component Public Keys Payload", e);
		}

	}

	public boolean verifyPayloadSignature(final ElectionEventContextPayload electionEventContextPayload) {
		checkNotNull(electionEventContextPayload);
		final String electionEventId = electionEventContextPayload.getElectionEventContext().electionEventId();

		final CryptoPrimitivesSignature signature = electionEventContextPayload.getSignature();

		checkState(signature != null, "The signature of the election event context payload is null. [electionEventId: %s, nodeId: %s]",
				electionEventId, nodeId);

		final Hashable additionalContextData = ChannelSecurityContextData.electionEventContext(electionEventId);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.SDM_CONFIG, electionEventContextPayload, additionalContextData,
					signature.signatureContents());
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not verify the signature of the election event context payload. [electionEventId: %s, nodeId: %s]",
							electionEventId, nodeId));
		}

		return isSignatureValid;
	}

	private CryptoPrimitivesSignature generateResponsePayloadSignature(final ControlComponentPublicKeysPayload payload) {
		final String electionEventId = payload.getElectionEventId();

		final Hashable additionalContextData = ChannelSecurityContextData.controlComponentPublicKeys(nodeId, electionEventId);

		try {
			final byte[] signature = signatureKeystoreService.generateSignature(payload, additionalContextData);

			return new CryptoPrimitivesSignature(signature);
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not generate control component public keys payload signature. [contextIds: %s, nodeId: %s]", electionEventId,
							nodeId));
		}
	}

}
