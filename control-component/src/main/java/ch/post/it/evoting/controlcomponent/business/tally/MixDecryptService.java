/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.business.tally;

import static ch.post.it.evoting.evotinglibraries.domain.validations.ControlComponentVotesHashPayloadValidation.validate;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.annotations.VisibleForTesting;

import ch.post.it.evoting.controlcomponent.domain.BallotBoxEntity;
import ch.post.it.evoting.controlcomponent.protocol.tally.mixonline.MixDecOnlineOutput;
import ch.post.it.evoting.controlcomponent.protocol.tally.mixonline.MixDecOnlineService;
import ch.post.it.evoting.controlcomponent.protocol.tally.mixonline.VerifyMixDecOnlineService;
import ch.post.it.evoting.controlcomponent.service.BallotBoxService;
import ch.post.it.evoting.controlcomponent.service.ElectionContextService;
import ch.post.it.evoting.controlcomponent.service.ElectionEventService;
import ch.post.it.evoting.controlcomponent.service.EncryptedVerifiableVoteService;
import ch.post.it.evoting.controlcomponent.service.MixnetInitialCiphertextsService;
import ch.post.it.evoting.controlcomponent.service.SetupComponentPublicKeysService;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.mixnet.VerifiableShuffle;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.VerifiableDecryptions;
import ch.post.it.evoting.evotinglibraries.domain.common.EncryptedVerifiableVote;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentVotesHashPayload;
import ch.post.it.evoting.evotinglibraries.domain.tally.ControlComponentBallotBoxPayload;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.PrimesMappingTableAlgorithms;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixonline.GetMixnetInitialCiphertextsOutput;

@Service
class MixDecryptService {
	private static final Logger LOGGER = LoggerFactory.getLogger(MixDecryptService.class);

	private final ElectionEventService electionEventService;
	private final BallotBoxService ballotBoxService;
	private final ElectionContextService electionContextService;
	private final SetupComponentPublicKeysService setupComponentPublicKeysService;
	private final MixDecOnlineService mixDecOnlineService;
	private final VerifyMixDecOnlineService verifyMixDecOnlineService;
	private final EncryptedVerifiableVoteService encryptedVerifiableVoteService;
	private final PrimesMappingTableAlgorithms primesMappingTableAlgorithms;
	private final MixnetInitialCiphertextsService mixnetInitialCiphertextsService;

	@Value("${nodeID}")
	private int nodeId;

	MixDecryptService(
			final ElectionEventService electionEventService,
			final BallotBoxService ballotBoxService,
			final ElectionContextService electionContextService,
			final SetupComponentPublicKeysService setupComponentPublicKeysService,
			final MixDecOnlineService mixDecOnlineService,
			final VerifyMixDecOnlineService verifyMixDecOnlineService,
			final EncryptedVerifiableVoteService encryptedVerifiableVoteService,
			final PrimesMappingTableAlgorithms primesMappingTableAlgorithms,
			final MixnetInitialCiphertextsService mixnetInitialCiphertextsService) {
		this.electionEventService = electionEventService;
		this.ballotBoxService = ballotBoxService;
		this.electionContextService = electionContextService;
		this.setupComponentPublicKeysService = setupComponentPublicKeysService;
		this.mixDecOnlineService = mixDecOnlineService;
		this.verifyMixDecOnlineService = verifyMixDecOnlineService;
		this.encryptedVerifiableVoteService = encryptedVerifiableVoteService;
		this.primesMappingTableAlgorithms = primesMappingTableAlgorithms;
		this.mixnetInitialCiphertextsService = mixnetInitialCiphertextsService;
	}

	@Transactional
	public MixDecryptServiceOutput performMixDecrypt(final String electionEventId, final String ballotBoxId,
			final List<ControlComponentVotesHashPayload> controlComponentVotesHashPayloads,
			final List<ControlComponentShufflePayload> controlComponentShufflePayloads) {

		validateUUID(electionEventId);
		validateUUID(ballotBoxId);
		validateIdsCorrectness(electionEventId, ballotBoxId);

		final List<ControlComponentVotesHashPayload> controlComponentVotesHashPayloadsCopy = validate(electionEventId, ballotBoxId,
				controlComponentVotesHashPayloads);
		final List<ControlComponentShufflePayload> controlComponentShufflePayloadsCopy = List.copyOf(checkNotNull(controlComponentShufflePayloads));
		final GqGroup encryptionGroup = electionEventService.getEncryptionGroup(electionEventId);

		validateConsistency(encryptionGroup, electionEventId, ballotBoxId, controlComponentShufflePayloadsCopy);

		final BallotBoxEntity ballotBoxEntity = ballotBoxService.getBallotBox(ballotBoxId);
		final PrimesMappingTable primesMappingTable = ballotBoxEntity.getPrimesMappingTable();
		final int numberOfWriteInsPlusOne = primesMappingTableAlgorithms.getDelta(primesMappingTable);

		final GetMixnetInitialCiphertextsOutput getMixnetInitialCiphertextsOutput = mixnetInitialCiphertextsService.getMixnetInitialCiphertextsOutput(
				electionEventId, ballotBoxId);

		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys = setupComponentPublicKeysService.getCcmElectionPublicKeys(
				electionEventId);
		final ElGamalMultiRecipientPublicKey electoralBoardPublicKey = setupComponentPublicKeysService.getElectoralBoardPublicKey(electionEventId);

		// The first node has nothing to verify.
		if (nodeId != 1) {
			if (!verifyMixDecOnlineService.verifyMixDecOnline(controlComponentShufflePayloadsCopy, encryptionGroup, numberOfWriteInsPlusOne,
					ccmElectionPublicKeys, electoralBoardPublicKey, getMixnetInitialCiphertextsOutput)) {
				throw new IllegalStateException(String.format(
						"The preceding control-component's mixing and decryption proofs are invalid. [electionEventId: %s, ballotBoxId: %s]",
						electionEventId, ballotBoxId));
			}
			LOGGER.info("VerifyMixDecOnline algorithm successfully performed. "
							+ "The preceding control-component's mixing and decryption proofs are valid. [electionEventId: {}, ballotBoxId: {}]",
					electionEventId, ballotBoxId);
		}

		final List<VerifiableDecryptions> precedingVerifiableDecryptions = controlComponentShufflePayloadsCopy.stream()
				.map(ControlComponentShufflePayload::getVerifiableDecryptions)
				.toList();

		final MixDecOnlineOutput mixDecOnlineOutput = mixDecOnlineService.mixDecOnline(controlComponentVotesHashPayloadsCopy, encryptionGroup,
				numberOfWriteInsPlusOne, ccmElectionPublicKeys, electoralBoardPublicKey, getMixnetInitialCiphertextsOutput,
				precedingVerifiableDecryptions);
		LOGGER.info("MixDecOnline algorithm successfully performed. Ballot box successfully mixed. [electionEventId: {}, ballotBoxId: {}]",
				electionEventId, ballotBoxId);

		final VerifiableDecryptions verifiableDecryptions = mixDecOnlineOutput.verifiableDecryptions();
		final VerifiableShuffle verifiableShuffle = mixDecOnlineOutput.verifiableShuffle();
		final ControlComponentShufflePayload controlComponentShufflePayload = new ControlComponentShufflePayload(encryptionGroup, electionEventId,
				ballotBoxId, nodeId, verifiableShuffle, verifiableDecryptions);
		LOGGER.info("Control component shuffle payload retrieved. [electionEventId: {}, ballotBoxId: {}]", electionEventId, ballotBoxId);

		final String verificationCardSetId = ballotBoxEntity.getVerificationCardSetEntity().getVerificationCardSetId();
		final List<EncryptedVerifiableVote> confirmedVotes = encryptedVerifiableVoteService.getConfirmedVotes(verificationCardSetId);
		final ControlComponentBallotBoxPayload controlComponentBallotBoxPayload = new ControlComponentBallotBoxPayload(encryptionGroup,
				electionEventId, ballotBoxId, nodeId, confirmedVotes);

		return new MixDecryptServiceOutput(controlComponentBallotBoxPayload, controlComponentShufflePayload);
	}

	private void validateConsistency(final GqGroup encryptionGroup, final String electionEventId, final String ballotBoxId,
			final List<ControlComponentShufflePayload> shufflePayloads) {
		validateShufflePayload(encryptionGroup, electionEventId, ballotBoxId, shufflePayloads);
		validateMixIsAllowed(electionEventId, ballotBoxId, LocalDateTime::now);
	}

	private void validateIdsCorrectness(final String electionEventId, final String ballotBoxId) {
		checkArgument(electionEventService.exists(electionEventId), "The given election event ID does not exist. [electionEventId: %s]",
				electionEventId);
		checkArgument(ballotBoxService.existsForElectionEventId(ballotBoxId, electionEventId),
				"The given ballot box ID does not exist for the given election event ID. [ballotBoxId: %s, electionEventId: %s]", ballotBoxId,
				electionEventId);
	}

	@VisibleForTesting
	void validateShufflePayload(final GqGroup encryptionGroup, final String electionEventId, final String ballotBoxId,
			final List<ControlComponentShufflePayload> shufflePayloads) {
		checkNotNull(encryptionGroup);
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);
		checkNotNull(shufflePayloads);
		checkArgument(shufflePayloads.size() == nodeId - 1,
				"There must be exactly the expected number of shuffle payloads. [expected: %s, actual: %s]", nodeId - 1, shufflePayloads.size());

		shufflePayloads.forEach(payload -> {
			checkState(electionEventId.equals(payload.getElectionEventId()),
					"Election event ID must be identical in shuffle payload. [expected: %s, actual: %s]", electionEventId,
					payload.getElectionEventId());
			checkState(ballotBoxId.equals(payload.getBallotBoxId()),
					"Ballot box ID must be identical in shuffle payload. [expected: %s, actual: %s]", ballotBoxId,
					payload.getBallotBoxId());
			checkState(encryptionGroup.equals(payload.getEncryptionGroup()),
					"Gq groups must be identical in shuffle payload. [expected: %s, actual: %s]", encryptionGroup,
					payload.getEncryptionGroup());
		});

		final Set<Integer> actualPayloadNodeIds = shufflePayloads.stream()
				.map(ControlComponentShufflePayload::getNodeId)
				.collect(Collectors.toSet());
		final Set<Integer> expectedPayloadNodeIds = IntStream.range(1, nodeId).boxed().collect(Collectors.toSet());

		checkState(actualPayloadNodeIds.containsAll(expectedPayloadNodeIds),
				"Payloads must come from expected nodes. [expected: %s, actual: %s]", expectedPayloadNodeIds, actualPayloadNodeIds);
	}

	@VisibleForTesting
	void validateMixIsAllowed(final String electionEventId, final String ballotBoxId, final Supplier<LocalDateTime> now) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);
		checkNotNull(now);

		final LocalDateTime currentTime = now.get();
		final LocalDateTime electionEndTime = electionContextService.getElectionEventFinishTime(electionEventId);
		final BallotBoxEntity ballotBoxEntity = ballotBoxService.getBallotBox(ballotBoxId);

		final boolean afterEndTime = currentTime.isAfter(electionEndTime.plusSeconds(ballotBoxEntity.getGracePeriod()));

		// Test ballot boxes can be mixed and decrypted at any time. Real ballot boxes can be mixed and decrypted only after the election event period ended.
		checkState(ballotBoxEntity.isTestBallotBox() || afterEndTime,
				"The ballot box can not be mixed. [isTestBallotBox: %s, finishTime: %s, electionEventId: %s, ballotBoxId: %s]",
				ballotBoxEntity.isTestBallotBox(), electionEndTime, electionEventId, ballotBoxId);
	}

	record MixDecryptServiceOutput(ControlComponentBallotBoxPayload controlComponentBallotBoxPayload,
								   ControlComponentShufflePayload controlComponentShufflePayload) {

		MixDecryptServiceOutput {
			checkNotNull(controlComponentBallotBoxPayload);
			checkNotNull(controlComponentShufflePayload);
		}

	}

}
