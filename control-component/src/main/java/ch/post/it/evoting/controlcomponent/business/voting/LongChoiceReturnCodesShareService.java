/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.business.voting;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.controlcomponent.domain.VerificationCardSetEntity;
import ch.post.it.evoting.controlcomponent.protocol.voting.sendvote.CreateLCCShareOutput;
import ch.post.it.evoting.controlcomponent.protocol.voting.sendvote.CreateLCCShareService;
import ch.post.it.evoting.controlcomponent.protocol.voting.sendvote.DecryptPCCService;
import ch.post.it.evoting.controlcomponent.service.BallotBoxService;
import ch.post.it.evoting.controlcomponent.service.ElectionEventService;
import ch.post.it.evoting.controlcomponent.service.IdentifierValidationService;
import ch.post.it.evoting.controlcomponent.service.VerificationCardSetService;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.LongChoiceReturnCodesShare;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;

/**
 * Decrypts the partially decrypted encrypted Choice Return Codes and creates the CCR_j long Choice Return Code shares.
 */
@Service
public class LongChoiceReturnCodesShareService {

	private static final Logger LOGGER = LoggerFactory.getLogger(LongChoiceReturnCodesShareService.class);

	private final DecryptPCCService decryptPCCService;
	private final CreateLCCShareService createLCCShareService;
	private final ElectionEventService electionEventService;
	private final VerificationCardSetService verificationCardSetService;
	private final IdentifierValidationService identifierValidationService;
	private final BallotBoxService ballotBoxService;

	@Value("${nodeID}")
	private int nodeId;

	public LongChoiceReturnCodesShareService(
			final DecryptPCCService decryptPCCService,
			final CreateLCCShareService createLCCShareService,
			final ElectionEventService electionEventService,
			final VerificationCardSetService verificationCardSetService,
			final IdentifierValidationService identifierValidationService,
			final BallotBoxService ballotBoxService) {
		this.decryptPCCService = decryptPCCService;
		this.createLCCShareService = createLCCShareService;
		this.electionEventService = electionEventService;
		this.verificationCardSetService = verificationCardSetService;
		this.identifierValidationService = identifierValidationService;
		this.ballotBoxService = ballotBoxService;
	}

	/**
	 * Decrypts the partially decrypted encrypted Choice Return Codes with the DecryptPCC algorithm and computes the CCR_j long Choice Return Codes
	 * share with the CreateLCCShare algorithm.
	 *
	 * @param controlComponentPartialDecryptPayloads the partially decrypted encrypted node contributions.
	 * @return the Long Choice Return Codes share.
	 */
	LongChoiceReturnCodesShare computeLCCShares(final List<ControlComponentPartialDecryptPayload> controlComponentPartialDecryptPayloads) {
		final ContextIds contextIds = controlComponentPartialDecryptPayloads.get(0).getPartiallyDecryptedEncryptedPCC().contextIds();

		identifierValidationService.validateContextIds(contextIds);
		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		final GqGroup encryptionGroup = electionEventService.getEncryptionGroup(electionEventId);

		checkArgument(controlComponentPartialDecryptPayloads.get(0).getEncryptionGroup().equals(encryptionGroup),
				"The control component partial decrypt payloads do not have the expected encryption group.");

		LOGGER.debug("Starting decryption of the partially decrypted encrypted Choice Return Codes. [contextIds: {}]", contextIds);

		final VerificationCardSetEntity verificationCardSetEntity = verificationCardSetService.getVerificationCardSet(verificationCardSetId);
		final PrimesMappingTable primesMappingTable = ballotBoxService.getBallotBoxPrimesMappingTable(verificationCardSetEntity);

		final GroupVector<GqElement, GqGroup> decryptedPartialChoiceReturnCodes = decryptPCCService.decryptPCC(encryptionGroup, primesMappingTable,
				controlComponentPartialDecryptPayloads);
		LOGGER.info("DecryptPCC algorithm successfully performed. [electionEventId: {}]", electionEventId);

		final CreateLCCShareOutput createLCCShareOutput = createLCCShareService.createLCCShare(encryptionGroup, contextIds, primesMappingTable,
				decryptedPartialChoiceReturnCodes);
		LOGGER.info("CreateLCCShare algorithm successfully performed. [electionEventId: {}]", electionEventId);

		return new LongChoiceReturnCodesShare(electionEventId, verificationCardSetId, verificationCardId, nodeId,
				createLCCShareOutput.longChoiceReturnCodeShare());
	}
}
