/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.business.voting;

import static ch.post.it.evoting.cryptoprimitives.utils.Validations.allEqual;
import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.SignatureException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponent.domain.ElectionEventState;
import ch.post.it.evoting.controlcomponent.protocol.voting.confirmvote.VerifyLVCCHashService;
import ch.post.it.evoting.controlcomponent.service.ElectionEventService;
import ch.post.it.evoting.controlcomponent.service.ElectionEventStateService;
import ch.post.it.evoting.controlcomponent.service.IdentifierValidationService;
import ch.post.it.evoting.controlcomponent.service.VerificationCardStateService;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.voting.confirmvote.ConfirmationKey;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponenthlVCCPayload;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponenthlVCCRequestPayload;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponentlVCCSharePayload;
import ch.post.it.evoting.domain.voting.confirmvote.LongVoteCastReturnCodesShare;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;

@Service
public class LongVoteCastReturnCodesShareVerifyProcessor {

	private static final Logger LOGGER = LoggerFactory.getLogger(LongVoteCastReturnCodesShareVerifyProcessor.class);

	private final ObjectMapper objectMapper;
	private final ElectionEventService electionEventService;
	private final VerifyLVCCHashService verifyLVCCHashService;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final ElectionEventStateService electionEventStateService;
	private final IdentifierValidationService identifierValidationService;
	private final VerificationCardStateService verificationCardStateService;
	private final LongVoteCastReturnCodesShareService longVoteCastReturnCodesShareService;

	@Value("${nodeID}")
	private int nodeId;

	LongVoteCastReturnCodesShareVerifyProcessor(
			final ObjectMapper objectMapper,
			final ElectionEventService electionEventService,
			final VerifyLVCCHashService verifyLVCCHashService,
			final SignatureKeystore<Alias> signatureKeystoreService,
			final ElectionEventStateService electionEventStateService,
			final IdentifierValidationService identifierValidationService,
			final VerificationCardStateService verificationCardStateService,
			final LongVoteCastReturnCodesShareService longVoteCastReturnCodesShareService) {
		this.objectMapper = objectMapper;
		this.electionEventService = electionEventService;
		this.verifyLVCCHashService = verifyLVCCHashService;
		this.signatureKeystoreService = signatureKeystoreService;
		this.electionEventStateService = electionEventStateService;
		this.identifierValidationService = identifierValidationService;
		this.verificationCardStateService = verificationCardStateService;
		this.longVoteCastReturnCodesShareService = longVoteCastReturnCodesShareService;
	}

	public ControlComponentlVCCSharePayload onRequest(final ControlComponenthlVCCRequestPayload controlComponenthlVCCRequestPayload) {
		checkNotNull(controlComponenthlVCCRequestPayload);

		final List<ControlComponenthlVCCPayload> controlComponenthlVCCPayloads = controlComponenthlVCCRequestPayload.controlComponenthlVCCPayloads();

		verifyPayloadsConsistency(controlComponenthlVCCPayloads);

		final ContextIds contextIds = controlComponenthlVCCPayloads.get(0).getConfirmationKey().contextIds();

		identifierValidationService.validateContextIds(contextIds);
		final String electionEventId = contextIds.electionEventId();
		final String verificationCardId = contextIds.verificationCardId();
		final String verificationCardSetId = contextIds.verificationCardSetId();

		longVoteCastReturnCodesShareService.validateConfirmationIsAllowed(electionEventId, verificationCardId, LocalDateTime::now);

		final int confirmationAttempts = verificationCardStateService.getConfirmationAttempts(verificationCardId);
		final String contextId = String.join("-", electionEventId, verificationCardSetId, verificationCardId, String.valueOf(confirmationAttempts));
		LOGGER.info("Received Long Vote Cast Return Codes Share verify request. [contextId: {}]", contextId);

		return generateControlComponentlVCCSharePayload(controlComponenthlVCCPayloads);
	}

	public boolean verifyPayloadSignature(final ControlComponenthlVCCRequestPayload controlComponenthlVCCRequestPayload) {
		checkNotNull(controlComponenthlVCCRequestPayload);

		for (final ControlComponenthlVCCPayload controlComponenthlVCCPayload : controlComponenthlVCCRequestPayload.controlComponenthlVCCPayloads()) {

			final CryptoPrimitivesSignature signature = controlComponenthlVCCPayload.getSignature();
			final ContextIds contextIds = controlComponenthlVCCRequestPayload.controlComponenthlVCCPayloads().get(0).getConfirmationKey()
					.contextIds();
			final int payloadNodeId = controlComponenthlVCCPayload.getNodeId();

			checkState(signature != null, "The signature of Control Component hlVCC Payload is null. [contextIds: %s]",
					contextIds);

			final Hashable additionalContextData = ChannelSecurityContextData.controlComponenthlVCC(payloadNodeId, contextIds.electionEventId(),
					contextIds.verificationCardSetId(), contextIds.verificationCardId());
			final boolean isSignatureValid;
			try {
				isSignatureValid = signatureKeystoreService.verifySignature(Alias.getControlComponentByNodeId(payloadNodeId),
						controlComponenthlVCCPayload, additionalContextData, signature.signatureContents());
			} catch (final SignatureException e) {
				throw new IllegalStateException(String.format(
						"Cannot verify the signature of Control Component hlVCC Payload. [contextIds: %s, payload nodeId: %s]",
						contextIds, payloadNodeId), e);
			}

			if (!isSignatureValid) {
				return false;
			}
		}
		return true;
	}

	private void verifyPayloadsConsistency(final List<ControlComponenthlVCCPayload> controlComponenthlVCCPayloads) {
		checkArgument(controlComponenthlVCCPayloads.size() == NODE_IDS.size(),
				"There are not exactly the expected number of Control Component hlVCC Payloads. [expected: %s, received: %s]",
				NODE_IDS.size(), controlComponenthlVCCPayloads.size());

		final ConfirmationKey confirmationKey = controlComponenthlVCCPayloads.get(0).getConfirmationKey();
		final ContextIds contextIds = confirmationKey.contextIds();

		final List<Integer> payloadNodeIds = controlComponenthlVCCPayloads.stream()
				.map(ControlComponenthlVCCPayload::getNodeId)
				.toList();

		final Set<Integer> payloadsNodeIdsSet = new HashSet<>(payloadNodeIds);
		checkArgument(payloadsNodeIdsSet.equals(NODE_IDS),
				"The Control Component hlVCC Payloads node ids do not match the expected node ids. [received: %s, expected: %s, contextIds: %s]",
				payloadsNodeIdsSet, NODE_IDS, contextIds);

		checkArgument(allEqual(controlComponenthlVCCPayloads.stream(), ControlComponenthlVCCPayload::getConfirmationKey),
				"The Long Vote Cast Return Codes Share hash payloads do not contain all the same confirmation key.[contextIds: %s]", contextIds);

		checkArgument(allEqual(controlComponenthlVCCPayloads.stream(), payload -> payload.getConfirmationKey().contextIds()),
				"The Control Component hlVCC Payloads do not contain all the same context ids. [contextIds: %s]", contextIds);

		// Verify that the confirmation key exists in the internal view.
		checkArgument(longVoteCastReturnCodesShareService.exists(contextIds.verificationCardId(), confirmationKey),
				"The received confirmation key must exist. [contextIds: %s]", contextIds);
	}

	private ControlComponentlVCCSharePayload generateControlComponentlVCCSharePayload(
			final List<ControlComponenthlVCCPayload> controlComponenthlVCCPayloads) {
		final ControlComponenthlVCCPayload controlComponenthlVCCPayload = controlComponenthlVCCPayloads.get(0);
		final ContextIds contextIds = controlComponenthlVCCPayload.getConfirmationKey().contextIds();
		final String electionEventId = contextIds.electionEventId();

		// Validate election event state.
		final ElectionEventState expectedState = ElectionEventState.CONFIGURED;
		final ElectionEventState electionEventState = electionEventStateService.getElectionEventState(electionEventId);
		checkState(expectedState.equals(electionEventState),
				"The election event is not in the expected state. [electionEventId: %s, nodeId: %s, expected: %s, actual: %s]", electionEventId,
				nodeId, expectedState, electionEventState);

		final GqGroup encryptionGroup = electionEventService.getEncryptionGroup(electionEventId);
		final boolean isVerified = verifyLVCCHashService.verifyLVCCHash(encryptionGroup, controlComponenthlVCCPayloads);
		LOGGER.info("VerifyLVCCHash algorithm successfully performed. [electionEventId: {}]", electionEventId);

		final String verificationCardId = contextIds.verificationCardId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final ConfirmationKey confirmationKey = controlComponenthlVCCPayload.getConfirmationKey();
		final ControlComponentlVCCSharePayload controlComponentlVCCSharePayload;

		if (isVerified) {
			final LongVoteCastReturnCodesShare longVoteCastReturnCodesShare = longVoteCastReturnCodesShareService.load(confirmationKey, nodeId);
			controlComponentlVCCSharePayload = new ControlComponentlVCCSharePayload(electionEventId, verificationCardSetId, verificationCardId,
					nodeId, encryptionGroup, longVoteCastReturnCodesShare, true, confirmationKey);
		} else {
			controlComponentlVCCSharePayload = new ControlComponentlVCCSharePayload(encryptionGroup, electionEventId, verificationCardSetId,
					verificationCardId, nodeId, confirmationKey, false);
		}

		controlComponentlVCCSharePayload.setSignature(getPayloadSignature(controlComponentlVCCSharePayload));
		LOGGER.info("Successfully signed Control Component lVCC Share payload. [contextIds: {}]", contextIds);

		return controlComponentlVCCSharePayload;
	}

	private CryptoPrimitivesSignature getPayloadSignature(final ControlComponentlVCCSharePayload controlComponentlVCCSharePayload) {
		final String electionEventId = controlComponentlVCCSharePayload.getElectionEventId();
		final String verificationCardId = controlComponentlVCCSharePayload.getVerificationCardId();
		final String verificationCardSetId = controlComponentlVCCSharePayload.getVerificationCardSetId();
		final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);

		final Hashable additionalContextData = ChannelSecurityContextData.controlComponentlVCCShare(nodeId, electionEventId, verificationCardSetId,
				verificationCardId);
		try {
			final byte[] signature = signatureKeystoreService.generateSignature(controlComponentlVCCSharePayload, additionalContextData);

			return new CryptoPrimitivesSignature(signature);
		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format("Could not generate Control Component lVCC Share payload signature. [contextIds: %s]", contextIds));
		}

	}

	public ControlComponenthlVCCRequestPayload deserializeRequest(final byte[] bytes) {
		checkNotNull(bytes);
		final byte[] bytesCopy = Arrays.copyOf(bytes, bytes.length);
		try {
			return objectMapper.readValue(bytesCopy, ControlComponenthlVCCRequestPayload.class);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	public byte[] serializeResponse(final ControlComponentlVCCSharePayload controlComponentlVCCSharePayload) {
		checkNotNull(controlComponentlVCCSharePayload);
		try {
			return objectMapper.writeValueAsBytes(controlComponentlVCCSharePayload);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(e);
		}
	}
}
