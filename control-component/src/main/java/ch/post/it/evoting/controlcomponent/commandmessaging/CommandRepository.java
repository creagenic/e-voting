/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.commandmessaging;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface CommandRepository extends CrudRepository<CommandEntity, CommandId> {

	@Transactional(isolation = Isolation.SERIALIZABLE)
	List<CommandEntity> findAllByContextIdAndContextAndNodeId(String contextId, String context, Integer nodeId);

	@Transactional(isolation = Isolation.SERIALIZABLE)
	List<CommandEntity> findAllByCorrelationId(String correlationId);
}
