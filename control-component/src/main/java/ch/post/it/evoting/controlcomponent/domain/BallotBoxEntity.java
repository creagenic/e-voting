/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.domain;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

import ch.post.it.evoting.domain.converters.BooleanConverter;
import ch.post.it.evoting.domain.converters.PrimesMappingTableConverter;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;

@Entity
@Table(name = "BALLOT_BOX")
public class BallotBoxEntity {

	@Id
	private Long id;

	private String ballotBoxId;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	@JoinColumn(name = "VERIFICATION_CARD_SET_FK_ID")
	private VerificationCardSetEntity verificationCardSetEntity;

	@Convert(converter = BooleanConverter.class)
	private boolean mixed = false;

	@Convert(converter = BooleanConverter.class)
	private boolean testBallotBox = false;

	private int numberOfVotingCards;

	// The grace period in seconds.
	private int gracePeriod;

	@Convert(converter = PrimesMappingTableConverter.class)
	private PrimesMappingTable primesMappingTable;

	@Version
	private Integer changeControlId;

	public BallotBoxEntity() {
	}

	private BallotBoxEntity(final String ballotBoxId, final VerificationCardSetEntity verificationCardSetEntity, final boolean testBallotBox,
			final int numberOfVotingCards, final int gracePeriod, final PrimesMappingTable primesMappingTable) {
		this.ballotBoxId = validateUUID(ballotBoxId);
		this.verificationCardSetEntity = checkNotNull(verificationCardSetEntity);
		this.testBallotBox = testBallotBox;
		checkArgument(numberOfVotingCards >= 0);
		this.numberOfVotingCards = numberOfVotingCards;
		checkArgument(gracePeriod >= 0);
		this.gracePeriod = gracePeriod;
		this.primesMappingTable = checkNotNull(primesMappingTable);
	}

	public Long getId() {
		return id;
	}

	public String getBallotBoxId() {
		return ballotBoxId;
	}

	public VerificationCardSetEntity getVerificationCardSetEntity() {
		return verificationCardSetEntity;
	}

	public Integer getChangeControlId() {
		return changeControlId;
	}

	public boolean isMixed() {
		return mixed;
	}

	public void setMixed(final boolean mixed) {
		this.mixed = mixed;
	}

	public boolean isTestBallotBox() {
		return testBallotBox;
	}

	public int getNumberOfVotingCards() {
		return numberOfVotingCards;
	}

	public int getGracePeriod() {
		return gracePeriod;
	}

	public PrimesMappingTable getPrimesMappingTable() {
		return primesMappingTable;
	}

	public static class Builder {

		private String ballotBoxId;
		private VerificationCardSetEntity verificationCardSetEntity;
		private boolean testBallotBox;
		private int numberOfVotingCards;
		private int gracePeriod;
		private PrimesMappingTable primesMappingTable;

		public Builder setBallotBoxId(final String ballotBoxId) {
			this.ballotBoxId = ballotBoxId;
			return this;
		}

		public Builder setVerificationCardSetEntity(final VerificationCardSetEntity verificationCardSetEntity) {
			this.verificationCardSetEntity = verificationCardSetEntity;
			return this;
		}

		public Builder setTestBallotBox(final boolean testBallotBox) {
			this.testBallotBox = testBallotBox;
			return this;
		}

		public Builder setNumberOfVotingCards(final int numberOfVotingCards) {
			this.numberOfVotingCards = numberOfVotingCards;
			return this;
		}

		public Builder setGracePeriod(final int gracePeriod) {
			this.gracePeriod = gracePeriod;
			return this;
		}

		public Builder setPrimesMappingTable(final PrimesMappingTable primesMappingTable) {
			this.primesMappingTable = primesMappingTable;
			return this;
		}

		public BallotBoxEntity build() {
			return new BallotBoxEntity(ballotBoxId, verificationCardSetEntity, testBallotBox, numberOfVotingCards, gracePeriod, primesMappingTable);
		}

	}
}
