/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.domain;

import static com.google.common.base.Preconditions.checkNotNull;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "CCR_RETURN_CODES_KEYS")
public class CcrjReturnCodesKeysEntity {

	@Id
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	@JoinColumn(name = "ELECTION_EVENT_FK_ID")
	private ElectionEventEntity electionEventEntity;

	@Version
	private Integer changeControlId;

	private byte[] ccrjReturnCodesGenerationSecretKey;

	private byte[] ccrjChoiceReturnCodesEncryptionKeyPair;

	public CcrjReturnCodesKeysEntity() {
		// Needed by the repository.
	}

	public CcrjReturnCodesKeysEntity(
			final byte[] ccrjReturnCodesGenerationSecretKey,
			final byte[] ccrjChoiceReturnCodesEncryptionKeyPair,
			final ElectionEventEntity electionEventEntity) {
		this.ccrjReturnCodesGenerationSecretKey = checkNotNull(ccrjReturnCodesGenerationSecretKey);
		this.ccrjChoiceReturnCodesEncryptionKeyPair = checkNotNull(ccrjChoiceReturnCodesEncryptionKeyPair);
		this.electionEventEntity = checkNotNull(electionEventEntity);
	}

	public byte[] getCcrjReturnCodesGenerationSecretKey() {
		return ccrjReturnCodesGenerationSecretKey;
	}

	public byte[] getCcrjChoiceReturnCodesEncryptionKeyPair() {
		return ccrjChoiceReturnCodesEncryptionKeyPair;
	}
}
