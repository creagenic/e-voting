/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.domain;

import static ch.post.it.evoting.evotinglibraries.domain.VotingOptionsConstants.MAXIMUM_SUPPORTED_NUMBER_OF_SELECTIONS;
import static ch.post.it.evoting.evotinglibraries.domain.VotingOptionsConstants.MAXIMUM_SUPPORTED_NUMBER_OF_WRITE_INS;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "ELECTION_EVENT_CONTEXT")
public class ElectionContextEntity {

	@Id
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	@JoinColumn(name = "ELECTION_EVENT_FK_ID")
	private ElectionEventEntity electionEventEntity;

	private LocalDateTime startTime;

	private LocalDateTime finishTime;
	private int maxNumberOfSelections;
	private int maxNumberOfWriteInsPlusOne;

	@Version
	@Column(name = "CHANGE_CONTROL_ID")
	private Integer changeControlId;

	public ElectionContextEntity() {

	}

	private ElectionContextEntity(final ElectionEventEntity electionEventEntity, final LocalDateTime startTime, final LocalDateTime finishTime,
			final int maxNumberOfSelections, final int maxNumberOfWriteInsPlusOne) {

		this.electionEventEntity = checkNotNull(electionEventEntity);

		checkNotNull(startTime);
		checkNotNull(finishTime);
		checkArgument(startTime.isBefore(finishTime), "Start time must be before finish time.");

		this.startTime = startTime;
		this.finishTime = finishTime;

		checkArgument(maxNumberOfSelections > 0, "The max number of selections must be strictly positive.");
		checkArgument(maxNumberOfSelections <= MAXIMUM_SUPPORTED_NUMBER_OF_SELECTIONS,
				"The maximum number of selections must be smaller or equal to the maximum supported number of selections.");

		checkArgument(maxNumberOfWriteInsPlusOne > 0, "The max number of write-ins + 1 must be strictly positive.");
		checkArgument(maxNumberOfWriteInsPlusOne <= MAXIMUM_SUPPORTED_NUMBER_OF_WRITE_INS + 1,
				"The maximum number of write-ins + 1 must be smaller or equal to the maximum supported number of write-ins + 1.");

		this.maxNumberOfSelections = maxNumberOfSelections;
		this.maxNumberOfWriteInsPlusOne = maxNumberOfWriteInsPlusOne;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public LocalDateTime getFinishTime() {
		return finishTime;
	}

	public int getMaxNumberOfSelections() {
		return maxNumberOfSelections;
	}

	public int getMaxNumberOfWriteInsPlusOne() {
		return maxNumberOfWriteInsPlusOne;
	}

	public ElectionEventEntity getElectionEventEntity() {
		return electionEventEntity;
	}

	public static class Builder {

		private ElectionEventEntity electionEventEntity;
		private LocalDateTime startTime;
		private LocalDateTime finishTime;
		private int maxNumberOfSelections;
		private int maxNumberOfWriteInsPlusOne;

		public Builder() {
			// Do nothing
		}

		public Builder setElectionEventEntity(final ElectionEventEntity electionEventEntity) {
			this.electionEventEntity = checkNotNull(electionEventEntity);
			return this;
		}

		public Builder setStartTime(final LocalDateTime startTime) {
			checkNotNull(startTime);
			this.startTime = startTime;
			return this;
		}

		public Builder setFinishTime(final LocalDateTime finishTime) {
			checkNotNull(finishTime);
			this.finishTime = finishTime;
			return this;
		}

		public Builder setMaxNumberOfSelections(final int maxNumberOfSelections) {
			this.maxNumberOfSelections = maxNumberOfSelections;
			return this;
		}

		public Builder setMaxNumberOfWriteInsPlusOne(final int maxNumberOfWriteInsPlusOne) {
			this.maxNumberOfWriteInsPlusOne = maxNumberOfWriteInsPlusOne;
			return this;
		}

		public ElectionContextEntity build() {
			return new ElectionContextEntity(electionEventEntity, startTime, finishTime, maxNumberOfSelections, maxNumberOfWriteInsPlusOne);
		}
	}
}
