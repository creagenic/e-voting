/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.domain;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "LONG_VOTE_CAST_RETURN_CODES_SHARE")
public class LongVoteCastReturnCodesShareEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LONG_VOTE_CAST_RETURN_CODES_SHARE_SEQ_GENERATOR")
	@SequenceGenerator(sequenceName = "LONG_VOTE_CAST_RETURN_CODES_SHARE_SEQ", allocationSize = 1, name = "LONG_VOTE_CAST_RETURN_CODES_SHARE_SEQ_GENERATOR")
	private Long id;

	private String confirmationKey;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "VERIFICATION_CARD_FK_ID", referencedColumnName = "ID")
	private VerificationCardEntity verificationCardEntity;

	private byte[] longVoteCastReturnCodeShare;

	private int confirmationAttempts = 1;

	@Version
	private Integer changeControlId;

	public LongVoteCastReturnCodesShareEntity(final VerificationCardEntity verificationCardEntity, final byte[] longVoteCastReturnCodeShare,
			final String confirmationKey, final int confirmationAttempts) {
		this.verificationCardEntity = checkNotNull(verificationCardEntity);
		this.longVoteCastReturnCodeShare = checkNotNull(longVoteCastReturnCodeShare);
		this.confirmationKey = checkNotNull(confirmationKey);

		// When saving a share, it means at least 1 confirmation attempt has been made.
		checkArgument(confirmationAttempts >= 1 && confirmationAttempts <= 5);
		this.confirmationAttempts = confirmationAttempts;
	}

	public LongVoteCastReturnCodesShareEntity() {
	}

	public VerificationCardEntity getVerificationCardEntity() {
		return verificationCardEntity;
	}

	public byte[] getLongVoteCastReturnCodeShare() {
		return longVoteCastReturnCodeShare;
	}

	public String getConfirmationKey() {
		return confirmationKey;
	}

	public int getConfirmationAttempts() {
		return confirmationAttempts;
	}
}
