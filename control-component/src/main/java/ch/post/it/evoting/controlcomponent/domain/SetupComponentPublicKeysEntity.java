/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.domain;

import static com.google.common.base.Preconditions.checkNotNull;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "SETUP_COMPONENT_PUBLIC_KEYS")
public class SetupComponentPublicKeysEntity {
	@Id
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	@MapsId
	@JoinColumn(name = "ELECTION_EVENT_FK_ID")
	private ElectionEventEntity electionEventEntity;

	private byte[] combinedControlComponentPublicKeys;

	private byte[] electoralBoardPublicKey;
	private byte[] electoralBoardSchnorrProofs;

	private byte[] electionPublicKey;

	private byte[] choiceReturnCodesEncryptionPublicKey;

	@Version
	@Column(name = "CHANGE_CONTROL_ID")
	private Integer changeControlId;

	private SetupComponentPublicKeysEntity(final ElectionEventEntity electionEventEntity, final byte[] combinedControlComponentPublicKeys,
			final byte[] electoralBoardPublicKey, final byte[] electoralBoardSchnorrProofs, final byte[] electionPublicKey,
			final byte[] choiceReturnCodesEncryptionPublicKey) {
		this.electionEventEntity = checkNotNull(electionEventEntity);
		this.combinedControlComponentPublicKeys = checkNotNull(combinedControlComponentPublicKeys);
		this.electoralBoardPublicKey = checkNotNull(electoralBoardPublicKey);
		this.electoralBoardSchnorrProofs = checkNotNull(electoralBoardSchnorrProofs);
		this.electionPublicKey = checkNotNull(electionPublicKey);
		this.choiceReturnCodesEncryptionPublicKey = checkNotNull(choiceReturnCodesEncryptionPublicKey);
	}

	public SetupComponentPublicKeysEntity() {

	}

	public byte[] getCombinedControlComponentPublicKeys() {
		return combinedControlComponentPublicKeys;
	}

	public byte[] getElectoralBoardPublicKey() {
		return electoralBoardPublicKey;
	}

	public byte[] getElectoralBoardSchnorrProofs() {
		return electoralBoardSchnorrProofs;
	}

	public byte[] getElectionPublicKey() {
		return electionPublicKey;
	}

	public byte[] getChoiceReturnCodesEncryptionPublicKey() {
		return choiceReturnCodesEncryptionPublicKey;
	}

	public static class Builder {
		private ElectionEventEntity electionEventEntity;
		private byte[] combinedControlComponentPublicKeys;
		private byte[] electoralBoardPublicKey;
		private byte[] electoralBoardSchnorrProofs;
		private byte[] electionPublicKey;
		private byte[] choiceReturnCodesEncryptionPublicKey;

		public Builder() {
			// Do nothing
		}

		public Builder setElectionEventEntity(final ElectionEventEntity electionEventEntity) {
			this.electionEventEntity = checkNotNull(electionEventEntity);
			return this;
		}

		public Builder setCombinedControlComponentPublicKey(final byte[] combinedControlComponentPublicKeys) {
			checkNotNull(combinedControlComponentPublicKeys);
			this.combinedControlComponentPublicKeys = combinedControlComponentPublicKeys;
			return this;
		}

		public Builder setElectoralBoardPublicKey(final byte[] electoralBoardPublicKey) {
			checkNotNull(electoralBoardPublicKey);
			this.electoralBoardPublicKey = electoralBoardPublicKey;
			return this;
		}

		public Builder setElectoralBoardSchnorrProofs(final byte[] electoralBoardSchnorrProofs) {
			checkNotNull(electoralBoardSchnorrProofs);
			this.electoralBoardSchnorrProofs = electoralBoardSchnorrProofs;
			return this;
		}

		public Builder setElectionPublicKey(final byte[] electionPublicKey) {
			checkNotNull(electionPublicKey);
			this.electionPublicKey = electionPublicKey;
			return this;
		}

		public Builder setChoiceReturnCodesEncryptionPublicKey(final byte[] choiceReturnCodesEncryptionPublicKey) {
			checkNotNull(choiceReturnCodesEncryptionPublicKey);
			this.choiceReturnCodesEncryptionPublicKey = choiceReturnCodesEncryptionPublicKey;
			return this;
		}

		public SetupComponentPublicKeysEntity build() {
			return new SetupComponentPublicKeysEntity(electionEventEntity, combinedControlComponentPublicKeys, electoralBoardPublicKey,
					electoralBoardSchnorrProofs, electionPublicKey, choiceReturnCodesEncryptionPublicKey);
		}
	}
}
