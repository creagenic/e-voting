/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.domain;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Objects;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "VERIFICATION_CARD_SET")
public class VerificationCardSetEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "VERIFICATION_CARD_SET_SEQ_GENERATOR")
	@SequenceGenerator(sequenceName = "VERIFICATION_CARD_SET_SEQ", allocationSize = 1, name = "VERIFICATION_CARD_SET_SEQ_GENERATOR")
	private Long id;

	private String verificationCardSetId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ELECTION_EVENT_FK_ID", referencedColumnName = "ID")
	private ElectionEventEntity electionEventEntity;

	@Version
	private Integer changeControlId;

	public VerificationCardSetEntity() {
	}

	public VerificationCardSetEntity(final String verificationCardSetId, final ElectionEventEntity electionEventEntity) {

		this.verificationCardSetId = validateUUID(verificationCardSetId);
		this.electionEventEntity = checkNotNull(electionEventEntity);
	}

	public String getVerificationCardSetId() {
		return verificationCardSetId;
	}

	public ElectionEventEntity getElectionEventEntity() {
		return electionEventEntity;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final VerificationCardSetEntity that = (VerificationCardSetEntity) o;
		return Objects.equals(id, that.id) && Objects.equals(verificationCardSetId, that.verificationCardSetId)
				&& Objects.equals(electionEventEntity, that.electionEventEntity) && Objects.equals(changeControlId, that.changeControlId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, verificationCardSetId, electionEventEntity, changeControlId);
	}
}
