/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.protocol.configuration.setuptally;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.VotingOptionsConstants.MAXIMUM_SUPPORTED_NUMBER_OF_WRITE_INS;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

/**
 * Regroups the context values needed for the SetupTallyCCM algorithm.
 *
 * <ul>
 *     <li>(p, q, g), the {@code GqGroup} with modulus p, cardinality q and generator g. Not null.</li>
 *     <li>j, the CCM's index. In range [1, 4].</li>
 *     <li>ee, the election event id. Not null and a valid UUID.</li>
 *     <li>&delta;<sub>max</sub>, the maximum number of write-ins plus one. In range [1, &delta;<sub>sup</sub>].</li>
 * </ul>
 */
public class SetupTallyCCMContext {

	private final GqGroup encryptionGroup;
	private final int nodeId;
	private final String electionEventId;
	private final int maximumNumberOfWriteInsPlusOne;

	private SetupTallyCCMContext(final GqGroup encryptionGroup, final int nodeId, final String electionEventId,
			final int maximumNumberOfWriteInsPlusOne) {
		this.encryptionGroup = encryptionGroup;
		this.nodeId = nodeId;
		this.electionEventId = electionEventId;
		this.maximumNumberOfWriteInsPlusOne = maximumNumberOfWriteInsPlusOne;
	}

	public GqGroup getEncryptionGroup() {
		return encryptionGroup;
	}

	public int getNodeId() {
		return nodeId;
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public int getMaximumNumberOfWriteInsPlusOne() {
		return maximumNumberOfWriteInsPlusOne;
	}

	/**
	 * Builder performing input validations before constructing a {@link SetupTallyCCMContext}.
	 */
	public static class Builder {

		private String electionEventId;
		private GqGroup encryptionGroup;
		private int nodeId;
		private int maximumNumberOfWriteInsPlusOne;

		public Builder setEncryptionGroup(final GqGroup gqGroup) {
			this.encryptionGroup = gqGroup;
			return this;
		}

		public Builder setNodeId(final int nodeId) {
			this.nodeId = nodeId;
			return this;
		}

		public Builder setElectionEventId(final String electionEventId) {
			this.electionEventId = electionEventId;
			return this;
		}

		public Builder setMaximumNumberOfWriteInsPlusOne(final int maximumNumberOfWriteInsPlusOne) {
			this.maximumNumberOfWriteInsPlusOne = maximumNumberOfWriteInsPlusOne;
			return this;
		}

		/**
		 * Creates the SetupTallyCCMContext object.
		 *
		 * @throws NullPointerException      if any of the fields are null.
		 * @throws FailedValidationException if the election event id is not a valid UUID.
		 * @throws IllegalArgumentException  if
		 *                                   <ul>
		 *                                       <li>the node id is not part of the known node ids.</li>
		 *                                       <li>the maximum number of write-ins plus one is strictly smaller than 1.</li>
		 *                                       <li>the maximum number of write-ins plus one is strictly greater than &delta;<sub>sup</sub>.</li>
		 *                                   </ul>
		 */
		public SetupTallyCCMContext build() {
			checkNotNull(encryptionGroup);
			checkArgument(NODE_IDS.contains(nodeId), "The node id must be part of the known node ids. [nodeId: %s]", nodeId);
			validateUUID(electionEventId);
			checkArgument(maximumNumberOfWriteInsPlusOne > 0, "The maximum number of write-ins plus one must be strictly positive.");

			final int delta_sup = MAXIMUM_SUPPORTED_NUMBER_OF_WRITE_INS + 1;
			checkArgument(maximumNumberOfWriteInsPlusOne <= delta_sup,
					"The maximum number of write-ins plus one must be smaller or equal to the maximum supported number of write-ins plus one. [delta_max: %s, delta_sup: %s]",
					maximumNumberOfWriteInsPlusOne, delta_sup);

			return new SetupTallyCCMContext(encryptionGroup, nodeId, electionEventId, maximumNumberOfWriteInsPlusOne);
		}
	}
}
