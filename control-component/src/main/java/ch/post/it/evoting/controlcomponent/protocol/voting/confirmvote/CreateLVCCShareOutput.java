/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.protocol.voting.confirmvote;

import static ch.post.it.evoting.controlcomponent.protocol.voting.confirmvote.CreateLVCCShareAlgorithm.MAX_CONFIRMATION_ATTEMPTS;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.math.GqElement;

/**
 * Regroups the output values needed by the CreateLVCCShare algorithm.
 *
 * <ul>
 *     <li>lVCC<sub>id, the CCR<sub>j</sub>’s long Vote Cast Return Code share. Not null.</li>
 *     <li>hlVCC<sub>id, the hashed long Vote Cast Return Code share. Not null.</li>
 *     <li>attempts<sub>id</sub>, the confirmation attempt number. In range [0, 4].</li>
 * </ul>
 */
public record CreateLVCCShareOutput(GqElement longVoteCastReturnCodeShare, String hashedLongVoteCastReturnCodeShare, int confirmationAttempts) {

	public CreateLVCCShareOutput {
		checkArgument(confirmationAttempts >= 0, "The confirmation attempt number must be positive.");
		checkArgument(confirmationAttempts < MAX_CONFIRMATION_ATTEMPTS, "The confirmation attempt number must be at most %s.",
				MAX_CONFIRMATION_ATTEMPTS);
		checkNotNull(longVoteCastReturnCodeShare);
		checkNotNull(hashedLongVoteCastReturnCodeShare);
	}

}
