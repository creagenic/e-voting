/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.protocol.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.math.GroupVector.toGroupVector;
import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.common.base.Preconditions;

import ch.post.it.evoting.controlcomponent.service.EncryptedVerifiableVoteService;
import ch.post.it.evoting.controlcomponent.service.SetupComponentPublicKeysService;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentPartialDecryptPayload;
import ch.post.it.evoting.domain.voting.sendvote.PartiallyDecryptedEncryptedPCC;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.common.EncryptedVerifiableVote;
import ch.post.it.evoting.evotinglibraries.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;

@Service
public class DecryptPCCService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DecryptPCCService.class);

	private final DecryptPCCAlgorithm decryptPCCAlgorithm;
	private final EncryptedVerifiableVoteService encryptedVerifiableVoteService;
	private final SetupComponentPublicKeysService setupComponentPublicKeysService;

	@Value("${nodeID}")
	private int nodeId;

	public DecryptPCCService(final DecryptPCCAlgorithm decryptPCCAlgorithm,
			final EncryptedVerifiableVoteService encryptedVerifiableVoteService,
			final SetupComponentPublicKeysService setupComponentPublicKeysService) {
		this.decryptPCCAlgorithm = decryptPCCAlgorithm;
		this.encryptedVerifiableVoteService = encryptedVerifiableVoteService;
		this.setupComponentPublicKeysService = setupComponentPublicKeysService;
	}

	/**
	 * Invokes the DecryptPCC algorithm.
	 *
	 * @param encryptionGroup                        the encryption group. Must be non-null.
	 * @param primesMappingTable                     the primes mapping table. Must be non-null.
	 * @param controlComponentPartialDecryptPayloads the control component partial decrypt payloads. Must be non-null.
	 * @throws NullPointerException     if any parameter is null.
	 * @throws IllegalArgumentException if the group of the primes mapping table is not equal to the encryption group.
	 */
	public GroupVector<GqElement, GqGroup> decryptPCC(final GqGroup encryptionGroup, final PrimesMappingTable primesMappingTable,
			final List<ControlComponentPartialDecryptPayload> controlComponentPartialDecryptPayloads) {
		checkNotNull(encryptionGroup);
		checkArgument(primesMappingTable.getEncryptionGroup().equals(encryptionGroup),
				"The group of the primes mapping table must be equal to the encryption group.");

		final Map<Boolean, List<ControlComponentPartialDecryptPayload>> controlComponentPartialDecryptPayloadsMap = checkNotNull(
				controlComponentPartialDecryptPayloads).stream()
				.map(Preconditions::checkNotNull)
				.collect(Collectors.partitioningBy(payload -> payload.getPartiallyDecryptedEncryptedPCC().nodeId() == nodeId));

		checkArgument(controlComponentPartialDecryptPayloadsMap.get(true).size() == 1,
				"Missing Control Component Partial Decrypt Payload node contribution. [missing node id: %s]", nodeId);
		checkArgument(controlComponentPartialDecryptPayloadsMap.get(false).size() == NODE_IDS.size() - 1,
				"Missing Control Component Partial Decrypt Payload node contribution for the other node ids. [nodeId: %s]", nodeId);

		final ContextIds contextIds = controlComponentPartialDecryptPayloadsMap.get(true)
				.get(0)
				.getPartiallyDecryptedEncryptedPCC()
				.contextIds();

		final DecryptPCCContext decryptPCCContext = buildDecryptPCCContext(encryptionGroup, primesMappingTable, contextIds);

		final DecryptPCCInput decryptPCCInput = buildDecryptPCCInput(controlComponentPartialDecryptPayloadsMap, contextIds);

		LOGGER.debug("Performing DecryptPCC algorithm... [contextIds: {}, nodeId: {}]", contextIds, nodeId);

		return decryptPCCAlgorithm.decryptPCC(decryptPCCContext, decryptPCCInput);
	}

	private DecryptPCCContext buildDecryptPCCContext(final GqGroup encryptionGroup, final PrimesMappingTable primesMappingTable,
			final ContextIds contextIds) {
		final String electionEventId = contextIds.electionEventId();
		final String verificationCardSetId = contextIds.verificationCardSetId();
		final String verificationCardId = contextIds.verificationCardId();

		final ElGamalMultiRecipientPublicKey electionPublicKey = setupComponentPublicKeysService.getElectionPublicKey(electionEventId);
		final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey = setupComponentPublicKeysService.getChoiceReturnCodesEncryptionPublicKey(
				electionEventId);
		final List<ControlComponentPublicKeys> combinedControlComponentPublicKeys = setupComponentPublicKeysService.getCombinedControlComponentPublicKeys(
				electionEventId);
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> otherCcrjEncryptionPublicKeys = combinedControlComponentPublicKeys.stream()
				.filter(ccpk -> ccpk.nodeId() != nodeId)
				.map(ControlComponentPublicKeys::ccrjChoiceReturnCodesEncryptionPublicKey)
				.collect(GroupVector.toGroupVector());

		return new DecryptPCCContext.Builder()
				.setEncryptionGroup(encryptionGroup)
				.setNodeId(nodeId)
				.setElectionEventId(electionEventId)
				.setVerificationCardSetId(verificationCardSetId)
				.setVerificationCardId(verificationCardId)
				.setPrimesMappingTable(primesMappingTable)
				.setElectionPublicKey(electionPublicKey)
				.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
				.setOtherCcrChoiceReturnCodesEncryptionKeys(otherCcrjEncryptionPublicKeys)
				.build();
	}

	private DecryptPCCInput buildDecryptPCCInput(
			final Map<Boolean, List<ControlComponentPartialDecryptPayload>> controlComponentPartialDecryptPayloadsMap,
			final ContextIds contextIds) {
		final GroupVector<GqElement, GqGroup> exponentiatedGammas = controlComponentPartialDecryptPayloadsMap.get(true).get(0)
				.getPartiallyDecryptedEncryptedPCC()
				.exponentiatedGammas();

		final List<PartiallyDecryptedEncryptedPCC> otherPartiallyDecryptedEncryptedPCC = controlComponentPartialDecryptPayloadsMap.get(false).stream()
				.map(ControlComponentPartialDecryptPayload::getPartiallyDecryptedEncryptedPCC)
				.sorted(Comparator.comparingInt(PartiallyDecryptedEncryptedPCC::nodeId))
				.toList();

		final GroupVector<GroupVector<GqElement, GqGroup>, GqGroup> otherCcrExponentiatedGammas = otherPartiallyDecryptedEncryptedPCC.stream()
				.map(PartiallyDecryptedEncryptedPCC::exponentiatedGammas)
				.collect(toGroupVector());

		final GroupVector<GroupVector<ExponentiationProof, ZqGroup>, ZqGroup> otherCcrExponentiationProofs = otherPartiallyDecryptedEncryptedPCC.stream()
				.map(PartiallyDecryptedEncryptedPCC::exponentiationProofs)
				.collect(toGroupVector());

		final String verificationCardId = contextIds.verificationCardId();
		final EncryptedVerifiableVote encryptedVerifiableVote = encryptedVerifiableVoteService.getEncryptedVerifiableVote(verificationCardId);

		return new DecryptPCCInput.Builder()
				.setExponentiatedGammaElements(exponentiatedGammas)
				.setOtherCcrExponentiatedGammaElements(otherCcrExponentiatedGammas)
				.setOtherCcrExponentiationProofs(otherCcrExponentiationProofs)
				.setEncryptedVote(encryptedVerifiableVote.encryptedVote())
				.setExponentiatedEncryptedVote(encryptedVerifiableVote.exponentiatedEncryptedVote())
				.setEncryptedPartialChoiceReturnCodes(encryptedVerifiableVote.encryptedPartialChoiceReturnCodes())
				.build();
	}
}
