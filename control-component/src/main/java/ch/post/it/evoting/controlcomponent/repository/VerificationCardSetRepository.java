/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.controlcomponent.domain.VerificationCardSetEntity;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface VerificationCardSetRepository extends CrudRepository<VerificationCardSetEntity, Long> {

	Optional<VerificationCardSetEntity> findByVerificationCardSetId(final String verificationCardSetId);

	boolean existsByVerificationCardSetId(final String verificationCardSetId);

	@SuppressWarnings("java:S100")
	int countAllByElectionEventEntity_ElectionEventId(final String electionEventId);

	@Query("select e from VerificationCardSetEntity e where e.electionEventEntity.electionEventId = ?1")
	List<VerificationCardSetEntity> findAllByElectionEventId(final String electionEventId);
}
