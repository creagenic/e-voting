/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.service;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.time.LocalDateTime;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.controlcomponent.domain.ElectionContextEntity;
import ch.post.it.evoting.controlcomponent.domain.ElectionEventEntity;
import ch.post.it.evoting.controlcomponent.repository.ElectionContextRepository;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionEventContext;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.PrimesMappingTableAlgorithms;

@Service
public class ElectionContextService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElectionContextService.class);

	private final BallotBoxService ballotBoxService;
	private final ElectionEventService electionEventService;
	private final ElectionContextRepository electionContextRepository;
	private final VerificationCardSetService verificationCardSetService;
	private final PrimesMappingTableAlgorithms primesMappingTableAlgorithms;

	public ElectionContextService(
			final BallotBoxService ballotBoxService,
			final ElectionEventService electionEventService,
			final ElectionContextRepository electionContextRepository,
			final VerificationCardSetService verificationCardSetService,
			final PrimesMappingTableAlgorithms primesMappingTableAlgorithms) {
		this.ballotBoxService = ballotBoxService;
		this.electionEventService = electionEventService;
		this.electionContextRepository = electionContextRepository;
		this.verificationCardSetService = verificationCardSetService;
		this.primesMappingTableAlgorithms = primesMappingTableAlgorithms;
	}

	@Transactional
	public void save(final ElectionEventContext electionEventContext) {
		checkNotNull(electionEventContext);

		// Save election event context entity.
		final String electionEventId = electionEventContext.electionEventId();
		final ElectionEventEntity electionEventEntity = electionEventService.getElectionEventEntity(electionEventId);
		final ElectionContextEntity electionContextEntity = new ElectionContextEntity.Builder()
				.setElectionEventEntity(electionEventEntity)
				.setStartTime(electionEventContext.startTime())
				.setFinishTime(electionEventContext.finishTime())
				.setMaxNumberOfSelections(electionEventContext.maximumNumberOfSelections())
				.setMaxNumberOfWriteInsPlusOne(electionEventContext.maximumNumberOfWriteInsPlusOne())
				.build();
		electionContextRepository.save(electionContextEntity);
		LOGGER.info("Saved election context entity. [electionEventId: {}]", electionEventId);

		verificationCardSetService.saveFromContext(electionEventContext);
		LOGGER.info("Saved verification card set entities. [electionEventId: {}]", electionEventId);

		// Save ballot box entities.
		ballotBoxService.saveFromContexts(electionEventContext.verificationCardSetContexts());
		LOGGER.info("Saved ballot box entities. [electionEventId: {}]", electionEventId);
	}

	@Transactional
	public LocalDateTime getElectionEventStartTime(final String electionEventId) {
		validateUUID(electionEventId);

		return getElectionContextEntity(electionEventId).getStartTime();
	}

	@Transactional
	public LocalDateTime getElectionEventFinishTime(final String electionEventId) {
		validateUUID(electionEventId);

		return getElectionContextEntity(electionEventId).getFinishTime();
	}

	@Transactional
	public int getMaxNumberOfSelections(final String electionEventId) {
		validateUUID(electionEventId);

		return getElectionContextEntity(electionEventId).getMaxNumberOfSelections();
	}

	@Transactional
	public int getMaxNumberOfWriteInsPlusOne(final String electionEventId) {
		validateUUID(electionEventId);

		return getElectionContextEntity(electionEventId).getMaxNumberOfWriteInsPlusOne();
	}

	private ElectionContextEntity getElectionContextEntity(final String electionEventId) {
		validateUUID(electionEventId);

		final Optional<ElectionContextEntity> electionContextEntity = electionContextRepository.findByElectionEventId(electionEventId);

		return electionContextEntity.orElseThrow(
				() -> new IllegalStateException(String.format("Election context entity not found. [electionEventId: %s]", electionEventId)));
	}
}
