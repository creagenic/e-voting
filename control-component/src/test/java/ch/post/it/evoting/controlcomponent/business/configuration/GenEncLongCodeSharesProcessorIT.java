/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.business.configuration;

import static ch.post.it.evoting.domain.SharedQueue.CONTROL_COMPONENTS_ADDRESS;
import static ch.post.it.evoting.domain.SharedQueue.MESSAGE_HEADER_MESSAGE_TYPE;
import static ch.post.it.evoting.domain.SharedQueue.MESSAGE_HEADER_NODE_ID;
import static ch.post.it.evoting.domain.SharedQueue.VOTING_SERVER_ADDRESS;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.SignatureException;
import java.util.List;
import java.util.UUID;

import jakarta.jms.JMSException;
import jakarta.jms.Message;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponent.ArtemisSupport;
import ch.post.it.evoting.controlcomponent.domain.CcrjReturnCodesKeys;
import ch.post.it.evoting.controlcomponent.domain.ElectionEventEntity;
import ch.post.it.evoting.controlcomponent.domain.PCCAllowListEntryEntity;
import ch.post.it.evoting.controlcomponent.domain.VerificationCardSetEntity;
import ch.post.it.evoting.controlcomponent.protocol.configuration.setupvoting.GenEncLongCodeSharesAlgorithm;
import ch.post.it.evoting.controlcomponent.service.BallotBoxService;
import ch.post.it.evoting.controlcomponent.service.CcrjReturnCodesKeysService;
import ch.post.it.evoting.controlcomponent.service.ElectionEventService;
import ch.post.it.evoting.controlcomponent.service.PCCAllowListEntryService;
import ch.post.it.evoting.controlcomponent.service.VerificationCardSetService;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.election.generators.PrimesMappingTableGenerator;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;

@DisplayName("GenEncLongCodeSharesProcessor consuming")
class GenEncLongCodeSharesProcessorIT extends ArtemisSupport {

	private static byte[] requestPayloadBytes;
	private static ElectionEventEntity electionEventEntity;
	private static SetupComponentVerificationDataPayload setupComponentVerificationDataPayload;

	private final String electionEventId = "A3FF69A9621BABE579760A17D82D2C4B";

	@Autowired
	private PCCAllowListEntryService pCCAllowListEntryService;

	@SpyBean
	private GenEncLongCodeSharesAlgorithm genEncLongCodeSharesAlgorithm;

	@MockBean
	private SignatureKeystore<Alias> signatureKeystoreService;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private VerificationCardSetService verificationCardSetService;

	@Autowired
	private BallotBoxService ballotBoxService;

	@BeforeAll
	static void setUpAll(
			@Autowired
			final ObjectMapper objectMapper,
			@Autowired
			final ElectionEventService electionEventService,
			@Autowired
			final VerificationCardSetService verificationCardSetService,
			@Autowired
			final BallotBoxService ballotBoxService,
			@Autowired
			final CcrjReturnCodesKeysService ccrjReturnCodesKeysService
	) throws IOException {

		final Resource payloadsResource = new ClassPathResource(
				"configuration/setupvoting/enclongcodeshares/setupComponentVerificationDataPayload.2.json");

		setupComponentVerificationDataPayload = objectMapper.readValue(payloadsResource.getFile(),
				SetupComponentVerificationDataPayload.class);

		requestPayloadBytes = objectMapper.writeValueAsBytes(setupComponentVerificationDataPayload);

		// Must match the group in the json.
		final GqGroup encryptionGroup = setupComponentVerificationDataPayload.getEncryptionGroup();

		// Save election event.
		final String electionEventId = setupComponentVerificationDataPayload.getElectionEventId();
		electionEventEntity = electionEventService.save(electionEventId, encryptionGroup);

		// Save verification card set.
		final String verificationCardSetId = setupComponentVerificationDataPayload.getVerificationCardSetId();
		final VerificationCardSetEntity verificationCardSetEntity = new VerificationCardSetEntity(verificationCardSetId, electionEventEntity);
		verificationCardSetService.save(verificationCardSetEntity);

		// Save ballot box.
		final String ballotBoxId = RandomFactory.createRandom().genRandomString(ID_LENGTH, Base16Alphabet.getInstance());
		final PrimesMappingTable primesMappingTable = new PrimesMappingTableGenerator(encryptionGroup).generate(25);
		ballotBoxService.save(ballotBoxId, verificationCardSetId, true, 32, 900, primesMappingTable);

		// Save ccrjReturnCodesKeys.
		final ZqGroupGenerator zqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(encryptionGroup));
		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(encryptionGroup);
		final ZqElement ccrKey = zqGroupGenerator.genRandomZqElementMember();
		final ElGamalMultiRecipientKeyPair elGamalMultiRecipientKeyPair = elGamalGenerator.genRandomKeyPair(10);
		ccrjReturnCodesKeysService.save(new CcrjReturnCodesKeys(electionEventId, ccrKey, elGamalMultiRecipientKeyPair));
	}

	@Test
	@DisplayName("Happy Path Gen Enc Long Code Share Processing")
	void happyPathGenEncLongCodeShareProcessing() throws SignatureException, IOException, JMSException {
		when(signatureKeystoreService.generateSignature(any(), any())).thenReturn(electionEventId.getBytes(StandardCharsets.UTF_8));
		when(signatureKeystoreService.verifySignature(eq(Alias.SDM_CONFIG), any(), any(), any())).thenReturn(true);

		final String verificationCardSetId = setupComponentVerificationDataPayload.getVerificationCardSetId();

		final String correlationId = UUID.randomUUID().toString();
		multicastJmsTemplate.convertAndSend(CONTROL_COMPONENTS_ADDRESS, requestPayloadBytes, jmsMessage -> {
			jmsMessage.setJMSCorrelationID(correlationId);
			jmsMessage.setStringProperty(MESSAGE_HEADER_MESSAGE_TYPE, SetupComponentVerificationDataPayload.class.getName());
			jmsMessage.setStringProperty(MESSAGE_HEADER_NODE_ID, "1");
			return jmsMessage;
		});

		final Message responseMessage = jmsTemplate.receive(VOTING_SERVER_ADDRESS);
		assertNotNull(responseMessage);

		final byte[] response = responseMessage.getBody(byte[].class);
		final ControlComponentCodeSharesPayload controlComponentCodeSharesPayload = objectMapper.readValue(response,
				ControlComponentCodeSharesPayload.class);

		assertEquals(correlationId, responseMessage.getJMSCorrelationID());

		verify(genEncLongCodeSharesAlgorithm, times(1)).genEncLongCodeShares(any(), any());
		verify(signatureKeystoreService, times(1)).verifySignature(eq(Alias.SDM_CONFIG), any(), any(), any());

		assertEquals(setupComponentVerificationDataPayload.getElectionEventId(), controlComponentCodeSharesPayload.getElectionEventId());
		assertEquals(verificationCardSetId, controlComponentCodeSharesPayload.getVerificationCardSetId());
		assertEquals(setupComponentVerificationDataPayload.getChunkId(), controlComponentCodeSharesPayload.getChunkId());
	}

	@Test
	@DisplayName("Happy Path concurrent Gen Enc Long Code Share Processing")
	void happyPathConcurrentGenEncLongCodeShareProcessing() throws SignatureException, IOException, JMSException {
		when(signatureKeystoreService.generateSignature(any(), any())).thenReturn(electionEventId.getBytes(StandardCharsets.UTF_8));
		when(signatureKeystoreService.verifySignature(eq(Alias.SDM_CONFIG), any(), any(), any())).thenReturn(true);

		final Resource payloadsResource0 = new ClassPathResource(
				"configuration/setupvoting/enclongcodeshares/setupComponentVerificationDataPayload.0.json");
		final SetupComponentVerificationDataPayload payload0 = objectMapper.readValue(payloadsResource0.getFile(),
				SetupComponentVerificationDataPayload.class);
		final byte[] requestPayloadBytes0 = objectMapper.writeValueAsBytes(payload0);

		final Resource payloadsResource1 = new ClassPathResource(
				"configuration/setupvoting/enclongcodeshares/setupComponentVerificationDataPayload.1.json");
		final SetupComponentVerificationDataPayload payload1 = objectMapper.readValue(payloadsResource1.getFile(),
				SetupComponentVerificationDataPayload.class);
		final byte[] requestPayloadBytes1 = objectMapper.writeValueAsBytes(payload1);

		// Save verification card set.
		final String verificationCardSetId = payload0.getVerificationCardSetId();
		final VerificationCardSetEntity verificationCardSetEntity = new VerificationCardSetEntity(verificationCardSetId, electionEventEntity);
		verificationCardSetService.save(verificationCardSetEntity);

		// Save ballot box.
		final String ballotBoxId = RandomFactory.createRandom().genRandomString(ID_LENGTH, Base16Alphabet.getInstance());
		final GqGroup encryptionGroup = electionEventEntity.getEncryptionGroup();
		final PrimesMappingTable primesMappingTable = new PrimesMappingTableGenerator(encryptionGroup).generate(25);
		ballotBoxService.save(ballotBoxId, verificationCardSetId, true, 32, 900, primesMappingTable);

		// Send two messages roughly at the same time to test that when two chunks are processed at the same time, one will success and one fail
		// to create the verification card set. The failing one will retry once and will be able to process the message. Depending on the thread
		// execution order, it may or may not generate a concurrent creation of the verification card set.
		final String correlationId0 = UUID.randomUUID().toString();
		multicastJmsTemplate.convertAndSend(CONTROL_COMPONENTS_ADDRESS, requestPayloadBytes0, jmsMessage -> {
			jmsMessage.setJMSCorrelationID(correlationId0);
			jmsMessage.setStringProperty(MESSAGE_HEADER_MESSAGE_TYPE, SetupComponentVerificationDataPayload.class.getName());
			jmsMessage.setStringProperty(MESSAGE_HEADER_NODE_ID, "1");
			return jmsMessage;
		});
		final String correlationId1 = UUID.randomUUID().toString();
		multicastJmsTemplate.convertAndSend(CONTROL_COMPONENTS_ADDRESS, requestPayloadBytes1, jmsMessage -> {
			jmsMessage.setJMSCorrelationID(correlationId1);
			jmsMessage.setStringProperty(MESSAGE_HEADER_MESSAGE_TYPE, SetupComponentVerificationDataPayload.class.getName());
			jmsMessage.setStringProperty(MESSAGE_HEADER_NODE_ID, "1");
			return jmsMessage;
		});

		final Message responseMessage0 = jmsTemplate.receive(VOTING_SERVER_ADDRESS);
		final Message responseMessage1 = jmsTemplate.receive(VOTING_SERVER_ADDRESS);

		assertNotNull(responseMessage0);
		assertNotNull(responseMessage1);

		final ControlComponentCodeSharesPayload responsePayload0 = objectMapper.readValue(responseMessage0.getBody(byte[].class),
				ControlComponentCodeSharesPayload.class);
		final ControlComponentCodeSharesPayload responsePayload1 = objectMapper.readValue(responseMessage1.getBody(byte[].class),
				ControlComponentCodeSharesPayload.class);

		assertNotEquals(responsePayload0, responsePayload1);

		// In the end, two chunks must have been successfully saved, regardless of concurrency.
		final List<PCCAllowListEntryEntity> pccAllowList = pCCAllowListEntryService.getPCCAllowList(payload0.getVerificationCardSetId());
		final List<Integer> chunkIds = pccAllowList.stream()
				.map(PCCAllowListEntryEntity::getChunkId)
				.distinct()
				.toList();
		assertEquals(2, chunkIds.size());
	}
}
