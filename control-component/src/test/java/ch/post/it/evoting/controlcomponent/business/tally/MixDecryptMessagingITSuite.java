/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.business.tally;

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;
import org.junit.platform.suite.api.SuiteDisplayName;

@Suite
@SuiteDisplayName("Online tally test suite")
@SelectClasses({ MixDecryptMessagingCC1ITCase.class, MixDecryptMessagingCC2ITCase.class, MixDecryptMessagingCC3ITCase.class,
		MixDecryptMessagingCC4ITCase.class })
public class MixDecryptMessagingITSuite {
}
