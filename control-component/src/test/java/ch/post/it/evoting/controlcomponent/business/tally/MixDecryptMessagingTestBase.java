/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.business.tally;

import static ch.post.it.evoting.controlcomponent.TestKeyStoreInitializer.KEYSTORE_FILENAME_PATH;
import static ch.post.it.evoting.controlcomponent.TestKeyStoreInitializer.KEYSTORE_PASSWORD_FILENAME_PATH;
import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;

import java.io.IOException;
import java.math.BigInteger;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeAll;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ch.post.it.evoting.controlcomponent.ArtemisSupport;
import ch.post.it.evoting.controlcomponent.TestSigner;
import ch.post.it.evoting.controlcomponent.domain.VerificationCard;
import ch.post.it.evoting.controlcomponent.service.ElectionContextService;
import ch.post.it.evoting.controlcomponent.service.ElectionEventService;
import ch.post.it.evoting.controlcomponent.service.EncryptedVerifiableVoteService;
import ch.post.it.evoting.controlcomponent.service.MixnetInitialCiphertextsService;
import ch.post.it.evoting.controlcomponent.service.SetupComponentPublicKeysService;
import ch.post.it.evoting.controlcomponent.service.VerificationCardService;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.PlaintextEqualityProof;
import ch.post.it.evoting.domain.generators.SetupComponentPublicKeysPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.common.EncryptedVerifiableVote;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionEventContext;
import ch.post.it.evoting.evotinglibraries.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentVotesHashPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.ElectionEventContextPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixonline.GetMixnetInitialCiphertextsAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixonline.GetMixnetInitialCiphertextsContext;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixonline.GetMixnetInitialCiphertextsInput;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.tally.mixonline.GetMixnetInitialCiphertextsOutput;

public class MixDecryptMessagingTestBase extends ArtemisSupport {

	private static final Logger LOGGER = LoggerFactory.getLogger(MixDecryptMessagingTestBase.class);
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final Map<Integer, MixDecryptResponse> responseMessages = new HashMap<>();
	private static final Random random = RandomFactory.createRandom();
	private static final ElGamal elGamal = ElGamalFactory.createElGamal();
	private static final int NUMBER_OF_VERIFICATION_CARDS = 10;
	private static final int NUMBER_OF_CONFIRMED_VOTES = 10; // Be sure to have NUMBER_OF_CONFIRMED_VOTES <= NUMBER_OF_VERIFICATION_CARDS
	private static final int numberOfWriteInsPlusOne = 4;

	protected static GqGroup gqGroup;
	protected static String ballotBoxIdToMix;
	protected static ElectionEventContext electionEventContext;
	protected static List<ControlComponentVotesHashPayload> controlComponentVotesHashPayloads;

	private static ElGamalGenerator elGamalGenerator;
	private static SetupComponentPublicKeys setupComponentPublicKeys;
	private static List<ElGamalMultiRecipientKeyPair> ccmjElectionKeyPairs;
	private static List<String> verificationCardIds;
	private static List<EncryptedVerifiableVote> confirmedVotes;

	static {
		// Generates initial election data shared by all control-components.
		setUpElection();
	}

	@BeforeAll
	static void setUpAll(
			@Autowired
			final ElectionEventService electionEventService,
			@Autowired
			final ElectionContextService electionContextService,
			@Autowired
			final SetupComponentPublicKeysService setupComponentPublicKeysService,
			@Autowired
			final EncryptedVerifiableVoteService encryptedVerifiableVoteService,
			@Autowired
			final VerificationCardService verificationCardService,
			@Autowired
			final MixnetInitialCiphertextsService mixnetInitialCiphertextsService,
			@Autowired
			final GetMixnetInitialCiphertextsAlgorithm getMixnetInitialCiphertextsAlgorithm) {

		// Save election event.
		final String electionEventId = electionEventContext.electionEventId();
		electionEventService.save(electionEventId, gqGroup);
		LOGGER.info("Election event saved.");

		// Save election context
		electionContextService.save(electionEventContext);
		setupComponentPublicKeysService.save(electionEventId, setupComponentPublicKeys);
		LOGGER.info("Election event context and public keys saved.");

		// Save cards.
		final List<VerificationCardSetContext> verificationCardSetContexts = electionEventContext.verificationCardSetContexts();
		final String verificationCardSetId = verificationCardSetContexts.get(0).getVerificationCardSetId();
		final List<VerificationCard> verificationCards = verificationCardIds.stream()
				.map(verificationCardId -> new VerificationCard(verificationCardId, verificationCardSetId, elGamalGenerator.genRandomPublicKey(1)))
				.toList();
		verificationCardService.saveAll(verificationCards);
		LOGGER.info("Verification cards saved.");

		// Save votes.
		confirmedVotes.forEach(encryptedVerifiableVoteService::save);
		LOGGER.info("Encrypted verifiable votes saved.");

		ballotBoxIdToMix = verificationCardSetContexts.get(0).getBallotBoxId();

		// Save getMixnetInitialCiphertextsOutputs and provide all 4 controlComponentVotesHashPayloads.
		setUpGetMixnetInitialCiphertexts(mixnetInitialCiphertextsService, getMixnetInitialCiphertextsAlgorithm, electionEventId);
	}

	private static void setUpElection() {
		final BigInteger p = new BigInteger(
				"BFF67CCCAE0F61B38BA70AD736CFA8EA284B5D6CAEBF2FED2FC88D0ADFF9E2B220BFD9CCDA59BD3BD52B12CDFCCF41AA3D9BF81F95A7D59452690BF45F7993BE760ABBCA3E29705D473A66638DCD6EA78663C0DB91E3E0AB1DFE1AFF25181D4D2C3BA059F9131D95D37F431233EA2276E052C960DCB130F9DFFDC0BE977C9947E7AE05EA516AA81B2528FEF03625ACFCF495C3AB5D5F176E06F1382AE96A470321092C0C1C02A196AB4DA20D3605B4E72A5CFD16CF9381C83513EBD18A8A4A21BF95B864EDA4C0214583E99A3180F7A561F19D451BC4354E7A284DC7EB0C5A05DC58856C6DC8CF3A57B42D866D85F453D1BD8CC61117FB606A40AF0A0EF76D603C7A307C0B8854355D5836774C6BB12238E09806782A487BB9888AE1DB54DECA3FEC374D30CC9A722D3052585069D212B62FD6758710337CA17411E82FF7E7E7B754F4C9F3A1C49AA15E0D0A0E9B05A2EA880216D052B780E68168CA336309D3C1802A278AFCF1C0F8FA3381C145DA0864892221B960ECD6D46165E057B55EEB",
				16);
		final BigInteger q = new BigInteger(
				"5FFB3E665707B0D9C5D3856B9B67D4751425AEB6575F97F697E446856FFCF159105FECE66D2CDE9DEA958966FE67A0D51ECDFC0FCAD3EACA293485FA2FBCC9DF3B055DE51F14B82EA39D3331C6E6B753C331E06DC8F1F0558EFF0D7F928C0EA6961DD02CFC898ECAE9BFA18919F5113B702964B06E58987CEFFEE05F4BBE4CA3F3D702F528B5540D92947F781B12D67E7A4AE1D5AEAF8BB703789C1574B52381908496060E0150CB55A6D1069B02DA73952E7E8B67C9C0E41A89F5E8C5452510DFCADC3276D26010A2C1F4CD18C07BD2B0F8CEA28DE21AA73D1426E3F5862D02EE2C42B636E4679D2BDA16C336C2FA29E8DEC663088BFDB035205785077BB6B01E3D183E05C42A1AAEAC1B3BA635D8911C704C033C15243DDCC44570EDAA6F651FF61BA698664D391698292C2834E9095B17EB3AC38819BE50BA08F417FBF3F3DBAA7A64F9D0E24D50AF0685074D82D17544010B68295BC07340B46519B184E9E0C01513C57E78E07C7D19C0E0A2ED0432449110DCB0766B6A30B2F02BDAAF75",
				16);
		final BigInteger g = new BigInteger(
				"3",
				16);
		gqGroup = new GqGroup(p, q, g);
		elGamalGenerator = new ElGamalGenerator(gqGroup);
		final int l = 4;

		// Generate key pairs for each control-component.
		ccmjElectionKeyPairs = NODE_IDS.stream()
				.map(nodeId -> ElGamalMultiRecipientKeyPair.genKeyPair(gqGroup, l, random))
				.toList();
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys = ccmjElectionKeyPairs.stream()
				.map(ElGamalMultiRecipientKeyPair::getPublicKey)
				.collect(GroupVector.toGroupVector());
		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccrChoiceReturnCodesEncryptionPublicKeys = NODE_IDS.stream()
				.map(nodeId -> ElGamalMultiRecipientKeyPair.genKeyPair(gqGroup, l, random))
				.map(ElGamalMultiRecipientKeyPair::getPublicKey)
				.collect(GroupVector.toGroupVector());

		electionEventContext = new ElectionEventContextPayloadGenerator(gqGroup).generate(l, numberOfWriteInsPlusOne, 2).getElectionEventContext();
		setupComponentPublicKeys = new SetupComponentPublicKeysPayloadGenerator(gqGroup).generate(ccrChoiceReturnCodesEncryptionPublicKeys,
				ccmElectionPublicKeys).getSetupComponentPublicKeys();

		verificationCardIds = IntStream.range(0, NUMBER_OF_VERIFICATION_CARDS)
				.mapToObj(i -> random.genRandomString(ID_LENGTH, base16Alphabet))
				.toList();

		final String electionEventId = electionEventContext.electionEventId();
		final String verificationCardSetId = electionEventContext.verificationCardSetContexts().getFirst().getVerificationCardSetId();
		confirmedVotes = verificationCardIds.stream()
				.limit(NUMBER_OF_CONFIRMED_VOTES)
				.map(verificationCardId -> {
					final ContextIds contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);
					return genEncryptedVerifiableVote(contextIds);
				})
				.toList();
		LOGGER.info("Election data generated.");
	}

	private static EncryptedVerifiableVote genEncryptedVerifiableVote(final ContextIds contextIds) {
		final ElGamalMultiRecipientCiphertext encryptedVote = elGamalGenerator.genRandomCiphertext(numberOfWriteInsPlusOne);
		final GqGroup encryptionGroup = encryptedVote.getGroup();
		final ZqGroup zqGroup = ZqGroup.sameOrderAs(encryptionGroup);
		final BigInteger exponentValue = RandomFactory.createRandom().genRandomInteger(encryptionGroup.getQ());
		final ZqElement exponent = ZqElement.create(exponentValue, zqGroup);
		final ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote = encryptedVote.getCiphertextExponentiation(exponent);
		final ZqGroupGenerator zqGroupGenerator = new ZqGroupGenerator(zqGroup);
		final ExponentiationProof exponentiationProof = new ExponentiationProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementMember());
		final PlaintextEqualityProof plaintextEqualityProof = new PlaintextEqualityProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementVector(2));
		final ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes = elGamalGenerator.genRandomCiphertext(1);

		return new EncryptedVerifiableVote(contextIds, encryptedVote, encryptedPartialChoiceReturnCodes, exponentiatedEncryptedVote,
				exponentiationProof, plaintextEqualityProof);
	}

	private static void setUpGetMixnetInitialCiphertexts(final MixnetInitialCiphertextsService mixnetInitialCiphertextsService,
			final GetMixnetInitialCiphertextsAlgorithm getMixnetInitialCiphertextsAlgorithm, final String electionEventId) {

		final Map<String, ElGamalMultiRecipientCiphertext> encryptedConfirmedVotes = confirmedVotes.stream()
				.collect(Collectors.toMap(vote -> vote.contextIds().verificationCardId(), EncryptedVerifiableVote::encryptedVote));

		final GetMixnetInitialCiphertextsContext getMixnetInitialCiphertextsContext = new GetMixnetInitialCiphertextsContext(gqGroup,
				encryptedConfirmedVotes.size(), numberOfWriteInsPlusOne, setupComponentPublicKeys.electionPublicKey());
		final GetMixnetInitialCiphertextsInput getMixnetInitialCiphertextsInput = new GetMixnetInitialCiphertextsInput(encryptedConfirmedVotes);

		final GetMixnetInitialCiphertextsOutput getMixnetInitialCiphertextsOutput = getMixnetInitialCiphertextsAlgorithm.getMixnetInitialCiphertexts(
				getMixnetInitialCiphertextsContext, getMixnetInitialCiphertextsInput);
		mixnetInitialCiphertextsService.save(electionEventId, ballotBoxIdToMix, getMixnetInitialCiphertextsOutput);

		// For simplicity, we give the encryptedConfirmedVotesHash of the first node to all nodes. In production, each node computes his own encryptedConfirmedVotesHash.
		controlComponentVotesHashPayloads = genControlComponentVotesHashPayloads(electionEventId, ballotBoxIdToMix,
				getMixnetInitialCiphertextsOutput.encryptedConfirmedVotesHash());
	}

	private static List<ControlComponentVotesHashPayload> genControlComponentVotesHashPayloads(final String electionEventId,
			final String ballotBoxId, final String encryptedConfirmedVotesHash) {

		return NODE_IDS.stream()
				.map(nodeId -> new ControlComponentVotesHashPayload(electionEventId, ballotBoxIdToMix, nodeId, encryptedConfirmedVotesHash))
				.map(controlComponentVotesHashPayload -> {
					final int nodeId = controlComponentVotesHashPayload.getNodeId();

					try {
						final TestSigner controlComponentSigner = new TestSigner(KEYSTORE_FILENAME_PATH, KEYSTORE_PASSWORD_FILENAME_PATH,
								Alias.getControlComponentByNodeId(nodeId));
						controlComponentSigner.sign(controlComponentVotesHashPayload,
								ChannelSecurityContextData.controlComponentVotesHash(nodeId, electionEventId, ballotBoxId));
					} catch (final IOException | SignatureException e) {
						throw new IllegalStateException("Failed to test sign control component votes hash payload", e);
					}

					return controlComponentVotesHashPayload;
				}).toList();
	}

	protected static ElGamalMultiRecipientKeyPair getCcmjKeyPair(final int nodeId) {
		return ccmjElectionKeyPairs.get(nodeId - 1);
	}

	protected void addResponseMessage(final int nodeId, final String electionEventId, final String ballotBoxId, final byte[] responseMessage) {
		responseMessages.put(nodeId, new MixDecryptResponse(electionEventId, ballotBoxId, responseMessage));
	}

	protected MixDecryptResponse getResponseMessage(final int nodeId) {
		return responseMessages.get(nodeId);
	}

	protected static class MixDecryptResponse {

		private final String electionEventId;
		private final String ballotBoxId;
		private final byte[] message;

		MixDecryptResponse(final String electionEventId, final String ballotBoxId, final byte[] message) {
			this.electionEventId = electionEventId;
			this.ballotBoxId = ballotBoxId;
			this.message = message;
		}

		public String getElectionEventId() {
			return electionEventId;
		}

		public String getBallotBoxId() {
			return ballotBoxId;
		}

		public byte[] getMessage() {
			return message;
		}
	}

}
