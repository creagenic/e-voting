/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.business.voting;

import static ch.post.it.evoting.controlcomponent.TestKeyStoreInitializer.KEYSTORE_FILENAME_PATH;
import static ch.post.it.evoting.controlcomponent.TestKeyStoreInitializer.KEYSTORE_PASSWORD_FILENAME_PATH;
import static ch.post.it.evoting.domain.SharedQueue.CONTROL_COMPONENTS_ADDRESS;
import static ch.post.it.evoting.domain.SharedQueue.MESSAGE_HEADER_MESSAGE_TYPE;
import static ch.post.it.evoting.domain.SharedQueue.MESSAGE_HEADER_NODE_ID;
import static ch.post.it.evoting.domain.SharedQueue.VOTING_SERVER_ADDRESS;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.security.SignatureException;
import java.util.UUID;

import jakarta.jms.JMSException;
import jakarta.jms.Message;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponent.ArtemisSupport;
import ch.post.it.evoting.controlcomponent.TestSigner;
import ch.post.it.evoting.controlcomponent.domain.CcrjReturnCodesKeys;
import ch.post.it.evoting.controlcomponent.domain.ElectionEventState;
import ch.post.it.evoting.controlcomponent.domain.VerificationCard;
import ch.post.it.evoting.controlcomponent.service.CcrjReturnCodesKeysService;
import ch.post.it.evoting.controlcomponent.service.ElectionContextService;
import ch.post.it.evoting.controlcomponent.service.ElectionEventService;
import ch.post.it.evoting.controlcomponent.service.ElectionEventStateService;
import ch.post.it.evoting.controlcomponent.service.VerificationCardService;
import ch.post.it.evoting.controlcomponent.service.VerificationCardStateService;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.domain.voting.confirmvote.ConfirmationKey;
import ch.post.it.evoting.domain.voting.confirmvote.ControlComponenthlVCCPayload;
import ch.post.it.evoting.domain.voting.confirmvote.VotingServerConfirmPayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionEventContext;
import ch.post.it.evoting.evotinglibraries.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.ElectionEventContextPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;

class LongVoteCastReturnCodeShareHashProcessorIT extends ArtemisSupport {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private static ContextIds contextIds;
	private static VotingServerConfirmPayload votingServerConfirmPayload;

	@Autowired
	private ObjectMapper objectMapper;

	@BeforeAll
	static void setUpAll(
			@Autowired
			final ElectionEventStateService electionEventStateService,
			@Autowired
			final ElectionContextService electionContextService,
			@Autowired
			final ElectionEventService electionEventService,
			@Autowired
			final VerificationCardService verificationCardService,
			@Autowired
			final CcrjReturnCodesKeysService ccrjReturnCodesKeysService,
			@Autowired
			final VerificationCardStateService verificationCardStateService) throws IOException, SignatureException {

		final ElectionEventContextPayloadGenerator electionEventContextPayloadGenerator = new ElectionEventContextPayloadGenerator();
		final ElectionEventContext electionEventContext = electionEventContextPayloadGenerator.generate().getElectionEventContext();
		final String electionEventId = electionEventContext.electionEventId();
		final VerificationCardSetContext verificationCardSetContext = electionEventContext.verificationCardSetContexts().get(0);
		final GqGroup gqGroup = verificationCardSetContext.getPrimesMappingTable().getEncryptionGroup();

		// Save election event. The election must be in the CONFIGURED state for this processor.
		electionEventService.save(electionEventId, gqGroup);
		electionEventStateService.updateElectionEventState(electionEventId, ElectionEventState.CONFIGURED);

		// Save election event context.
		electionContextService.save(electionEventContext);

		// Create CCRj election keys.
		final ZqGroupGenerator zqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(gqGroup));
		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);

		final ZqElement ccrKey = zqGroupGenerator.genRandomZqElementMember();
		final ElGamalMultiRecipientKeyPair elGamalMultiRecipientKeyPair = elGamalGenerator.genRandomKeyPair(10);
		ccrjReturnCodesKeysService.save(new CcrjReturnCodesKeys(electionEventId, ccrKey, elGamalMultiRecipientKeyPair));

		// Create verification card.
		final String verificationCardId = random.genRandomString(ID_LENGTH, base16Alphabet);
		final String verificationCardSetId = verificationCardSetContext.getVerificationCardSetId();
		final ElGamalMultiRecipientPublicKey publicKey = elGamalGenerator.genRandomPublicKey(1);
		verificationCardService.save(new VerificationCard(verificationCardId, verificationCardSetId, publicKey));

		contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);

		//Mark card as LCC share created
		verificationCardStateService.setSentVote(verificationCardId);

		// Generate request payload.
		final GqElement confirmationKeyElement = new GqGroupGenerator(gqGroup).genMember();
		final ConfirmationKey confirmationKey = new ConfirmationKey(contextIds, confirmationKeyElement);
		final int unsuccessfulConfirmationAttemptCount = 0;

		votingServerConfirmPayload = new VotingServerConfirmPayload(gqGroup, confirmationKey, unsuccessfulConfirmationAttemptCount);

		final TestSigner votingServerSigner = new TestSigner(KEYSTORE_FILENAME_PATH, KEYSTORE_PASSWORD_FILENAME_PATH, Alias.VOTING_SERVER);
		votingServerSigner.sign(votingServerConfirmPayload,
				ChannelSecurityContextData.votingServerConfirm(electionEventId, verificationCardSetId, verificationCardId));
	}

	@Test
	void happyPath() throws IOException, JMSException {
		final String correlationId = UUID.randomUUID().toString();
		multicastJmsTemplate.convertAndSend(CONTROL_COMPONENTS_ADDRESS, objectMapper.writeValueAsBytes(votingServerConfirmPayload), jmsMessage -> {
			jmsMessage.setJMSCorrelationID(correlationId);
			jmsMessage.setStringProperty(MESSAGE_HEADER_MESSAGE_TYPE, votingServerConfirmPayload.getClass().getName());
			jmsMessage.setStringProperty(MESSAGE_HEADER_NODE_ID, "1");
			return jmsMessage;
		});

		final Message responseMessage = jmsTemplate.receive(VOTING_SERVER_ADDRESS);
		assertNotNull(responseMessage);

		final ControlComponenthlVCCPayload responsePayload = objectMapper.readValue(responseMessage.getBody(byte[].class),
				ControlComponenthlVCCPayload.class);

		assertEquals(contextIds, responsePayload.getConfirmationKey().contextIds());
		assertNotNull(responsePayload.getHashLongVoteCastCodeShare());
	}
}

