/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.protocol.configuration.setuptally;

import static ch.post.it.evoting.evotinglibraries.domain.VotingOptionsConstants.MAXIMUM_SUPPORTED_NUMBER_OF_WRITE_INS;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProofFactory;

/**
 * Tests of SetupTallyCCMAlgorithm.
 */
@DisplayName("A SetupTallyCCMAlgorithm")
class SetupTallyCCMAlgorithmTest extends TestGroupSetup {

	private static final String ELECTION_EVENT_ID = "0b88257ec32142bb8ee0ed1bb70f362e";
	private static final int NODE_ID = 1;
	private static SetupTallyCCMAlgorithm setupTallyCCMAlgorithm;
	private static SetupTallyCCMContext context;
	private final int DELTA_SUP = MAXIMUM_SUPPORTED_NUMBER_OF_WRITE_INS + 1;

	@BeforeAll
	static void setUpAll() {
		final Random random = RandomFactory.createRandom();
		gqGroup = GroupTestData.getLargeGqGroup();
		final ZeroKnowledgeProof zeroKnowledgeProof = ZeroKnowledgeProofFactory.createZeroKnowledgeProof();
		setupTallyCCMAlgorithm = new SetupTallyCCMAlgorithm(random, zeroKnowledgeProof);
	}

	@BeforeEach
	void setup() {
		context = new SetupTallyCCMContext.Builder()
				.setEncryptionGroup(gqGroup)
				.setNodeId(NODE_ID)
				.setElectionEventId(ELECTION_EVENT_ID)
				.setMaximumNumberOfWriteInsPlusOne(DELTA_SUP)
				.build();
	}

	@Test
	@DisplayName("with a valid parameter does not throw any Exception.")
	void validParamDoesNotThrow() {
		assertDoesNotThrow(() -> setupTallyCCMAlgorithm.setupTallyCCM(context));
	}

	@Test
	@DisplayName("with a null parameter throws a NullPointerException.")
	void nullParamThrowsANullPointer() {
		assertThrows(NullPointerException.class, () -> setupTallyCCMAlgorithm.setupTallyCCM(null));
	}

	@Test
	@DisplayName("with a valid parameter returns a non-null keypair with expected size.")
	void nonNullOutput() {
		final SetupTallyCCMOutput setupTallyCCMOutput = setupTallyCCMAlgorithm.setupTallyCCM(context);

		assertNotNull(setupTallyCCMOutput);
		assertEquals(DELTA_SUP, setupTallyCCMOutput.getCcmjElectionKeyPair().getPublicKey().size());
		assertEquals(DELTA_SUP, setupTallyCCMOutput.getCcmjElectionKeyPair().getPrivateKey().size());
	}
}
