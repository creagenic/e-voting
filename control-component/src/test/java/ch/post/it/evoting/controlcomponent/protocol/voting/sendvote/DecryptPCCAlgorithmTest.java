/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.protocol.voting.sendvote;

import static ch.post.it.evoting.evotinglibraries.domain.VotingOptionsConstants.MAXIMUM_SUPPORTED_NUMBER_OF_SELECTIONS;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyList;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.election.generators.PrimesMappingTableGenerator;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.GetHashContextAlgorithm;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.PrimesMappingTableAlgorithms;

/**
 * Tests of DecryptPCCAlgorithm.
 */
@DisplayName("DecryptPCCService")
class DecryptPCCAlgorithmTest extends TestGroupSetup {

	private static final int PSI_SUP = MAXIMUM_SUPPORTED_NUMBER_OF_SELECTIONS;
	private static final int PSI = 5;
	private static final int PSI_MAX = 6;
	private static final int DELTA_MAX = 2;
	private static final int NODE_ID = 1;
	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final ZeroKnowledgeProof zeroKnowledgeProofMock = spy(ZeroKnowledgeProof.class);
	private static final GetHashContextAlgorithm getHashContextAlgorithm = new GetHashContextAlgorithm();

	private static DecryptPCCAlgorithm decryptPCCAlgorithm;

	private DecryptPCCContext decryptPCCContext;
	private DecryptPCCInput decryptPCCInput;

	@BeforeAll
	static void init() {
		final PrimesMappingTableAlgorithms primesMappingTableAlgorithms = mock(PrimesMappingTableAlgorithms.class);
		decryptPCCAlgorithm = new DecryptPCCAlgorithm(zeroKnowledgeProofMock, primesMappingTableAlgorithms, getHashContextAlgorithm);

		when(primesMappingTableAlgorithms.getPsi(any())).thenReturn(PSI);
		when(primesMappingTableAlgorithms.getDelta(any())).thenReturn(1);
	}

	private GroupVector<ExponentiationProof, ZqGroup> genRandomExponentiationProofVector() {
		return Stream.generate(
						() -> new ExponentiationProof(zqGroupGenerator.genRandomZqElementMember(), zqGroupGenerator.genRandomZqElementMember()))
				.limit(DecryptPCCAlgorithmTest.PSI)
				.collect(GroupVector.toGroupVector());
	}

	@Nested
	@DisplayName("calling decryptPCC with")
	class DecryptPCCTest {

		@BeforeEach
		void setup() {
			final String electionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);
			final String verificationCardId = random.genRandomString(ID_LENGTH, base16Alphabet);
			final String verificationCardSetId = random.genRandomString(ID_LENGTH, base16Alphabet);
			final GroupVector<GqElement, GqGroup> exponentiatedGammaElements = gqGroupGenerator.genRandomGqElementVector(PSI);
			final GroupVector<GroupVector<GqElement, GqGroup>, GqGroup> otherExponentiatedGammaElements = GroupVector.of(
					gqGroupGenerator.genRandomGqElementVector(PSI),
					gqGroupGenerator.genRandomGqElementVector(PSI),
					gqGroupGenerator.genRandomGqElementVector(PSI)
			);
			final GroupVector<GroupVector<ExponentiationProof, ZqGroup>, ZqGroup> otherExponentiationProofs = GroupVector.of(
					genRandomExponentiationProofVector(),
					genRandomExponentiationProofVector(),
					genRandomExponentiationProofVector()
			);
			final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> otherChoiceReturnCodesEncryptionKeys = GroupVector.of(
					elGamalGenerator.genRandomPublicKey(PSI_SUP),
					elGamalGenerator.genRandomPublicKey(PSI_SUP),
					elGamalGenerator.genRandomPublicKey(PSI_SUP)
			);
			final ElGamalMultiRecipientCiphertext encryptedVote = elGamalGenerator.genRandomCiphertext(1);
			final ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote = elGamalGenerator.genRandomCiphertext(1);
			final ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes = elGamalGenerator.genRandomCiphertext(PSI);
			final PrimesMappingTable primesMappingTable = new PrimesMappingTableGenerator(gqGroup).generate(1);
			final ElGamalMultiRecipientPublicKey electionPublicKey = elGamalGenerator.genRandomPublicKey(DELTA_MAX);
			final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey = elGamalGenerator.genRandomPublicKey(PSI_MAX);

			decryptPCCContext = new DecryptPCCContext.Builder()
					.setEncryptionGroup(gqGroup)
					.setNodeId(NODE_ID)
					.setElectionEventId(electionEventId)
					.setVerificationCardSetId(verificationCardSetId)
					.setVerificationCardId(verificationCardId)
					.setPrimesMappingTable(primesMappingTable)
					.setElectionPublicKey(electionPublicKey)
					.setChoiceReturnCodesEncryptionPublicKey(choiceReturnCodesEncryptionPublicKey)
					.setOtherCcrChoiceReturnCodesEncryptionKeys(otherChoiceReturnCodesEncryptionKeys)
					.build();

			decryptPCCInput = new DecryptPCCInput.Builder()
					.setExponentiatedGammaElements(exponentiatedGammaElements)
					.setOtherCcrExponentiatedGammaElements(otherExponentiatedGammaElements)
					.setOtherCcrExponentiationProofs(otherExponentiationProofs)
					.setEncryptedVote(encryptedVote)
					.setExponentiatedEncryptedVote(exponentiatedEncryptedVote)
					.setEncryptedPartialChoiceReturnCodes(encryptedPartialChoiceReturnCodes)
					.build();
		}

		@Test
		@DisplayName("any null arguments throws a NullPointerException")
		void decryptPCCWithNullArgumentsThrows() {
			assertThrows(NullPointerException.class, () -> decryptPCCAlgorithm.decryptPCC(null, decryptPCCInput));
			assertThrows(NullPointerException.class, () -> decryptPCCAlgorithm.decryptPCC(decryptPCCContext, null));
		}

		@Test
		@DisplayName("valid arguments does not throw")
		void decryptPCCWithValidInputDoesNotThrow() {
			doReturn(true).when(zeroKnowledgeProofMock).verifyExponentiation(any(), any(), any(), anyList());

			final GroupVector<GqElement, GqGroup> gqElements = assertDoesNotThrow(
					() -> decryptPCCAlgorithm.decryptPCC(decryptPCCContext, decryptPCCInput));

			assertEquals(PSI, gqElements.size());
		}

		@Test
		@DisplayName("context and input having different groups throws an IllegalArgumentException")
		void decryptPCCWithContextAndInputFromDifferentGroupsThrows() {
			final DecryptPCCContext spyDecryptPCCContext = spy(DecryptPCCAlgorithmTest.this.decryptPCCContext);
			doReturn(otherGqGroup).when(spyDecryptPCCContext).getEncryptionGroup();
			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
					() -> decryptPCCAlgorithm.decryptPCC(spyDecryptPCCContext, decryptPCCInput));
			assertEquals("The context and input must have the same group.", Throwables.getRootCause(exception).getMessage());
		}

		@Test
		@DisplayName("encrypted partial choice return codes with a size different from numberOfSelectableVotingOptions throws an IllegalArgumentException")
		void decryptPCCWithEncryptedPartialChoiceReturnCodesWrongSizeThrows() {
			// Encrypted partial choice return codes of size numberOfSelectableVotingOptions - 1
			final DecryptPCCInput decryptPCCInputSpy = spy(decryptPCCInput);
			final ElGamalMultiRecipientCiphertext tooShortEncryptedPartialChoiceReturnCodes = elGamalGenerator.genRandomCiphertext(PSI - 1);
			doReturn(tooShortEncryptedPartialChoiceReturnCodes).when(decryptPCCInputSpy).getEncryptedPartialChoiceReturnCodes();
			IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
					() -> decryptPCCAlgorithm.decryptPCC(decryptPCCContext, decryptPCCInputSpy));
			assertEquals(String.format("The encrypted partial Choice Return Codes size must be equal to psi. [psi: %s]", PSI),
					Throwables.getRootCause(exception).getMessage());

			// Encrypted partial choice return codes of size numberOfSelectableVotingOptions + 1
			final ElGamalMultiRecipientCiphertext tooLongEncryptedPartialChoiceReturnCodes = elGamalGenerator.genRandomCiphertext(PSI + 1);
			doReturn(tooLongEncryptedPartialChoiceReturnCodes).when(decryptPCCInputSpy).getEncryptedPartialChoiceReturnCodes();
			exception = assertThrows(IllegalArgumentException.class,
					() -> decryptPCCAlgorithm.decryptPCC(decryptPCCContext, decryptPCCInputSpy));
			assertEquals(String.format("The encrypted partial Choice Return Codes size must be equal to psi. [psi: %s]", PSI),
					Throwables.getRootCause(exception).getMessage());
		}

		@Test
		@DisplayName("with failing zero knowledge proof validation throws an IllegalStateException")
		void decryptPCCWithFailingZeroKnowledgeProofVerification() {
			doReturn(false).when(zeroKnowledgeProofMock).verifyExponentiation(any(), any(), any(), anyList());

			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> decryptPCCAlgorithm.decryptPCC(decryptPCCContext, decryptPCCInput));
			assertEquals(String.format("The verification of the other control component's exponentiation proof failed [control component: %d]", 2),
					Throwables.getRootCause(exception).getMessage());
		}

	}
}
