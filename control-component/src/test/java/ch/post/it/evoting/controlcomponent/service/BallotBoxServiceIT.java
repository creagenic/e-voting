/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.service;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.security.SecureRandom;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import ch.post.it.evoting.controlcomponent.domain.BallotBoxEntity;
import ch.post.it.evoting.controlcomponent.domain.ElectionEventEntity;
import ch.post.it.evoting.controlcomponent.domain.VerificationCardSetEntity;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.election.generators.PrimesMappingTableGenerator;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

@SpringBootTest
@ActiveProfiles("test")
@DisplayName("BallotBoxService")
class BallotBoxServiceIT {
	private static final Random RANDOM_SERVICE = RandomFactory.createRandom();
	private static final SecureRandom RANDOM = new SecureRandom();
	private static final String BAD_ID = "Bad Id";
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static PrimesMappingTable primesMappingTable;

	@Autowired
	private BallotBoxService ballotBoxService;

	@Autowired
	private ElectionEventService electionEventService;

	@Autowired
	private VerificationCardSetService verificationCardSetService;

	// This test does not need keystore functionality
	@MockBean
	private SignatureKeystore<Alias> signatureKeystoreService;

	private String ballotBoxId;
	private String electionEventId;
	private String verificationCardSetId;
	private GqGroup encryptionGroup;
	private boolean testBallotBox;

	@BeforeAll
	static void setupAll() {
		final PrimesMappingTableGenerator primesMappingTableGenerator = new PrimesMappingTableGenerator();
		primesMappingTable = primesMappingTableGenerator.generate(5);
	}

	@BeforeEach
	void setup() {
		ballotBoxId = RANDOM_SERVICE.genRandomString(ID_LENGTH, base16Alphabet);
		electionEventId = RANDOM_SERVICE.genRandomString(ID_LENGTH, base16Alphabet);
		verificationCardSetId = RANDOM_SERVICE.genRandomString(ID_LENGTH, base16Alphabet);
		encryptionGroup = GroupTestData.getGqGroup();
		testBallotBox = RANDOM.nextBoolean();
	}

	@Test
	void testSaveWithBadArgumentThrows() {
		assertThrows(NullPointerException.class,
				() -> ballotBoxService.save(null, verificationCardSetId, testBallotBox, 10, 900, primesMappingTable
				));
		assertThrows(NullPointerException.class,
				() -> ballotBoxService.save(ballotBoxId, null, testBallotBox, 10, 900, primesMappingTable
				));
		assertThrows(NullPointerException.class,
				() -> ballotBoxService.save(ballotBoxId, null, testBallotBox, 10, 900, null));
		assertThrows(IllegalArgumentException.class,
				() -> ballotBoxService.save(ballotBoxId, verificationCardSetId, testBallotBox, -1, 900, primesMappingTable
				));
		assertThrows(IllegalArgumentException.class,
				() -> ballotBoxService.save(ballotBoxId, verificationCardSetId, testBallotBox, 10, -1, primesMappingTable
				));
	}

	@Test
	void testExistsWithBadArgumentThrows() {
		assertThrows(NullPointerException.class, () -> ballotBoxService.existsForElectionEventId(null, electionEventId));
		assertThrows(NullPointerException.class, () -> ballotBoxService.existsForElectionEventId(ballotBoxId, null));
		assertThrows(FailedValidationException.class, () -> ballotBoxService.existsForElectionEventId(BAD_ID, electionEventId));
		assertThrows(FailedValidationException.class, () -> ballotBoxService.existsForElectionEventId(ballotBoxId, BAD_ID));
	}

	@Test
	void testExistsWithValidArgumentDoesNotThrow() {
		final boolean existsBefore = ballotBoxService.existsForElectionEventId(ballotBoxId, electionEventId);
		assertFalse(existsBefore);
		setUpElection();
		final boolean existsAfter = ballotBoxService.existsForElectionEventId(ballotBoxId, electionEventId);
		assertTrue(existsAfter);
	}

	@Test
	void testGetBallotBoxWithBadArgumentThrows() {
		assertThrows(NullPointerException.class, () -> ballotBoxService.getBallotBox((String) null));
		assertThrows(FailedValidationException.class, () -> ballotBoxService.getBallotBox(BAD_ID));
	}

	@Test
	void testGetBallotBoxForUnsavedBallotBoxThrows() {
		assertThrows(IllegalStateException.class, () -> ballotBoxService.getBallotBox(ballotBoxId));
	}

	@Test
	void testGetBallotBoxWithValidArgumentDoesNotThrow() {
		setUpElection();
		final BallotBoxEntity ballotBoxEntity = ballotBoxService.getBallotBox(ballotBoxId);
		assertEquals(ballotBoxId, ballotBoxEntity.getBallotBoxId());
		assertEquals(testBallotBox, ballotBoxEntity.isTestBallotBox());
	}

	@Test
	void testIsMixedWithBadArgumentThrows() {
		assertThrows(NullPointerException.class, () -> ballotBoxService.isMixed(null));
	}

	@Test
	void testIsMixedWithValidArgumentDoesNotThrow() {
		setUpElection();
		assertFalse(ballotBoxService.isMixed(ballotBoxId));
		ballotBoxService.setMixed(ballotBoxId);
		assertTrue(ballotBoxService.isMixed(ballotBoxId));
	}

	@Test
	void testSetMixedSetsMixedStateCorrectly() {
		setUpElection();
		final BallotBoxEntity ballotBoxEntity = ballotBoxService.setMixed(ballotBoxId);
		assertTrue(ballotBoxEntity.isMixed());
	}

	private void setUpElection() {
		// Save election event.
		final ElectionEventEntity savedElectionEventEntity = electionEventService.save(electionEventId, encryptionGroup);
		// Save verification card set.
		final VerificationCardSetEntity verificationCardSetEntity = new VerificationCardSetEntity(verificationCardSetId,
				savedElectionEventEntity);
		verificationCardSetService.save(verificationCardSetEntity);

		ballotBoxService.save(ballotBoxId, verificationCardSetId, testBallotBox, 10, 900, primesMappingTable);
	}
}
