/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponent.service;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.controlcomponent.domain.ElectionContextEntity;
import ch.post.it.evoting.controlcomponent.domain.ElectionEventEntity;
import ch.post.it.evoting.controlcomponent.repository.ElectionContextRepository;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionEventContext;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.PrimesMappingTableAlgorithms;

@DisplayName("ElectionContextServiceTest")
class ElectionContextServiceTest {

	private static final ElectionContextRepository electionContextRepository = mock(ElectionContextRepository.class);
	private static final ElectionEventService electionEventService = mock(ElectionEventService.class);
	private static final VerificationCardSetService verificationCardSetService = mock(VerificationCardSetService.class);
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private static ElectionContextService electionContextService;
	private static ElectionEventContext electionEventContext;

	@BeforeAll
	static void setUpAll() {
		final String electionEventId = RandomFactory.createRandom().genRandomString(ID_LENGTH, base16Alphabet);

		electionEventContext = mock(ElectionEventContext.class);
		when(electionEventContext.electionEventId()).thenReturn(electionEventId);
		when(electionEventContext.startTime()).thenReturn(LocalDateTime.now());
		when(electionEventContext.finishTime()).thenReturn(LocalDateTime.now().plusDays(2));
		when(electionEventContext.maximumNumberOfSelections()).thenReturn(10);
		when(electionEventContext.maximumNumberOfWriteInsPlusOne()).thenReturn(2);

		final GqGroup encryptionGroup = GroupTestData.getGqGroup();
		final ElectionEventEntity electionEventEntity = new ElectionEventEntity(electionEventId, encryptionGroup);

		final ElectionContextEntity electionContextEntity = new ElectionContextEntity.Builder()
				.setElectionEventEntity(electionEventEntity)
				.setStartTime(electionEventContext.startTime())
				.setFinishTime(electionEventContext.finishTime())
				.setMaxNumberOfSelections(electionEventContext.maximumNumberOfSelections())
				.setMaxNumberOfWriteInsPlusOne(electionEventContext.maximumNumberOfWriteInsPlusOne())
				.build();

		final BallotBoxService ballotBoxService = mock(BallotBoxService.class);
		doNothing().when(ballotBoxService).saveFromContexts(any());

		when(electionContextRepository.save(any())).thenReturn(new ElectionContextEntity());
		when(electionContextRepository.findByElectionEventId(electionEventContext.electionEventId()))
				.thenReturn(Optional.ofNullable(electionContextEntity));
		when(electionEventService.getElectionEventEntity(electionEventId)).thenReturn(electionEventEntity);
		when(electionEventService.getEncryptionGroup(any())).thenReturn(encryptionGroup);

		electionContextService = new ElectionContextService(ballotBoxService, electionEventService, electionContextRepository,
				verificationCardSetService, new PrimesMappingTableAlgorithms());
	}

	@Test
	@DisplayName("saving with valid ElectionEventContext does not throw")
	void savingValidElectionEventContextDoesNotThrow() {
		electionContextService.save(electionEventContext);
		verify(electionContextRepository, times(1)).save(any());
	}

	@Test
	@DisplayName("loading non existing ElectionEventContext throws IllegalStateException")
	void loadNonExistingThrows() {
		final String nonExistingElectionId = "e77dbe3c70874ea584c490a0c6ac0ca4";
		final IllegalStateException exceptionCcm = assertThrows(IllegalStateException.class,
				() -> electionContextService.getElectionEventStartTime(nonExistingElectionId));
		assertEquals(String.format("Election context entity not found. [electionEventId: %s]", nonExistingElectionId),
				Throwables.getRootCause(exceptionCcm).getMessage());
	}
}
