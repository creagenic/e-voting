ARG builder_image
FROM ${builder_image} AS builder

USER root

RUN apt-get update && apt-get install -y wget

ARG FLYWAY_URL
RUN wget -O /tmp/flyway.tar.gz ${FLYWAY_URL}
RUN mkdir /flyway && tar -xzf /tmp/flyway.tar.gz -C /flyway

FROM postgres:15.4
COPY --from=builder /flyway /flyway/

RUN mv /flyway/flyway-* /flyway/flyway-commandline && \
    chmod a+x /flyway/flyway-commandline/flyway

COPY ./postgres/resources/check_db_health.sh /usr/local/bin/check_db_health.sh
RUN chmod +x /usr/local/bin/check_db_health.sh
COPY ./postgres/resources/oracle_to_postgres.sh /flyway/oracle_to_postgres.sh
COPY ./target/sql/schema /flyway/flyway-commandline/sql

# Migrate sql to postgres.
RUN chmod 755 /flyway/oracle_to_postgres.sh && /flyway/oracle_to_postgres.sh -i -d /flyway/flyway-commandline/sql/

COPY ./postgres/resources/create.sh /docker-entrypoint-initdb.d/