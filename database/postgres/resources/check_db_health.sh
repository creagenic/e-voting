#!/bin/bash

#
# (c) Copyright 2024 Swiss Post Ltd.
#
# Schemas.
declare -a StringArray=("voting_server" "control_component_1" "control_component_2" "control_component_3" "control_component_4")

# Validate migration with flyway validate.
function validate_flyway {
  local schema=$1

  if [[ "$schema" == control_component* ]]; then
      location="filesystem:/flyway/flyway-commandline/sql/control_components"
  else
      location="filesystem:/flyway/flyway-commandline/sql/$schema"
  fi

  /flyway/flyway-commandline/flyway validate -url="jdbc:postgresql://localhost:5432/postgres" -user="${schema,,}" -password="${schema,,}" -schemas="$schema" -locations="$location" >/dev/null 2>&1
  local exit_code=$?
  if [ $exit_code -ne 0 ]; then
    echo "Error: migration for schema $schema failed."
    return $exit_code
  fi
}

all_validations_successful=true

# Validate each migration for each schema.
for schema in "${StringArray[@]}"; do
  validate_flyway "$schema" || all_validations_successful=false
done

# Verify all validations.
if [ "$all_validations_successful" = true ]; then
  echo "All schema validations successful."
  exit 0
else
  echo "At least one schema validation failed."
  exit 1
fi
