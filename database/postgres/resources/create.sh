#!/bin/bash

#
# (c) Copyright 2024 Swiss Post Ltd.
#
set -e
set -u

function create_user_and_database() {
	local database=$1
	echo "  Creating user and database '$database'"
	psql -v db_schema="$database" --set=ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
	    CREATE USER :db_schema WITH PASSWORD :'db_schema';
	    CREATE SCHEMA :db_schema AUTHORIZATION :db_schema;
EOSQL
}


POSTGRES_MULTIPLE_DATABASES=control_component_1,control_component_2,control_component_3,control_component_4,voting_server

if [ -n "$POSTGRES_MULTIPLE_DATABASES" ]; then
	echo "Multiple database creation requested: $POSTGRES_MULTIPLE_DATABASES"
	for db in $(echo $POSTGRES_MULTIPLE_DATABASES | tr ',' ' '); do
		create_user_and_database "$db"
	done
	echo "Multiple databases created"
fi

pg_isready
pg_ctl -D "$PGDATA" -m fast -w stop
pg_ctl -D "$PGDATA" \
            -o "-c listen_addresses='localhost'" \
            -w start

echo "Creating schemas"
function run_flyway {
  local schema=$1
  local location=$2

  /flyway/flyway-commandline/flyway baseline -url="jdbc:postgresql://localhost:5432/postgres" -user="${schema,,}" -password="${schema,,}" -schemas="$schema"
  /flyway/flyway-commandline/flyway -cleanDisabled="false" clean -url="jdbc:postgresql://localhost:5432/postgres" -user="${schema,,}" -password="${schema,,}" -schemas="$schema"
  /flyway/flyway-commandline/flyway migrate -url="jdbc:postgresql://localhost:5432/postgres" -user="${schema,,}" -password="${schema,,}" -schemas="$schema" -locations="$location"
}

declare -a StringArray=("voting_server" "control_component_1" "control_component_2" "control_component_3" "control_component_4")

for schema in "${StringArray[@]}"; do
    if [[ "$schema" == control_component* ]]; then
        location="filesystem:/flyway/flyway-commandline/sql/control_components"
    else
        location="filesystem:/flyway/flyway-commandline/sql/$schema"
    fi
    run_flyway "$schema" "$location"
done