/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.configuration;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Base64;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableList;
import ch.post.it.evoting.cryptoprimitives.hashing.HashableString;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.evotinglibraries.domain.signature.SignedPayload;
import ch.post.it.evoting.evotinglibraries.domain.validations.Validations;

@JsonPropertyOrder({ "electionEventId", "electoralBoardHashes", "signature" })
public class ElectoralBoardHashesPayload implements SignedPayload {

	private final String electionEventId;

	private final List<String> electoralBoardHashes;

	private CryptoPrimitivesSignature signature;

	@JsonCreator
	public ElectoralBoardHashesPayload(
			@JsonProperty("electionEventId")
			final String electionEventId,
			@JsonProperty("electoralBoardHashes")
			final List<String> electoralBoardHashes,
			@JsonProperty("signature")
			final CryptoPrimitivesSignature signature
	) {
		this.electionEventId = validateUUID(electionEventId);
		this.electoralBoardHashes = List.copyOf(checkNotNull(electoralBoardHashes));
		checkArgument(this.electoralBoardHashes.size() >= 2);
		this.electoralBoardHashes.forEach(Validations::validateBase64Encoded);
		this.signature = checkNotNull(signature);
	}

	public ElectoralBoardHashesPayload(final String electionEventId, final List<byte[]> electoralBoardHashes) {
		this.electionEventId = validateUUID(electionEventId);
		final List<byte[]> electoralBoardHashesCopy = List.copyOf(checkNotNull(electoralBoardHashes));
		checkArgument(electoralBoardHashesCopy.size() >= 2);
		this.electoralBoardHashes = electoralBoardHashesCopy.stream().parallel()
				.map(byte[]::clone)
				.map(hash -> Base64.getEncoder().encodeToString(hash))
				.toList();
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public List<byte[]> getElectoralBoardHashes() {
		return List.copyOf(electoralBoardHashes.stream()
				.map(hash -> Base64.getDecoder().decode(hash))
				.toList());
	}

	public CryptoPrimitivesSignature getSignature() {
		return signature;
	}

	public void setSignature(final CryptoPrimitivesSignature signature) {
		this.signature = checkNotNull(signature);
	}

	@Override
	public List<Hashable> toHashableForm() {
		return List.of(
				HashableString.from(electionEventId),
				HashableList.from(electoralBoardHashes.stream().map(HashableString::from).toList()));
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final ElectoralBoardHashesPayload that = (ElectoralBoardHashesPayload) o;
		return electionEventId.equals(that.electionEventId) &&
				electoralBoardHashes.equals(that.electoralBoardHashes) &&
				Objects.equals(signature, that.signature);
	}

	@Override
	public int hashCode() {
		return Objects.hash(electionEventId, electoralBoardHashes, signature);
	}
}
