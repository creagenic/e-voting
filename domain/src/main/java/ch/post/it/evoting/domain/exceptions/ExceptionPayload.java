/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.exceptions;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public record ExceptionPayload(String correlationId, int nodeId, Throwable throwable) {

	public ExceptionPayload {
		checkNotNull(correlationId);
		checkArgument(NODE_IDS.contains(nodeId));
		checkNotNull(throwable);
	}
}
