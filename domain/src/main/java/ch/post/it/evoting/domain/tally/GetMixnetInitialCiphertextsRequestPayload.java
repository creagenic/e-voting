/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.tally;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;

public record GetMixnetInitialCiphertextsRequestPayload(String electionEventId, String ballotBoxId) {

	public GetMixnetInitialCiphertextsRequestPayload {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);
	}
}
