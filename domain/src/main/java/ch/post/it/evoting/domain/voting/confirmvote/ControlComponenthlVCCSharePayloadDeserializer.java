/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.voting.confirmvote;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.domain.mapper.EncryptionGroupUtils;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;

public class ControlComponenthlVCCSharePayloadDeserializer extends JsonDeserializer<ControlComponenthlVCCPayload> {

	@Override
	public ControlComponenthlVCCPayload deserialize(final JsonParser parser, final DeserializationContext context)
			throws IOException {
		final ObjectMapper mapper = DomainObjectMapper.getNewInstance();

		final JsonNode node = mapper.readTree(parser);
		final JsonNode encryptionGroupNode = node.get("encryptionGroup");
		final GqGroup encryptionGroup = EncryptionGroupUtils.getEncryptionGroup(mapper, encryptionGroupNode);

		final String hashLongVoteCastCodeShare = mapper.readValue(node.get("hashLongVoteCastCodeShare").toString(), String.class);

		final ConfirmationKey confirmationKey = mapper.reader().withAttribute("group", encryptionGroup)
				.readValue(node.get("confirmationKey"), ConfirmationKey.class);

		final int unsuccessfulConfirmationAttemptCount = mapper.readValue(node.get("unsuccessfulConfirmationAttemptCount").toString(), Integer.class);

		final int nodeId = mapper.readValue(node.get("nodeId").toString(), Integer.class);

		final CryptoPrimitivesSignature signature = mapper.readValue(node.get("signature").toString(), CryptoPrimitivesSignature.class);

		return new ControlComponenthlVCCPayload(encryptionGroup, nodeId, hashLongVoteCastCodeShare, confirmationKey,
				unsuccessfulConfirmationAttemptCount, signature);
	}
}
