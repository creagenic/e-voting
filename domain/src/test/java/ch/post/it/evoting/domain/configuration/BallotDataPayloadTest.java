/*
 * (c) Copyright 2023 Swiss Post Ltd
 */
package ch.post.it.evoting.domain.configuration;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base64Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.election.Ballot;
import ch.post.it.evoting.evotinglibraries.domain.election.Contest;
import ch.post.it.evoting.evotinglibraries.domain.election.Identifier;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

class BallotDataPayloadTest {

	private static final Random RANDOM = RandomFactory.createRandom();

	private String electionEventId;
	private String ballotId;
	private Ballot ballot;
	private String ballotTextJson;

	@BeforeEach
	void setup() {
		final Alphabet base16Alphabet = Base16Alphabet.getInstance();
		final Alphabet base64Alphabet = Base64Alphabet.getInstance();

		electionEventId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		ballotId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String defaultTitle = String.join("_", "ballot", RANDOM.genRandomString(5, base64Alphabet));
		final String defaultDescription = String.join("_", "ballot", RANDOM.genRandomString(5, base64Alphabet));
		final String alias = RANDOM.genRandomString(5, base16Alphabet);
		final String status = RANDOM.genRandomString(5, base16Alphabet);
		final Identifier electionEvent = new Identifier(RANDOM.genRandomString(ID_LENGTH, base16Alphabet));
		final Contest contest = mock(Contest.class);
		doReturn(RANDOM.genRandomString(ID_LENGTH, base16Alphabet)).when(contest).id();
		doReturn(RANDOM.genRandomString(5, base64Alphabet)).when(contest).alias();
		final List<Contest> contests = List.of(contest);
		ballot = new Ballot(ballotId, defaultTitle, defaultDescription, alias, status, electionEvent, contests);
		ballotTextJson = RANDOM.genRandomString(100, base64Alphabet);
	}

	@Test
	void constructWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class, () -> new BallotDataPayload(null, ballotId, ballot, ballotTextJson));
		assertThrows(NullPointerException.class, () -> new BallotDataPayload(electionEventId, null, ballot, ballotTextJson));
		assertThrows(NullPointerException.class, () -> new BallotDataPayload(electionEventId, ballotId, null, ballotTextJson));
		assertThrows(NullPointerException.class, () -> new BallotDataPayload(electionEventId, ballotId, ballot, null));
	}

	@Test
	void constructWithNonUuidsThrows() {
		assertThrows(FailedValidationException.class, () -> new BallotDataPayload("nonUUID", ballotId, ballot, ballotTextJson));
		assertThrows(FailedValidationException.class, () -> new BallotDataPayload(electionEventId, "nonUUID", ballot, ballotTextJson));
	}

	@Test
	void constructWithValidArgumentsCreatesPayload() {
		final BallotDataPayload ballotDataPayload = assertDoesNotThrow(
				() -> new BallotDataPayload(electionEventId, ballotId, ballot, ballotTextJson));
		assertEquals(electionEventId, ballotDataPayload.electionEventId());
		assertEquals(ballotId, ballotDataPayload.ballotId());
		assertEquals(ballot, ballotDataPayload.ballot());
		assertEquals(ballotTextJson, ballotDataPayload.ballotTextsJson());
	}
}
