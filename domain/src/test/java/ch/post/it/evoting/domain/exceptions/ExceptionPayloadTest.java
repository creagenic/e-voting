/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.exceptions;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.security.SecureRandom;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;

class ExceptionPayloadTest {

	private static final Random RANDOM = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final SecureRandom SECURE_RANDOM = new SecureRandom();

	private String correlationId;
	private int nodeId;
	private Throwable throwable;

	@BeforeEach
	void setup() {
		correlationId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		nodeId = SECURE_RANDOM.nextInt(4) + 1;
		throwable = new Throwable();
	}

	@Test
	void constructWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class, () -> new ExceptionPayload(null, nodeId, throwable));
		assertThrows(NullPointerException.class, () -> new ExceptionPayload(correlationId, nodeId, null));
	}

	@Test
	void constructWithNodeIdNotInRangeThrows() {
		assertThrows(IllegalArgumentException.class, () -> new ExceptionPayload(correlationId, 0, throwable));
		assertThrows(IllegalArgumentException.class, () -> new ExceptionPayload(correlationId, 5, throwable));
	}

	@Test
	void constructWithValidInputCreatesPayload() {
		assertDoesNotThrow(() -> new ExceptionPayload(correlationId, nodeId, throwable));
	}
}
