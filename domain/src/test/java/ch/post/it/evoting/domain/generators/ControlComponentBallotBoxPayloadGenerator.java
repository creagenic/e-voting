/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.generators;

import static ch.post.it.evoting.cryptoprimitives.hashing.HashFactory.createHash;
import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;

import java.util.List;
import java.util.stream.IntStream;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.PlaintextEqualityProof;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.common.EncryptedVerifiableVote;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.evotinglibraries.domain.tally.ControlComponentBallotBoxPayload;

public class ControlComponentBallotBoxPayloadGenerator {

	private static final Hash hash = createHash();
	private static final Random random = RandomFactory.createRandom();
	private static final Base16Alphabet base16Alphabet = Base16Alphabet.getInstance();

	final GqGroup encryptionGroup;
	final ZqGroup zqGroup;
	final ElGamalGenerator elGamalGenerator;
	final ZqGroupGenerator zqGroupGenerator;

	public ControlComponentBallotBoxPayloadGenerator(final GqGroup encryptionGroup) {
		this.encryptionGroup = encryptionGroup;
		this.elGamalGenerator = new ElGamalGenerator(encryptionGroup);
		this.zqGroup = ZqGroup.sameOrderAs(encryptionGroup);
		this.zqGroupGenerator = new ZqGroupGenerator(zqGroup);
	}

	public ControlComponentBallotBoxPayloadGenerator() {
		this(GroupTestData.getLargeGqGroup());
	}

	public List<ControlComponentBallotBoxPayload> generate() {
		final String electionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);
		final String ballotBoxId = random.genRandomString(ID_LENGTH, base16Alphabet);
		final int psi = 5;
		final int delta = 1;

		return generate(electionEventId, ballotBoxId, psi, delta);
	}

	public List<ControlComponentBallotBoxPayload> generate(final String electionEventId, final String ballotBoxId, final int numberOfSelections,
			final int numberOfWriteInsPlusOne) {
		final String verificationCardSetId = random.genRandomString(ID_LENGTH, base16Alphabet);

		final int numberOfConfirmedVotes = 10;
		final List<EncryptedVerifiableVote> encryptedVerifiableVote = IntStream.range(0, numberOfConfirmedVotes)
				.mapToObj(i -> {
					final ContextIds contextIds = new ContextIds(
							electionEventId,
							verificationCardSetId,
							random.genRandomString(ID_LENGTH, base16Alphabet)
					);
					return generateEncryptedVerifiableVote(contextIds, numberOfSelections, numberOfWriteInsPlusOne);
				})
				.toList();

		return NODE_IDS.stream()
				.map(nodeId -> generateControlComponentBallotBoxPayload(electionEventId, ballotBoxId, nodeId, encryptedVerifiableVote))
				.toList();
	}

	private ControlComponentBallotBoxPayload generateControlComponentBallotBoxPayload(final String electionEventId,
			final String ballotBoxId, final int nodeId, final List<EncryptedVerifiableVote> encryptedVerifiableVote) {

		final ControlComponentBallotBoxPayload controlComponentBallotBoxPayload = new ControlComponentBallotBoxPayload(encryptionGroup,
				electionEventId, ballotBoxId, nodeId, encryptedVerifiableVote);

		final byte[] payloadHash = hash.recursiveHash(controlComponentBallotBoxPayload);
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature(payloadHash);
		controlComponentBallotBoxPayload.setSignature(signature);

		return controlComponentBallotBoxPayload;
	}

	private EncryptedVerifiableVote generateEncryptedVerifiableVote(final ContextIds contextIds, final int psi, final int delta) {
		final ElGamalMultiRecipientCiphertext encryptedVote = elGamalGenerator.genRandomCiphertext(delta);

		final ElGamalMultiRecipientCiphertext encryptedPartialChoiceReturnCodes = elGamalGenerator.genRandomCiphertext(psi);

		final ElGamalMultiRecipientCiphertext exponentiatedEncryptedVote = elGamalGenerator.genRandomCiphertext(1);

		final ExponentiationProof exponentiationProof = new ExponentiationProof(
				zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementMember()
		);

		final PlaintextEqualityProof plaintextEqualityProof = new PlaintextEqualityProof(
				zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementVector(2)
		);

		return new EncryptedVerifiableVote(contextIds, encryptedVote, exponentiatedEncryptedVote, encryptedPartialChoiceReturnCodes,
				exponentiationProof, plaintextEqualityProof);
	}
}
