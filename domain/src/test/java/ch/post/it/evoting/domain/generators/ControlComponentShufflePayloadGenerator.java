/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

package ch.post.it.evoting.domain.generators;

import static ch.post.it.evoting.cryptoprimitives.hashing.HashFactory.createHash;
import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;

import java.util.List;

import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.mixnet.VerifiableShuffle;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.VerifiableShuffleGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.VerifiableDecryptionGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.VerifiableDecryptions;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;

public class ControlComponentShufflePayloadGenerator {

	private static final Hash hash = createHash();
	private static final Random random = RandomFactory.createRandom();
	private static final Base16Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private final GqGroup encryptionGroup;

	public ControlComponentShufflePayloadGenerator(final GqGroup encryptionGroup) {
		this.encryptionGroup = encryptionGroup;
	}

	public ControlComponentShufflePayloadGenerator() {
		this(GroupTestData.getLargeGqGroup());
	}

	public List<ControlComponentShufflePayload> generate() {
		final String electionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);
		final String ballotBoxId = random.genRandomString(ID_LENGTH, base16Alphabet);
		final int numberOfMixedVotes = 10;
		final int numberOfWriteInsPlusOne = 1;

		return generate(electionEventId, ballotBoxId, numberOfMixedVotes, numberOfWriteInsPlusOne);
	}

	public List<ControlComponentShufflePayload> generate(final String electionEventId, final String ballotBoxId, final int numberOfMixedVotes,
			final int numberOfWriteInsPlusOne) {
		return NODE_IDS.stream()
				.map(nodeId -> generate(electionEventId, ballotBoxId, nodeId, numberOfMixedVotes, numberOfWriteInsPlusOne))
				.toList();
	}

	public ControlComponentShufflePayload generate(final int nodeId, final int numberOfMixedVotes, final int numberOfWriteInsPlusOne) {
		final String electionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);
		final String ballotBoxId = random.genRandomString(ID_LENGTH, base16Alphabet);

		return generate(electionEventId, ballotBoxId, nodeId, numberOfMixedVotes, numberOfWriteInsPlusOne);
	}

	public ControlComponentShufflePayload generate(final String electionEventId, final String ballotBoxId, final int nodeId,
			final int numberOfMixedVotes, final int numberOfWriteInsPlusOne) {
		final VerifiableShuffle verifiableShuffle = new VerifiableShuffleGenerator(encryptionGroup).genVerifiableShuffle(numberOfMixedVotes,
				numberOfWriteInsPlusOne);

		final VerifiableDecryptions verifiableDecryptions = new VerifiableDecryptionGenerator(encryptionGroup).genVerifiableDecryption(
				numberOfMixedVotes, numberOfWriteInsPlusOne);

		final ControlComponentShufflePayload controlComponentShufflePayload = new ControlComponentShufflePayload(encryptionGroup, electionEventId,
				ballotBoxId, nodeId, verifiableShuffle, verifiableDecryptions);

		final byte[] payloadHash = hash.recursiveHash(controlComponentShufflePayload);
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature(payloadHash);
		controlComponentShufflePayload.setSignature(signature);

		return controlComponentShufflePayload;
	}
}
