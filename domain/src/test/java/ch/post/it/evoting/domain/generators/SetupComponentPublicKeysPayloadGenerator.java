/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.generators;

import static ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory.createElGamal;
import static ch.post.it.evoting.cryptoprimitives.hashing.HashFactory.createHash;
import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import com.google.common.collect.Streams;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamal;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;
import ch.post.it.evoting.evotinglibraries.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.election.SetupComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.SetupComponentPublicKeysPayload;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;

public class SetupComponentPublicKeysPayloadGenerator {

	private static final Hash hash = createHash();
	private static final ElGamal elGamal = createElGamal();
	private static final Random random = RandomFactory.createRandom();
	private static final Base16Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private final GqGroup encryptionGroup;
	private final ElGamalGenerator elGamalGenerator;
	private final ZqGroupGenerator zqGroupGenerator;

	public SetupComponentPublicKeysPayloadGenerator(final GqGroup encryptionGroup) {
		this.encryptionGroup = encryptionGroup;
		this.elGamalGenerator = new ElGamalGenerator(encryptionGroup);
		final ZqGroup zqGroup = ZqGroup.sameOrderAs(encryptionGroup);
		this.zqGroupGenerator = new ZqGroupGenerator(zqGroup);
	}

	public SetupComponentPublicKeysPayloadGenerator() {
		this(GroupTestData.getLargeGqGroup());
	}

	public SetupComponentPublicKeysPayload generate() {
		final int maximumNumberOfSelections = 10;
		final int maximumNumberOfWriteInsPlusOne = 1;

		return generate(maximumNumberOfSelections, maximumNumberOfWriteInsPlusOne);
	}

	public SetupComponentPublicKeysPayload generate(final int maximumNumberOfSelections, final int maximumNumberOfWriteInsPlusOne) {

		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccrChoiceReturnCodesEncryptionPublicKeys = Stream.generate(
						() -> elGamalGenerator.genRandomPublicKey(maximumNumberOfSelections))
				.limit(NODE_IDS.size())
				.collect(GroupVector.toGroupVector());

		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys = Stream.generate(
						() -> elGamalGenerator.genRandomPublicKey(maximumNumberOfWriteInsPlusOne))
				.limit(NODE_IDS.size())
				.collect(GroupVector.toGroupVector());

		return generate(ccrChoiceReturnCodesEncryptionPublicKeys, ccmElectionPublicKeys);
	}

	public SetupComponentPublicKeysPayload generate(
			final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccrChoiceReturnCodesEncryptionPublicKeys,
			final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> ccmElectionPublicKeys) {

		final List<ControlComponentPublicKeys> combinedControlComponentPublicKeys = NODE_IDS.stream()
				.map(nodeId -> generateCombinedControlComponentPublicKeys(nodeId, ccrChoiceReturnCodesEncryptionPublicKeys.get(nodeId - 1),
						ccmElectionPublicKeys.get(nodeId - 1)))
				.toList();

		final int maximumNumberOfWriteInsPlusOne = ccmElectionPublicKeys.getElementSize();
		final ElGamalMultiRecipientPublicKey electoralBoardPublicKey = elGamalGenerator.genRandomPublicKey(maximumNumberOfWriteInsPlusOne);
		final GroupVector<SchnorrProof, ZqGroup> electoralBoardSchnorrProofs = generateSchnorrProofs(maximumNumberOfWriteInsPlusOne);

		final GroupVector<ElGamalMultiRecipientPublicKey, GqGroup> publicKeys = Streams.concat(
						ccmElectionPublicKeys.stream(),
						Stream.of(electoralBoardPublicKey))
				.collect(GroupVector.toGroupVector());
		final ElGamalMultiRecipientPublicKey electionPublicKey = elGamal.combinePublicKeys(publicKeys);

		final ElGamalMultiRecipientPublicKey choiceReturnCodesPublicKey = elGamal.combinePublicKeys(ccrChoiceReturnCodesEncryptionPublicKeys);

		final SetupComponentPublicKeys setupComponentPublicKeys = new SetupComponentPublicKeys(combinedControlComponentPublicKeys,
				electoralBoardPublicKey, electoralBoardSchnorrProofs, electionPublicKey, choiceReturnCodesPublicKey);

		final SetupComponentPublicKeysPayload setupComponentPublicKeysPayload = new SetupComponentPublicKeysPayload(encryptionGroup,
				random.genRandomString(ID_LENGTH, base16Alphabet), setupComponentPublicKeys);

		final byte[] payloadHash = hash.recursiveHash(setupComponentPublicKeysPayload);
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature(payloadHash);
		setupComponentPublicKeysPayload.setSignature(signature);

		return setupComponentPublicKeysPayload;
	}

	private ControlComponentPublicKeys generateCombinedControlComponentPublicKeys(final int nodeId,
			final ElGamalMultiRecipientPublicKey ccrChoiceReturnCodesEncryptionPublicKey, final ElGamalMultiRecipientPublicKey ccmElectionPublicKey) {
		final int psiMax = ccrChoiceReturnCodesEncryptionPublicKey.size();
		final GroupVector<SchnorrProof, ZqGroup> ccrChoiceReturnCodesEncryptionSchnorrProofs = generateSchnorrProofs(psiMax);

		final int deltaMax = ccmElectionPublicKey.size();
		final GroupVector<SchnorrProof, ZqGroup> ccmElectionSchnorrProofs = generateSchnorrProofs(deltaMax);

		return new ControlComponentPublicKeys(nodeId, ccrChoiceReturnCodesEncryptionPublicKey, ccrChoiceReturnCodesEncryptionSchnorrProofs,
				ccmElectionPublicKey, ccmElectionSchnorrProofs);
	}

	private GroupVector<SchnorrProof, ZqGroup> generateSchnorrProofs(final int size) {
		final ZqElement e = zqGroupGenerator.genRandomZqElementMember();
		final ZqElement z = zqGroupGenerator.genRandomZqElementMember();
		final SchnorrProof schnorrProof = new SchnorrProof(e, z);
		return Collections.nCopies(size, schnorrProof).stream()
				.collect(GroupVector.toGroupVector());
	}
}
