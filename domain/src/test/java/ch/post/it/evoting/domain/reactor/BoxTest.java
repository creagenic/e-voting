/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.reactor;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class BoxTest {

	@Test
	void constructWithNullArgumentThrows() {
		assertThrows(NullPointerException.class, () -> new Box<>(null));
	}

	@Test
	void constructWithValidArgumentDoesNotThrow() {
		final String argument = "argument";
		final Box<String> box = assertDoesNotThrow(() -> new Box<>(argument));
		assertEquals(argument, box.boxed());
	}
}
