/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.domain.voting.sendvote;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.GqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.domain.MapperSetUp;
import ch.post.it.evoting.evotinglibraries.domain.SerializationUtils;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;

@DisplayName("A ControlComponentPartialDecryptPayload")
class CombinedControlComponentPartialDecryptPayloadTest extends MapperSetUp {
	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final String electionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final String verificationCardSetId = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final String verificationCardId = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final Hash hash = HashFactory.createHash();

	private static ObjectNode rootNode;
	private static CombinedControlComponentPartialDecryptPayload combinedControlComponentPartialDecryptPayload;
	private static GqGroup gqGroup;
	private static ContextIds contextIds;
	private static GroupVector<GqElement, GqGroup> exponentiatedGammas;
	private static GroupVector<ExponentiationProof, ZqGroup> exponentiationProofs;

	@BeforeAll
	static void setUpAll() {
		gqGroup = GroupTestData.getGqGroup();
		final GqGroupGenerator gqGroupGenerator = new GqGroupGenerator(gqGroup);
		final ZqGroupGenerator zqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(gqGroup));

		// Create payload.
		contextIds = new ContextIds(electionEventId, verificationCardSetId, verificationCardId);
		exponentiatedGammas = gqGroupGenerator.genRandomGqElementVector(2);
		exponentiationProofs = Stream.generate(
						() -> new ExponentiationProof(zqGroupGenerator.genRandomZqElementMember(), zqGroupGenerator.genRandomZqElementMember()))
				.limit(2)
				.collect(GroupVector.toGroupVector());

		final List<ControlComponentPartialDecryptPayload> payloads = IntStream.range(0, 4)
				.mapToObj(i -> buildPayload(contextIds, i + 1, exponentiatedGammas, exponentiationProofs, gqGroup))
				.collect(Collectors.toList());

		// Create combined payloads.
		combinedControlComponentPartialDecryptPayload = new CombinedControlComponentPartialDecryptPayload(payloads);

		// Create expected Json.

		// Combined node.
		rootNode = mapper.createObjectNode();

		// Payload node.
		final ArrayNode payloadsNode = mapper.createArrayNode();
		for (int i = 0; i < 4; i++) {
			final ObjectNode payloadNode = mapper.createObjectNode();
			final JsonNode encryptionGroupNode = SerializationUtils.createEncryptionGroupNode(gqGroup);
			payloadNode.set("encryptionGroup", encryptionGroupNode);

			final ObjectNode contextIdsNode = mapper.createObjectNode();
			contextIdsNode.put("electionEventId", electionEventId);
			contextIdsNode.put("verificationCardSetId", verificationCardSetId);
			contextIdsNode.put("verificationCardId", verificationCardId);

			final ObjectNode partiallyDecryptedEncryptedPCCNode = mapper.createObjectNode();
			final ArrayNode exponentiationProofsNode = SerializationUtils.createExponentiationProofsNode(exponentiationProofs);
			final ArrayNode exponentiatedGammasNode = SerializationUtils.createGqGroupVectorNode(exponentiatedGammas);
			partiallyDecryptedEncryptedPCCNode.set("contextIds", contextIdsNode);
			partiallyDecryptedEncryptedPCCNode.put("nodeId", i + 1);
			partiallyDecryptedEncryptedPCCNode.set("exponentiatedGammas", exponentiatedGammasNode);
			partiallyDecryptedEncryptedPCCNode.set("exponentiationProofs", exponentiationProofsNode);

			payloadNode.set("partiallyDecryptedEncryptedPCC", partiallyDecryptedEncryptedPCCNode);

			final JsonNode payloadSignatureNode = SerializationUtils.createSignatureNode(payloads.get(i).getSignature());
			payloadNode.set("signature", payloadSignatureNode);
			payloadsNode.add(payloadNode);
		}
		rootNode.set("controlComponentPartialDecryptPayloads", payloadsNode);
	}

	@Test
	@DisplayName("serialized gives expected json")
	void serializePayload() throws JsonProcessingException {
		final String serializedPayload = mapper.writeValueAsString(combinedControlComponentPartialDecryptPayload);

		assertEquals(rootNode.toString(), serializedPayload);
	}

	@Test
	@DisplayName("deserialized gives expected payload")
	void deserializePayload() throws JsonProcessingException {
		final CombinedControlComponentPartialDecryptPayload deserializedPayload = mapper.readValue(rootNode.toString(),
				CombinedControlComponentPartialDecryptPayload.class);

		assertEquals(combinedControlComponentPartialDecryptPayload, deserializedPayload);
	}

	@Test
	@DisplayName("serialized then deserialized gives original payload")
	void cycle() throws JsonProcessingException {
		final CombinedControlComponentPartialDecryptPayload deserializedPayload = mapper.readValue(
				mapper.writeValueAsString(combinedControlComponentPartialDecryptPayload), CombinedControlComponentPartialDecryptPayload.class);

		assertEquals(combinedControlComponentPartialDecryptPayload, deserializedPayload);
	}

	@Test
	@DisplayName("not enough contributions throws IllegalArgumentException")
	void notEnoughContributions() {
		final List<ControlComponentPartialDecryptPayload> emptyList = Collections.emptyList();

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new CombinedControlComponentPartialDecryptPayload(emptyList));
		assertEquals("There must be the expected number of control component partial decrypt payloads.",
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("inconsistent groups throws IllegalArgumentException")
	void inconsistentGroups() {
		final List<ControlComponentPartialDecryptPayload> payloads = IntStream.range(0, 3)
				.mapToObj(i -> buildPayload(contextIds, i + 1, exponentiatedGammas, exponentiationProofs, gqGroup))
				.collect(Collectors.toList());

		final GqGroup otherGqGroup = GroupTestData.getDifferentGqGroup(gqGroup);
		final GqGroupGenerator otherGqGroupGenerator = new GqGroupGenerator(otherGqGroup);
		final ZqGroupGenerator otherZqGroupGenerator = new ZqGroupGenerator(ZqGroup.sameOrderAs(otherGqGroup));

		payloads.add(buildPayload(contextIds, 4, otherGqGroupGenerator.genRandomGqElementVector(2),
				Stream.generate(() -> new ExponentiationProof(otherZqGroupGenerator.genRandomZqElementMember(),
								otherZqGroupGenerator.genRandomZqElementMember()))
						.limit(2)
						.collect(GroupVector.toGroupVector()), otherGqGroup));

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new CombinedControlComponentPartialDecryptPayload(payloads));
		assertEquals("All control component partial decrypt payloads must have the same group.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("inconsistent contextIds throws IllegalArgumentException")
	void inconsistentContextIds() {
		final List<ControlComponentPartialDecryptPayload> payloads = IntStream.range(0, 3)
				.mapToObj(i -> buildPayload(contextIds, i + 1, exponentiatedGammas, exponentiationProofs, gqGroup))
				.collect(Collectors.toList());
		final String otherVerificationCardId = random.genRandomString(ID_LENGTH, base16Alphabet);
		final ContextIds otherContextIds = new ContextIds(electionEventId, verificationCardSetId, otherVerificationCardId);
		payloads.add(buildPayload(otherContextIds, 4, exponentiatedGammas, exponentiationProofs, gqGroup));

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new CombinedControlComponentPartialDecryptPayload(payloads));
		assertEquals("All control component partial decrypt payloads must have the same contextIds.",
				Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("inconsistent nodeIds throws IllegalArgumentException")
	void inconsistentNodeIds() {
		final List<ControlComponentPartialDecryptPayload> payloads = IntStream.range(0, 3)
				.mapToObj(i -> buildPayload(contextIds, i + 1, exponentiatedGammas, exponentiationProofs, gqGroup))
				.collect(Collectors.toList());
		payloads.add(buildPayload(contextIds, 1, exponentiatedGammas, exponentiationProofs, gqGroup));

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new CombinedControlComponentPartialDecryptPayload(payloads));
		assertEquals("The control component partial decrypt payloads must contain the expected node ids.",
				Throwables.getRootCause(exception).getMessage());
	}

	private static ControlComponentPartialDecryptPayload buildPayload(final ContextIds contextIds, final int nodeId,
			final GroupVector<GqElement, GqGroup> exponentiatedGammas, final GroupVector<ExponentiationProof, ZqGroup> exponentiationProofs,
			final GqGroup gqGroup) {

		final PartiallyDecryptedEncryptedPCC partiallyDecryptedEncryptedPCC = new PartiallyDecryptedEncryptedPCC(contextIds, nodeId,
				exponentiatedGammas, exponentiationProofs);

		// Create and sign payload.
		final ControlComponentPartialDecryptPayload payload = new ControlComponentPartialDecryptPayload(gqGroup, partiallyDecryptedEncryptedPCC
		);
		final byte[] payloadBytes = hash.recursiveHash(payload);
		final CryptoPrimitivesSignature payloadSignature = new CryptoPrimitivesSignature(payloadBytes);
		payload.setSignature(payloadSignature);

		return payload;
	}

}
