/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.securedatamanager.shared.process.PathResolver;

@Service
@ConditionalOnProperty(prefix = "role", name = { "isSetup", "isTally" }, havingValue = "false")
public class OnlinePathResolver extends PathResolver {

	private final Path output;

	public OnlinePathResolver(
			@Value("${sdm.workspace}")
			final Path workspace,
			@Value("${sdm.output.folder.path}")
			final Path output) throws IOException {
		super(workspace);

		checkNotNull(output, "The output path is required.");

		this.output = output.toRealPath(LinkOption.NOFOLLOW_LINKS);

		checkArgument(Files.isDirectory(this.output), "The given output path is not a directory. [path: %s]", this.output);
	}

	@Override
	public Path resolveOutputPath() {
		return output;
	}

	@Override
	public Path resolveVerifierOutputPath() {
		throw new UnsupportedOperationException("The verifier output path is not available in the online SDM.");
	}

	@Override
	public Path resolvePrintingOutputPath() {
		throw new UnsupportedOperationException("The printing output path is not available in the online SDM.");
	}

	@Override
	public Path resolveExternalConfigurationPath() {
		throw new UnsupportedOperationException("The external configuration path is not available in the online SDM.");
	}

	@Override
	public Path resolveVotingCardSetPath(final String electionEventId, final String votingCardSetId) {
		throw new UnsupportedOperationException(
				"The voting card set path is not available in the online SDM. [electionEventId: %s, votingCardSetId: %s]".formatted(electionEventId,
						votingCardSetId));
	}

	@Override
	public Path resolveVotingCardSetsPath(final String electionEventId) {
		throw new UnsupportedOperationException(
				"The voting card set paths are not available in the online SDM. [electionEventId: %s]".formatted(electionEventId));
	}
}
