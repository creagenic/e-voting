/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.compute;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.UncheckedIOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.RetryContext;
import org.springframework.retry.annotation.Retryable;
import org.springframework.retry.support.RetrySynchronizationManager;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.evotinglibraries.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.securedatamanager.shared.WebClientFactory;

import reactor.core.publisher.Flux;

@Service
public class EncryptedLongReturnCodeSharesComputeService {
	private static final Logger LOGGER = LoggerFactory.getLogger(EncryptedLongReturnCodeSharesComputeService.class);
	private final WebClientFactory webClientFactory;

	public EncryptedLongReturnCodeSharesComputeService(final WebClientFactory webClientFactory) {
		this.webClientFactory = webClientFactory;
	}

	/**
	 * Send a Setup Component verification data list to the control components to generate the encrypted long Return Code Shares (encrypted long
	 * Choice Return Code shares and encrypted long Vote Cast Return Code shares).
	 */
	@Retryable(retryFor = { UncheckedIOException.class,
			IllegalStateException.class }, maxAttemptsExpression = "${compute-gen-enc-long-code-shares.retryable-calls.max-attempts}")
	public void computeGenEncLongCodeShares(final String electionEventId, final String verificationCardSetId,
			final List<SetupComponentVerificationDataPayload> setupComponentVerificationDataPayloads) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkNotNull(setupComponentVerificationDataPayloads);
		setupComponentVerificationDataPayloads.forEach(setupComponentVerificationDataPayload ->
		{
			checkNotNull(setupComponentVerificationDataPayload);
			checkArgument(setupComponentVerificationDataPayload.getElectionEventId().equals(electionEventId));
			checkArgument(setupComponentVerificationDataPayload.getVerificationCardSetId().equals(verificationCardSetId));
		});

		final RetryContext retryContext = RetrySynchronizationManager.getContext();
		checkState(retryContext != null, "Unavailable retry context.");
		final int retryNumber = retryContext.getRetryCount();
		if (retryNumber != 0) {
			LOGGER.warn(
					"Retrying of request for compute the encrypted long return code shares. [retryNumber: {}, electionEventId: {}, verificationCardSetId: {}]",
					retryNumber, electionEventId, verificationCardSetId);
		}

		final ResponseEntity<Void> response = webClientFactory.getWebClient(
						String.format("Compute unsuccessful. [electionEventId: %s, verificationCardSetId: %s]",
								electionEventId, verificationCardSetId))
				.put()
				.uri(uriBuilder -> uriBuilder.path(
								"api/v1/configuration/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/computegenenclongcodeshares")
						.build(electionEventId, verificationCardSetId))
				.contentType(MediaType.APPLICATION_NDJSON)
				.accept(MediaType.APPLICATION_JSON)
				.body(Flux.fromIterable(setupComponentVerificationDataPayloads), SetupComponentVerificationDataPayload.class)
				.retrieve()
				.toBodilessEntity()
				.block();

		checkState(checkNotNull(response).getStatusCode().is2xxSuccessful());
	}
}
