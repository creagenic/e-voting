/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.download;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.shared.Constants.DOWNLOAD_UNSUCCESSFUL_MESSAGE;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.retry.RetryContext;
import org.springframework.retry.annotation.Retryable;
import org.springframework.retry.support.RetrySynchronizationManager;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.domain.reactor.Box;
import ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.securedatamanager.shared.WebClientFactory;

import reactor.core.publisher.Flux;

@Service
public class EncryptedLongReturnCodeSharesDownloadService {

	private final RetryableRemoteCall retryableRemoteCall;

	public EncryptedLongReturnCodeSharesDownloadService(final RetryableRemoteCall retryableRemoteCall) {
		this.retryableRemoteCall = retryableRemoteCall;
	}

	/**
	 * Download the control components' encrypted long Choice Return codes and the encrypted long Vote Cast Return code shares.
	 */
	public List<List<ControlComponentCodeSharesPayload>> downloadGenEncLongCodeShares(final String electionEventId,
			final String verificationCardSetId, final int chunkCount) {

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkArgument(chunkCount > 0);

		return retryableRemoteCall.downloadGenEncLongCodeShares(
						electionEventId, verificationCardSetId, chunkCount).stream()
				.map(chunkList -> {
					final List<ControlComponentCodeSharesPayload> result = chunkList.stream()
							.sorted(Comparator.comparingInt(ControlComponentCodeSharesPayload::getNodeId))
							.toList();

					checkState(chunkList.stream().map(ControlComponentCodeSharesPayload::getChunkId).distinct().count() == 1,
							"All node contributions must have the same chunkId");

					checkState(ControlComponentConstants.NODE_IDS.equals(result.stream()
									.parallel()
									.map(controlComponentCodeSharesPayload -> {

										checkState(controlComponentCodeSharesPayload.getElectionEventId().equals(electionEventId),
												"The controlComponentCodeSharesPayload's election event id does not correspond to the election event id of the request");

										checkState(controlComponentCodeSharesPayload.getVerificationCardSetId().equals(verificationCardSetId),
												"The controlComponentCodeSharesPayload's verification card set id does not correspond to the verification card set id of the request");

										return controlComponentCodeSharesPayload.getNodeId();
									})
									.collect(Collectors.toSet())),
							"There must be exactly the expected number of control component code shares payloads with the expected node ids.");
					return result;
				})
				.toList();
	}

	@Service
	public static class RetryableRemoteCall {
		private static final Logger LOGGER = LoggerFactory.getLogger(EncryptedLongReturnCodeSharesDownloadService.RetryableRemoteCall.class);
		private final WebClientFactory webClientFactory;

		public RetryableRemoteCall(final WebClientFactory webClientFactory) {
			this.webClientFactory = webClientFactory;
		}

		@Retryable(retryFor = { UncheckedIOException.class,
				IOException.class }, maxAttemptsExpression = "${download-genenclongcodeshares.retryable-calls.max-attempts}")
		List<List<ControlComponentCodeSharesPayload>> downloadGenEncLongCodeShares(final String electionEventId, final String verificationCardSetId,
				final int chunkCount) {
			validateUUID(electionEventId);
			validateUUID(verificationCardSetId);
			checkArgument(chunkCount > 0);

			final RetryContext retryContext = RetrySynchronizationManager.getContext();
			checkState(retryContext != null, "Unavailable retry context.");
			final int retryNumber = retryContext.getRetryCount();
			if (retryNumber != 0) {
				LOGGER.warn(
						"Retrying of request for downloading the GenEncLongCodeShares. [retryNumber: {}, electionEventId: {}, verificationCardSetId: {}]",
						retryNumber, electionEventId, verificationCardSetId);
			}

			final List<Integer> chunkIds = IntStream.range(0, chunkCount).boxed().toList();

			return webClientFactory.getWebClient(
							String.format("%s [electionEventId: %s, verificationCardSetId: %s]",
									DOWNLOAD_UNSUCCESSFUL_MESSAGE, electionEventId, verificationCardSetId))
					.post()
					.uri(uriBuilder -> uriBuilder.path(
									"api/v1/configuration/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/download")
							.build(electionEventId, verificationCardSetId))
					.contentType(MediaType.APPLICATION_NDJSON)
					.accept(MediaType.APPLICATION_NDJSON)
					.body(Flux.fromIterable(chunkIds), Integer.class)
					.retrieve()
					.bodyToFlux(new ParameterizedTypeReference<Box<List<ControlComponentCodeSharesPayload>>>() {
					})
					.map(Box::boxed)
					.collectList()
					.block();
		}
	}
}
