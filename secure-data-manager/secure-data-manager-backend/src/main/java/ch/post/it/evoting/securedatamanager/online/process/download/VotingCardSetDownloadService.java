/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.download;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.shared.process.Status.VCS_DOWNLOADED;
import static com.google.common.base.Preconditions.checkState;
import static java.nio.file.Files.delete;
import static java.nio.file.Files.newDirectoryStream;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.DirectoryStream;
import java.nio.file.DirectoryStream.Filter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.securedatamanager.shared.Constants;
import ch.post.it.evoting.securedatamanager.shared.process.ConfigurationEntityStatusService;
import ch.post.it.evoting.securedatamanager.shared.process.PathResolver;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentVerificationDataPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;

/**
 * This is an application service that manages voting card sets.
 */
@Service
public class VotingCardSetDownloadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetDownloadService.class);

	private final PathResolver pathResolver;
	private final VotingCardSetRepository votingCardSetRepository;
	private final EncryptedLongReturnCodeSharesDownloadService encryptedLongReturnCodeSharesDownloadService;
	private final ConfigurationEntityStatusService configurationEntityStatusService;
	private final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository;

	public VotingCardSetDownloadService(
			final PathResolver pathResolver,
			final VotingCardSetRepository votingCardSetRepository,
			final EncryptedLongReturnCodeSharesDownloadService encryptedLongReturnCodeSharesDownloadService,
			final ConfigurationEntityStatusService configurationEntityStatusService,
			final SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepository) {
		this.pathResolver = pathResolver;
		this.votingCardSetRepository = votingCardSetRepository;
		this.configurationEntityStatusService = configurationEntityStatusService;
		this.encryptedLongReturnCodeSharesDownloadService = encryptedLongReturnCodeSharesDownloadService;
		this.setupComponentVerificationDataPayloadFileRepository = setupComponentVerificationDataPayloadFileRepository;
	}

	/**
	 * Download the computed values for a votingCardSet.
	 */
	public void download(final String votingCardSetId, final String electionEventId) {
		validateUUID(votingCardSetId);
		validateUUID(electionEventId);

		final String verificationCardSetId = votingCardSetRepository.getVerificationCardSetId(votingCardSetId);

		LOGGER.info("Downloading the computed values. [electionEventId: {}, votingCardSetId: {}, verificationCardSetId: {}]", electionEventId,
				votingCardSetId, verificationCardSetId);

		try {
			deleteNodeContributions(electionEventId, verificationCardSetId);
		} catch (final IOException e) {
			throw new UncheckedIOException(
					String.format("Failed to delete node contribution before voting cards download. [electionEventId: %s, votingCardSetId: %s]",
							electionEventId, votingCardSetId), e);
		}

		final int chunkCount = setupComponentVerificationDataPayloadFileRepository.getCount(electionEventId, verificationCardSetId);

		checkState(chunkCount > 0, "No chunk found for download. [electionEventId: %s, verificationCardSetId: %s]", electionEventId,
				verificationCardSetId);

		encryptedLongReturnCodeSharesDownloadService.downloadGenEncLongCodeShares(electionEventId, verificationCardSetId, chunkCount)
				.stream()
				.parallel()
				.forEach(contributions -> {
					final int chunkId = contributions.getFirst().getChunkId();
					try {
						writeNodeContributions(electionEventId, verificationCardSetId, chunkId, contributions);
					} catch (final IOException e) {
						throw new UncheckedIOException(e);
					}
				});

		configurationEntityStatusService.update(VCS_DOWNLOADED.name(), votingCardSetId, votingCardSetRepository);

	}

	private static boolean isNodeContributions(final Path file) {
		final String name = file.getFileName().toString();
		return name.startsWith(Constants.CONFIG_FILE_CONTROL_COMPONENT_CODE_SHARES_PAYLOAD) && name.endsWith(Constants.JSON);
	}

	private void deleteNodeContributions(final String electionEventId, final String verificationCardSetId) throws IOException {
		final Path folder = pathResolver.resolveVerificationCardSetPath(electionEventId, verificationCardSetId);
		final Filter<Path> filter = VotingCardSetDownloadService::isNodeContributions;
		try (final DirectoryStream<Path> files = newDirectoryStream(folder, filter)) {
			for (final Path file : files) {
				delete(file);
			}
		}
	}

	private void writeNodeContributions(final String electionEventId, final String verificationCardSetId, final int chunkId,
			final List<ControlComponentCodeSharesPayload> contributions)
			throws IOException {

		final String fileName = Constants.CONFIG_FILE_CONTROL_COMPONENT_CODE_SHARES_PAYLOAD + "." + chunkId + Constants.JSON;
		final Path path = pathResolver.resolveVerificationCardSetPath(electionEventId, verificationCardSetId).resolve(fileName);

		final byte[] bytes = DomainObjectMapper.getNewInstance().writeValueAsBytes(contributions);
		Files.write(path, bytes);
	}
}
