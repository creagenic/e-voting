/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.mixdownload;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.shared.Constants.BALLOT_BOX_NOT_CLOSED_MESSAGE;
import static ch.post.it.evoting.securedatamanager.shared.Constants.DOWNLOAD_UNSUCCESSFUL_MESSAGE;
import static ch.post.it.evoting.securedatamanager.shared.Constants.START_ONLINE_MIXING_FAILED_MESSAGE;
import static ch.post.it.evoting.securedatamanager.shared.Constants.VOTER_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.UncheckedIOException;
import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.domain.tally.MixDecryptOnlinePayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.evotinglibraries.domain.tally.ControlComponentBallotBoxPayload;
import ch.post.it.evoting.securedatamanager.shared.WebClientFactory;
import ch.post.it.evoting.securedatamanager.shared.process.BallotBoxService;
import ch.post.it.evoting.securedatamanager.shared.process.ControlComponentBallotBoxPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.shared.process.ControlComponentShufflePayloadFileRepository;

@Service
public class MixDecryptOnlineService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MixDecryptOnlineService.class);
	private final BallotBoxService ballotBoxService;
	private final WebClientFactory webClientFactory;
	private final ControlComponentShufflePayloadFileRepository controlComponentShufflePayloadFileRepository;
	private final ControlComponentBallotBoxPayloadFileRepository controlComponentBallotBoxPayloadFileRepository;
	private final boolean isVoterPortalEnabled;

	public MixDecryptOnlineService(
			final BallotBoxService ballotBoxService,
			final WebClientFactory webClientFactory,
			final ControlComponentShufflePayloadFileRepository controlComponentShufflePayloadFileRepository,
			final ControlComponentBallotBoxPayloadFileRepository controlComponentBallotBoxPayloadFileRepository,
			@Value("${voter.portal.enabled}")
			final boolean isVoterPortalEnabled) {
		this.ballotBoxService = ballotBoxService;
		this.webClientFactory = webClientFactory;
		this.controlComponentShufflePayloadFileRepository = controlComponentShufflePayloadFileRepository;
		this.controlComponentBallotBoxPayloadFileRepository = controlComponentBallotBoxPayloadFileRepository;
		this.isVoterPortalEnabled = isVoterPortalEnabled;
	}

	/**
	 * Requests the control-components to mix the ballot box with given id {@code ballotBoxId}.
	 *
	 * @param electionEventId the election event id of the ballot box.
	 * @param ballotBoxId     the id of the ballot box to mix.
	 * @return {@code true} if the mixing was started and {@code false} if it hasn't because the ballot box was not yet closed.
	 * @throws UncheckedIOException if the communication with the orchestrator fails.
	 */
	public boolean startOnlineMixing(final String electionEventId, final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		checkState(isVoterPortalEnabled, VOTER_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		final boolean isTestBallotBox = ballotBoxService.isTestBallotBox(ballotBoxId);
		final LocalDateTime ballotBoxFinishDate = ballotBoxService.getDateTo(ballotBoxId);

		final int gracePeriod = ballotBoxService.getGracePeriod(ballotBoxId);
		final boolean beforeGracePeriod = LocalDateTime.now().isBefore(ballotBoxFinishDate.plusSeconds(gracePeriod));

		if (!isTestBallotBox && beforeGracePeriod) {
			LOGGER.error("The ballot box is not yet closed and cannot be mixed. [electionEventId: {}, ballotBoxId: {}]", electionEventId,
					ballotBoxId);
			final String errorMessage = String.format(BALLOT_BOX_NOT_CLOSED_MESSAGE + "[electionEventId: %s, ballotBoxId: %s]",
					electionEventId, ballotBoxId);
			throw new IllegalStateException(errorMessage);
		}

		final ResponseEntity<Void> response = webClientFactory.getWebClient(
						String.format(START_ONLINE_MIXING_FAILED_MESSAGE + "[electionEventId: %s, ballotBoxId: %s]", electionEventId, ballotBoxId))
				.put()
				.uri(uriBuilder -> uriBuilder.path("api/v1/tally/mixonline/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/mix")
						.build(electionEventId, ballotBoxId))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.toBodilessEntity()
				.block();

		checkState(checkNotNull(response).getStatusCode().is2xxSuccessful());

		ballotBoxService.updateBallotBoxStatus(ballotBoxId, BallotBoxStatus.MIXING);
		LOGGER.info("Ballot box status updated. [ballotBoxId: {}, status: {}", ballotBoxId, BallotBoxStatus.MIXING);

		return true;
	}

	/**
	 * Gets the status of the ballot box with id {@code ballotBoxId} from the control-components.
	 *
	 * @param electionEventId the election event id of the ballot box.
	 * @param ballotBoxId     the id of the ballot box to get the status.
	 * @return the status of the ballot box.
	 * @throws UncheckedIOException  if the communication with the orchestrator fails.
	 * @throws IllegalStateException if the response from the orchestrator was unsuccessful.
	 */
	public BallotBoxStatus getOnlineStatus(final String electionEventId, final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		checkState(isVoterPortalEnabled, VOTER_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		final BallotBoxStatus ballotBoxStatus = webClientFactory.getWebClient(
						String.format("Get online status unsuccessful. [ballotBoxId: %s]", ballotBoxId))
				.get()
				.uri(uriBuilder -> uriBuilder.path("api/v1/tally/mixonline/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/status")
						.build(electionEventId, ballotBoxId))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToMono(BallotBoxStatus.class)
				.block();

		checkNotNull(ballotBoxStatus, "Get online status returned a null response. [ballotBoxId: %s]", ballotBoxId);

		return ballotBoxStatus;
	}

	/**
	 * Downloads and persists the outputs generated when mixing the requested ballot box on the online control component nodes.
	 *
	 * @param electionEventId the election event id.
	 * @param ballotBoxId     the ballot box id.
	 * @throws IllegalArgumentException         if the ballot box is not {@link BallotBoxStatus#MIXED}.
	 * @throws UncheckedIOException             if the communication with the orchestrator fails.
	 * @throws IllegalStateException            if the response from the orchestrator was unsuccessful.
	 * @throws InvalidPayloadSignatureException if the signature of the response's content is invalid.
	 */
	public void downloadOnlineMixnetPayloads(final String electionEventId, final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		checkState(isVoterPortalEnabled, VOTER_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		LOGGER.info("Requesting payloads for ballot box... [ballotBoxId: {}]", ballotBoxId);

		// Check that the full ballot box has been mixed.
		if (!ballotBoxService.hasStatus(ballotBoxId, BallotBoxStatus.MIXED)) {
			throw new IllegalArgumentException(String.format("Ballot box is not mixed [ballotBoxId : %s]", ballotBoxId));
		}

		final MixDecryptOnlinePayload mixDecryptOnlinePayload = webClientFactory.getWebClient(
						String.format("%s [ballotBoxId: %s]", DOWNLOAD_UNSUCCESSFUL_MESSAGE, ballotBoxId))
				.get()
				.uri(uriBuilder -> uriBuilder.path("api/v1/tally/mixonline/electionevent/{electionEventId}/ballotbox/{ballotBoxId}/download")
						.build(electionEventId, ballotBoxId))
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToMono(MixDecryptOnlinePayload.class)
				.block();

		checkNotNull(mixDecryptOnlinePayload, "Downloaded content is null. [ballotBoxId: %s]", ballotBoxId);

		final String ballotId = ballotBoxService.getBallotId(ballotBoxId);
		final List<ControlComponentBallotBoxPayload> controlComponentBallotBoxPayloads = mixDecryptOnlinePayload.controlComponentBallotBoxPayloads();
		final List<ControlComponentShufflePayload> controlComponentShufflePayloads = mixDecryptOnlinePayload.controlComponentShufflePayloads();

		// Check consistency of ballot box ID and election event ID
		controlComponentBallotBoxPayloads.forEach(ballotBoxPayload -> checkState(ballotBoxPayload.getBallotBoxId().equals(ballotBoxId),
				"The controlComponentBallotBoxPayload's ballot box id must correspond to the ballot box id of the request"));
		controlComponentBallotBoxPayloads.forEach(ballotBoxPayload -> checkState(ballotBoxPayload.getElectionEventId().equals(electionEventId),
				"The controlComponentBallotBoxPayload's election event id must correspond to the election event id of the request"));

		controlComponentShufflePayloads.forEach(shufflePayload -> checkState(shufflePayload.getBallotBoxId().equals(ballotBoxId),
				"The controlComponentShufflePayload's ballot box id must correspond to the ballot box id of the request"));
		controlComponentShufflePayloads.forEach(shufflePayload -> checkState(shufflePayload.getElectionEventId().equals(electionEventId),
				"The controlComponentShufflePayload's election event id must correspond to the election event id of the request"));

		// Save mixnet payloads.
		controlComponentBallotBoxPayloadFileRepository.saveAll(ballotId, controlComponentBallotBoxPayloads);
		LOGGER.info("Confirmed encrypted votes payloads successfully stored. [electionEventId:{}, ballotId:{}, ballotBoxId:{}]", electionEventId,
				ballotId, ballotBoxId);

		controlComponentShufflePayloads.forEach(payload -> controlComponentShufflePayloadFileRepository.savePayload(ballotId, payload));
		LOGGER.info("Control component shuffle payloads successfully stored. [electionEventId:{}, ballotId:{}, ballotBoxId:{}]", electionEventId,
				ballotId, ballotBoxId);

		ballotBoxService.updateBallotBoxStatus(ballotBoxId, BallotBoxStatus.DOWNLOADED);
		LOGGER.info("Ballot box status updated. [electionEventId: {}, ballotId: {}, ballotBoxId: {}, status: {}]", BallotBoxStatus.DOWNLOADED,
				electionEventId, ballotId, ballotBoxId);
	}

}
