/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.requestcckeys;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.shared.Constants.UPLOAD_ELECTION_EVENT_CONTEXT_FAILED_MESSAGE;
import static ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStep.REQUEST_CC_KEYS;
import static com.google.common.base.Preconditions.checkState;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants;
import ch.post.it.evoting.evotinglibraries.domain.configuration.ControlComponentPublicKeysPayload;
import ch.post.it.evoting.evotinglibraries.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.securedatamanager.shared.WebClientFactory;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStepRunner;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowTask;

import reactor.core.publisher.Mono;

@Service
@ConditionalOnProperty(prefix = "role", name = { "isSetup", "isTally" }, havingValue = "false")
public class RequestCcKeysService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RequestCcKeysService.class);

	private final WebClientFactory webClientFactory;
	private final WorkflowStepRunner workflowStepRunner;
	private final ControlComponentPublicKeysService controlComponentPublicKeysService;
	private final ElectionEventContextPayloadService electionEventContextPayloadService;

	public RequestCcKeysService(
			final WebClientFactory webClientFactory,
			final WorkflowStepRunner workflowStepRunner,
			final ControlComponentPublicKeysService controlComponentPublicKeysService,
			final ElectionEventContextPayloadService electionEventContextPayloadService) {
		this.webClientFactory = webClientFactory;
		this.workflowStepRunner = workflowStepRunner;
		this.controlComponentPublicKeysService = controlComponentPublicKeysService;
		this.electionEventContextPayloadService = electionEventContextPayloadService;
	}

	/**
	 * Uploads the election event context and requests the Control Components keys for an election event based on the given id.
	 *
	 * @param electionEventId identifies the election event for which Control Components keys are requested.
	 */
	public void requestCCKeys(final String electionEventId) {
		validateUUID(electionEventId);

		final WorkflowTask workflowTask = new WorkflowTask(
				() -> performRequestCCKeys(electionEventId),
				() -> LOGGER.info("Control Components keys requested successfully. [electionEventId: {}]", electionEventId),
				throwable -> LOGGER.error("Control Components keys request failed. [electionEventId: {}]", electionEventId, throwable)
		);

		workflowStepRunner.run(REQUEST_CC_KEYS, workflowTask);
	}

	void performRequestCCKeys(final String electionEventId) {
		// Upload the Election Event Context
		final ElectionEventContextPayload electionEventContextPayload = electionEventContextPayloadService.load(electionEventId);
		final List<ControlComponentPublicKeysPayload> controlComponentPublicKeysPayloads = webClientFactory.getWebClient(
						String.format(UPLOAD_ELECTION_EVENT_CONTEXT_FAILED_MESSAGE + "[electionEventId: %s]",
								electionEventId))
				.post()
				.uri(uriBuilder -> uriBuilder.path("api/v1/configuration/setupvoting/keygeneration/electionevent/{electionEventId}")
						.build(electionEventId))
				.accept(MediaType.APPLICATION_JSON)
				.body(Mono.just(electionEventContextPayload), ElectionEventContextPayload.class)
				.retrieve()
				.bodyToFlux(ControlComponentPublicKeysPayload.class)
				.collectList()
				.block();

		checkState(controlComponentPublicKeysPayloads != null,
				"Response of the request for the Control Components keys body is null. [electionEventId: %s]",
				electionEventId);
		LOGGER.info(
				"Successfully uploaded the election event context payload and retrieved control component public keys payloads. [electionEventId: {}]",
				electionEventId);

		// Check the consistency of the Control component public keys
		checkState(controlComponentPublicKeysPayloads.size() == ControlComponentConstants.NODE_IDS.size(),
				"There number of control component public keys payloads expected is incorrect. [received: %s, expected: %s, electionEventId: %s]",
				controlComponentPublicKeysPayloads.size(), ControlComponentConstants.NODE_IDS.size(), electionEventId);

		final Set<Integer> payloadNodeIds = controlComponentPublicKeysPayloads.stream()
				.map(ControlComponentPublicKeysPayload::getControlComponentPublicKeys)
				.map(ControlComponentPublicKeys::nodeId)
				.collect(Collectors.toSet());

		checkState(payloadNodeIds.equals(ControlComponentConstants.NODE_IDS),
				"The control component public keys payloads node ids do not match the expected node ids. [received: %s, expected: %s, electionEventId: %s]",
				payloadNodeIds, ControlComponentConstants.NODE_IDS, electionEventId);

		controlComponentPublicKeysPayloads.forEach(payload -> checkState(payload.getElectionEventId().equals(electionEventId),
				"The controlComponentPublicKeysPayload's election event id must correspond to the election event id of the request"));
		final GqGroup encryptionGroup = electionEventContextPayload.getEncryptionGroup();
		controlComponentPublicKeysPayloads.forEach(payload -> checkState(payload.getEncryptionGroup().equals(encryptionGroup),
				"The controlComponentPublicKeysPayload's group must correspond to the group stored by the secure-data-manager."));

		// Persists the Control component public keys
		controlComponentPublicKeysPayloads.forEach(controlComponentPublicKeysService::save);
	}

}
