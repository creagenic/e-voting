/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.upload;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.shared.Constants.VOTER_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.UncheckedIOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.RetryContext;
import org.springframework.retry.annotation.Retryable;
import org.springframework.retry.support.RetrySynchronizationManager;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.domain.configuration.SetupComponentCMTablePayload;
import ch.post.it.evoting.domain.configuration.SetupComponentCMTablePayloadChunks;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.shared.WebClientFactory;

import reactor.core.publisher.Flux;

@Repository
public class SetupComponentCMTablePayloadUploadRepository {
	private static final Logger LOGGER = LoggerFactory.getLogger(SetupComponentCMTablePayloadUploadRepository.class);

	private final boolean isVoterPortalEnabled;
	private final RetryableRemoteCall retryableRemoteCall;

	public SetupComponentCMTablePayloadUploadRepository(
			@Value("${voter.portal.enabled}")
			final boolean isVoterPortalEnabled,
			final RetryableRemoteCall retryableRemoteCall) {
		this.isVoterPortalEnabled = isVoterPortalEnabled;
		this.retryableRemoteCall = retryableRemoteCall;
	}

	/**
	 * Uploads the setup component CMTable payload to the vote verification.
	 *
	 * @param electionEventId                    the election event id. Must be non-null and a valid UUID.
	 * @param verificationCardSetId              the verification card set id. Must be non-null and a valid UUID.
	 * @param setupComponentCMTablePayloadChunks the list of {@link SetupComponentCMTablePayload} to upload. Must be non-null.
	 * @throws NullPointerException      if any of the inputs is null.
	 * @throws FailedValidationException if {@code electionEventId} or {@code verificationCardSetId} is invalid.
	 */
	public void uploadReturnCodesMappingTable(final String electionEventId, final String verificationCardSetId,
			final SetupComponentCMTablePayloadChunks setupComponentCMTablePayloadChunks) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkNotNull(setupComponentCMTablePayloadChunks);
		checkState(isVoterPortalEnabled, VOTER_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		LOGGER.debug("Uploading CMTable chunk. [electionEventId: {}, verificationCardSetId: {}]", electionEventId, verificationCardSetId);
		retryableRemoteCall.remoteCall(electionEventId, verificationCardSetId, Flux.fromIterable(setupComponentCMTablePayloadChunks.payloads()));

		LOGGER.debug("Successfully uploaded setup component CMTable payloads. [electionEventId: {}, verificationCardSetId: {}, chunkCount: {}]",
				electionEventId, verificationCardSetId, setupComponentCMTablePayloadChunks.getChunkCount());
	}

	@Service
	static class RetryableRemoteCall {
		private static final Logger LOGGER = LoggerFactory.getLogger(RetryableRemoteCall.class);
		private final WebClientFactory webClientFactory;

		public RetryableRemoteCall(final WebClientFactory webClientFactory) {
			this.webClientFactory = webClientFactory;
		}

		@Retryable(retryFor = UncheckedIOException.class, maxAttemptsExpression = "${cmtable-upload.retryable-calls.max-attempts}")
		void remoteCall(final String electionEventId, final String verificationCardSetId,
				final Flux<SetupComponentCMTablePayload> setupComponentCMTablePayloads) {
			validateUUID(electionEventId);
			validateUUID(verificationCardSetId);
			checkNotNull(setupComponentCMTablePayloads);

			final RetryContext retryContext = RetrySynchronizationManager.getContext();
			checkState(retryContext != null, "Unavailable retry context.");
			final int retryNumber = retryContext.getRetryCount();
			if (retryNumber != 0) {
				LOGGER.warn(
						"Retrying of request for uploading setup component CMTable payload. [retryNumber: {}, electionEventId: {}, verificationCardSetId: {}]",
						retryNumber, electionEventId, verificationCardSetId);
			}

			final ResponseEntity<Void> response = webClientFactory.getWebClient(String.format(
							"Request for uploading setup component CMTable payloads failed. [electionEventId: %s, verificationCardSetId: %s]",
							electionEventId, verificationCardSetId))
					.post()
					.uri(uriBuilder -> uriBuilder.path(
									"api/v1/processor/configuration/returncodesmappingtable/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}")
							.build(electionEventId, verificationCardSetId))
					.contentType(MediaType.APPLICATION_NDJSON)
					.accept(MediaType.APPLICATION_JSON)
					.body(setupComponentCMTablePayloads, SetupComponentCMTablePayload.class)
					.retrieve()
					.toBodilessEntity()
					.block();

			checkState(checkNotNull(response).getStatusCode().is2xxSuccessful());
		}
	}
}
