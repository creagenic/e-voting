/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.upload;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStep.UPLOAD_CONFIGURATION_1;
import static com.google.common.base.Preconditions.checkNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.securedatamanager.shared.process.BallotBoxService;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStep;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStepRunner;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowTask;

@Service
public class UploadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UploadService.class);

	private final BallotBoxService ballotBoxService;
	private final BallotUploadService ballotUploadService;
	private final VotingCardSetUploadService votingCardSetUploadService;
	private final ElectoralBoardsUploadService electoralBoardsUploadService;
	private final WorkflowStepRunner workflowStepRunner;

	public UploadService(
			final BallotBoxService ballotBoxService,
			final BallotUploadService ballotUploadService,
			final VotingCardSetUploadService votingCardSetUploadService,
			final ElectoralBoardsUploadService electoralBoardsUploadService,
			final WorkflowStepRunner workflowStepRunner) {
		this.ballotBoxService = ballotBoxService;
		this.ballotUploadService = ballotUploadService;
		this.votingCardSetUploadService = votingCardSetUploadService;
		this.electoralBoardsUploadService = electoralBoardsUploadService;
		this.workflowStepRunner = workflowStepRunner;
	}

	public void upload(final String electionEventId, final WorkflowStep uploadConfigurationStep) {
		validateUUID(electionEventId);
		checkNotNull(uploadConfigurationStep);

		LOGGER.debug("Uploading the election configuration... [electionEventId: {}]", electionEventId);

		final WorkflowTask workflowTask = new WorkflowTask(
				() -> performUpload(electionEventId, uploadConfigurationStep),
				() -> LOGGER.info("Upload of election configuration successful. [electionEventId: {}]", electionEventId),
				throwable -> LOGGER.error("Upload of election configuration failed. [electionEventId: {}]", electionEventId, throwable)
		);

		workflowStepRunner.run(uploadConfigurationStep, workflowTask);
	}

	private void performUpload(final String electionEventId, final WorkflowStep uploadConfigurationStep) {
		if (uploadConfigurationStep.equals(UPLOAD_CONFIGURATION_1)) {
			ballotUploadService.uploadBallots(electionEventId);
			votingCardSetUploadService.uploadVotingCardSets(electionEventId);
		} else {
			ballotBoxService.updateStatuses(electionEventId);
			electoralBoardsUploadService.uploadSignedAuthenticationContextConfiguration(electionEventId);
		}
	}

}
