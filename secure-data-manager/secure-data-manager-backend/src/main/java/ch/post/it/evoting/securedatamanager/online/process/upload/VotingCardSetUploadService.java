/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.upload;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;

import java.util.HashMap;
import java.util.Map;

import jakarta.json.Json;
import jakarta.json.JsonArray;
import jakarta.json.JsonObject;
import jakarta.json.JsonObjectBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.securedatamanager.shared.JsonConstants;
import ch.post.it.evoting.securedatamanager.shared.process.Status;
import ch.post.it.evoting.securedatamanager.shared.process.SynchronizeStatus;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;
import ch.post.it.evoting.securedatamanager.shared.process.JsonUtils;

/**
 * Service which will upload the information related to the voting card sets.
 */
@Service
public class VotingCardSetUploadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VotingCardSetUploadService.class);

	private final VotingCardSetRepository votingCardSetRepository;
	private final SetupComponentCMTablePayloadUploadService setupComponentCMTablePayloadUploadService;
	private final LongVoteCastReturnCodesAllowListUploadService longVoteCastReturnCodesAllowListUploadService;
	private final SetupComponentVoterAuthenticationPayloadUploadService setupComponentVoterAuthenticationPayloadUploadService;

	public VotingCardSetUploadService(
			final VotingCardSetRepository votingCardSetRepository,
			final SetupComponentCMTablePayloadUploadService setupComponentCMTablePayloadUploadService,
			final LongVoteCastReturnCodesAllowListUploadService longVoteCastReturnCodesAllowListUploadService,
			final SetupComponentVoterAuthenticationPayloadUploadService setupComponentVoterAuthenticationPayloadUploadService
	) {
		this.votingCardSetRepository = votingCardSetRepository;
		this.setupComponentCMTablePayloadUploadService = setupComponentCMTablePayloadUploadService;
		this.longVoteCastReturnCodesAllowListUploadService = longVoteCastReturnCodesAllowListUploadService;
		this.setupComponentVoterAuthenticationPayloadUploadService = setupComponentVoterAuthenticationPayloadUploadService;
	}

	/**
	 * Uploads to the voter portal:
	 * <ul>
	 *     <li>the voter information.</li>
	 *     <li>the return codes mapping tables.</li>
	 *     <li>the long Vote Cast Return Codes allow lists.</li>
	 * </ul>
	 */
	public void uploadVotingCardSets(final String electionEventId) {
		validateUUID(electionEventId);

		final Map<String, Object> votingCardSetsParams = new HashMap<>();
		addSigned(votingCardSetsParams);
		addPendingToSynchronize(votingCardSetsParams);
		votingCardSetsParams.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEventId);

		final String votingCardSetDocuments = votingCardSetRepository.list(votingCardSetsParams);
		final JsonArray votingCardSets = JsonUtils.getJsonObject(votingCardSetDocuments).getJsonArray(JsonConstants.RESULT);

		for (int i = 0; i < votingCardSets.size(); i++) {
			final JsonObject votingCardSetInArray = votingCardSets.getJsonObject(i);

			checkArgument(electionEventId.equals(votingCardSetInArray.getJsonObject(JsonConstants.ELECTION_EVENT).getString(JsonConstants.ID)));

			final String verificationCardSetId = votingCardSetInArray.getString(JsonConstants.VERIFICATION_CARD_SET_ID);

			LOGGER.debug("Uploading voter authentication... [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
					verificationCardSetId);
			setupComponentVoterAuthenticationPayloadUploadService.upload(electionEventId, verificationCardSetId);

			LOGGER.debug("Uploading return codes mapping table... [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
					verificationCardSetId);
			setupComponentCMTablePayloadUploadService.upload(electionEventId, verificationCardSetId);

			LOGGER.debug("Uploading long Vote Cast Return Codes allow list... [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
					verificationCardSetId);
			longVoteCastReturnCodesAllowListUploadService.upload(electionEventId, verificationCardSetId);

			LOGGER.info("The voting card and verification card sets where successfully uploaded. [electionEventId: {}, verificationCardSetId: {}]",
					electionEventId, verificationCardSetId);

			final String votingCardSetId = votingCardSetInArray.getString(JsonConstants.ID);
			final JsonObjectBuilder builder = Json.createObjectBuilder();
			builder.add(JsonConstants.ID, votingCardSetId);
			builder.add(JsonConstants.SYNCHRONIZED, SynchronizeStatus.SYNCHRONIZED.getIsSynchronized().toString());
			builder.add(JsonConstants.DETAILS, SynchronizeStatus.SYNCHRONIZED.getStatus());

			votingCardSetRepository.update(builder.build().toString());
			LOGGER.info("Changed the state of the voting card set. [votingCardSetId: {}, verificationCardSetId: {}]", votingCardSetId,
					verificationCardSetId);
		}
	}

	private void addSigned(final Map<String, Object> votingCardSetsParams) {
		votingCardSetsParams.put(JsonConstants.STATUS, Status.SIGNED.name());
	}

	private void addPendingToSynchronize(final Map<String, Object> votingCardSetsParams) {
		votingCardSetsParams.put(JsonConstants.SYNCHRONIZED, SynchronizeStatus.PENDING.getIsSynchronized().toString());
	}

}
