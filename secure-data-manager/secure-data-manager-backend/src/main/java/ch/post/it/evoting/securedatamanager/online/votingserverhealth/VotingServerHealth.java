/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.votingserverhealth;

public record VotingServerHealth(boolean status, String serverName) {
}
