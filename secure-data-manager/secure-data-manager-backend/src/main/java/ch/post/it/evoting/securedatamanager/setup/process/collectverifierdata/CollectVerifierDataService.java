/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.collectverifierdata;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStep.SETUP_DATA_COLLECTION;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.securedatamanager.shared.process.DatasetInfo;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;
import ch.post.it.evoting.securedatamanager.shared.process.PathResolver;
import ch.post.it.evoting.securedatamanager.shared.process.VerifierCollectorService;
import ch.post.it.evoting.securedatamanager.shared.process.VerifierExportType;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStepRunner;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowTask;

@Service
@ConditionalOnProperty("role.isSetup")
public class CollectVerifierDataService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CollectVerifierDataService.class);

	private final PathResolver pathResolver;
	private final WorkflowStepRunner workflowStepRunner;
	private final ElectionEventService electionEventService;
	private final VerifierCollectorService verifierCollectorService;

	public CollectVerifierDataService(
			final PathResolver pathResolver,
			final WorkflowStepRunner workflowStepRunner,
			final ElectionEventService electionEventService,
			final VerifierCollectorService verifierCollectorService) {
		this.pathResolver = pathResolver;
		this.workflowStepRunner = workflowStepRunner;
		this.electionEventService = electionEventService;
		this.verifierCollectorService = verifierCollectorService;
	}

	public void collectVerifierData(final String electionEventId) {
		validateUUID(electionEventId);

		LOGGER.debug("Collecting the setup data... [electionEventId: {}]", electionEventId);

		final WorkflowTask workflowTask = new WorkflowTask(
				() -> performCollect(electionEventId),
				() -> LOGGER.info("Collection of setup data successful. [electionEventId: {}]", electionEventId),
				throwable -> LOGGER.error("Collection of setup data failed. [electionEventId: {}]", electionEventId, throwable)
		);

		workflowStepRunner.run(SETUP_DATA_COLLECTION, workflowTask);
	}

	public DatasetInfo getDatasetFilenameList() {
		final String electionEventId = electionEventService.findElectionEventId();
		final String verifierOutputFolderPath = pathResolver.resolveVerifierOutputPath().toString();
		return new DatasetInfo(verifierOutputFolderPath,
				List.of(verifierCollectorService.getExportFilename(VerifierExportType.CONTEXT, electionEventId),
						verifierCollectorService.getExportFilename(VerifierExportType.SETUP, electionEventId)));
	}

	private void performCollect(final String electionEventId) {
		final List<VerifierExportType> datasetExportTypes = List.of(VerifierExportType.CONTEXT, VerifierExportType.SETUP);
		datasetExportTypes.forEach(verifierExportType -> verifierCollectorService.collectDataset(verifierExportType, electionEventId));
	}

}
