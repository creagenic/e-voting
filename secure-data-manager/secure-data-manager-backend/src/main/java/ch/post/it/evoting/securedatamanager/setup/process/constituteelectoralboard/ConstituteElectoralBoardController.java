/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.constituteelectoralboard;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.securedatamanager.shared.process.BoardMember;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;

@RestController
@RequestMapping("/sdm-setup/constitute-electoral-board")
@ConditionalOnProperty("role.isSetup")
public class ConstituteElectoralBoardController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConstituteElectoralBoardController.class);

	private final ElectionEventService electionEventService;
	private final ConstituteElectoralBoardService constituteElectoralBoardService;

	public ConstituteElectoralBoardController(
			final ElectionEventService electionEventService,
			final ConstituteElectoralBoardService constituteElectoralBoardService) {
		this.electionEventService = electionEventService;
		this.constituteElectoralBoardService = constituteElectoralBoardService;
	}

	@PostMapping
	public void constitute(
			@RequestBody
			final List<char[]> electoralBoardPasswords) {

		checkNotNull(electoralBoardPasswords);
		checkArgument(electoralBoardPasswords.size() >= 2);

		final String electionEventId = electionEventService.findElectionEventId();

		LOGGER.debug("Received request to constitute the electoral boards. [electionEventId: {}]", electionEventId);

		constituteElectoralBoardService.constitute(electionEventId, electoralBoardPasswords);

		LOGGER.info("The constitution of the electoral board has been started. [electionEventId: {}]", electionEventId);
	}

	@GetMapping
	public List<BoardMember> getElectoralBoardMembers() {
		return constituteElectoralBoardService.getElectoralBoardMembers();
	}

}
