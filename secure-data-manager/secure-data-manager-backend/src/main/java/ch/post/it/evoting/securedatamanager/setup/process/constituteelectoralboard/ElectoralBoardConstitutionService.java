/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.constituteelectoralboard;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.google.common.base.Preconditions;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.SchnorrProof;
import ch.post.it.evoting.evotinglibraries.domain.election.ControlComponentPublicKeys;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setuptally.SetupTallyEBOutput;
import ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setuptally.SetupTallyEBService;
import ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting.GenVerCardSetKeysService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;

@Service
@ConditionalOnProperty("role.isSetup")
public class ElectoralBoardConstitutionService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElectoralBoardConstitutionService.class);

	private final ControlComponentPublicKeysConfigService controlComponentPublicKeysConfigService;
	private final GenVerCardSetKeysService genVerCardSetKeysService;
	private final SetupTallyEBService setupTallyEBService;
	private final ElectoralBoardPersistenceService electoralBoardPersistenceService;
	private final SetupComponentVerificationCardKeystoresPayloadGenerationService setupComponentVerificationCardKeystoresPayloadGenerationService;
	private final ElectionEventService electionEventService;

	public ElectoralBoardConstitutionService(
			final ControlComponentPublicKeysConfigService controlComponentPublicKeysConfigService,
			final GenVerCardSetKeysService genVerCardSetKeysService,
			final SetupTallyEBService setupTallyEBService,
			final ElectoralBoardPersistenceService electoralBoardPersistenceService,
			final SetupComponentVerificationCardKeystoresPayloadGenerationService setupComponentVerificationCardKeystoresPayloadGenerationService,
			final ElectionEventService electionEventService) {
		this.controlComponentPublicKeysConfigService = controlComponentPublicKeysConfigService;
		this.genVerCardSetKeysService = genVerCardSetKeysService;
		this.setupTallyEBService = setupTallyEBService;
		this.electoralBoardPersistenceService = electoralBoardPersistenceService;
		this.setupComponentVerificationCardKeystoresPayloadGenerationService = setupComponentVerificationCardKeystoresPayloadGenerationService;
		this.electionEventService = electionEventService;
	}

	/**
	 * Constitutes the electoral board.
	 *
	 * @param electionEventId                the election event id. Must be non-null and a valid UUID.
	 * @param electoralBoardMembersPasswords the passwords of the electoral board members. Must be non-null and a valid EBPassword.
	 * @param electoralBoardMembersHashes    the hashes of the electoral board members' passwords. Must be non-null.
	 * @throws FailedValidationException if the election event id is invalid.
	 * @throws NullPointerException      if any of the inputs is null.
	 * @throws IllegalStateException     if any hash is empty.
	 */
	public void constitute(final String electionEventId, final List<char[]> electoralBoardMembersPasswords,
			final List<byte[]> electoralBoardMembersHashes) {
		validateUUID(electionEventId);
		checkNotNull(electoralBoardMembersPasswords);
		checkArgument(electionEventService.exists(electionEventId));
		electoralBoardMembersPasswords.forEach(Preconditions::checkNotNull);
		final List<char[]> passwords = electoralBoardMembersPasswords.stream().parallel().map(char[]::clone).toList();
		passwords.stream().parallel().forEach(Preconditions::checkNotNull);
		checkArgument(passwords.size() >= 2);

		// Wipe the passwords after usage
		electoralBoardMembersPasswords.stream().parallel().forEach(pw -> Arrays.fill(pw, '\u0000'));

		checkNotNull(electoralBoardMembersHashes);
		electoralBoardMembersHashes.stream().parallel().forEach(Preconditions::checkNotNull);
		final List<byte[]> passwordHashes = checkNotNull(electoralBoardMembersHashes).stream().parallel().map(byte[]::clone).toList();
		passwordHashes.stream().parallel().forEach(Preconditions::checkNotNull);
		passwordHashes.stream().parallel().forEach(hash -> checkArgument(hash.length > 0));

		checkArgument(passwords.size() == passwordHashes.size());

		LOGGER.debug("Loading control component public keys... [electionEventId: {}]", electionEventId);

		final List<ControlComponentPublicKeys> controlComponentPublicKeys = controlComponentPublicKeysConfigService.loadOrderByNodeId(
				electionEventId);

		LOGGER.debug("Loaded control component public keys. [electionEventId: {}]", electionEventId);

		final ElGamalMultiRecipientPublicKey choiceReturnCodesEncryptionPublicKey = genVerCardSetKeysService.genVerCardSetKeys(electionEventId,
				controlComponentPublicKeys);

		LOGGER.info("GenVerCardSetKeys algorithm successfully performed. [electionEventId: {}]", electionEventId);

		final SetupTallyEBOutput setupTallyEBOutput = setupTallyEBService.setupTallyEB(electionEventId, passwords, controlComponentPublicKeys);

		LOGGER.info("Setup Tally EB algorithm successfully performed. [electionEventId: {}]", electionEventId);

		final ElGamalMultiRecipientPublicKey electionPublicKey = setupTallyEBOutput.getElectionPublicKey();
		final ElGamalMultiRecipientPublicKey electoralBoardPublicKey = setupTallyEBOutput.getElectoralBoardPublicKey();
		final GroupVector<SchnorrProof, ZqGroup> electoralBoardSchnorrProofs = setupTallyEBOutput.getElectoralBoardSchnorrProofs();

		LOGGER.debug("Persisting Electoral Board... [electionEventId: {}]", electionEventId);

		electoralBoardPersistenceService.persist(electionEventId, controlComponentPublicKeys, choiceReturnCodesEncryptionPublicKey, electionPublicKey,
				electoralBoardPublicKey, electoralBoardSchnorrProofs, passwordHashes);

		LOGGER.info("Electoral board successfully constituted and persisted. [electionEventId: {}]", electionEventId);

		setupComponentVerificationCardKeystoresPayloadGenerationService.generate(electionEventId, choiceReturnCodesEncryptionPublicKey,
				electionPublicKey);

		LOGGER.info("Successfully generated and persisted setup component verification card keystores payloads. [electionEventId: {}]",
				electionEventId);
	}

}
