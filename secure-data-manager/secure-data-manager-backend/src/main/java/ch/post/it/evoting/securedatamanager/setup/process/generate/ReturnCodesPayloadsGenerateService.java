/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.generate;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;

import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.IntStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.configuration.ChoiceReturnCodeToEncodedVotingOptionEntry;
import ch.post.it.evoting.domain.configuration.SetupComponentCMTablePayload;
import ch.post.it.evoting.domain.configuration.SetupComponentCMTablePayloadChunks;
import ch.post.it.evoting.domain.configuration.VoterReturnCodes;
import ch.post.it.evoting.domain.configuration.VoterReturnCodesPayload;
import ch.post.it.evoting.domain.configuration.setupvoting.SetupComponentLVCCAllowListPayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.evotinglibraries.domain.signature.SignedPayload;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.shared.process.ConfigurationEntityStatusService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.Status;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;

@Service
@ConditionalOnProperty("role.isSetup")
public class ReturnCodesPayloadsGenerateService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReturnCodesPayloadsGenerateService.class);

	private final VotingCardSetRepository votingCardSetRepository;
	private final ConfigurationEntityStatusService configurationEntityStatusService;
	private final ElectionEventContextPayloadService electionEventContextPayloadService;
	private final VotingCardSetDataGenerationService votingCardSetDataGenerationService;
	private final ReturnCodesPayloadsPersistenceService returnCodesPayloadsPersistenceService;
	private final int chunkSize;
	private final SignatureKeystore<Alias> signatureKeystoreService;

	public ReturnCodesPayloadsGenerateService(
			final VotingCardSetRepository votingCardSetRepository,
			final ConfigurationEntityStatusService configurationEntityStatusService,
			final ElectionEventContextPayloadService electionEventContextPayloadService,
			final VotingCardSetDataGenerationService votingCardSetDataGenerationService,
			final ReturnCodesPayloadsPersistenceService returnCodesPayloadsPersistenceService,
			@Value("${CMTable.chunk.size}")
			final int chunkSize,
			final SignatureKeystore<Alias> signatureKeystoreService) {
		this.votingCardSetRepository = votingCardSetRepository;
		this.configurationEntityStatusService = configurationEntityStatusService;
		this.electionEventContextPayloadService = electionEventContextPayloadService;
		this.votingCardSetDataGenerationService = votingCardSetDataGenerationService;
		this.returnCodesPayloadsPersistenceService = returnCodesPayloadsPersistenceService;
		this.chunkSize = chunkSize;
		this.signatureKeystoreService = signatureKeystoreService;
	}

	/**
	 * Generates and persists on the file system the following payloads:
	 * <ul>
	 *     <li>Setup component CMTable payload</li>
	 *     <li>Voter Return Codes payload</li>
	 *     <li>Setup component LVCC allow list payload</li>
	 * </ul>
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @param votingCardSetId the voting card set id. Must be non-null and a valid UUID.
	 * @throws NullPointerException      if {@code electionEventId} or {@code votingCardSetId} is null.
	 * @throws FailedValidationException if {@code electionEventId} or {@code votingCardSetId} is invalid.
	 */
	public void generate(final String electionEventId, final String votingCardSetId) {
		validateUUID(electionEventId);
		validateUUID(votingCardSetId);

		LOGGER.info("Generating return codes payloads... [electionEventId: {}, votingCardSetId: {}]", electionEventId, votingCardSetId);

		// ReturnCodesGenerationOutput
		final ReturnCodesGenerationOutput returnCodesGenerationOutput = votingCardSetDataGenerationService.generate(electionEventId, votingCardSetId);

		// Setup Component CMTable Payloads
		final SetupComponentCMTablePayloadChunks setupComponentCMTablePayloadChunks = getSetupComponentCMTablePayloads(electionEventId,
				returnCodesGenerationOutput);
		LOGGER.debug("Successfully created the setup component CMTable payloads. [electionEventId: {}, verificationCardSetId: {}, chunkCount: {}]",
				electionEventId, returnCodesGenerationOutput.getVerificationCardSetId(), setupComponentCMTablePayloadChunks.getChunkCount());

		// Voter Return Codes payload
		final VoterReturnCodesPayload voterReturnCodesPayload = generateVoterReturnCodesPayload(returnCodesGenerationOutput);

		// Setup component LVCC allow list payload
		final SetupComponentLVCCAllowListPayload setupComponentLVCCAllowListPayload =
				generateLongVoteCastReturnCodesAllowListPayload(returnCodesGenerationOutput);

		// Persist the payloads
		returnCodesPayloadsPersistenceService.save(returnCodesGenerationOutput.getElectionEventId(), votingCardSetId,
				setupComponentCMTablePayloadChunks, voterReturnCodesPayload, setupComponentLVCCAllowListPayload);

		LOGGER.info("Return codes payloads are successfully generated and persisted. [electionEventId: {}, votingCardSetId: {}]",
				returnCodesGenerationOutput.getElectionEventId(), votingCardSetId);

		configurationEntityStatusService.update(Status.GENERATED.name(), votingCardSetId, votingCardSetRepository);
		LOGGER.info("Voting card set is generated. [electionEventId: {}, votingCardSetId: {}]", electionEventId, votingCardSetId);
	}

	private SetupComponentCMTablePayloadChunks getSetupComponentCMTablePayloads(final String electionEventId,
			final ReturnCodesGenerationOutput returnCodesGenerationOutput) {
		final String verificationCardSetId = returnCodesGenerationOutput.getVerificationCardSetId();
		final SortedMap<String, String> returnCodesMappingTable = returnCodesGenerationOutput.getReturnCodesMappingTable();
		final Iterator<Map.Entry<String, String>> iterator = returnCodesMappingTable.entrySet().iterator();

		final List<SetupComponentCMTablePayload> payloadChunks = new ArrayList<>();
		SortedMap<String, String> returnCodesMappingTableChunk = new TreeMap<>();
		for (int i = 0; i < returnCodesMappingTable.size(); i++) {

			final Map.Entry<String, String> entry = iterator.next();
			returnCodesMappingTableChunk.put(entry.getKey(), entry.getValue());

			// If the chunked CMTable is full then we create the payload, sign it and add it to the list of payloads.
			if (returnCodesMappingTableChunk.size() == chunkSize || !iterator.hasNext()) {
				final SetupComponentCMTablePayload setupComponentCMTablePayload = new SetupComponentCMTablePayload.Builder()
						.setElectionEventId(electionEventId)
						.setVerificationCardSetId(verificationCardSetId)
						.setChunkId(i / chunkSize)
						.setReturnCodesMappingTable(returnCodesMappingTableChunk)
						.build();

				final Hashable additionalContextData = ChannelSecurityContextData.setupComponentCMTable(electionEventId, verificationCardSetId);
				final CryptoPrimitivesSignature setupComponentCMTablePayloadSignature = getPayloadSignature(setupComponentCMTablePayload,
						additionalContextData);
				setupComponentCMTablePayload.setSignature(setupComponentCMTablePayloadSignature);

				payloadChunks.add(setupComponentCMTablePayload);

				// Prepare for the next chunk
				returnCodesMappingTableChunk = new TreeMap<>();
			}
		}
		return new SetupComponentCMTablePayloadChunks(payloadChunks);
	}

	private VoterReturnCodesPayload generateVoterReturnCodesPayload(final ReturnCodesGenerationOutput returnCodesGenerationOutput) {
		final String electionEventId = returnCodesGenerationOutput.getElectionEventId();
		final String verificationCardSetId = returnCodesGenerationOutput.getVerificationCardSetId();
		final GqGroup encryptionGroup = electionEventContextPayloadService.loadEncryptionGroup(electionEventId);

		final PrimesMappingTable primesMappingTable = electionEventContextPayloadService.loadPrimesMappingTable(electionEventId,
				verificationCardSetId);
		final GroupVector<PrimeGqElement, GqGroup> encodedVotingOptions = primesMappingTable.getPTable().stream()
				.parallel()
				.map(PrimesMappingTableEntry::encodedVotingOption)
				.collect(GroupVector.toGroupVector());

		final List<String> verificationCardIds = returnCodesGenerationOutput.getVerificationCardIds();
		final List<List<String>> shortChoiceReturnCodesList = returnCodesGenerationOutput.getShortChoiceReturnCodes();
		final List<String> shortVoteCastReturnCodes = returnCodesGenerationOutput.getShortVoteCastReturnCodes();

		final List<VoterReturnCodes> voterReturnCodes = IntStream.range(0, verificationCardIds.size())
				.parallel()
				.mapToObj(i -> {
					final List<String> shortChoiceReturnCodes = shortChoiceReturnCodesList.get(i);

					final GroupVector<ChoiceReturnCodeToEncodedVotingOptionEntry, GqGroup> choiceReturnCodesToEncodedVotingOptions =
							IntStream.range(0, encodedVotingOptions.size())
									.parallel()
									.mapToObj(idx ->
											new ChoiceReturnCodeToEncodedVotingOptionEntry(shortChoiceReturnCodes.get(idx),
													encodedVotingOptions.get(idx)))
									.collect(GroupVector.toGroupVector());

					return new VoterReturnCodes(verificationCardIds.get(i), shortVoteCastReturnCodes.get(i), choiceReturnCodesToEncodedVotingOptions);
				}).toList();

		return new VoterReturnCodesPayload(encryptionGroup, electionEventId, verificationCardSetId, voterReturnCodes);
	}

	private SetupComponentLVCCAllowListPayload generateLongVoteCastReturnCodesAllowListPayload(
			final ReturnCodesGenerationOutput returnCodesGenerationOutput) {

		final String electionEventId = returnCodesGenerationOutput.getElectionEventId();
		final String verificationCardSetId = returnCodesGenerationOutput.getVerificationCardSetId();
		final List<String> longVoteCastReturnCodesAllowList = returnCodesGenerationOutput.getLongVoteCastReturnCodesAllowList();

		final SetupComponentLVCCAllowListPayload setupComponentLVCCAllowListPayload =
				new SetupComponentLVCCAllowListPayload(electionEventId, verificationCardSetId, longVoteCastReturnCodesAllowList);
		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentLVCCAllowList(electionEventId, verificationCardSetId);

		setupComponentLVCCAllowListPayload.setSignature(getPayloadSignature(setupComponentLVCCAllowListPayload, additionalContextData));

		LOGGER.debug("Successfully signed setup component LVCC allow list payload. [electionEventId: {}, verificationCardSetId: {}]",
				electionEventId, verificationCardSetId);

		return setupComponentLVCCAllowListPayload;
	}

	private CryptoPrimitivesSignature getPayloadSignature(final SignedPayload payload, final Hashable additionalContextData) {
		try {
			final byte[] signature = signatureKeystoreService.generateSignature(payload, additionalContextData);

			return new CryptoPrimitivesSignature(signature);
		} catch (final SignatureException e) {
			throw new IllegalStateException(String.format(
					"Failed to generate the payload signature. [name: %s]", payload.getClass().getName()), e);
		}
	}

}
