/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.precompute;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import jakarta.json.Json;
import jakarta.json.JsonArray;
import jakarta.json.JsonObject;
import jakarta.json.JsonObjectBuilder;
import jakarta.json.JsonValue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.evotinglibraries.domain.election.Ballot;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.securedatamanager.shared.JsonConstants;
import ch.post.it.evoting.securedatamanager.shared.process.BallotBoxService;
import ch.post.it.evoting.securedatamanager.shared.process.BallotRepository;
import ch.post.it.evoting.securedatamanager.shared.process.ConfigurationEntityStatusService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.JsonUtils;
import ch.post.it.evoting.securedatamanager.shared.process.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.shared.process.SynchronizeStatus;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;

/**
 * Service for operates with ballots.
 */
@Service
@ConditionalOnProperty("role.isSetup")
public class BallotConfigService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BallotConfigService.class);

	private final ObjectMapper objectMapper;
	private final BallotRepository ballotRepository;
	private final BallotBoxService ballotBoxService;
	private final BallotUpdateService ballotUpdateService;
	private final VotingCardSetRepository votingCardSetRepository;
	private final ElectionEventContextPayloadService electionEventContextPayloadService;
	private final ConfigurationEntityStatusService configurationEntityStatusService;

	public BallotConfigService(
			final ObjectMapper objectMapper,
			final BallotRepository ballotRepository,
			final BallotBoxService ballotBoxService,
			final BallotUpdateService ballotUpdateService,
			final VotingCardSetRepository votingCardSetRepository,
			final ElectionEventContextPayloadService electionEventContextPayloadService,
			final ConfigurationEntityStatusService configurationEntityStatusService) {
		this.objectMapper = objectMapper;
		this.ballotRepository = ballotRepository;
		this.ballotBoxService = ballotBoxService;
		this.ballotUpdateService = ballotUpdateService;
		this.votingCardSetRepository = votingCardSetRepository;
		this.electionEventContextPayloadService = electionEventContextPayloadService;
		this.configurationEntityStatusService = configurationEntityStatusService;
	}

	/**
	 * Updates the given ballot and related ballot texts, and change the state of the ballot from locked to {@code BallotBoxStatus.UPDATED} for a
	 * given election event and ballot id.
	 *
	 * @param electionEventId the election event id.
	 * @param ballotId        the ballot id.
	 * @throws ResourceNotFoundException if the ballot is not found.
	 */
	public void updateBallot(final String electionEventId, final String ballotId) throws ResourceNotFoundException {
		validateUUID(electionEventId);
		validateUUID(ballotId);

		final JsonObject ballot = getValidBallot(electionEventId, ballotId);
		final JsonObject modifiedBallot = removeBallotMetaData(ballot);

		final String ballotBoxId = ballotBoxService.listBallotBoxIds(ballotId).get(0);
		final String verificationCardSetId = votingCardSetRepository.getVerificationCardSetIdByBallotBox(ballotBoxId);
		final PrimesMappingTable primesMappingTable = electionEventContextPayloadService.loadPrimesMappingTable(electionEventId,
				verificationCardSetId);

		final JsonObject updatedBallot = ballotUpdateService.updateOptionsRepresentation(electionEventId, modifiedBallot, primesMappingTable);
		ballotRepository.updateBallotContests(ballotId, updatedBallot.getJsonArray(JsonConstants.CONTESTS).toString());
		LOGGER.debug("Ballot options representation updated. [electionEventId: {}, ballotId:{}]", electionEventId, ballotId);

		configurationEntityStatusService.updateWithSynchronizedStatus(BallotBoxStatus.UPDATED.name(), ballotId, ballotRepository,
				SynchronizeStatus.PENDING);
		LOGGER.debug("Ballot status updated. [electionEventId: {}, ballotId:{}, status:{}]", electionEventId, ballotId, BallotBoxStatus.UPDATED);

		LOGGER.info("The ballot was successfully updated. [electionEventId: {}, ballotId:{}]", electionEventId, ballotId);
	}

	private JsonObject removeBallotMetaData(final JsonObject ballot) {
		final JsonObject modifiedBallot = removeField(JsonConstants.DETAILS, ballot);
		return removeField(JsonConstants.SYNCHRONIZED, modifiedBallot);
	}

	private JsonObject removeField(final String field, final JsonObject obj) {

		final JsonObjectBuilder builder = Json.createObjectBuilder();

		for (final Map.Entry<String, JsonValue> e : obj.entrySet()) {
			final String key = e.getKey();
			final JsonValue value = e.getValue();
			if (!key.equals(field)) {
				builder.add(key, value);
			}

		}
		return builder.build();
	}

	private JsonObject getValidBallot(final String electionEventId, final String ballotId) throws ResourceNotFoundException {

		final Optional<JsonObject> possibleBallot = getPossibleValidBallot(electionEventId, ballotId);

		if (possibleBallot.isEmpty()) {
			throw new ResourceNotFoundException("Ballot not found");
		}

		return possibleBallot.get();
	}

	/**
	 * Pre: there is just one matching element
	 *
	 * @return single {@link BallotBoxStatus#LOCKED} ballot in json object format
	 */
	private Optional<JsonObject> getPossibleValidBallot(final String electionEventId, final String ballotId) {

		Optional<JsonObject> ballot = Optional.empty();
		final Map<String, Object> attributeValueMap = new HashMap<>();

		attributeValueMap.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEventId);
		attributeValueMap.put(JsonConstants.ID, ballotId);
		attributeValueMap.put(JsonConstants.STATUS, BallotBoxStatus.LOCKED.name());
		final String ballotResultListAsJson = ballotRepository.list(attributeValueMap);

		if (ballotResultListAsJson == null || ballotResultListAsJson.isEmpty()) {
			return ballot;
		} else {
			final JsonArray ballotResultList = JsonUtils.getJsonObject(ballotResultListAsJson).getJsonArray(JsonConstants.RESULT);

			if (ballotResultList != null && !ballotResultList.isEmpty()) {
				ballot = Optional.of(ballotResultList.getJsonObject(0));
			} else {
				return ballot;
			}
		}
		return ballot;
	}

	public Ballot getBallot(final String electionEventId, final String ballotId) {
		validateUUID(electionEventId);
		validateUUID(ballotId);

		final Map<String, Object> attributeValueMap = new HashMap<>();
		attributeValueMap.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEventId);
		attributeValueMap.put(JsonConstants.ID, ballotId);
		final String ballotAsJson = ballotRepository.find(attributeValueMap);
		try {
			return objectMapper.readValue(ballotAsJson, Ballot.class);
		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot deserialize the ballot box json string to a valid Ballot object.", e);
		}
	}
}
