/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.precompute;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting.GenVerDatOutput;
import ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting.GenVerDatService;
import ch.post.it.evoting.securedatamanager.shared.process.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.shared.process.Status;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;
import ch.post.it.evoting.securedatamanager.shared.process.BallotBoxService;
import ch.post.it.evoting.securedatamanager.shared.process.ConfigurationEntityStatusService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;

/**
 * Service that deals with the pre-computation of voting card sets.
 */
@Service
@ConditionalOnProperty("role.isSetup")
public class VerificationCardSetPreComputeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerificationCardSetPreComputeService.class);

	private final BallotBoxService ballotBoxService;
	private final GenVerDatService genVerDatService;
	private final ElectionEventService electionEventService;
	private final VotingCardSetRepository votingCardSetRepository;
	private final ConfigurationEntityStatusService configurationEntityStatusService;
	private final ElectionEventContextPayloadService electionEventContextPayloadService;
	private final VotingCardSetPreComputationPersistenceService votingCardSetPrecomputationPersistenceService;

	@Autowired
	public VerificationCardSetPreComputeService(
			final BallotBoxService ballotBoxService,
			final GenVerDatService genVerDatService,
			final ElectionEventService electionEventService,
			final VotingCardSetRepository votingCardSetRepository,
			final ConfigurationEntityStatusService configurationEntityStatusService,
			final ElectionEventContextPayloadService electionEventContextPayloadService,
			final VotingCardSetPreComputationPersistenceService votingCardSetPrecomputationPersistenceService) {
		this.ballotBoxService = ballotBoxService;
		this.genVerDatService = genVerDatService;
		this.electionEventService = electionEventService;
		this.votingCardSetRepository = votingCardSetRepository;
		this.configurationEntityStatusService = configurationEntityStatusService;
		this.electionEventContextPayloadService = electionEventContextPayloadService;
		this.votingCardSetPrecomputationPersistenceService = votingCardSetPrecomputationPersistenceService;
	}

	public void preComputeVerificationCardSet(final VerificationCardSet precomputeContext)
			throws ResourceNotFoundException {
		final String electionEventId = precomputeContext.electionEventId();
		final String verificationCardSetId = precomputeContext.verificationCardSetId();
		final String votingCardSetId = precomputeContext.votingCardSetId();

		checkArgument(electionEventService.exists(electionEventId), "The election event id of the given context does not exist.");

		LOGGER.debug("Starting pre-computation of voting card set... [electionEventId: {}, verificationCardSetId: {}, votingCardSetId: {}]",
				electionEventId, verificationCardSetId, votingCardSetId);

		final int numberOfVotingCardsToGenerate = votingCardSetRepository.getNumberOfVotingCards(electionEventId, votingCardSetId);
		LOGGER.debug(
				"Generating voting cards... [numberOfVotingCardsToGenerate: {}, electionEventId: {}, verificationCardSetId: {}, votingCardSetId: {}]",
				numberOfVotingCardsToGenerate, electionEventId, verificationCardSetId, votingCardSetId);

		final List<GenVerDatOutput> genVerDatOutputs = genVerDatService.genVerDat(precomputeContext, numberOfVotingCardsToGenerate);

		final GqGroup encryptionGroup = electionEventContextPayloadService.loadEncryptionGroup(precomputeContext.electionEventId());
		checkState(genVerDatOutputs.stream().parallel().map(GenVerDatOutput::getGroup).allMatch(group -> group.equals(encryptionGroup)),
				"The group from the GenVerDat outputs does not correspond to the encryption group.");

		final String ballotBoxesByElectionEvent = ballotBoxService.listByElectionEvent(electionEventId);
		final String ballotId = ballotBoxService.getBallotId(precomputeContext.ballotBoxId());
		final String ballotAlias = ballotBoxService.listAliases(ballotId).get(0);

		// Build and persist payloads to request the return code generation (choice return codes and vote cast return code) from the control components.
		votingCardSetPrecomputationPersistenceService.persistPreComputationPayloads(precomputeContext, genVerDatOutputs, ballotId, ballotAlias,
				ballotBoxesByElectionEvent);

		LOGGER.info("Successfully generated voting cards. [numberGenerated: {}, electionEventId: {}, verificationCardSetId: {}, votingCardSetId: {}]",
				numberOfVotingCardsToGenerate, electionEventId, verificationCardSetId, votingCardSetId);

		// Update the voting card set status to 'pre-computed'.
		configurationEntityStatusService.update(Status.PRECOMPUTED.name(), votingCardSetId, votingCardSetRepository);
		LOGGER.info("Updated voting card set status to pre-computed. [electionEventId: {}, verificationCardSetId: {}, votingCardSetId: {}]",
				electionEventId, verificationCardSetId, votingCardSetId);
	}

}

