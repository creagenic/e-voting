/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.preconfigure;

import static ch.post.it.evoting.evotinglibraries.domain.validations.EncryptionParametersSeedValidation.validateSeed;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.security.SignatureException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.PrimeGqElement;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionEventContext;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.evotinglibraries.domain.signature.SignedPayload;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting.GenSetupDataOutput;
import ch.post.it.evoting.securedatamanager.shared.process.BallotBoxRepository;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;
import ch.post.it.evoting.securedatamanager.shared.process.ResourceNotFoundException;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;

@Service
@ConditionalOnProperty("role.isSetup")
public class ElectionEventContextPersistenceService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElectionEventContextPersistenceService.class);

	private final BallotBoxRepository ballotBoxRepository;
	private final ElectionEventService electionEventService;
	private final VotingCardSetRepository votingCardSetRepository;
	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final ElectionEventContextPayloadService electionEventContextPayloadService;

	public ElectionEventContextPersistenceService(
			final BallotBoxRepository ballotBoxRepository,
			final ElectionEventService electionEventService,
			final VotingCardSetRepository votingCardSetRepository,
			final SignatureKeystore<Alias> signatureKeystoreService,
			final ElectionEventContextPayloadService electionEventContextPayloadService) {
		this.ballotBoxRepository = ballotBoxRepository;
		this.electionEventService = electionEventService;
		this.votingCardSetRepository = votingCardSetRepository;
		this.signatureKeystoreService = signatureKeystoreService;
		this.electionEventContextPayloadService = electionEventContextPayloadService;
	}

	/**
	 * Persists the election event context.
	 *
	 * @param electionEventId    the election event id for which to persist the context. Must be non-null and a valid UUID.
	 * @param seed               the encryption parameters seed. Must be non-null and a valid seed.
	 * @param genSetupDataOutput the output of the GenSetupData algorithm as a {@link GenSetupDataOutput}. Must be non-null.
	 */
	public void persist(final String electionEventId, final String seed, final GenSetupDataOutput genSetupDataOutput) {
		validateUUID(electionEventId);
		validateSeed(seed);
		checkNotNull(genSetupDataOutput);

		LOGGER.debug("Building Verification Card Set Contexts... [electionEventId: {}]", electionEventId);

		final List<VerificationCardSetContext> verificationCardSetContexts = buildVerificationCardSetContexts(electionEventId, genSetupDataOutput);

		LOGGER.debug("Built Verification Card Set Contexts. [electionEventId: {}]", electionEventId);

		final String electionEventAlias = electionEventService.getElectionEventAlias(electionEventId);
		final String electionEventDescription = electionEventService.getElectionEventDescription(electionEventId);
		final LocalDateTime startTime = electionEventService.getDateFrom(electionEventId);
		final LocalDateTime finishTime = electionEventService.getDateTo(electionEventId);
		final int maximumNumberOfVotingOptions = genSetupDataOutput.getMaximumNumberOfVotingOptions();
		final int maximumNumberOfSelections = genSetupDataOutput.getMaximumNumberOfSelections();
		final int maximumNumberOfWriteInsPlusOne = genSetupDataOutput.getMaximumNumberOfWriteInsPlusOne();
		final ElectionEventContext electionEventContext = new ElectionEventContext(electionEventId, electionEventAlias, electionEventDescription,
				verificationCardSetContexts, startTime, finishTime, maximumNumberOfVotingOptions, maximumNumberOfSelections,
				maximumNumberOfWriteInsPlusOne);

		final GqGroup encryptionGroup = genSetupDataOutput.getEncryptionGroup();
		final GroupVector<PrimeGqElement, GqGroup> smallPrimes = genSetupDataOutput.getSmallPrimes();
		final ElectionEventContextPayload electionEventContextPayload = createElectionEventContextPayload(encryptionGroup, seed, smallPrimes,
				electionEventContext);
		electionEventContextPayloadService.save(electionEventContextPayload);

		LOGGER.info("Built and persisted the election event context payload. [electionEventId: {}]", electionEventId);
	}

	/**
	 * Builds the list of {@link VerificationCardSetContext} for the given election event id.
	 *
	 * @param electionEventId    the election event id. Must be non-null and a valid UUID.
	 * @param genSetupDataOutput the output of the GenSetupData algorithm as a {@link GenSetupDataOutput}. Must be non-null.
	 * @return the list of {@link VerificationCardSetContext}.
	 * @throws NullPointerException      if the input is null.
	 * @throws FailedValidationException if the input is not a valid UUID.
	 */
	private List<VerificationCardSetContext> buildVerificationCardSetContexts(final String electionEventId,
			final GenSetupDataOutput genSetupDataOutput) {
		validateUUID(electionEventId);
		checkNotNull(genSetupDataOutput);

		final List<String> votingCardSetIds = votingCardSetRepository.findAllVotingCardSetIds(electionEventId);
		final Map<String, PrimesMappingTable> primesMappingTables = genSetupDataOutput.getPrimesMappingTables();

		return votingCardSetIds.stream()
				.parallel()
				.map(votingCardSetId -> {
					final String verificationCardSetId = votingCardSetRepository.getVerificationCardSetId(votingCardSetId);
					final String votingCardSetAlias = votingCardSetRepository.getVotingCardSetAlias(votingCardSetId);
					final String votingCardSetDefaultDescription = votingCardSetRepository.getVotingCardSetDefaultDescription(votingCardSetId);
					final String ballotBoxId = votingCardSetRepository.getBallotBoxId(votingCardSetId);
					final LocalDateTime ballotBoxDateFrom = ballotBoxRepository.getDateFrom(ballotBoxId);
					final LocalDateTime ballotBoxDateTo = ballotBoxRepository.getDateTo(ballotBoxId);

					final PrimesMappingTable primesMappingTable = primesMappingTables.get(verificationCardSetId);

					final boolean testBallotBox = ballotBoxRepository.isTestBallotBox(ballotBoxId);
					final int gracePeriod = ballotBoxRepository.getGracePeriod(ballotBoxId);

					final int numberOfVotingCards;
					try {
						numberOfVotingCards = votingCardSetRepository.getNumberOfVotingCards(electionEventId, votingCardSetId);
					} catch (final ResourceNotFoundException e) {
						throw new IllegalStateException(
								String.format("Number of voting cards not found. [electionEventId: %s, votingCardSetId: %s]", electionEventId,
										votingCardSetId));
					}

					return new VerificationCardSetContext.Builder()
							.setVerificationCardSetId(verificationCardSetId)
							.setVerificationCardSetAlias(votingCardSetAlias)
							.setVerificationCardSetDescription(votingCardSetDefaultDescription)
							.setBallotBoxId(ballotBoxId)
							.setBallotBoxStartTime(ballotBoxDateFrom)
							.setBallotBoxFinishTime(ballotBoxDateTo)
							.setTestBallotBox(testBallotBox)
							.setNumberOfVotingCards(numberOfVotingCards)
							.setGracePeriod(gracePeriod)
							.setPrimesMappingTable(primesMappingTable)
							.build();
				})
				.toList();
	}

	private ElectionEventContextPayload createElectionEventContextPayload(final GqGroup group, final String seed,
			final GroupVector<PrimeGqElement, GqGroup> smallPrimes, final ElectionEventContext electionEventContext) {
		final String electionEventId = electionEventContext.electionEventId();
		final ElectionEventContextPayload electionEventContextPayload = new ElectionEventContextPayload(group, seed, smallPrimes,
				electionEventContext);

		final Hashable additionalContextData = ChannelSecurityContextData.electionEventContext(electionEventId);

		final CryptoPrimitivesSignature electionEventContextPayloadSignature = getPayloadSignature(electionEventContextPayload,
				additionalContextData);
		electionEventContextPayload.setSignature(electionEventContextPayloadSignature);

		return electionEventContextPayload;
	}

	private CryptoPrimitivesSignature getPayloadSignature(final SignedPayload payload, final Hashable additionalContextData) {

		final byte[] signature;
		try {
			signature = signatureKeystoreService.generateSignature(payload, additionalContextData);
		} catch (final SignatureException e) {
			throw new IllegalStateException(String.format("Failed to generate payload signature. [name: %s]", payload.getClass().getName()));
		}

		return new CryptoPrimitivesSignature(signature);
	}
}
