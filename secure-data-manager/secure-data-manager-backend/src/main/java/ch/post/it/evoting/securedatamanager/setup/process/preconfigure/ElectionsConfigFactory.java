/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.preconfigure;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static ch.post.it.evoting.evotinglibraries.domain.election.ElectionAttributesAliasConstants.ALIAS_JOIN_DELIMITER;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.hasNoDuplicates;
import static ch.post.it.evoting.securedatamanager.setup.process.preconfigure.BallotContestFactory.createContest;
import static ch.post.it.evoting.securedatamanager.setup.process.preconfigure.TranslationsFactory.Translation;
import static ch.post.it.evoting.securedatamanager.setup.process.preconfigure.TranslationsFactory.createTranslationsWithoutBallotId;
import static ch.post.it.evoting.securedatamanager.setup.process.preconfigure.TranslationsFactory.getInLanguage;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static java.util.function.Predicate.not;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.MoreCollectors;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.election.Ballot;
import ch.post.it.evoting.evotinglibraries.domain.election.BallotBox;
import ch.post.it.evoting.evotinglibraries.domain.election.Contest;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionEvent;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectoralBoard;
import ch.post.it.evoting.evotinglibraries.domain.election.Identifier;
import ch.post.it.evoting.evotinglibraries.domain.election.VotingCardSet;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.AuthorizationObjectType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.AuthorizationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.CandidatePositionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.CandidateType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ContestDescriptionInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ContestType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.DomainOfInfluenceType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionGroupBallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectoralBoardType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.LanguageType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ListType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.StandardBallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.TieBreakQuestionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VariantBallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.WriteInCandidateType;
import ch.post.it.evoting.securedatamanager.setup.process.preconfigure.BallotContestFactory.ContestWithPosition;
import ch.post.it.evoting.securedatamanager.shared.JsonConstants;

/**
 * Creates the elections_config.json from the configuration-anonymized.xml and generates the election ids.
 */
public class ElectionsConfigFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElectionsConfigFactory.class);
	private static final String STATUS_LOCKED = "LOCKED";
	private static final String RESOURCE_TYPE_CONFIGURATION = "configuration";

	private static final Random random = RandomFactory.createRandom();
	private static final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private ElectionsConfigFactory() {
		// static usage only.
	}

	/**
	 * Creates the elections_config file and generates the election ids.
	 *
	 * @param configuration the {@link Configuration} representing the configuration-anonymized. Must be non-null.
	 * @return the elections_config as a {@link ObjectNode}.
	 */
	public static ObjectNode createElectionsConfig(final Configuration configuration) {
		LOGGER.debug("Creating the elections_config...");
		checkNotNull(configuration);

		final List<AuthorizationType> authorizationTypes = configuration.getAuthorizations().getAuthorization();

		final ObjectNode electionEventId = generateIdObjectFor(JsonConstants.ELECTION_EVENT);
		final ObjectNode electoralBoardId = generateIdObjectFor(JsonConstants.ELECTORAL_BOARD);
		final Map<String, AuthorizationTypeIds> authorizationTypeIdsMap = generateAuthorizationTypeIds(authorizationTypes);
		final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap = generateVoteTypeAttributeIdsMap(configuration);
		final Map<String, ElectionTypeAttributeIds> electionTypeAttributeIdsMap = generateElectionTypeAttributeIds(configuration);

		final Map<String, List<ContestWithPosition>> domainOfInfluenceToContests = createContest(configuration, voteTypeAttributeIdsMap,
				electionTypeAttributeIdsMap);
		final Map<String, List<Translation>> domainOfInfluenceToTranslations = createTranslationsWithoutBallotId(configuration,
				voteTypeAttributeIdsMap, domainOfInfluenceToContests, electionTypeAttributeIdsMap);

		final ArrayNode electionEvents = createElectionEvents(configuration, electionEventId);
		final ArrayNode settings = createSettings(electionEventId);
		final ArrayNode ballots = createBallots(authorizationTypes, electionEventId, authorizationTypeIdsMap, domainOfInfluenceToContests);
		final ArrayNode ballotBoxes = createBallotBoxes(authorizationTypes, electionEventId, authorizationTypeIdsMap, electoralBoardId);
		final ArrayNode votingCardSets = createVotingCardSets(configuration, electionEventId, authorizationTypeIdsMap);
		final ArrayNode electoralBoards = createElectoralBoards(configuration, electionEventId, electoralBoardId);
		final ArrayNode translations = createTranslations(authorizationTypes, authorizationTypeIdsMap, domainOfInfluenceToTranslations);

		final ObjectNode electionsConfig = createGenericElectionsConfig(electionEvents, settings, ballots, ballotBoxes, votingCardSets,
				electoralBoards, translations);

		LOGGER.info("Successfully created the elections_config.");

		return electionsConfig;
	}

	private static Map<String, AuthorizationTypeIds> generateAuthorizationTypeIds(final List<AuthorizationType> authorizationTypes) {
		return authorizationTypes.stream()
				.collect(Collectors.toMap(
						AuthorizationType::getAuthorizationIdentification,
						authorizationType -> {
							final ObjectNode ballotId = generateIdObjectFor(JsonConstants.BALLOT);
							final ObjectNode ballotBoxId = generateIdObjectFor(JsonConstants.BALLOT_BOX);
							final String votingCardSetId = random.genRandomString(ID_LENGTH, base16Alphabet);

							return new AuthorizationTypeIds(ballotId, ballotBoxId, votingCardSetId);
						}));
	}

	private static ObjectNode generateIdObjectFor(final String object) {
		return objectMapper.createObjectNode()
				.set(object, objectMapper.createObjectNode()
						.put(JsonConstants.ID, random.genRandomString(ID_LENGTH, base16Alphabet)));
	}

	private static Map<String, VoteTypeAttributeIds> generateVoteTypeAttributeIdsMap(final Configuration configuration) {
		return configuration.getContest().getVoteInformation().stream()
				.map(VoteInformationType::getVote)
				.map(VoteType::getBallot)
				.flatMap(List::stream)
				.map(ballotType -> {
					final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap = new HashMap<>();

					final StandardBallotType standardBallot = ballotType.getStandardBallot();
					if (standardBallot != null) {
						final StandardBallotTypeAdapter standardBallotTypeAdapter = new StandardBallotTypeAdapter(standardBallot);
						voteTypeAttributeIdsMap.putAll(generateVoteTypeAttributeIds(Stream.of(standardBallotTypeAdapter)));
					}

					final VariantBallotType variantBallot = ballotType.getVariantBallot();
					if (variantBallot != null) {
						final Stream<QuestionType> standardQuestionTypeAdapterStream = variantBallot.getStandardQuestion().stream()
								.parallel()
								.map(StandardQuestionTypeAdapter::new);
						voteTypeAttributeIdsMap.putAll(generateVoteTypeAttributeIds(standardQuestionTypeAdapterStream));

						final List<TieBreakQuestionType> tieBreakQuestion = variantBallot.getTieBreakQuestion();
						if (tieBreakQuestion != null) {
							final Stream<QuestionType> tieBreakQuestionAdapter = tieBreakQuestion.stream()
									.parallel()
									.map(TieBreakQuestionTypeAdapter::new);
							voteTypeAttributeIdsMap.putAll(generateVoteTypeAttributeIds(tieBreakQuestionAdapter));
						}
					}

					return voteTypeAttributeIdsMap;
				}).flatMap(map -> map.entrySet().stream())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}

	private static Map<String, VoteTypeAttributeIds> generateVoteTypeAttributeIds(final Stream<QuestionType> questions) {
		return questions
				.collect(Collectors.toMap(
						QuestionType::getQuestionIdentification,
						question -> {
							final String attributeQuestionId = random.genRandomString(ID_LENGTH, base16Alphabet);
							final Map<String, String> answerTypeAttributeId = question.getAnswer().stream()
									.collect(Collectors.toMap(
											AnswerType::getAnswerIdentification,
											ignored -> random.genRandomString(ID_LENGTH, base16Alphabet)));
							return new VoteTypeAttributeIds(attributeQuestionId, answerTypeAttributeId);
						}));
	}

	private static Map<String, ElectionTypeAttributeIds> generateElectionTypeAttributeIds(final Configuration configuration) {
		return configuration.getContest().getElectionGroupBallot().stream().parallel()
				.map(ElectionGroupBallotType::getElectionInformation)
				.flatMap(Collection::stream)
				.collect(Collectors.toConcurrentMap(
						electionInformationType -> electionInformationType.getElection().getElectionIdentification(),
						electionInformationType -> {
							// generate an attribute id for the attribute candidates
							final String candidatesAttributeId = random.genRandomString(ID_LENGTH, base16Alphabet);

							// generate an attribute id for all candidates
							final Map<String, String> candidateAttributeIdsMap = generateCandidateAttributeIdsMap(electionInformationType);

							// generate an attribute id for the attribute lists if there is at least one non-empty list
							final boolean electionWithLists = electionInformationType.getList().stream().parallel()
									.anyMatch(not(ListType::isListEmpty));
							final String listsAttributeId = electionWithLists ? random.genRandomString(ID_LENGTH, base16Alphabet) : null;

							final Map<String, String> listAttributeIdsMap = electionWithLists
									// generate an attribute id for all lists if there is at least one non-empty list
									? electionInformationType.getList().stream().parallel().collect(Collectors.toMap(
									ListType::getListIdentification,
									ignored -> random.genRandomString(ID_LENGTH, base16Alphabet)))
									// generate an attribute for all candidate's dummy lists otherwise
									: electionInformationType.getCandidate().stream().parallel().collect(Collectors.toMap(
									CandidateType::getCandidateIdentification,
									ignored -> random.genRandomString(ID_LENGTH, base16Alphabet))
							);

							return new ElectionTypeAttributeIds(candidatesAttributeId, candidateAttributeIdsMap, listsAttributeId,
									listAttributeIdsMap);
						}));
	}

	private static Map<String, String> generateCandidateAttributeIdsMap(final ElectionInformationType electionInformationType) {
		// generate an attribute id for all candidate identifications considering accumulation
		final ElectionType electionType = electionInformationType.getElection();
		final int candidateAccumulation = electionType.getCandidateAccumulation().intValue();
		final Map<String, String> candidateAttributeIdsMap = electionInformationType.getCandidate().stream()
				.flatMap(candidateType ->
						IntStream.range(0, candidateAccumulation)
								.mapToObj(String::valueOf)
								.map(acc -> String.join(ALIAS_JOIN_DELIMITER, electionType.getElectionIdentification(),
										candidateType.getCandidateIdentification(), acc))
				).collect(Collectors.toMap(candidateAlias -> candidateAlias, ignored -> random.genRandomString(ID_LENGTH, base16Alphabet)));

		// generate an attribute id for all blank candidates
		candidateAttributeIdsMap.putAll(generateBlankCandidateAttributeIdsMap(electionInformationType));

		// generate an attribute id for all write-in candidates if write-ins allowed
		candidateAttributeIdsMap.putAll(generateWriteInCandidateAttributeIdsMap(electionInformationType));

		return candidateAttributeIdsMap;
	}

	private static Map<String, String> generateBlankCandidateAttributeIdsMap(final ElectionInformationType electionInformationType) {
		final List<CandidatePositionType> emptyListCandidatePositions = electionInformationType.getList().stream()
				.filter(ListType::isListEmpty)
				.collect(MoreCollectors.onlyElement())
				.getCandidatePosition();

		final Map<String, String> candidateAttributeIdsMap = emptyListCandidatePositions.stream()
				.map(CandidatePositionType::getCandidateListIdentification)
				.distinct()
				.map(candidateListIdentification -> {
					final String blankCandidateAttributeId = random.genRandomString(ID_LENGTH, base16Alphabet);
					return Map.of(candidateListIdentification, blankCandidateAttributeId);
				})
				.flatMap(map -> map.entrySet().stream())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		final String electionIdentification = electionInformationType.getElection().getElectionIdentification();
		checkState(candidateAttributeIdsMap.size() == emptyListCandidatePositions.size(),
				"The empty list candidate position identifications must be unique. [electionIdentification: %s]",
				electionIdentification);

		return candidateAttributeIdsMap;
	}

	private static Map<String, String> generateWriteInCandidateAttributeIdsMap(final ElectionInformationType electionInformationType) {
		final ElectionType election = electionInformationType.getElection();
		final boolean atLeastOneWriteInCandidate = !electionInformationType.getWriteInCandidate().isEmpty();
		checkState(election.isWriteInsAllowed() == atLeastOneWriteInCandidate,
				"If write-ins are allowed then the election must contain at least one write-in candidate, otherwise it must contain none.");
		checkState(hasNoDuplicates(electionInformationType.getWriteInCandidate().stream()
						.map(WriteInCandidateType::getWriteInCandidateIdentification).toList()),
				"The write-in candidate identifications must be unique.");
		checkState(!election.isWriteInsAllowed() || (electionInformationType.getWriteInCandidate().size() == election.getNumberOfMandates()),
				"There must be as many write-in candidates as number of mandates in the election. [electionIdentification: %s]",
				election.getElectionIdentification());

		return electionInformationType.getWriteInCandidate().stream()
				.map(WriteInCandidateType::getWriteInCandidateIdentification)
				.distinct()
				.map(writeInCandidateIdentification -> {
					final String writeInAlias = String.join(ALIAS_JOIN_DELIMITER, election.getElectionIdentification(),
							writeInCandidateIdentification);
					final String writeInCandidateAttributeId = random.genRandomString(ID_LENGTH, base16Alphabet);
					return Map.of(writeInAlias, writeInCandidateAttributeId);
				})
				.flatMap(map -> map.entrySet().stream())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}

	private static ArrayNode createElectionEvents(final Configuration configuration, final ObjectNode electionEventId) {
		final ContestType contestType = configuration.getContest();
		final String contestDescription = getInLanguage(LanguageType.DE, contestType.getContestDescription().getContestDescriptionInfo(),
				ContestDescriptionInformationType.ContestDescriptionInfo::getLanguage,
				ContestDescriptionInformationType.ContestDescriptionInfo::getContestDescription);
		final int gracePeriod = configuration.getAuthorizations().getAuthorization().get(0).getAuthorizationGracePeriod();

		final ElectionEvent electionEvent = new ElectionEvent.ElectionEventBuilder()
				.setId(electionEventId.get(JsonConstants.ELECTION_EVENT).get(JsonConstants.ID).asText())
				.setDefaultTitle(contestDescription)
				.setDefaultDescription(contestDescription)
				.setAlias(contestType.getContestIdentification())
				.setDateFrom(toLocalDateTime(contestType.getEvotingFromDate()))
				.setDateTo(toLocalDateTime(contestType.getEvotingToDate()))
				.setGracePeriod(gracePeriod)
				.build();

		final JsonNode electionEventsNode;
		try {
			electionEventsNode = objectMapper.readTree(objectMapper.writeValueAsBytes(List.of(electionEvent)));
		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot serialize the ElectionEvent object to a JsonNode.", e);
		}

		return (ArrayNode) electionEventsNode;
	}

	private static ArrayNode createSettings(final ObjectNode electionEventId) {
		return objectMapper.createArrayNode()
				.add(objectMapper.createObjectNode()
						.setAll(electionEventId));
	}

	private static ArrayNode createBallots(final List<AuthorizationType> authorizationTypes, final ObjectNode electionEventId,
			final Map<String, AuthorizationTypeIds> authorizationTypeIdsMap,
			final Map<String, List<ContestWithPosition>> domainOfInfluenceToContests) {
		return IntStream.range(0, authorizationTypes.size())
				.mapToObj(index -> {
					final AuthorizationType authorizationType = authorizationTypes.get(index);
					final AuthorizationTypeIds authorizationTypeIds = authorizationTypeIdsMap.get(authorizationType.getAuthorizationIdentification());
					final ObjectNode ballotId = authorizationTypeIds.ballotId();
					return createBallot(authorizationType, electionEventId, ballotId, index + 1, domainOfInfluenceToContests);
				})
				.reduce(objectMapper.createArrayNode(), ArrayNode::add, ArrayNode::addAll);
	}

	private static JsonNode createBallot(final AuthorizationType authorizationType, final ObjectNode electionEventId, final ObjectNode ballotId,
			final int index, final Map<String, List<ContestWithPosition>> domainOfInfluenceToContests) {
		final Identifier electionEvent = new Identifier(electionEventId.get(JsonConstants.ELECTION_EVENT).get(JsonConstants.ID).asText());
		final List<Contest> contests = authorizationType.getAuthorizationObject().stream()
				.map(AuthorizationObjectType::getDomainOfInfluence)
				.map(DomainOfInfluenceType::getDomainOfInfluenceIdentification)
				.flatMap(domainOfInfluenceIdentification -> checkNotNull(domainOfInfluenceToContests.get(domainOfInfluenceIdentification),
						"The domain of influence id of the authorization object does not exist.").stream())
				.sorted(Comparator.comparingInt(ContestWithPosition::position)
						.thenComparing(contestWithPosition -> contestWithPosition.contest().template()))
				.map(ContestWithPosition::contest)
				.toList();

		final Ballot ballot = new Ballot(ballotId.get(JsonConstants.BALLOT).get(JsonConstants.ID).asText(),
				Ballot.PREFIX + authorizationType.getAuthorizationName(),
				Ballot.PREFIX + authorizationType.getAuthorizationAlias(),
				Ballot.PREFIX + index,
				STATUS_LOCKED,
				electionEvent,
				contests);

		final JsonNode ballotNode;
		try {
			ballotNode = objectMapper.readTree(objectMapper.writeValueAsBytes(ballot));
		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot serialize the Ballot object to a JsonNode.", e);
		}

		return ballotNode;
	}

	private static ArrayNode createBallotBoxes(final List<AuthorizationType> authorizationTypes, final ObjectNode electionEventId,
			final Map<String, AuthorizationTypeIds> authorizationTypeIdsMap, final ObjectNode electoralBoardId) {
		return authorizationTypes.stream()
				.map(authorizationType -> {
					final AuthorizationTypeIds authorizationTypeIds = authorizationTypeIdsMap.get(authorizationType.getAuthorizationIdentification());
					final ObjectNode ballotId = authorizationTypeIds.ballotId();
					final ObjectNode ballotBoxId = authorizationTypeIds.ballotBoxId();
					return createBallotBox(authorizationType, electionEventId, ballotId, ballotBoxId, electoralBoardId);
				})
				.reduce(objectMapper.createArrayNode(), ArrayNode::add, ArrayNode::addAll);
	}

	private static JsonNode createBallotBox(final AuthorizationType authorizationType, final ObjectNode electionEventId,
			final ObjectNode ballotId, final ObjectNode ballotBoxId, final ObjectNode electoralBoardId) {
		final Identifier electionEvent = new Identifier(electionEventId.get(JsonConstants.ELECTION_EVENT).get(JsonConstants.ID).asText());
		final Identifier ballot = new Identifier(ballotId.get(JsonConstants.BALLOT).get(JsonConstants.ID).asText());
		final Identifier electoralBoard = new Identifier(electoralBoardId.get(JsonConstants.ELECTORAL_BOARD).get(JsonConstants.ID).asText());

		final BallotBox ballotBox = new BallotBox.BallotBoxBuilder()
				.setId(ballotBoxId.get(JsonConstants.BALLOT_BOX).get(JsonConstants.ID).asText())
				.setDefaultTitle(authorizationType.getAuthorizationName())
				.setDefaultDescription(authorizationType.getAuthorizationAlias())
				.setAlias(authorizationType.getAuthorizationIdentification())
				.setDateFrom(toLocalDateTime(authorizationType.getAuthorizationFromDate()))
				.setDateTo(toLocalDateTime(authorizationType.getAuthorizationToDate()))
				.setTest(authorizationType.isAuthorizationTest())
				.setStatus(STATUS_LOCKED)
				.setGracePeriod(authorizationType.getAuthorizationGracePeriod())
				.setElectionEvent(electionEvent)
				.setBallot(ballot)
				.setElectoralBoard(electoralBoard)
				.build();

		final JsonNode ballotBoxNode;
		try {
			ballotBoxNode = objectMapper.readTree(objectMapper.writeValueAsBytes(ballotBox));
		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot serialize the BallotBox object to a JsonNode.", e);
		}

		return ballotBoxNode;
	}

	private static LocalDateTime toLocalDateTime(final XMLGregorianCalendar xmlGregorianCalendar) {
		return OffsetDateTime.parse(xmlGregorianCalendar.toXMLFormat(), DateTimeFormatter.ISO_DATE_TIME)
				.atZoneSameInstant(ZoneId.systemDefault())
				.toLocalDateTime();
	}

	private static ArrayNode createVotingCardSets(final Configuration configuration, final ObjectNode electionEventId,
			final Map<String, AuthorizationTypeIds> authorizationTypeIdsMap) {
		return configuration.getAuthorizations().getAuthorization().stream()
				.map(authorizationType -> {
					final AuthorizationTypeIds authorizationTypeIds = authorizationTypeIdsMap.get(authorizationType.getAuthorizationIdentification());
					final ObjectNode ballotBoxId = authorizationTypeIds.ballotBoxId();
					final String votingCardSetId = authorizationTypeIds.votingCardSetId();

					return createVotingCardSet(configuration, authorizationType, electionEventId, ballotBoxId, votingCardSetId);
				})
				.reduce(objectMapper.createArrayNode(), ArrayNode::add, ArrayNode::addAll);
	}

	private static JsonNode createVotingCardSet(final Configuration configuration, final AuthorizationType authorizationType,
			final ObjectNode electionEventId, final ObjectNode ballotBoxId, final String votingCardSetId) {
		final int numberOfVotingCardsToGenerate = (int) configuration.getRegister().getVoter().stream()
				.filter(voterType -> voterType.getAuthorization().equals(authorizationType.getAuthorizationIdentification()))
				.count();
		final Identifier electionEvent = new Identifier(electionEventId.get(JsonConstants.ELECTION_EVENT).get(JsonConstants.ID).asText());
		final Identifier ballotBox = new Identifier(ballotBoxId.get(JsonConstants.BALLOT_BOX).get(JsonConstants.ID).asText());

		final VotingCardSet votingCardSet = new VotingCardSet.VotingCardSetBuilder()
				.setId(votingCardSetId)
				.setDefaultTitle(VotingCardSet.PREFIX + authorizationType.getAuthorizationName())
				.setDefaultDescription(VotingCardSet.PREFIX + authorizationType.getAuthorizationAlias())
				.setAlias(VotingCardSet.PREFIX + authorizationType.getAuthorizationIdentification())
				.setNumberOfVotingCardsToGenerate(numberOfVotingCardsToGenerate)
				.setVerificationCardSetId(random.genRandomString(ID_LENGTH, base16Alphabet))
				.setStatus(STATUS_LOCKED)
				.setElectionEvent(electionEvent)
				.setBallotBox(ballotBox)
				.build();

		final JsonNode votingCardSetNode;
		try {
			votingCardSetNode = objectMapper.readTree(objectMapper.writeValueAsBytes(votingCardSet));
		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot serialize the VotingCardSet object to a JsonNode.", e);
		}

		return votingCardSetNode;
	}

	private static ArrayNode createElectoralBoards(final Configuration configuration, final ObjectNode electionEventId,
			final ObjectNode electoralBoardId) {
		final ElectoralBoardType electoralBoardType = configuration.getContest().getElectoralBoard();
		final List<String> boardMembers = electoralBoardType.getElectoralBoardMembers().getElectoralBoardMemberName();
		final Identifier electionEvent = new Identifier(electionEventId.get(JsonConstants.ELECTION_EVENT).get(JsonConstants.ID).asText());

		final ElectoralBoard electoralBoard = new ElectoralBoard.ElectoralBoardBuilder()
				.setId(electoralBoardId.get(JsonConstants.ELECTORAL_BOARD).get(JsonConstants.ID).asText())
				.setDefaultTitle(electoralBoardType.getElectoralBoardName())
				.setDefaultDescription(electoralBoardType.getElectoralBoardDescription())
				.setAlias(electoralBoardType.getElectoralBoardIdentification())
				.setStatus(STATUS_LOCKED)
				.setBoardMembers(boardMembers)
				.setElectionEvent(electionEvent)
				.build();

		final JsonNode electoralBoardNode;
		try {
			electoralBoardNode = objectMapper.readTree(objectMapper.writeValueAsBytes(List.of(electoralBoard)));
		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot serialize the ElectoralBoard object to a JsonNode.", e);
		}

		return (ArrayNode) electoralBoardNode;
	}

	private static ArrayNode createTranslations(final List<AuthorizationType> authorizationTypes,
			final Map<String, AuthorizationTypeIds> authorizationTypeIdsMap, final Map<String, List<Translation>> domainOfInfluenceToTranslations) {
		return authorizationTypes.stream()
				.map(authorizationType -> {
					final AuthorizationTypeIds authorizationTypeIds = authorizationTypeIdsMap.get(authorizationType.getAuthorizationIdentification());
					final ObjectNode ballotId = authorizationTypeIds.ballotId();
					return addBallotIdToTranslations(authorizationType, ballotId, domainOfInfluenceToTranslations);
				})
				.reduce(objectMapper.createArrayNode(), ArrayNode::addAll);
	}

	private static ArrayNode addBallotIdToTranslations(final AuthorizationType authorizationType, final ObjectNode ballotId,
			final Map<String, List<Translation>> domainOfInfluenceToTranslations) {
		// group by locale the translations belonging to the same domain of influence
		final Map<String, List<Translation>> localeToTranslations = authorizationType.getAuthorizationObject().stream()
				.map(AuthorizationObjectType::getDomainOfInfluence)
				.map(DomainOfInfluenceType::getDomainOfInfluenceIdentification)
				.flatMap(domainOfInfluenceType -> checkNotNull(domainOfInfluenceToTranslations.get(domainOfInfluenceType),
						"The domain of influence id of the authorization object does not exist.").stream())
				.collect(Collectors.groupingBy(Translation::locale));

		// merge the texts of the translations belonging to the same domain of influence and locale
		return localeToTranslations.keySet().stream()
				.map(locale -> new Translation(locale, localeToTranslations.get(locale).stream()
						.map(Translation::texts)
						.reduce(objectMapper.createObjectNode(), ObjectNode::setAll)))
				.map(translation -> createGenericTranslation(translation.texts(), translation.locale(), ballotId))
				.reduce(objectMapper.createArrayNode(), ArrayNode::add, ArrayNode::addAll);
	}

	private static ObjectNode createGenericTranslation(final ObjectNode texts, final String locale, final ObjectNode ballotId) {
		return ((ObjectNode) objectMapper.createObjectNode()
				.put(JsonConstants.LOCALE, locale)
				.put(JsonConstants.STATUS, STATUS_LOCKED)
				.set(JsonConstants.TEXTS, texts))
				.setAll(ballotId);
	}

	private static ObjectNode createGenericElectionsConfig(final ArrayNode electionEvents, final ArrayNode settings, final ArrayNode ballots,
			final ArrayNode ballotBoxes, final ArrayNode votingCardSets, final ArrayNode electoralBoards, final ArrayNode translations) {

		final ObjectNode electionsConfig = objectMapper.createObjectNode();
		electionsConfig.put(JsonConstants.RESOURCE_TYPE, RESOURCE_TYPE_CONFIGURATION);
		electionsConfig.set(JsonConstants.ELECTION_EVENTS, electionEvents);
		electionsConfig.set(JsonConstants.SETTINGS, settings);
		electionsConfig.set(JsonConstants.BALLOTS, ballots);
		electionsConfig.set(JsonConstants.BALLOT_BOXES, ballotBoxes);
		electionsConfig.set(JsonConstants.VOTING_CARD_SETS, votingCardSets);
		electionsConfig.set(JsonConstants.ELECTORAL_BOARDS, electoralBoards);
		electionsConfig.set(JsonConstants.TRANSLATIONS, translations);

		return electionsConfig;
	}

	record AuthorizationTypeIds(ObjectNode ballotId, ObjectNode ballotBoxId, String votingCardSetId) {
	}

	record VoteTypeAttributeIds(String attributeQuestionId, Map<String, String> attributeAnswerId) {
	}

	/**
	 * @param listsAttributeId    null if there are no non-empty lists in the election.
	 * @param listAttributeIdsMap null if there are no non-empty lists in the election.
	 */
	record ElectionTypeAttributeIds(String candidatesAttributeId, Map<String, String> candidateAttributeIdsMap, String listsAttributeId,
									Map<String, String> listAttributeIdsMap) {
	}
}
