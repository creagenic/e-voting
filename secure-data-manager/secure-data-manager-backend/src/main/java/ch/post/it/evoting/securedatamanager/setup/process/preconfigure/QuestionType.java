/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.preconfigure;

import java.util.List;

import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.BallotQuestionType;

public sealed interface QuestionType permits StandardBallotTypeAdapter, StandardQuestionTypeAdapter, TieBreakQuestionTypeAdapter {

	String getQuestionIdentification();

	BallotQuestionType getBallotQuestion();

	String getQuestionNumber();

	List<AnswerType> getAnswer();
}
