/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.preconfigure;

import java.util.List;

import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.BallotQuestionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.StandardBallotType;

/**
 * Adapter for {@link StandardBallotTypeAdapter}.
 */
public final class StandardBallotTypeAdapter implements QuestionType {

	final StandardBallotType standardBallotType;

	public StandardBallotTypeAdapter(final StandardBallotType standardBallotType) {
		this.standardBallotType = standardBallotType;
	}

	@Override
	public String getQuestionIdentification() {
		return standardBallotType.getQuestionIdentification();
	}

	@Override
	public BallotQuestionType getBallotQuestion() {
		return standardBallotType.getBallotQuestion();
	}

	@Override
	public String getQuestionNumber() {
		return standardBallotType.getQuestionNumber();
	}

	public List<AnswerType> getAnswer() {
		return standardBallotType.getAnswer().stream()
				.parallel()
				.<AnswerType>map(StandardAnswerTypeAdapter::new)
				.toList();
	}
}
