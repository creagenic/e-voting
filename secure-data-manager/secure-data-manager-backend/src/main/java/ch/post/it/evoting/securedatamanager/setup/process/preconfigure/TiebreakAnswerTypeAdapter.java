/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.preconfigure;

import java.util.List;

import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.AnswerInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.TiebreakAnswerType;

/**
 * Adapter for {@link TiebreakAnswerType}.
 */
public final class TiebreakAnswerTypeAdapter implements AnswerType {

	final TiebreakAnswerType tiebreakAnswerType;

	public TiebreakAnswerTypeAdapter(TiebreakAnswerType tiebreakAnswerType) {
		this.tiebreakAnswerType = tiebreakAnswerType;
	}

	@Override
	public boolean isBlankAnswer() {
		return Boolean.TRUE.equals(tiebreakAnswerType.isHiddenAnswer());
	}

	@Override
	public String getAnswerIdentification() {
		return tiebreakAnswerType.getAnswerIdentification();
	}

	@Override
	public List<AnswerInformationType> getAnswerInfo() {
		return tiebreakAnswerType.getAnswerInfo();
	}
}
