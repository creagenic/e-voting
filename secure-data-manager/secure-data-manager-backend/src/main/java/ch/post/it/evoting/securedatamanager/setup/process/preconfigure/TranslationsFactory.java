/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.preconfigure;

import static ch.post.it.evoting.evotinglibraries.domain.election.ElectionAttributesAliasConstants.ALIAS_JOIN_DELIMITER;
import static ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.LanguageType.DE;
import static ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.LanguageType.FR;
import static ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.LanguageType.IT;
import static ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.LanguageType.RM;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static java.util.function.Predicate.not;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.MoreCollectors;

import ch.post.it.evoting.evotinglibraries.domain.election.Contest;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.AnswerInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.BallotQuestionType.BallotQuestionInfo;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.CandidatePositionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.CandidateTextInformationType.CandidateTextInfo;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.CandidateType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.DisplayLineInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionDescriptionInformationType.ElectionDescriptionInfo;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionGroupBallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionRulesExplanationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.LanguageType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ListDescriptionInformationType.ListDescriptionInfo;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ListType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.StandardBallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.TieBreakQuestionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VariantBallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteDescriptionInformationType.VoteDescriptionInfo;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteType;
import ch.post.it.evoting.securedatamanager.setup.process.preconfigure.BallotContestFactory.ContestWithPosition;
import ch.post.it.evoting.securedatamanager.setup.process.preconfigure.ElectionsConfigFactory.ElectionTypeAttributeIds;
import ch.post.it.evoting.securedatamanager.setup.process.preconfigure.ElectionsConfigFactory.VoteTypeAttributeIds;
import ch.post.it.evoting.securedatamanager.shared.JsonConstants;

/**
 * Creates the translations of the elections_config.json.
 */
public class TranslationsFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(TranslationsFactory.class);
	private static final String TYPE_TEXT = "text";
	private static final String SUFFIX_LANGUAGE_CH = "-CH";
	private static final String LIST_TYPE_ATTRIBUTE_1_ATTRIBUTE_KEYS = "Party Name";
	private static final ArrayList<String> CANDIDATE_TYPE_ATTRIBUTE_VALUES = new ArrayList<>(List.of(
			"Candidate text",
			"Candidate Text",
			"Incumbent"
	));
	private static final Map<LanguageType, String> CANDIDATE_TYPE_INITIAL_ACCUMULATION_VALUES = Map.of(
			DE, "Initiales Kumulieren",
			FR, "Cumul initial",
			IT, "Cumulo iniziale",
			RM, "Cumular inizial"
	);
	private static final Map<LanguageType, String> CANDIDATE_TYPE_POSITION_ON_LIST_VALUES = Map.of(
			DE, "Initiale Position",
			FR, "Position initiale",
			IT, "Posizione initiale",
			RM, "Posiziun inizial"
	);

	private static final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();

	private TranslationsFactory() {
		// static usage only.
	}

	/**
	 * Creates the translations without ballotId for all elections and votes.
	 *
	 * @param configuration the {@link Configuration} representing the configuration-anonymized. Must be non-null.
	 * @return the mapping of the domain of influence of the election or vote to the translations.
	 */
	public static Map<String, List<Translation>> createTranslationsWithoutBallotId(final Configuration configuration,
			final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap,
			final Map<String, List<ContestWithPosition>> domainOfInfluenceToContests,
			final Map<String, ElectionTypeAttributeIds> electionTypeAttributeIdsMap) {
		checkNotNull(configuration);
		checkNotNull(voteTypeAttributeIdsMap);
		checkNotNull(domainOfInfluenceToContests);
		checkNotNull(electionTypeAttributeIdsMap);

		// Create translations for all elections
		final Map<String, List<Translation>> electionTranslations = configuration.getContest().getElectionGroupBallot().stream()
				.collect(Collectors.groupingBy(
						ElectionGroupBallotType::getDomainOfInfluence,
						Collectors.flatMapping(
								electionGroupBallotType -> electionGroupBallotType.getElectionInformation().stream()
										.flatMap(electionInformationType -> {
											final ElectionTypeAttributeIds electionTypeAttributeIds = checkNotNull(electionTypeAttributeIdsMap
													.get(electionInformationType.getElection().getElectionIdentification()));
											return createElectionTranslations(electionInformationType, domainOfInfluenceToContests,
													electionTypeAttributeIds,
													electionGroupBallotType.getDomainOfInfluence());
										}),
								Collectors.toList())
				));

		// Create translations for all votes
		final Map<String, List<Translation>> voteTranslations = configuration.getContest().getVoteInformation().stream()
				.map(VoteInformationType::getVote)
				.collect(Collectors.groupingBy(
						VoteType::getDomainOfInfluence,
						Collectors.flatMapping(voteType -> createVoteTranslations(voteType, domainOfInfluenceToContests, voteTypeAttributeIdsMap),
								Collectors.toList())
				));

		final Map<String, List<Translation>> domainOfInfluenceToTranslations = mergeTranslations(electionTranslations, voteTranslations);

		LOGGER.info("Successfully created the translations.");

		return domainOfInfluenceToTranslations;
	}

	/**
	 * Gets the description of the given description info class in the desired language.
	 *
	 * @param languageType    a {@link LanguageType}. Must not be null.
	 * @param descriptionInfo the list of description info. Must not be null.
	 * @param getLanguage     the method to get the language from the description info. Must not be null.
	 * @param getDescription  the method to get the description from the description info. Must not be null.
	 * @param <T>             the description info class.
	 * @return the description in the desired language.
	 */
	public static <T> String getInLanguage(final LanguageType languageType, final List<T> descriptionInfo,
			final Function<T, LanguageType> getLanguage, final Function<T, String> getDescription) {
		checkNotNull(languageType);
		checkNotNull(descriptionInfo);
		checkNotNull(getLanguage);
		checkNotNull(getDescription);

		return descriptionInfo.stream()
				.filter(info -> getLanguage.apply(info).equals(languageType))
				.map(getDescription)
				.collect(MoreCollectors.onlyElement());
	}

	private static Stream<Translation> createElectionTranslations(final ElectionInformationType electionInformationType,
			final Map<String, List<ContestWithPosition>> domainOfInfluenceToContests, final ElectionTypeAttributeIds electionTypeAttributeIds,
			final String domainOfInfluence) {
		final ElectionType electionType = electionInformationType.getElection();
		final Map<String, String> candidateAttributeIdsMap = electionTypeAttributeIds.candidateAttributeIdsMap();
		final Map<String, String> listAttributeIdsMap = electionTypeAttributeIds.listAttributeIdsMap();
		final boolean electionWithLists = electionInformationType.getList().stream().anyMatch(not(ListType::isListEmpty));

		return Arrays.stream(LanguageType.values())
				.map(languageType -> {
					final ObjectNode texts = objectMapper.createObjectNode();

					if (electionWithLists) {
						texts.setAll(electionInformationType.getList().stream()
								.filter(not(ListType::isListEmpty))
								.map(listType -> {
									// create a candidate text with list information
									final ObjectNode textsListsElection = createGenericElectionTranslationTextListCandidate(electionInformationType,
											listType.getCandidatePosition(), electionTypeAttributeIds, languageType);

									// create a list text
									final String listAttributeId = listAttributeIdsMap.get(listType.getListIdentification());
									return (ObjectNode) textsListsElection
											.set(listAttributeId, createElectionTranslationTextList(listType, languageType));
								})
								.reduce(objectMapper.createObjectNode(), ObjectNode::setAll));
					} else {
						texts.setAll(electionInformationType.getCandidate().stream()
								.map(candidateType -> {
									// create a candidate text
									final ObjectNode textLists = objectMapper.createObjectNode();
									final String candidateIdentification = candidateType.getCandidateIdentification();
									final ArrayNode textCandidate = createGenericElectionTranslationTextCandidate(candidateType, languageType);
									IntStream.range(0, electionType.getCandidateAccumulation().intValue())
											.mapToObj(String::valueOf)
											.map(acc -> String.join(ALIAS_JOIN_DELIMITER, electionType.getElectionIdentification(),
													candidateIdentification, acc))
											.map(candidateAttributeIdsMap::get)
											.forEach(candidateAttributeId -> textLists.set(candidateAttributeId, textCandidate));

									// create a candidate's dummy list text
									return (ObjectNode) textLists
											.set(listAttributeIdsMap.get(candidateIdentification), objectMapper.createArrayNode()
													.add(objectMapper.createObjectNode().put(JsonConstants.LIST_TYPE_ATTRIBUTE_1,
															textCandidate.get(0).get(JsonConstants.CANDIDATE_TYPE_ATTRIBUTE_1).asText())));
								})
								.reduce(objectMapper.createObjectNode(), ObjectNode::setAll));
					}

					final ObjectNode textContest = createElectionTranslationTextContest(electionInformationType, languageType,
							electionWithLists);
					final String contestId = getContestId(domainOfInfluence, electionType.getElectionIdentification(),
							domainOfInfluenceToContests);
					texts.set(contestId, textContest);

					texts.setAll(createElectionTranslationTextBlank(electionInformationType, electionTypeAttributeIds, languageType,
							electionWithLists));

					return createGenericTranslationWithoutBallotId(texts, languageType);
				});
	}

	private static ObjectNode createGenericElectionTranslationTextListCandidate(final ElectionInformationType electionInformationType,
			final List<CandidatePositionType> candidatePositions, final ElectionTypeAttributeIds electionTypeAttributeIds,
			final LanguageType languageType) {
		final Map<String, List<CandidatePositionType>> candidatePositionMap = candidatePositions.stream()
				.collect(Collectors.groupingBy(CandidatePositionType::getCandidateIdentification));
		final Map<String, String> candidateAttributeIdsMap = electionTypeAttributeIds.candidateAttributeIdsMap();

		return candidatePositions.stream()
				.map(candidatePosition -> {
					final String candidateIdentification = candidatePosition.getCandidateIdentification();
					// find the candidate type corresponding to the candidate position
					final CandidateType candidateType = electionInformationType.getCandidate().stream()
							.filter(candidate -> candidate.getCandidateIdentification().equals(candidateIdentification))
							.collect(MoreCollectors.onlyElement());

					final List<CandidatePositionType> candidatePositionTypes = candidatePositionMap.get(candidateIdentification);
					final String candidateTypeInitialAccumulation = Integer.toString(candidatePositionTypes.size());
					final String candidateTypePositionOnList = candidatePositionTypes.stream()
							.map(CandidatePositionType::getPositionOnList)
							.map(pos -> String.format("%d", pos))
							.collect(Collectors.joining(","));

					final ArrayNode textCandidate = createGenericElectionTranslationTextCandidate(candidateType, languageType);
					textCandidate.addObject().put(JsonConstants.CANDIDATE_TYPE_INITIAL_ACCUMULATION, candidateTypeInitialAccumulation);
					textCandidate.addObject().put(JsonConstants.CANDIDATE_TYPE_POSITION_ON_LIST, candidateTypePositionOnList);

					final ObjectNode textListCandidate = objectMapper.createObjectNode();
					IntStream.range(0, electionInformationType.getElection().getCandidateAccumulation().intValue())
							.mapToObj(String::valueOf)
							.map(acc -> String.join(ALIAS_JOIN_DELIMITER, electionInformationType.getElection().getElectionIdentification(),
									candidateIdentification, acc))
							.map(candidateAttributeIdsMap::get)
							.forEach(candidateAttributeId -> textListCandidate.set(candidateAttributeId, textCandidate));
					return textListCandidate;
				}).reduce(objectMapper.createObjectNode(), ObjectNode::setAll);
	}

	private static ArrayNode createGenericElectionTranslationTextCandidate(final CandidateType candidateType, final LanguageType languageType) {
		final ArrayNode textCandidate = objectMapper.createArrayNode();

		final String candidateTypeAttribute1 = candidateType.getDisplayCandidateLine1().getDisplayLineInfo().stream()
				.filter(displayLineInfo -> displayLineInfo.getLanguage().equals(languageType))
				.map(DisplayLineInformationType.DisplayLineInfo::getDisplayLineText)
				.collect(MoreCollectors.onlyElement());
		textCandidate.addObject().put(JsonConstants.CANDIDATE_TYPE_ATTRIBUTE_1, candidateTypeAttribute1);

		final String candidateTypeAttribute2 = candidateType.getDisplayCandidateLine2().getDisplayLineInfo().stream()
				.filter(displayLineInfo -> displayLineInfo.getLanguage().equals(languageType))
				.map(DisplayLineInformationType.DisplayLineInfo::getDisplayLineText)
				.collect(MoreCollectors.onlyElement());
		textCandidate.addObject().put(JsonConstants.CANDIDATE_TYPE_ATTRIBUTE_2, candidateTypeAttribute2);

		final String candidateTypeAttribute3;
		if (candidateType.getDisplayCandidateLine3() != null) {
			candidateTypeAttribute3 = candidateType.getDisplayCandidateLine3().getDisplayLineInfo().stream()
					.filter(displayLineInfo -> displayLineInfo.getLanguage().equals(languageType))
					.map(DisplayLineInformationType.DisplayLineInfo::getDisplayLineText)
					.collect(MoreCollectors.onlyElement());
		} else {
			candidateTypeAttribute3 = "";
		}
		textCandidate.addObject().put(JsonConstants.CANDIDATE_TYPE_ATTRIBUTE_3, candidateTypeAttribute3);

		final String candidateTypeAttribute4;
		if (candidateType.getDisplayCandidateLine4() != null) {
			candidateTypeAttribute4 = candidateType.getDisplayCandidateLine4().getDisplayLineInfo().stream()
					.filter(displayLineInfo -> displayLineInfo.getLanguage().equals(languageType))
					.map(DisplayLineInformationType.DisplayLineInfo::getDisplayLineText)
					.collect(MoreCollectors.onlyElement());
		} else {
			candidateTypeAttribute4 = "";
		}
		textCandidate.addObject().put(JsonConstants.CANDIDATE_TYPE_ATTRIBUTE_4, candidateTypeAttribute4);

		return textCandidate;
	}

	private static ArrayNode createElectionTranslationTextList(final ListType listType, final LanguageType languageType) {
		final String listTypeAttribute1 = listType.getDisplayListLine1().getDisplayLineInfo().stream()
				.filter(displayLineInfo -> displayLineInfo.getLanguage().equals(languageType))
				.map(DisplayLineInformationType.DisplayLineInfo::getDisplayLineText)
				.collect(MoreCollectors.onlyElement());
		final ArrayNode arrayNode = objectMapper.createArrayNode()
				.add(objectMapper.createObjectNode().put(JsonConstants.LIST_TYPE_ATTRIBUTE_1, listTypeAttribute1));

		if (listType.getDisplayListLine2() != null) {
			final String listTypeAttribute2 = listType.getDisplayListLine2().getDisplayLineInfo().stream()
					.filter(displayLineInfo -> displayLineInfo.getLanguage().equals(languageType))
					.map(DisplayLineInformationType.DisplayLineInfo::getDisplayLineText)
					.collect(MoreCollectors.onlyElement());
			arrayNode.add(objectMapper.createObjectNode().put(JsonConstants.LIST_TYPE_APPARENTMENT, listTypeAttribute2));
		}

		if (listType.getDisplayListLine3() != null) {
			final String listTypeAttribute3 = listType.getDisplayListLine3().getDisplayLineInfo().stream()
					.filter(displayLineInfo -> displayLineInfo.getLanguage().equals(languageType))
					.map(DisplayLineInformationType.DisplayLineInfo::getDisplayLineText)
					.collect(MoreCollectors.onlyElement());
			arrayNode.add(objectMapper.createObjectNode().put(JsonConstants.LIST_TYPE_SOUS_APPARENTMENT, listTypeAttribute3));
		}
		return arrayNode;
	}

	private static ObjectNode createElectionTranslationTextContest(final ElectionInformationType electionInformationType,
			final LanguageType languageType, final boolean electionWithLists) {
		final ElectionType electionType = electionInformationType.getElection();
		final String electionDescription = getInLanguage(languageType, electionType.getElectionDescription().getElectionDescriptionInfo(),
				ElectionDescriptionInfo::getLanguage, ElectionDescriptionInfo::getElectionDescription);
		final boolean hasElectionRulesExplanation = electionType.getElectionRulesExplanation() != null;
		final String electionRulesExplanation = hasElectionRulesExplanation ?
				getInLanguage(languageType, electionType.getElectionRulesExplanation().getDisplayLineInfo(),
						ElectionRulesExplanationType.DisplayLineInfo::getLanguage, ElectionRulesExplanationType.DisplayLineInfo::getDisplayLineText) :
				null;
		final ObjectNode textContest = objectMapper.createObjectNode()
				.put(JsonConstants.TITLE, electionDescription)
				.put(JsonConstants.DESCRIPTION, electionDescription)
				.put(JsonConstants.HOW_TO_VOTE, electionRulesExplanation);

		final ObjectNode attributeKeys = objectMapper.createObjectNode();
		attributeKeys.setAll(IntStream.range(1, 4)
				.mapToObj(i -> {
					checkState(i <= CANDIDATE_TYPE_ATTRIBUTE_VALUES.size(), "Unknown index. [index: %s]", i);
					final String candidateTypeAttribute = CANDIDATE_TYPE_ATTRIBUTE_VALUES.get(i - 1);
					return objectMapper.createObjectNode().put(JsonConstants.CANDIDATE_TYPE_ATTRIBUTE + i, candidateTypeAttribute);
				})
				.reduce(objectMapper.createObjectNode(), ObjectNode::setAll));

		if (electionWithLists) {
			attributeKeys
					.put(JsonConstants.CANDIDATE_TYPE_INITIAL_ACCUMULATION, CANDIDATE_TYPE_INITIAL_ACCUMULATION_VALUES.get(languageType))
					.put(JsonConstants.CANDIDATE_TYPE_POSITION_ON_LIST, CANDIDATE_TYPE_POSITION_ON_LIST_VALUES.get(languageType));
		}

		attributeKeys.put(JsonConstants.LIST_TYPE_ATTRIBUTE_1, LIST_TYPE_ATTRIBUTE_1_ATTRIBUTE_KEYS);

		textContest.set(JsonConstants.ATTRIBUTE_KEYS, attributeKeys);

		return textContest;
	}

	private static ObjectNode createElectionTranslationTextBlank(final ElectionInformationType electionInformationType,
			final ElectionTypeAttributeIds electionTypeAttributeIds, final LanguageType languageType, final boolean electionWithLists) {
		return electionInformationType.getList().stream()
				.filter(ListType::isListEmpty)
				.map(emptyList -> {
					// blank candidate
					final ObjectNode textBlank = objectMapper.createObjectNode();
					emptyList.getCandidatePosition()
							.forEach(emptyListCandidate -> {
								final String blankCandidateId = electionTypeAttributeIds.candidateAttributeIdsMap()
										.get(emptyListCandidate.getCandidateListIdentification());
								final String textCandidate = getInLanguage(languageType,
										emptyListCandidate.getCandidateTextOnPosition().getCandidateTextInfo(),
										CandidateTextInfo::getLanguage, CandidateTextInfo::getCandidateText);
								textBlank.putArray(blankCandidateId).addObject().put(JsonConstants.TEXT, textCandidate);
							});

					// blank list
					if (electionWithLists) {
						final String blankListId = electionTypeAttributeIds.listAttributeIdsMap().get(emptyList.getListIdentification());
						final String textList = getInLanguage(languageType, emptyList.getListDescription().getListDescriptionInfo(),
								ListDescriptionInfo::getLanguage, ListDescriptionInfo::getListDescription);
						final ObjectNode textBlankList = objectMapper.createObjectNode();
						textBlankList.putArray(blankListId).addObject().put(JsonConstants.TEXT, textList);

						textBlank.setAll(textBlankList);
					}

					return textBlank;
				}).collect(MoreCollectors.onlyElement());
	}

	private static Stream<Translation> createVoteTranslations(final VoteType voteType,
			final Map<String, List<ContestWithPosition>> domainOfInfluenceToContests,
			final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap) {
		return Arrays.stream(LanguageType.values())
				.map(languageType -> {
					final ObjectNode texts = voteType.getBallot().stream()
							.map(ballotType -> {
								final ObjectNode textQuestionAnswer = objectMapper.createObjectNode();

								final StandardBallotType standardBallotType = ballotType.getStandardBallot();
								if (standardBallotType != null) {
									final StandardBallotTypeAdapter standardBallotTypeAdapter = new StandardBallotTypeAdapter(standardBallotType);
									textQuestionAnswer.setAll(
											createVoteTranslationText(Stream.of(standardBallotTypeAdapter), voteTypeAttributeIdsMap, languageType));
								}

								final VariantBallotType variantBallot = ballotType.getVariantBallot();
								if (variantBallot != null) {
									final Stream<QuestionType> standardQuestionTypeAdapterStream = variantBallot.getStandardQuestion().stream()
											.map(StandardQuestionTypeAdapter::new);
									textQuestionAnswer.setAll(
											createVoteTranslationText(standardQuestionTypeAdapterStream, voteTypeAttributeIdsMap, languageType));

									final List<TieBreakQuestionType> tieBreakQuestion = variantBallot.getTieBreakQuestion();
									if (tieBreakQuestion != null) {
										final Stream<QuestionType> tieBreakQuestionAdapter = tieBreakQuestion.stream()
												.map(TieBreakQuestionTypeAdapter::new);
										textQuestionAnswer.setAll(
												createVoteTranslationText(tieBreakQuestionAdapter, voteTypeAttributeIdsMap, languageType));
									}
								}

								return textQuestionAnswer;
							})
							.reduce(objectMapper.createObjectNode(), ObjectNode::setAll);

					final ObjectNode textContest = createVoteTranslationTextContest(voteType, languageType);
					texts.set(getContestId(voteType.getDomainOfInfluence(), voteType.getVoteIdentification(), domainOfInfluenceToContests),
							textContest);

					return createGenericTranslationWithoutBallotId(texts, languageType);
				});
	}

	private static ObjectNode createVoteTranslationText(final Stream<QuestionType> questions,
			final Map<String, VoteTypeAttributeIds> voteTypeAttributeIdsMap, final LanguageType languageType) {
		return questions
				.map(question -> {
					final String questionIdentification = question.getQuestionIdentification();
					final VoteTypeAttributeIds attributeIds = voteTypeAttributeIdsMap.get(questionIdentification);

					final ObjectNode texts = createVoteTranslationTextAnswer(question.getAnswer().stream(), attributeIds, languageType);

					final String attributeQuestionId = attributeIds.attributeQuestionId();
					final String ballotQuestion = getInLanguage(languageType,
							question.getBallotQuestion().getBallotQuestionInfo(),
							BallotQuestionInfo::getLanguage, BallotQuestionInfo::getBallotQuestion);

					final ArrayNode textQuestion = objectMapper.createArrayNode()
							.add(objectMapper.createObjectNode()
									.put(JsonConstants.QUESTION_TYPE_TEXT, ballotQuestion));

					return (ObjectNode) texts.set(attributeQuestionId, textQuestion);
				}).reduce(objectMapper.createObjectNode(), ObjectNode::setAll);
	}

	private static ObjectNode createVoteTranslationTextAnswer(final Stream<AnswerType> answers,
			final VoteTypeAttributeIds voteTypeAttributeIds, final LanguageType languageType) {
		return answers
				.map(answer -> {
					final String attributeAnswerId = voteTypeAttributeIds.attributeAnswerId().get(answer.getAnswerIdentification());
					final String answerInfo = getInLanguage(languageType, answer.getAnswerInfo(), AnswerInformationType::getLanguage,
							AnswerInformationType::getAnswer);

					final ArrayNode textAnswer = objectMapper.createArrayNode();
					textAnswer.add(objectMapper.createObjectNode()
							.put(answer.isBlankAnswer() ? JsonConstants.TEXT : JsonConstants.ANSWER_TYPE_TEXT, answerInfo));

					return (ObjectNode) objectMapper.createObjectNode()
							.set(attributeAnswerId, textAnswer);
				}).reduce(objectMapper.createObjectNode(), ObjectNode::setAll);
	}

	private static ObjectNode createVoteTranslationTextContest(final VoteType voteType, final LanguageType languageType) {
		final String voteDescription = getInLanguage(languageType, voteType.getVoteDescription().getVoteDescriptionInfo(),
				VoteDescriptionInfo::getLanguage, VoteDescriptionInfo::getVoteDescription);
		final ObjectNode textContest = objectMapper.createObjectNode()
				.put(JsonConstants.TITLE, voteDescription)
				.put(JsonConstants.DESCRIPTION, voteDescription)
				.put(JsonConstants.HOW_TO_VOTE, (String) null);
		final ObjectNode attributeKeys = objectMapper.createObjectNode()
				.put(JsonConstants.ANSWER_TYPE_TEXT, TYPE_TEXT)
				.put(JsonConstants.QUESTION_TYPE_TEXT, TYPE_TEXT);
		textContest.set(JsonConstants.ATTRIBUTE_KEYS, attributeKeys);
		return textContest;
	}

	private static String getContestId(final String domainOfInfluence, final String identification,
			final Map<String, List<ContestWithPosition>> domainOfInfluenceToContests) {
		final List<ContestWithPosition> contests = checkNotNull(domainOfInfluenceToContests.get(domainOfInfluence),
				"The contest for the given domain of influence does not exist.");
		return contests.stream()
				.map(ContestWithPosition::contest)
				.filter(contest -> contest.alias().equals(identification))
				.map(Contest::id)
				.collect(MoreCollectors.onlyElement());
	}

	private static Map<String, List<Translation>> mergeTranslations(Map<String, List<Translation>> electionTranslations,
			Map<String, List<Translation>> voteTranslations) {
		return Stream.of(electionTranslations, voteTranslations)
				.flatMap(map -> map.entrySet().stream())
				.collect(Collectors.toMap(
						Map.Entry::getKey,
						entry -> new ArrayList<>(entry.getValue()),
						(translationsLeft, translationsRight) -> {
							translationsLeft.addAll(translationsRight);
							return translationsLeft;
						})
				);
	}

	private static Translation createGenericTranslationWithoutBallotId(final ObjectNode texts, final LanguageType languageType) {
		return new Translation(languageType.value() + SUFFIX_LANGUAGE_CH, texts);
	}

	record Translation(String locale, ObjectNode texts) {
	}
}
