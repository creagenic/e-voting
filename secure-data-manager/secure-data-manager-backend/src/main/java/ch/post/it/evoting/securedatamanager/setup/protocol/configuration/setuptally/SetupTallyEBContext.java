/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setuptally;

import static ch.post.it.evoting.evotinglibraries.domain.VotingOptionsConstants.MAXIMUM_SUPPORTED_NUMBER_OF_WRITE_INS;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.cryptoprimitives.math.GqGroup;

/**
 * Regroups the context needed by the SetupTallyEB algorithm.
 *
 * <ul>
 *     <li>(p, q, g), the encryption group. Not null.</li>
 *     <li>ee, the election event id. Not null and a valid UUID.</li>
 *     <li>&delta;<sub>max</sub>, the maximum number of write-ins plus one. In range [1, &delta;<sub>sup</sub>].</li>
 * </ul>
 */
public record SetupTallyEBContext(GqGroup encryptionGroup, String electionEventId, int maximumNumberOfWriteInsPlusOne) {

	public SetupTallyEBContext {
		checkNotNull(encryptionGroup);
		validateUUID(electionEventId);
		checkArgument(maximumNumberOfWriteInsPlusOne > 0, "The maximum number of write-ins plus one must be strictly positive.");

		final int delta_sup = MAXIMUM_SUPPORTED_NUMBER_OF_WRITE_INS + 1;
		checkArgument(maximumNumberOfWriteInsPlusOne <= delta_sup,
				"The maximum number of write-ins plus one must be smaller or equal to the maximum supported number of write-ins plus one. [delta_max: %s, delta_sup: %s]",
				maximumNumberOfWriteInsPlusOne, delta_sup);
	}
}
