/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import com.google.common.base.Preconditions;

/**
 * Regroups the output needed by the GenCredDat algorithm.
 *
 * <ul>
 *     <li>VCks, the vector of verification card keystores. Not null.</li>
 * </ul>
 */
public record GenCredDatOutput(List<String> verificationCardKeystores) {

	public GenCredDatOutput {
		verificationCardKeystores = checkNotNull(verificationCardKeystores).stream()
				.map(Preconditions::checkNotNull)
				.toList();
		checkArgument(!verificationCardKeystores.isEmpty(), "The vector of verification card keystores must not be empty.");
	}
}
