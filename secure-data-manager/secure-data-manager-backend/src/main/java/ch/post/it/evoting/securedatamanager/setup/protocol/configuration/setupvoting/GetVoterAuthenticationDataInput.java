/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import com.google.common.base.Preconditions;

import ch.post.it.evoting.evotinglibraries.domain.validations.StartVotingKeyValidation;

/**
 * Regroups the input values needed by the GetVoterAuthenticationData algorithm.
 *
 * <ul>
 *     <li>SVK, the vector of start voting keys. Non-null and contains N<sub>E</sub> valid Base32 strings of size l<sub>SVK</sub>.</li>
 *     <li>EA, the vector of extended authentication factors. Non-null and contains N<sub>E</sub> digit sequences of size l<sub>EA</sub>.</li>
 * </ul>
 */
public record GetVoterAuthenticationDataInput(List<String> startVotingKeys, List<String> extendedAuthenticationFactors) {

	public GetVoterAuthenticationDataInput {
		checkNotNull(startVotingKeys);
		startVotingKeys = startVotingKeys.stream().map(StartVotingKeyValidation::validate).toList();
		checkArgument(!startVotingKeys.isEmpty(), "The start voting keys must not be empty.");

		checkNotNull(extendedAuthenticationFactors).forEach(Preconditions::checkNotNull);
		extendedAuthenticationFactors = List.copyOf(extendedAuthenticationFactors);
		checkArgument(!extendedAuthenticationFactors.isEmpty(), "The extended authentication factors must not be empty.");

		checkArgument(startVotingKeys.size() == extendedAuthenticationFactors.size(),
				"There must be as many extended authentication factors as start voting keys.");
	}

	@Override
	public List<String> extendedAuthenticationFactors() {
		return List.copyOf(extendedAuthenticationFactors);
	}

	@Override
	public List<String> startVotingKeys() {
		return List.copyOf(startVotingKeys);
	}
}
