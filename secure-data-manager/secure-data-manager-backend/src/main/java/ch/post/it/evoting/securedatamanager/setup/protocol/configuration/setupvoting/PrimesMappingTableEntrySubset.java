/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import ch.post.it.evoting.evotinglibraries.domain.validations.ActualVotingOptionsValidation;
import ch.post.it.evoting.evotinglibraries.domain.validations.CorrectnessInformationValidation;
import ch.post.it.evoting.evotinglibraries.domain.validations.SemanticInformationValidation;

/**
 * Represents a subset of a {@link ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTableEntry}.
 *
 * @param actualVotingOption     v<sub>i</sub>, the actual voting option. Must be non-null. It must not be blank and its length must be at most
 *                               {@value ch.post.it.evoting.evotinglibraries.domain.VotingOptionsConstants#MAXIMUM_ACTUAL_VOTING_OPTION_LENGTH}.
 * @param semanticInformation    &#963;<sub>i</sub>, the semantic information related to the voting option. Must be non-null. It must not be blank.
 * @param correctnessInformation &#964;<sub>i</sub>, the correctness information related to the voting option. Must be non-null and a valid
 *                               combination of xml xs:token.
 */
public record PrimesMappingTableEntrySubset(String actualVotingOption, String semanticInformation, String correctnessInformation) {

	public PrimesMappingTableEntrySubset {
		ActualVotingOptionsValidation.validate(actualVotingOption);
		SemanticInformationValidation.validate(semanticInformation);
		CorrectnessInformationValidation.validate(correctnessInformation);
	}
}