/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.database;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.orientechnologies.orient.core.db.ODatabaseType;
import com.orientechnologies.orient.core.db.OrientDB;
import com.orientechnologies.orient.core.db.OrientDBConfig;
import com.orientechnologies.orient.core.metadata.security.OSecurityResourceAll;
import com.orientechnologies.orient.core.metadata.security.OUser;

import ch.post.it.evoting.evotinglibraries.domain.validations.PasswordValidation;

/**
 * Factory of {@link DatabaseManager} for a single local database.
 *
 * <p>
 * It is the caller's responsibility to ensure that there is only one factory per database url.
 * </p>
 *
 * <p>
 * This implementation is thread safe.
 * </p>
 */
@Component
public final class DatabaseManagerFactory implements AutoCloseable {

	private static final String DEFAULT_USERNAME = OUser.ADMIN;

	private final OrientDB orientDB;
	private final ODatabaseType databaseType;
	private final String password;

	/**
	 * Create a DatabaseManagerFactory.
	 *
	 * @param databaseType a non-null string value representing the database type. It can take values from {@link ODatabaseType}.
	 * @param databasePath the non-null path to the directory where database instances are stored. Can be empty.
	 * @param password     the non-null password for the admin user.
	 */
	public DatabaseManagerFactory(
			@Value("${database.type}")
			final String databaseType,
			@Value("${database.path:}")
			final Path databasePath,
			@Value("${database.password}")
			final String password) throws IOException {
		checkNotNull(databaseType);
		checkNotNull(databasePath);
		checkNotNull(password);
		PasswordValidation.validate(password.toCharArray(), "database");
		checkArgument(Files.isDirectory(databasePath.toRealPath(LinkOption.NOFOLLOW_LINKS)),
				"The given database path is not a directory. [path: %s]", databasePath);

		this.password = password;
		this.databaseType = ODatabaseType.valueOf(databaseType.toUpperCase(Locale.ENGLISH));
		this.orientDB = new OrientDB(databaseType + ":" + databasePath, OrientDBConfig.builder()
				.addGlobalUser(DEFAULT_USERNAME, this.password, OSecurityResourceAll.INSTANCE.getResourceString())
				.build());
	}

	/**
	 * Create a DatabaseManager for the given database.
	 *
	 * @param databaseName The name of the database to be managed.
	 * @return a DatabaseManager for the given database.
	 */
	public DatabaseManager newDatabaseManager(final String databaseName) {
		checkNotNull(databaseName);
		return new DatabaseManager(orientDB, databaseType, databaseName, DEFAULT_USERNAME, this.password);
	}

	/**
	 * Close the database connection. This will close all open databases.
	 */
	@Override
	public void close() {
		orientDB.close();
	}

}
