/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.shared.Constants.VOTER_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.UncheckedIOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jakarta.json.Json;
import jakarta.json.JsonArray;
import jakarta.json.JsonObject;
import jakarta.json.JsonObjectBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.evotinglibraries.domain.election.BallotBox;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.shared.JsonConstants;
import ch.post.it.evoting.securedatamanager.shared.database.DatabaseException;

/**
 * Service to operate with ballot boxes.
 */
@Service
public class BallotBoxService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BallotBoxService.class);

	private final ObjectMapper objectMapper;
	private final BallotBoxRepository ballotBoxRepository;
	private final ConfigurationEntityStatusService statusService;
	private final boolean isVoterPortalEnabled;

	public BallotBoxService(
			final ObjectMapper objectMapper,
			final BallotBoxRepository ballotBoxRepository,
			final ConfigurationEntityStatusService statusService,
			@Value("${voter.portal.enabled}")
			final boolean isVoterPortalEnabled) {
		this.objectMapper = objectMapper;
		this.ballotBoxRepository = ballotBoxRepository;
		this.statusService = statusService;
		this.isVoterPortalEnabled = isVoterPortalEnabled;
	}

	/**
	 * Sign the ballot box configuration and change the state of the ballot box from ready to SIGNED for a given ballot box id.
	 *
	 * @param ballotBoxId the ballot box id.
	 */
	public void sign(final String ballotBoxId) {
		validateUUID(ballotBoxId);

		statusService.updateWithSynchronizedStatus(BallotBoxStatus.SIGNED.name(), ballotBoxId, ballotBoxRepository, SynchronizeStatus.PENDING);
		LOGGER.info("Ballot box status signed. [ballotBoxId: {}]", ballotBoxId);
	}

	public void signBallotBoxes(final String electionEventId) {
		validateUUID(electionEventId);
		this.getBallotBoxesId(electionEventId).forEach(this::sign);
	}

	public void updateStatuses(final String electionEventId) {
		validateUUID(electionEventId);
		checkState(isVoterPortalEnabled, VOTER_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE);

		final JsonArray ballotBoxes = getBallotBoxesReadyToSynchronize(electionEventId);

		for (int i = 0; i < ballotBoxes.size(); i++) {

			final JsonObject ballotBox = ballotBoxes.getJsonObject(i);

			final String ballotBoxId = ballotBox.getString(JsonConstants.ID);

			setStatusToSynchronized(ballotBoxId);
		}
	}

	/**
	 * Query ballot boxes to process in the synchronization process.
	 *
	 * @return JsonArray with ballot boxes to upload
	 */
	private JsonArray getBallotBoxesReadyToSynchronize(final String electionEventId) {
		final Map<String, Object> params = new HashMap<>();

		params.put(JsonConstants.STATUS, BallotBoxStatus.SIGNED.name());
		params.put(JsonConstants.SYNCHRONIZED, SynchronizeStatus.PENDING.getIsSynchronized().toString());
		params.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEventId);
		final String serializedBallotBoxes = ballotBoxRepository.list(params);

		return JsonUtils.getJsonObject(serializedBallotBoxes).getJsonArray(JsonConstants.RESULT);
	}

	/**
	 * Updates the state of the synchronization status of the ballot box
	 */
	private void setStatusToSynchronized(final String ballotBoxId) {
		final JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
		jsonObjectBuilder.add(JsonConstants.ID, ballotBoxId);
		jsonObjectBuilder.add(JsonConstants.SYNCHRONIZED, SynchronizeStatus.SYNCHRONIZED.getIsSynchronized().toString());
		jsonObjectBuilder.add(JsonConstants.DETAILS, SynchronizeStatus.SYNCHRONIZED.getStatus());
		ballotBoxRepository.update(jsonObjectBuilder.build().toString());
	}

	/**
	 * Checks that the ballot box has status {@link BallotBoxStatus#DOWNLOADED}.
	 */
	public boolean isDownloaded(final String ballotBoxId) {
		validateUUID(ballotBoxId);

		return hasStatus(ballotBoxId, BallotBoxStatus.DOWNLOADED);
	}

	/**
	 * Checks that the ballot box has {@code expectedStatus}
	 *
	 * @throws IllegalStateException if the ballot box cannot be found in the ballot box repository or the subsequent retrieved JSON can't be
	 *                               correctly parsed.
	 */
	public boolean hasStatus(final String ballotBoxId, final BallotBoxStatus expectedStatus) {
		validateUUID(ballotBoxId);
		checkNotNull(expectedStatus);

		final BallotBoxStatus actualStatus = getBallotBoxStatus(ballotBoxId);

		return expectedStatus.equals(actualStatus);
	}

	/**
	 * Gets the status of a ballot box.
	 *
	 * @param ballotBoxId the ballot box id to get the status.
	 * @return the status of the ballot box.
	 * @throws IllegalStateException if the ballot box cannot be found in the ballot box repository or the subsequent retrieved JSON can't be
	 *                               correctly parsed.
	 */
	public BallotBoxStatus getBallotBoxStatus(final String ballotBoxId) {
		validateUUID(ballotBoxId);

		final JsonNode ballotBox = getBallotBox(ballotBoxId);
		final JsonNode status = ballotBox.path("status");
		checkState(!status.isMissingNode(), "Can't find status for [ballotBoxId: %s] ", ballotBoxId);

		final BallotBoxStatus actualStatus;
		try {
			actualStatus = objectMapper.readValue(status.toString(), BallotBoxStatus.class);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(String.format("Can't deserialize the status [ballotBoxId: %s] ", ballotBoxId), e);
		}

		return actualStatus;
	}

	/**
	 * Gets the ballot id associated with this ballot box.
	 */
	public String getBallotId(final String ballotBoxId) {
		validateUUID(ballotBoxId);

		final JsonNode ballotBox = getBallotBox(ballotBoxId);
		final JsonNode id = ballotBox.path(JsonConstants.BALLOT).path(JsonConstants.ID);
		checkState(!id.isMissingNode(), "Can't find id for ballotBox. [ballotBoxId: %s]", ballotBoxId);

		return id.textValue();
	}

	/**
	 * Returns the grace period of the ballot box identified by the given ballotBoxId.
	 *
	 * @param ballotBoxId identifies the ballot box where to search. Must be non-null and a valid UUID.
	 * @return the grace period.
	 * @throws FailedValidationException if the given ballot box is null or not a valid UUID.
	 * @throws IllegalArgumentException  if the found ballot box is a {@link JsonConstants#EMPTY_OBJECT}.
	 * @throws DatabaseException         if no ballot box is found.
	 */
	public int getGracePeriod(final String ballotBoxId) {
		validateUUID(ballotBoxId);
		return ballotBoxRepository.getGracePeriod(ballotBoxId);
	}

	/**
	 * Indicates if the ballot box corresponding to the given ballot box id is a test ballot box.
	 *
	 * @param ballotBoxId the ballot box id. Must be non-null and a valid UUID.
	 * @return true if the corresponding ballot box is a test ballot box, false otherwise.
	 * @throws FailedValidationException if the given ballot box is null or not a valid UUID.
	 * @throws IllegalArgumentException  if the found ballot box is a {@link JsonConstants#EMPTY_OBJECT}.
	 * @throws DatabaseException         if no ballot box is found.
	 */
	public boolean isTestBallotBox(final String ballotBoxId) {
		validateUUID(ballotBoxId);
		return ballotBoxRepository.isTestBallotBox(ballotBoxId);
	}

	/**
	 * Returns the date until which the ballot box identified by the given ballotBoxId is valid.
	 *
	 * @param ballotBoxId identifies the ballot box where to search. Must be non-null and a valid UUID.
	 * @return the ballot box finish date.
	 * @throws NullPointerException      if {@code ballotId} is null.
	 * @throws FailedValidationException if {@code ballotId} is not a valid UUID.
	 */
	public LocalDateTime getDateTo(final String ballotBoxId) {
		validateUUID(ballotBoxId);
		return ballotBoxRepository.getDateTo(ballotBoxId);
	}

	/**
	 * Lists the aliases of the ballot boxes which belongs to the specified ballot.
	 *
	 * @param ballotId the ballot identifier.
	 * @return the aliases.
	 * @throws DatabaseException failed to list aliases.
	 */
	public List<String> listAliases(final String ballotId) {
		validateUUID(ballotId);
		return ballotBoxRepository.listAliases(ballotId);
	}

	/**
	 * Lists the id of the ballot boxes which belongs to the specified ballot.
	 *
	 * @param ballotId the ballot identifier.
	 * @return the ballot box ids.
	 * @DatabaseException failed to list identifiers.
	 */
	public List<String> listBallotBoxIds(final String ballotId) {
		validateUUID(ballotId);
		return ballotBoxRepository.listBallotBoxIds(ballotId);
	}

	/**
	 * Checks if the given ballot box has the {@link BallotBoxStatus#DECRYPTED} status.
	 *
	 * @param ballotBoxId the ballot box id to check.
	 * @return {@code true} if the ballot box has the decrypted status, {@code false} otherwise.
	 * @throws NullPointerException      if {@code ballotBoxId} is null.
	 * @throws FailedValidationException if {@code ballotBoxId} is invalid.
	 */
	public boolean isDecrypted(final String ballotBoxId) {
		validateUUID(ballotBoxId);

		return hasStatus(ballotBoxId, BallotBoxStatus.DECRYPTED);
	}

	/**
	 * Sets the status of the given ballot box to {@link BallotBoxStatus#DECRYPTED}.
	 *
	 * @param ballotBoxId the ballot box id to set the status.
	 * @throws NullPointerException      if {@code ballotBoxId} is null.
	 * @throws FailedValidationException if {@code ballotBoxId} is invalid.
	 */
	public void setDecrypted(final String ballotBoxId) {
		validateUUID(ballotBoxId);

		updateBallotBoxStatus(ballotBoxId, BallotBoxStatus.DECRYPTED);
	}

	/**
	 * Updates the status of a ballot box with {@code newStatus}.
	 *
	 * @param ballotBoxId the ballot box if to update the status.
	 * @param newStatus   the new status of the ballot box.
	 * @return the new status after update.
	 */
	public BallotBoxStatus updateBallotBoxStatus(final String ballotBoxId, final BallotBoxStatus newStatus) {
		validateUUID(ballotBoxId);
		checkNotNull(newStatus);

		final JsonNode ballotBox = getBallotBox(ballotBoxId);

		((ObjectNode) ballotBox).put(JsonConstants.STATUS, newStatus.toString());
		ballotBoxRepository.update(ballotBox.toString());

		return newStatus;
	}

	/**
	 * Retrieves all ballot boxes associated to the election event with {@code electionEventId}.
	 *
	 * @param electionEventId the election event id for which to retrieve the ballot boxes.
	 * @return all ballot boxes as a JSON string.
	 */
	public String getBallotBoxesAsString(final String electionEventId) {
		validateUUID(electionEventId);

		final Map<String, Object> attributeValueMap = new HashMap<>();
		attributeValueMap.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEventId);

		return ballotBoxRepository.list(attributeValueMap);
	}

	public List<BallotBox> getBallotBoxes(final String electionEventId) {
		final Map<String, Object> criteria = new HashMap<>();
		criteria.put(JsonConstants.ELECTION_EVENT_DOT_ID, electionEventId);

		final String ballotBoxDocuments = ballotBoxRepository.list(criteria);

		try {
			final JsonNode resultNode = objectMapper.readTree(ballotBoxDocuments);
			final JsonNode result = resultNode.path("result");
			return Arrays.asList(objectMapper.treeToValue(result, BallotBox[].class));
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(e);
		}
	}

	/**
	 * Retrieves all ballot boxes associated to the election event with {@code electionEventId}.
	 *
	 * @param electionEventId the election event id for which to retrieve the ballot boxes. Must be a valid UUID.
	 * @return all ballot boxes as a JSON string.
	 */
	public List<String> getBallotBoxesId(final String electionEventId) {
		validateUUID(electionEventId);

		final JsonArray ballotBoxResultList = JsonUtils.getJsonObject(getBallotBoxesAsString(electionEventId)).getJsonArray(JsonConstants.RESULT);

		return ballotBoxResultList.stream().map(jsonValue -> jsonValue.asJsonObject().getString(JsonConstants.ID)).toList();
	}

	/**
	 * Retrieves the ballot box with {@code ballotBoxId}.
	 *
	 * @param ballotBoxId the ballot box id to retrieve.
	 * @return the ballot box as a {@link JsonNode}.
	 * @throws UncheckedIOException if the deserialization of the ballot box fails.
	 */
	public JsonNode getBallotBox(final String ballotBoxId) {
		validateUUID(ballotBoxId);
		try {
			return objectMapper.readTree(ballotBoxRepository.find(ballotBoxId));
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(String.format("Failed to deserialize ballot box. [ballotBoxId: %s] ", ballotBoxId), e);
		}
	}

	/**
	 * Lists the ballot boxes corresponding to a specific election event ID.
	 *
	 * @param electionEventId the election event identifier.
	 * @return the ballot boxes in JSON format.
	 * @throws DatabaseException failed to list the ballot boxes.
	 */
	public String listByElectionEvent(final String electionEventId) {
		validateUUID(electionEventId);
		return ballotBoxRepository.listByElectionEvent(electionEventId);
	}

}
