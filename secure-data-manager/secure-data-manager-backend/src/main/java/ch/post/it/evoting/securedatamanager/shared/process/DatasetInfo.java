/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process;

import java.util.List;

public record DatasetInfo(String outputFolder, List<String> filenames) {
}
