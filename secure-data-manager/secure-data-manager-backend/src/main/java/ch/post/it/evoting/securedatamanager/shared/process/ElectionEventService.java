/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.stereotype.Service;

/**
 * This is a service for handling election event entities.
 */
@Service
public class ElectionEventService {

	private final ElectionEventRepository electionEventRepository;

	public ElectionEventService(final ElectionEventRepository electionEventRepository) {
		this.electionEventRepository = electionEventRepository;
	}

	/**
	 * @return the election event alias based on the given id.
	 */
	public String getElectionEventAlias(final String electionEventId) {
		validateUUID(electionEventId);

		return electionEventRepository.getElectionEventAlias(electionEventId);
	}

	/**
	 * @return the election event alias.
	 */
	public String getElectionEventAlias() {
		return electionEventRepository.getElectionEventAlias(findElectionEventId());
	}

	/**
	 * @return the election event description based on the given id.
	 */
	public String getElectionEventDescription(final String electionEventId) {
		validateUUID(electionEventId);

		return electionEventRepository.getElectionEventDescription(electionEventId);
	}

	public LocalDateTime getDateFrom(final String electionEventId) {
		validateUUID(electionEventId);

		return LocalDateTime.parse(electionEventRepository.getDateFrom(electionEventId), DateTimeFormatter.ISO_DATE_TIME);
	}

	public LocalDateTime getDateTo(final String electionEventId) {
		validateUUID(electionEventId);

		return LocalDateTime.parse(electionEventRepository.getDateTo(electionEventId), DateTimeFormatter.ISO_DATE_TIME);
	}

	public boolean exists(final String electionEventId) {
		validateUUID(electionEventId);

		return electionEventRepository.exists(electionEventId);
	}

	public String findElectionEventId() {
		final List<String> electionEventIds = electionEventRepository.listIds();
		if (electionEventIds.isEmpty()) {
			return null;
		}
		if (electionEventIds.size() > 1) {
			throw new IllegalStateException("More than one election event is available in the database.");
		}

		final String electionEventId = electionEventRepository.listIds().get(0);
		return validateUUID(electionEventId);
	}

}
