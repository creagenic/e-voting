/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static java.util.Collections.singletonMap;

import java.util.ArrayList;
import java.util.List;

import jakarta.annotation.PostConstruct;
import jakarta.json.JsonArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.orientechnologies.common.exception.OException;
import com.orientechnologies.orient.core.record.impl.ODocument;

import ch.post.it.evoting.evotinglibraries.domain.election.ElectoralBoard;
import ch.post.it.evoting.securedatamanager.shared.JsonConstants;
import ch.post.it.evoting.securedatamanager.shared.database.DatabaseManager;
import ch.post.it.evoting.securedatamanager.shared.database.DatabaseException;

/**
 * Implementation of the interface which offers operations on the repository of electoral repository.
 */
@Repository
public class ElectoralBoardRepository extends AbstractEntityRepository {

	BallotBoxRepository ballotBoxRepository;

	public ElectoralBoardRepository(
			final DatabaseManager databaseManager,
			final BallotBoxRepository ballotBoxRepository) {
		super(databaseManager);
		this.ballotBoxRepository = ballotBoxRepository;
	}

	@PostConstruct
	@Override
	public void initialize() {
		super.initialize();
	}

	/**
	 * Updates the related ballot box(es).
	 *
	 * @param electoralBoardsIds The list of identifiers of the electoral board where to update the identifiers of the related ballot boxes.
	 */
	public void updateRelatedBallotBox(final List<String> electoralBoardsIds) {
		try {
			for (final String id : electoralBoardsIds) {
				final ODocument board = getDocument(id);
				final List<String> aliases = getBallotBoxAliases(id);
				board.field(JsonConstants.BALLOT_BOX_ALIAS, aliases);
				saveDocument(board);
			}
		} catch (final OException e) {
			throw new DatabaseException("Failed to update related ballot box.", e);
		}
	}

	/**
	 * Lists the electoral boards matching a specific election event ID.
	 *
	 * @param electionEventId the election event identifier
	 * @return the electoral boards in JSON format
	 * @throws DatabaseException failed to list the electoral boards
	 */
	public String listByElectionEvent(final String electionEventId) {
		validateUUID(electionEventId);
		return list(singletonMap("electionEvent.id", electionEventId));
	}

	@Override
	protected String entityName() {
		return ElectoralBoard.class.getSimpleName();
	}

	// Return the aliases of all the ballot boxes for the electoral board
	// identified by electoralBoardId.
	private List<String> getBallotBoxAliases(final String electoralBoardId) {
		final JsonArray ballotBoxesResult = JsonUtils.getJsonObject(ballotBoxRepository.findByElectoralBoard(electoralBoardId))
				.getJsonArray(JsonConstants.RESULT);
		final List<String> ballotBoxIds = new ArrayList<>();
		for (int index = 0; index < ballotBoxesResult.size(); index++) {
			ballotBoxIds.add(ballotBoxesResult.getJsonObject(index).getString(JsonConstants.ALIAS));
		}

		return ballotBoxIds;
	}
}
