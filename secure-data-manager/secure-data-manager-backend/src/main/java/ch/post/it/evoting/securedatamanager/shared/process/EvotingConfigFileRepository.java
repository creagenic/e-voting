/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process;

import java.util.Optional;

import ch.post.it.evoting.evotinglibraries.xml.XmlFileRepository;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;

public abstract class EvotingConfigFileRepository extends XmlFileRepository<Configuration> {

	public abstract void save(final Configuration configuration);

	public abstract Optional<Configuration> load();

}
