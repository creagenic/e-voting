/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateNonBlankUCS;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.shared.Constants.DATE_TIME_FORMAT_PATTERN;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;

import ch.post.it.evoting.evotinglibraries.domain.encryption.StreamedEncryptionDecryptionService;
import ch.post.it.evoting.evotinglibraries.domain.validations.PasswordValidation;

@Service
public class VerifierCollectorService {
	private static final Logger LOGGER = LoggerFactory.getLogger(VerifierCollectorService.class);
	private static final byte[] ASSOCIATED_DATA = new byte[] {};

	private final PathResolver pathResolver;
	private final CompressionService compressionService;
	private final ElectionEventService electionEventService;
	private final ImportExportFileSystemService importExportFileSystemService;
	private final StreamedEncryptionDecryptionService streamedEncryptionDecryptionService;

	private final char[] verifierExportZipPassword;

	public VerifierCollectorService(
			final PathResolver pathResolver,
			final CompressionService compressionService,
			final ElectionEventService electionEventService,
			final ImportExportFileSystemService importExportFileSystemService,
			final StreamedEncryptionDecryptionService streamedEncryptionDecryptionService,
			@Value("${verifier.export.zip.password}")
			final char[] verifierExportZipPassword) {
		this.pathResolver = pathResolver;
		this.compressionService = compressionService;
		this.electionEventService = electionEventService;
		this.importExportFileSystemService = importExportFileSystemService;
		this.streamedEncryptionDecryptionService = streamedEncryptionDecryptionService;
		this.verifierExportZipPassword = verifierExportZipPassword;
		PasswordValidation.validate(this.verifierExportZipPassword, "verifier export zip");
	}

	public void collectDataset(final VerifierExportType verifierExportType, final String electionEventId) {
		checkNotNull(verifierExportType);
		validateUUID(electionEventId);

		final Path zipDirectory = createTemporaryDirectory();

		try {
			final String electionEventAlias = validateNonBlankUCS(electionEventService.getElectionEventAlias(electionEventId));
			importExportFileSystemService.collectForVerifier(verifierExportType, electionEventId, electionEventAlias, zipDirectory);

			final String datasetFilename = getExportFilename(verifierExportType, electionEventId);
			final Path filePath = pathResolver.resolveVerifierOutputPath().resolve(datasetFilename);

			try (final PipedOutputStream zipPipedOutputStream = new PipedOutputStream();
					final PipedInputStream zipPipedInputStream = new PipedInputStream(zipPipedOutputStream);
					final OutputStream outputStream = new FileOutputStream(filePath.toString())) {
				final Thread t = new Thread(() -> {
					try {
						compressionService.zipDirectory(zipPipedOutputStream, zipDirectory);
					} catch (final IOException e) {
						throw new UncheckedIOException(e);
					}
				});
				t.start();

				streamedEncryptionDecryptionService.encrypt(outputStream, zipPipedInputStream, verifierExportZipPassword, ASSOCIATED_DATA);
			}
		} catch (final IOException e) {
			throw new UncheckedIOException("Error during export.", e);
		}

		deleteDirectory(zipDirectory);
	}

	public String getExportFilename(final VerifierExportType verifierExportType, final String electionEventAlias) {
		final String verifierExportTypeName = verifierExportType.toString().toLowerCase(Locale.ENGLISH);

		final LocalDateTime timestamp = LocalDateTime.now();
		final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_PATTERN);
		final String exportTime = timestamp.format(timeFormatter);

		return String.format("dataset-%s-%s-%s.zip", verifierExportTypeName, electionEventAlias, exportTime);
	}

	private Path createTemporaryDirectory() {
		try {
			final Path temporaryDirectory = Files.createTempDirectory(UUID.randomUUID().toString()).toAbsolutePath();

			LOGGER.info("Temporary directory for verifier export created. [path: {}]", temporaryDirectory);

			return temporaryDirectory;
		} catch (final IOException e) {
			throw new UncheckedIOException("Unable to create temporary directory for verifier export.", e);
		}
	}

	private void deleteDirectory(final Path directory) {
		try {
			FileSystemUtils.deleteRecursively(directory);
		} catch (final IOException e) {
			LOGGER.warn("Fail to remove temporary directory for verifier export. [directory: {}]", directory);
		}
	}

}
