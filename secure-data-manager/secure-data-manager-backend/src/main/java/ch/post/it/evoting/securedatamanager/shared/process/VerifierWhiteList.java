/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateNonBlankUCS;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static ch.post.it.evoting.securedatamanager.shared.Constants.CONFIGURATION;
import static com.google.common.base.Preconditions.checkNotNull;

import java.nio.file.Path;
import java.util.List;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

import ch.post.it.evoting.securedatamanager.shared.Constants;

public final class VerifierWhiteList {

	private static final String CONTEXT = "context";
	private static final String SETUP = "setup";
	private static final String TALLY = "tally";

	private VerifierWhiteList() {
		// static usage only
	}

	/**
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @return the list of entries needed to be exported for the Verifier.
	 */
	@SuppressWarnings("java:S1192") // Suppress duplication warning for patterns.
	public static List<VerifierEntry> getList(final VerifierExportType verifierExportType, final String electionEventId,
			final String electionEventAlias) {
		checkNotNull(verifierExportType);
		validateUUID(electionEventId);
		validateNonBlankUCS(electionEventAlias);

		final String folderRegex = "[a-zA-Z0-9]{32}";

		final String verificationCardSetsDirectory = join(electionEventId, Constants.VERIFICATION_CARD_SETS, folderRegex);
		final String ballotBoxesDirectory = join(electionEventId, Constants.BALLOTS, folderRegex, Constants.BALLOT_BOXES);

		final Path contextPath = Path.of(CONTEXT);

		final Path setupPath = Path.of(SETUP);
		final UnaryOperator<Path> verificationCardSetsPath = basePath -> basePath.resolve(Constants.VERIFICATION_CARD_SETS);

		final Path tallyPath = Path.of(TALLY);
		final Path ballotBoxesPath = tallyPath.resolve(Constants.BALLOT_BOXES);
		final UnaryOperator<String> tallyFilesFormat = prefix -> String.format("%s_%s\\.xml", prefix, electionEventAlias);

		switch (verifierExportType) {
		case CONTEXT -> {
			return List.of(
					createEntry(join(electionEventId, "controlComponentPublicKeysPayload\\.[1-4]{1}\\.json"), contextPath, false),
					createEntry(join(electionEventId, "electionEventContextPayload\\.json"), contextPath, false),
					createEntry(join(electionEventId, "setupComponentPublicKeysPayload\\.json"), contextPath, false),

					createEntry(join(CONFIGURATION, "configuration-anonymized\\.xml"), contextPath, false),

					createEntry(join(verificationCardSetsDirectory, "setupComponentTallyDataPayload\\.json"),
							verificationCardSetsPath.apply(contextPath), true)
			);
		}
		case SETUP -> {
			return List.of(
					createEntry(join(verificationCardSetsDirectory, "controlComponentCodeSharesPayload\\.[0-9]+\\.json"),
							verificationCardSetsPath.apply(setupPath), true),
					createEntry(join(verificationCardSetsDirectory, "setupComponentVerificationDataPayload\\.[0-9]+\\.json"),
							verificationCardSetsPath.apply(setupPath), true)
			);
		}
		case TALLY -> {
			return List.of(
					createEntry(join(electionEventId, tallyFilesFormat.apply("evoting-decrypt")), tallyPath, false),
					createEntry(join(electionEventId, tallyFilesFormat.apply("eCH-0110")), tallyPath, false),
					createEntry(join(electionEventId, tallyFilesFormat.apply("eCH-0222")), tallyPath, false),

					createEntry(join(ballotBoxesDirectory, folderRegex, "controlComponentBallotBoxPayload_[1-4]{1}\\.json"), ballotBoxesPath, true),
					createEntry(join(ballotBoxesDirectory, folderRegex, "controlComponentShufflePayload_[1-4]{1}\\.json"), ballotBoxesPath, true),
					createEntry(join(ballotBoxesDirectory, folderRegex, "tallyComponent(Shuffle|Votes)Payload\\.json"), ballotBoxesPath, true)
			);
		}
		default -> throw new IllegalArgumentException("The verifier export type does not exist.");
		}
	}

	private static VerifierEntry createEntry(final String regex, final Path destinationPath, final boolean extendWithLastFolder) {
		return new VerifierEntry(Pattern.compile(regex), destinationPath, extendWithLastFolder);
	}

	private static String join(final String... params) {
		final String folderDelimiter = "/";
		return String.join(folderDelimiter, params);
	}

	/**
	 * Represents an entry to be exported.
	 *
	 * @param pattern                the pattern to match the entry.
	 * @param destinationPath        the destination path.
	 * @param extendWithParentFolder if true, the entry is exported with its parent (last) folder. If false, only the entry itself is exported.
	 */
	public record VerifierEntry(Pattern pattern, Path destinationPath, boolean extendWithParentFolder) {

	}
}
