/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process.summary;

import java.util.Map;

public record AnswerSummary(int answerPosition, Map<String, String> answerInfo) {
}
