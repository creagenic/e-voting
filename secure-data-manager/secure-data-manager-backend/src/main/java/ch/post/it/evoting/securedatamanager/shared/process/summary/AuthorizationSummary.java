/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process.summary;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AuthorizationSummary {

	private final String authorizationId;
	private final String authorizationName;
	private final String authorizationAlias;
	private final boolean isTest;
	private final Date fromDate;
	private final java.util.Date toDate;
	private final long numberOfVoters;
	private final List<AuthorizationObjectSummary> authorizationObjects;

	private AuthorizationSummary(final String authorizationId, final String authorizationName, final String authorizationAlias, final boolean isTest,
			final Date fromDate, final Date toDate, final long numberOfVoters,
			final List<AuthorizationObjectSummary> authorizationObjects) {
		this.authorizationId = authorizationId;
		this.authorizationName = authorizationName;
		this.authorizationAlias = authorizationAlias;
		this.isTest = isTest;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.numberOfVoters = numberOfVoters;
		this.authorizationObjects = authorizationObjects;
	}

	public String getAuthorizationId() {
		return authorizationId;
	}

	public String getAuthorizationName() {
		return authorizationName;
	}

	public String getAuthorizationAlias() {
		return authorizationAlias;
	}

	@JsonProperty("isTest")
	public boolean isTest() {
		return isTest;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public long getNumberOfVoters() {
		return numberOfVoters;
	}

	public List<AuthorizationObjectSummary> getAuthorizationObjects() {
		return authorizationObjects;
	}

	public static class Builder {

		private String authorizationId;
		private String authorizationName;
		private String authorizationAlias;
		private boolean isTest;
		private Date fromDate;
		private Date toDate;
		private long numberOfVoters;
		private List<AuthorizationObjectSummary> authorizationObjects;

		public Builder authorizationId(final String authorizationId) {
			this.authorizationId = authorizationId;
			return this;
		}

		public Builder authorizationName(final String authorization) {
			this.authorizationName = authorization;
			return this;
		}

		public Builder authorizationAlias(final String authorizationAlias) {
			this.authorizationAlias = authorizationAlias;
			return this;
		}

		public Builder isTest(final boolean isTest) {
			this.isTest = isTest;
			return this;
		}

		public Builder fromDate(final Date fromDate) {
			this.fromDate = fromDate;
			return this;
		}

		public Builder toDate(final Date toDate) {
			this.toDate = toDate;
			return this;
		}

		public Builder numberOfVoters(final long numberOfVoters) {
			this.numberOfVoters = numberOfVoters;
			return this;
		}

		public Builder authorizationObjects(final List<AuthorizationObjectSummary> authorizationObjects) {
			this.authorizationObjects = authorizationObjects;
			return this;
		}

		public AuthorizationSummary build() {
			return new AuthorizationSummary(authorizationId, authorizationName, authorizationAlias, isTest, fromDate, toDate, numberOfVoters,
					authorizationObjects);
		}
	}
}
