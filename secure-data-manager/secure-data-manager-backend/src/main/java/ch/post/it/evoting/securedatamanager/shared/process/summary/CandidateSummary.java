/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process.summary;

import java.util.Date;

public class CandidateSummary {

	private final String candidateId;
	private final String familyName;
	private final String firstName;
	private final String callName;
	private final Date dateOfBirth;
	private final boolean isIncumbent;
	private final String referenceOnPosition;
	private final String eligibility;

	private CandidateSummary(final String candidateId, final String familyName, final String firstName, final String callName, final Date dateOfBirth, final boolean isIncumbent, final String referenceOnPosition, final String eligibility) {
		this.candidateId = candidateId;
		this.familyName = familyName;
		this.firstName = firstName;
		this.callName = callName;
		this.dateOfBirth = dateOfBirth;
		this.isIncumbent = isIncumbent;
		this.referenceOnPosition = referenceOnPosition;
		this.eligibility = eligibility;
	}

	public String getCandidateId() {
		return candidateId;
	}

	public String getFamilyName() {
		return familyName;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getCallName() {
		return callName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public boolean isIncumbent() {
		return isIncumbent;
	}

	public String getReferenceOnPosition() {
		return referenceOnPosition;
	}

	public String getEligibility() {
		return eligibility;
	}

	public static class Builder {

		private String candidateId;
		private String familyName;
		private String firstName;
		private String callName;
		private Date dateOfBirth;
		private boolean isIncumbent;
		private String referenceOnPosition;
		private String eligibility;

		public Builder candidateId(final String candidateId) {
			this.candidateId = candidateId;
			return this;
		}

		public Builder familyName(final String familyName) {
			this.familyName = familyName;
			return this;
		}

		public Builder firstName(final String firstName) {
			this.firstName = firstName;
			return this;
		}

		public Builder callName(final String callName) {
			this.callName = callName;
			return this;
		}

		public Builder dateOfBirth(final Date dateOfBirth) {
			this.dateOfBirth = dateOfBirth;
			return this;
		}

		public Builder isIncumbent(final boolean isIncumbent) {
			this.isIncumbent = isIncumbent;
			return this;
		}

		public Builder referenceOnPosition(final String referenceOnPosition) {
			this.referenceOnPosition = referenceOnPosition;
			return this;
		}

		public Builder eligibility(final String eligibility) {
			this.eligibility = eligibility;
			return this;
		}

		public CandidateSummary build() {
			return new CandidateSummary(candidateId, familyName, firstName, callName, dateOfBirth, isIncumbent, referenceOnPosition, eligibility);
		}
	}
}
