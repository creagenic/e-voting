/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process.summary;

import static ch.post.it.evoting.evotinglibraries.domain.validations.EncryptionParametersSeedValidation.validateSeed;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Date;
import java.util.List;
import java.util.Map;

import ch.post.it.evoting.securedatamanager.shared.process.summary.preconfigure.PreconfigureSummary;

public class ConfigurationSummary {

	private final String contestId;
	private final Map<String, String> contestDescription;
	private final Date contestDate;
	private final String electionEventSeed;
	private final Date evotingFromDate;
	private final Date evotingToDate;
	private final int gracePeriod;
	private final int voterTotal;
	private final List<String> extendedAuthenticationType;
	private final ElectoralBoardSummary electoralBoard;
	private final List<ElectionGroupSummary> electionGroups;
	private final List<VoteSummary> votes;
	private final List<AuthorizationSummary> authorizations;
	private final PreconfigureSummary preconfigureSummary;
	private final String configurationSignature;

	/**
	 * Summary of the contest.
	 *
	 * @param contestId             the contest identifier.
	 * @param contestDescription    the contest description.
	 * @param contestDate           the contest date.
	 * @param electionEventSeed     the election event seed.
	 * @param evotingFromDate       the start date of the contest.
	 * @param evotingToDate         the end date of the contest
	 * @param voterTotal            the total number of voters.
	 * @param electionGroups        the election groups.
	 * @param votes                 the votes.
	 * @param authorizations        the authorizations.
	 * @param configurationSignature the signature of the configuration file.
	 */
	private ConfigurationSummary(final String contestId,
			final Map<String, String> contestDescription,
			final Date contestDate,
			final String electionEventSeed,
			final Date evotingFromDate,
			final Date evotingToDate,
			final int gracePeriod,
			final int voterTotal,
			final List<String> extendedAuthenticationType,
			final ElectoralBoardSummary electoralBoard,
			final List<ElectionGroupSummary> electionGroups,
			final List<VoteSummary> votes,
			final List<AuthorizationSummary> authorizations,
			final PreconfigureSummary preconfigureSummary, final String configurationSignature) {
		this.contestId = checkNotNull(contestId);
		this.contestDescription = checkNotNull(contestDescription);
		this.contestDate = checkNotNull(contestDate);
		this.electionEventSeed = validateSeed(electionEventSeed);
		this.evotingFromDate = checkNotNull(evotingFromDate);
		this.evotingToDate = checkNotNull(evotingToDate);
		this.configurationSignature = configurationSignature;
		checkArgument(gracePeriod >= 0, "The grace period must be positive.");
		this.gracePeriod = gracePeriod;
		checkArgument(voterTotal > 0, "There must be at least one voter.");
		this.voterTotal = voterTotal;
		this.extendedAuthenticationType = checkNotNull(extendedAuthenticationType);
		this.electoralBoard = checkNotNull(electoralBoard);
		this.electionGroups = checkNotNull(electionGroups);
		this.votes = checkNotNull(votes);
		this.authorizations = checkNotNull(authorizations);
		this.preconfigureSummary = preconfigureSummary;
	}

	public String getContestId() {
		return contestId;
	}

	public Map<String, String> getContestDescription() {
		return contestDescription;
	}

	public Date getContestDate() {
		return contestDate;
	}

	public String getElectionEventSeed() {
		return electionEventSeed;
	}

	public Date getEvotingFromDate() {
		return evotingFromDate;
	}

	public Date getEvotingToDate() {
		return evotingToDate;
	}

	public int getGracePeriod() {
		return gracePeriod;
	}

	public int getVoterTotal() {
		return voterTotal;
	}

	public List<String> getExtendedAuthenticationType() {
		return extendedAuthenticationType;
	}

	public ElectoralBoardSummary getElectoralBoard() {
		return electoralBoard;
	}

	public List<ElectionGroupSummary> getElectionGroups() {
		return electionGroups;
	}

	public List<VoteSummary> getVotes() {
		return votes;
	}

	public List<AuthorizationSummary> getAuthorizations() {
		return authorizations;
	}

	public PreconfigureSummary getPreconfigureSummary() {
		return preconfigureSummary;
	}

	public String getConfigurationSignature() {
		return configurationSignature;
	}

	public static class Builder {
		private String contestId;
		private Map<String, String> contestDescription;
		private Date contestDate;
		private String electionEventSeed;
		private Date evotingFromDate;
		private Date evotingToDate;
		private int gracePeriod;
		private int voterTotal;
		private List<String> extendedAuthenticationType;
		private ElectoralBoardSummary electoralBoard;
		private List<ElectionGroupSummary> electionGroups;
		private List<VoteSummary> votes;
		private List<AuthorizationSummary> authorizations;
		private PreconfigureSummary preconfigureSummary;
		private String configurationSignature;

		public Builder withContestId(final String contestId) {
			this.contestId = contestId;
			return this;
		}

		public Builder withContestDescription(final Map<String, String> contest) {
			this.contestDescription = contest;
			return this;
		}

		public Builder withContestDate(final Date contestDate) {
			this.contestDate = contestDate;
			return this;
		}

		public Builder withElectionEventSeed(final String electionEventSeed) {
			this.electionEventSeed = electionEventSeed;
			return this;
		}

		public Builder withEvotingFromDate(final Date evotingFromDate) {
			this.evotingFromDate = evotingFromDate;
			return this;
		}

		public Builder withEvotingToDate(final Date evotingToDate) {
			this.evotingToDate = evotingToDate;
			return this;
		}

		public Builder withGracePeriod(final int gracePeriod) {
			this.gracePeriod = gracePeriod;
			return this;
		}

		public Builder withVoterTotal(final int voterTotal) {
			this.voterTotal = voterTotal;
			return this;
		}

		public Builder withExtendedAuthenticationType(final List<String> extendedAuthenticationType) {
			this.extendedAuthenticationType = extendedAuthenticationType;
			return this;
		}

		public Builder withElectoralBoard(final ElectoralBoardSummary electoralBoard) {
			this.electoralBoard = electoralBoard;
			return this;
		}

		public Builder withElectionGroups(final List<ElectionGroupSummary> electionGroups) {
			this.electionGroups = electionGroups;
			return this;
		}

		public Builder withVotes(final List<VoteSummary> votes) {
			this.votes = votes;
			return this;
		}

		public Builder withAuthorizations(final List<AuthorizationSummary> authorizations) {
			this.authorizations = authorizations;
			return this;
		}

		public Builder withPreconfigureSummary(final PreconfigureSummary preconfigureSummary) {
			this.preconfigureSummary = preconfigureSummary;
			return this;
		}

		public Builder withConfigurationSignature(final String configurationSignature) {
			this.configurationSignature = configurationSignature;
			return this;
		}

		public ConfigurationSummary build() {
			return new ConfigurationSummary(contestId, contestDescription, contestDate, electionEventSeed, evotingFromDate, evotingToDate, gracePeriod,
					voterTotal, extendedAuthenticationType, electoralBoard, electionGroups, votes, authorizations, preconfigureSummary, configurationSignature);
		}
	}
}
