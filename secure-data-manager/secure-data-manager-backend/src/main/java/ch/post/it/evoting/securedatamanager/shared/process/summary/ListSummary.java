/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process.summary;

import java.util.List;

public class ListSummary {

	private final String listId;
	private final String listIndentureNumber;
	private final List<Description> listDescription;
	private final int listOrderOfPrecedence;
	private final boolean isEmpty;
	private final List<CandidateSummary> candidates;

	public ListSummary(final String listId, final String listIndentureNumber, final List<Description> listDescription,
			final int listOrderOfPrecedence, final boolean isEmpty, final List<CandidateSummary> candidates) {
		this.listId = listId;
		this.listIndentureNumber = listIndentureNumber;
		this.listDescription = listDescription;
		this.listOrderOfPrecedence = listOrderOfPrecedence;
		this.isEmpty = isEmpty;
		this.candidates = candidates;
	}

	public String getListId() {
		return listId;
	}

	public String getListIndentureNumber() {
		return listIndentureNumber;
	}

	public List<Description> getListDescription() {
		return listDescription;
	}

	public int getListOrderOfPrecedence() {
		return listOrderOfPrecedence;
	}

	public boolean isEmpty() {
		return isEmpty;
	}

	public List<CandidateSummary> getCandidates() {
		return candidates;
	}
}
