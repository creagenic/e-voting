/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process.summary;

import static ch.post.it.evoting.evotinglibraries.domain.validations.EncryptionParametersSeedValidation.validateSeed;
import static ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowStep.PRE_CONFIGURE;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.evotinglibraries.domain.election.ElectionEventContext;
import ch.post.it.evoting.evotinglibraries.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.AnswerInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.AuthorizationObjectType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.AuthorizationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.AuthorizationsType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.BallotQuestionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.BallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.CandidatePositionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ContestDescriptionInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ContestType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionGroupBallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectionType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ElectoralBoardType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.RegisterType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.StandardAnswerType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.StandardBallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VariantBallotType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteDescriptionInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteInformationType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.VoteType;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;
import ch.post.it.evoting.securedatamanager.shared.process.EvotingConfigService;
import ch.post.it.evoting.securedatamanager.shared.process.summary.preconfigure.PreconfigureSummary;
import ch.post.it.evoting.securedatamanager.shared.process.summary.preconfigure.VerificationCardSetSummary;
import ch.post.it.evoting.securedatamanager.shared.workflow.WorkflowService;

@Service
public class SummaryService {

	private final String electionEventSeed;
	private final WorkflowService workflowService;
	private final EvotingConfigService evotingConfigService;
	private final ElectionEventService electionEventService;
	private final ElectionEventContextPayloadService electionEventContextPayloadService;

	public SummaryService(
			@Value("${sdm.election.event.seed}")
			final String electionEventSeed,
			final WorkflowService workflowService,
			final EvotingConfigService evotingConfigService,
			final ElectionEventService electionEventService,
			final ElectionEventContextPayloadService electionEventContextPayloadService) {
		this.electionEventSeed = validateSeed(electionEventSeed);
		this.workflowService = workflowService;
		this.evotingConfigService = evotingConfigService;
		this.electionEventService = electionEventService;
		this.electionEventContextPayloadService = electionEventContextPayloadService;
	}

	/**
	 * Builds the {@link ConfigurationSummary} from the given {@code configuration}.
	 *
	 * @param configuration the configuration anonymized file.
	 * @return the configuration summary.
	 * @throws NullPointerException if {@code configuration} is null.
	 */
	public ConfigurationSummary getConfigurationSummary(final Configuration configuration) {
		checkNotNull(configuration);

		return buildConfigurationSummary(configuration);
	}

	/**
	 * Reads the configuration anonymized XML file and builds the {@link ConfigurationSummary}.
	 *
	 * @return the serialized ConfigurationSummary object.
	 */
	public ConfigurationSummary getConfigurationSummary() {
		final Configuration configuration = evotingConfigService.load();

		checkState(configuration.getHeader().getVoterTotal() == configuration.getRegister().getVoter().size(),
				"The counted number of voters in the configuration-anonymized does not equal the voter total.");

		return buildConfigurationSummary(configuration);
	}

	private ConfigurationSummary buildConfigurationSummary(final Configuration configuration) {
		final ContestType contest = configuration.getContest();
		final String contestIdentification = contest.getContestIdentification();
		final Map<String, String> contestDescription = contest.getContestDescription().getContestDescriptionInfo().stream()
				.collect(Collectors.toMap(contestDescriptionInfo -> contestDescriptionInfo.getLanguage().value(),
						ContestDescriptionInformationType.ContestDescriptionInfo::getContestDescription));
		final XMLGregorianCalendar contestDate = contest.getContestDate();
		final XMLGregorianCalendar evotingFromDate = contest.getEvotingFromDate();
		final XMLGregorianCalendar evotingToDate = contest.getEvotingToDate();
		final int voterTotal = configuration.getHeader().getVoterTotal();
		final List<String> extendedAuthenticationType = contest.getExtendedAuthenticationKeys().getKeyName();

		final ElectoralBoardType electoralBoard = contest.getElectoralBoard();
		final ElectoralBoardSummary electoralBoardSummary = new ElectoralBoardSummary.Builder()
				.electoralBoardId(electoralBoard.getElectoralBoardIdentification())
				.electoralBoardName(electoralBoard.getElectoralBoardName())
				.electoralBoardDescription(electoralBoard.getElectoralBoardDescription())
				.members(electoralBoard.getElectoralBoardMembers().getElectoralBoardMemberName())
				.build();

		final AuthorizationsType authorizationsType = configuration.getAuthorizations();
		final List<AuthorizationType> authorizations = authorizationsType.getAuthorization();
		final List<AuthorizationSummary> authorizationsList = authorizations.stream()
				.map(authorizationType -> getAuthorizationSummary(authorizationType, configuration.getRegister()))
				.toList();
		final List<ElectionGroupSummary> electionGroupList = contest.getElectionGroupBallot().stream()
				.map(electionGroupBallotType -> getElectionGroupSummary(electionGroupBallotType, authorizationsList)).toList();

		final List<VoteSummary> voteList = contest.getVoteInformation().stream()
				.map(voteInformationType -> getVoteSummary(voteInformationType, authorizationsList))
				.toList();

		ConfigurationSummary.Builder configurationSummaryBuilder = new ConfigurationSummary.Builder()
				.withContestId(contestIdentification)
				.withContestDescription(contestDescription)
				.withContestDate(contestDate.toGregorianCalendar().getTime())
				.withElectionEventSeed(electionEventSeed)
				.withEvotingFromDate(evotingFromDate.toGregorianCalendar().getTime())
				.withEvotingToDate(evotingToDate.toGregorianCalendar().getTime())
				.withGracePeriod(authorizations.getFirst().getAuthorizationGracePeriod())
				.withVoterTotal(voterTotal)
				.withExtendedAuthenticationType(extendedAuthenticationType)
				.withElectoralBoard(electoralBoardSummary)
				.withElectionGroups(electionGroupList)
				.withVotes(voteList)
				.withAuthorizations(authorizationsList)
				.withConfigurationSignature(new String(configuration.getSignature(), StandardCharsets.UTF_8));

		if (workflowService.isStepComplete(PRE_CONFIGURE)) {
			final ElectionEventContextPayload electionEventContextPayload = electionEventContextPayloadService.load(
					electionEventService.findElectionEventId());
			final ElectionEventContext electionEventContext = electionEventContextPayload.getElectionEventContext();
			final List<VerificationCardSetSummary> verificationCardSetSummaryList = electionEventContext.verificationCardSetContexts().stream()
					.map(this::getVerificationCardSetSummary)
					.toList();

			final PreconfigureSummary preconfigureSummary = new PreconfigureSummary.Builder()
					.withEncryptionGroup(electionEventContextPayload.getEncryptionGroup())
					.withMaximumNumberOfVotingOptions(electionEventContext.maximumNumberOfVotingOptions())
					.withMaximumNumberOfSelections(electionEventContext.maximumNumberOfSelections())
					.withMaximumNumberOfWriteInsPlusOne(electionEventContext.maximumNumberOfWriteInsPlusOne())
					.withVerificationCardSets(verificationCardSetSummaryList)
					.build();

			configurationSummaryBuilder = configurationSummaryBuilder.withPreconfigureSummary(preconfigureSummary);
		}

		return configurationSummaryBuilder.build();
	}

	private AuthorizationSummary getAuthorizationSummary(final AuthorizationType authorizationType, final RegisterType registerType) {
		final List<AuthorizationObjectType> authorizationObjects = authorizationType.getAuthorizationObject();
		final List<AuthorizationObjectSummary> authorizationObjectList = authorizationObjects.stream()
				.map(authorizationObject -> new AuthorizationObjectSummary.Builder()
						.domainOfInfluenceId(authorizationObject.getDomainOfInfluence().getDomainOfInfluenceIdentification())
						.domainOfInfluenceType(authorizationObject.getDomainOfInfluence().getDomainOfInfluenceType())
						.domainOfInfluenceName(authorizationObject.getDomainOfInfluence().getDomainOfInfluenceName())
						.countingCircleId(authorizationObject.getCountingCircle().getCountingCircleIdentification())
						.countingCircleName(authorizationObject.getCountingCircle().getCountingCircleName())
						.build())
				.toList();

		final long voterCount = registerType.getVoter().stream()
				.filter(voterType -> voterType.getAuthorization().equals(authorizationType.getAuthorizationIdentification()))
				.count();

		return new AuthorizationSummary.Builder()
				.authorizationId(authorizationType.getAuthorizationIdentification())
				.authorizationName(authorizationType.getAuthorizationName())
				.authorizationAlias(authorizationType.getAuthorizationAlias())
				.isTest(authorizationType.isAuthorizationTest())
				.fromDate(authorizationType.getAuthorizationFromDate().toGregorianCalendar().getTime())
				.toDate(authorizationType.getAuthorizationToDate().toGregorianCalendar().getTime())
				.numberOfVoters(voterCount)
				.authorizationObjects(authorizationObjectList)
				.build();
	}

	private ElectionGroupSummary getElectionGroupSummary(final ElectionGroupBallotType electionGroupBallotType,
			final List<AuthorizationSummary> authorizations) {
		final String domainOfInfluence = electionGroupBallotType.getDomainOfInfluence();
		final List<AuthorizationSummary> electionGroupAuthorizations = authorizations.stream()
				.filter(authorizationSummary -> authorizationSummary.getAuthorizationObjects().stream()
						.anyMatch(authorizationObjectSummary -> authorizationObjectSummary.getDomainOfInfluenceId().equals(domainOfInfluence)))
				.toList();
		final List<Description> descriptionList = electionGroupBallotType.getElectionGroupDescription().getElectionGroupDescriptionInfo().stream()
				.map(electionGroupDescriptionInfo -> new Description(electionGroupDescriptionInfo.getLanguage().value(),
						electionGroupDescriptionInfo.getElectionGroupDescriptionShort(),
						electionGroupDescriptionInfo.getElectionGroupDescription()))
				.toList();
		return new ElectionGroupSummary(electionGroupBallotType.getElectionGroupIdentification(), descriptionList,
				electionGroupBallotType.getElectionGroupPosition(), electionGroupAuthorizations,
				electionGroupBallotType.getElectionInformation().stream().map(this::getElectionSummary).toList());
	}

	private ElectionSummary getElectionSummary(final ElectionInformationType electionInformationType) {
		final ElectionType election = electionInformationType.getElection();
		final List<CandidateSummary> candidateList = electionInformationType.getCandidate().stream()
				.map(candidateInformationType -> new CandidateSummary.Builder()
						.candidateId(candidateInformationType.getCandidateIdentification())
						.familyName(candidateInformationType.getFamilyName())
						.firstName(candidateInformationType.getFirstName())
						.callName(candidateInformationType.getCallName())
						.dateOfBirth(candidateInformationType.getDateOfBirth().toGregorianCalendar().getTime())
						.isIncumbent(candidateInformationType.getIncumbent().isIncumbent())
						.referenceOnPosition(candidateInformationType.getReferenceOnPosition())
						.eligibility(candidateInformationType.getEligibility().value())
						.build())
				.toList();
		final List<ListSummary> lists = electionInformationType.getList().stream()
				.map(listInformationType -> {
							final List<Description> descriptionList = listInformationType.getListDescription().getListDescriptionInfo().stream()
									.map(listDescriptionInfo -> new Description(listDescriptionInfo.getLanguage().value(),
											listDescriptionInfo.getListDescriptionShort(), listDescriptionInfo.getListDescription()))
									.toList();
							final List<String> candidateIdentifications = listInformationType.getCandidatePosition().stream()
									.map(CandidatePositionType::getCandidateIdentification)
									.toList();
							final List<CandidateSummary> listCandidates = candidateList.stream()
									.filter(candidate -> candidateIdentifications.contains(candidate.getCandidateId())).toList();

							return new ListSummary(listInformationType.getListIdentification(), listInformationType.getListIndentureNumber(), descriptionList,
									listInformationType.getListOrderOfPrecedence(), listInformationType.isListEmpty(), listCandidates);
						}
				)
				.toList();

		final List<Description> descriptionList = election.getElectionDescription().getElectionDescriptionInfo().stream()
				.map(electionDescriptionInfo -> new Description(electionDescriptionInfo.getLanguage().value(),
						electionDescriptionInfo.getElectionDescriptionShort(), electionDescriptionInfo.getElectionDescription()))
				.toList();

		final Integer primarySecondaryType = election.getReferencedElection().stream()
				// When the election is primary, the election relation is 2, when it is secondary, the election relation is 1.
				// Therefore, the primarySecondaryType is 1 when the election is primary and 2 when the election is secondary.
				.map(referencedElection -> 3 - referencedElection.getElectionRelation().intValueExact())
				.findFirst()
				.orElse(0);

		return new ElectionSummary.Builder()
				.electionId(election.getElectionIdentification())
				.electionPosition(election.getElectionPosition())
				.electionType(election.getTypeOfElection().intValueExact())
				.primarySecondaryType(primarySecondaryType)
				.electionDescription(descriptionList)
				.numberOfMandates(election.getNumberOfMandates())
				.writeInsAllowed(election.isWriteInsAllowed())
				.candidateAccumulation(election.getCandidateAccumulation().intValueExact())
				.candidates(candidateList)
				.lists(lists)
				.build();

	}

	private VoteSummary getVoteSummary(final VoteInformationType voteInformationType, final List<AuthorizationSummary> authorizations) {
		final VoteType vote = voteInformationType.getVote();
		final Map<String, String> voteDescription = vote.getVoteDescription().getVoteDescriptionInfo().stream()
				.collect(Collectors.toMap(descriptionInfo -> descriptionInfo.getLanguage().value(),
						VoteDescriptionInformationType.VoteDescriptionInfo::getVoteDescription));

		final String domainOfInfluence = vote.getDomainOfInfluence();
		final List<AuthorizationSummary> voteAuthorizations = authorizations.stream()
				.filter(authorizationSummary -> authorizationSummary.getAuthorizationObjects().stream()
						.anyMatch(authorizationObjectSummary -> authorizationObjectSummary.getDomainOfInfluenceId().equals(domainOfInfluence)))
				.toList();

		final List<BallotSummary> ballots = vote.getBallot().stream().map(this::getBallotSummary).toList();

		return new VoteSummary(vote.getVoteIdentification(), vote.getVotePosition().intValueExact(), voteDescription, domainOfInfluence,
				voteAuthorizations, ballots);
	}

	private BallotSummary getBallotSummary(final BallotType ballotType) {
		final String ballotIdentification = ballotType.getBallotIdentification();
		final int ballotPosition = ballotType.getBallotPosition();
		final List<Description> ballotDescription = ballotType.getBallotDescription().getBallotDescriptionInfo().stream()
				.map(ballotDescriptionInfo -> new Description(ballotDescriptionInfo.getLanguage().value(),
						ballotDescriptionInfo.getBallotDescriptionShort(), ballotDescriptionInfo.getBallotDescriptionLong()))
				.toList();

		final List<QuestionSummary> questions = new ArrayList<>();
		final StandardBallotType standardBallot = ballotType.getStandardBallot();
		if (standardBallot != null) {
			final List<Description> questionInfo = getQuestionInfo(standardBallot.getBallotQuestion());
			final List<AnswerSummary> answerList = getAnswerSummaries(standardBallot.getAnswer());

			questions.add(new QuestionSummary(standardBallot.getQuestionNumber(), questionInfo, answerList));
		}

		final VariantBallotType variantBallot = ballotType.getVariantBallot();
		if (variantBallot != null) {
			variantBallot.getStandardQuestion().forEach(standardQuestionType -> {
				final List<Description> questionInfo = getQuestionInfo(standardQuestionType.getBallotQuestion());
				final List<AnswerSummary> answerList = getAnswerSummaries(standardQuestionType.getAnswer());

				questions.add(new QuestionSummary(standardQuestionType.getQuestionNumber(), questionInfo, answerList));
			});
			variantBallot.getTieBreakQuestion().forEach(tieBreakQuestionType -> {
				final List<Description> questionInfo = getQuestionInfo(tieBreakQuestionType.getBallotQuestion());
				final List<AnswerSummary> answerList = tieBreakQuestionType.getAnswer().stream()
						.map(tiebreakAnswerType -> {
							final Map<String, String> answers = tiebreakAnswerType.getAnswerInfo().stream()
									.collect(Collectors.toMap(answerInfo -> answerInfo.getLanguage().value(), AnswerInformationType::getAnswer));
							return new AnswerSummary(tiebreakAnswerType.getAnswerPosition(), answers);

						})
						.toList();

				questions.add(new QuestionSummary(tieBreakQuestionType.getQuestionNumber(), questionInfo, answerList));
			});
		}

		return new BallotSummary(ballotIdentification, ballotPosition, ballotDescription, questions);
	}

	private List<Description> getQuestionInfo(final BallotQuestionType ballotQuestionType) {
		return ballotQuestionType.getBallotQuestionInfo().stream()
				.map(ballotQuestionInfo -> new Description(ballotQuestionInfo.getLanguage().value(), ballotQuestionInfo.getBallotQuestionTitle(),
						ballotQuestionInfo.getBallotQuestion()))
				.toList();
	}

	private List<AnswerSummary> getAnswerSummaries(final List<StandardAnswerType> standardAnswers) {
		return standardAnswers.stream()
				.map(standardAnswerType -> {
					final Map<String, String> answers = standardAnswerType.getAnswerInfo().stream()
							.collect(Collectors.toMap(answerInfo -> answerInfo.getLanguage().value(), AnswerInformationType::getAnswer));
					return new AnswerSummary(standardAnswerType.getAnswerPosition(), answers);
				})
				.toList();
	}

	private VerificationCardSetSummary getVerificationCardSetSummary(final VerificationCardSetContext verificationCardSetContext) {
		return new VerificationCardSetSummary.Builder()
				.setVerificationCardSetAlias(verificationCardSetContext.getVerificationCardSetAlias())
				.setTestBallotBox(verificationCardSetContext.isTestBallotBox())
				.setNumberOfVotingCards(verificationCardSetContext.getNumberOfVotingCards())
				.setNumberOfVotingOptions(verificationCardSetContext.getNumberOfVotingOptions())
				.setGracePeriod(verificationCardSetContext.getGracePeriod())
				.build();
	}
}
