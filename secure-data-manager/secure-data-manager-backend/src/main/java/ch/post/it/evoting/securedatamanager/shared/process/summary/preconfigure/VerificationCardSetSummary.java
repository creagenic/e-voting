/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process.summary.preconfigure;

public class VerificationCardSetSummary {

	private final String verificationCardSetAlias;
	private final boolean testBallotBox;
	private final int numberOfVotingCards;
	private final int numberOfVotingOptions;
	private final int gracePeriod;

	private VerificationCardSetSummary(final String verificationCardSetAlias, final boolean testBallotBox, final int numberOfVotingCards,
			final int numberOfVotingOptions, final int gracePeriod) {
		this.verificationCardSetAlias = verificationCardSetAlias;
		this.testBallotBox = testBallotBox;
		this.numberOfVotingCards = numberOfVotingCards;
		this.numberOfVotingOptions = numberOfVotingOptions;
		this.gracePeriod = gracePeriod;
	}

	public String getVerificationCardSetAlias() {
		return verificationCardSetAlias;
	}

	public boolean isTestBallotBox() {
		return testBallotBox;
	}

	public int getNumberOfVotingCards() {
		return numberOfVotingCards;
	}

	public int getNumberOfVotingOptions() {
		return numberOfVotingOptions;
	}

	public int getGracePeriod() {
		return gracePeriod;
	}

	public static class Builder {
		private String verificationCardSetAlias;
		private boolean testBallotBox;
		private int numberOfVotingCards;
		private int numberOfVotingOptions;
		private int gracePeriod;

		public Builder setVerificationCardSetAlias(final String verificationCardSetAlias) {
			this.verificationCardSetAlias = verificationCardSetAlias;
			return this;
		}

		public Builder setTestBallotBox(final boolean testBallotBox) {
			this.testBallotBox = testBallotBox;
			return this;
		}

		public Builder setNumberOfVotingCards(final int numberOfVotingCards) {
			this.numberOfVotingCards = numberOfVotingCards;
			return this;
		}

		public Builder setNumberOfVotingOptions(final int numberOfVotingOptions) {
			this.numberOfVotingOptions = numberOfVotingOptions;
			return this;
		}

		public Builder setGracePeriod(final int gracePeriod) {
			this.gracePeriod = gracePeriod;
			return this;
		}

		public VerificationCardSetSummary build() {
			return new VerificationCardSetSummary(verificationCardSetAlias, testBallotBox, numberOfVotingCards, numberOfVotingOptions, gracePeriod);
		}
	}
}
