/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.process.collectverifierdata;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;
import ch.post.it.evoting.securedatamanager.shared.process.DatasetInfo;

@RestController
@RequestMapping("/sdm-tally/collect-verifier-data")
@ConditionalOnProperty("role.isTally")
public class CollectVerifierDataController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CollectVerifierDataController.class);
	private final CollectVerifierDataService collectVerifierDataService;
	private final ElectionEventService electionEventService;

	public CollectVerifierDataController(
			final CollectVerifierDataService collectVerifierDataService,
			final ElectionEventService electionEventService) {
		this.collectVerifierDataService = collectVerifierDataService;
		this.electionEventService = electionEventService;
	}

	@PostMapping()
	public void collectVerifierData() {

		final String electionEventId = electionEventService.findElectionEventId();

		LOGGER.debug("Starting tally data collection... [electionEventId: {}]", electionEventId);

		collectVerifierDataService.collectVerifierData(electionEventId);

		LOGGER.info("Tally data collection has been started. [electionEventId: {}]", electionEventId);
	}

	@GetMapping()
	public DatasetInfo getFilenames() {
		return collectVerifierDataService.getDatasetFilenameList();
	}
}
