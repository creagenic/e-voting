/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.process.decrypt;

import java.util.List;

public record BallotBoxTestVote(String id, String name, List<List<String>> writeIns, List<List<String>> testVotes) {
}
