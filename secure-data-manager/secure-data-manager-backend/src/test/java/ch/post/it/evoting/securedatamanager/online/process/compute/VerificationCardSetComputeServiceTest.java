/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.compute;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.security.SecureRandom;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.reactive.function.client.WebClient;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.generators.SetupComponentVerificationDataPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.shared.WebClientFactory;
import ch.post.it.evoting.securedatamanager.shared.process.ConfigurationEntityStatusService;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentVerificationDataPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.shared.process.Status;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetServiceTestBase;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@ExtendWith(MockitoExtension.class)
class VerificationCardSetComputeServiceTest extends VotingCardSetServiceTestBase {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final SecureRandom secureRandom = new SecureRandom();

	@InjectMocks
	private VerificationCardSetComputeService verificationCardSetComputeService;

	@Spy
	// Injected in the service.
	private ExecutorService executorService = Executors.newFixedThreadPool(10);

	@Mock
	private VotingCardSetRepository votingCardSetRepositoryMock;

	@Mock
	private ConfigurationEntityStatusService configurationEntityStatusServiceMock;

	@Mock
	private EncryptedLongReturnCodeSharesComputeService encryptedLongReturnCodeSharesComputeServiceMock;

	@Mock
	private SetupComponentVerificationDataPayloadFileRepository setupComponentVerificationDataPayloadFileRepositoryMock;

	@Mock
	private SignatureKeystore<Alias> signatureKeystoreService;

	@Mock
	private WebClientFactory webClientFactoryMock;

	private String electionEventId;
	private int numberOfVotingCardSets;
	private List<String> votingCardSetIds;

	@BeforeEach
	void beforeEach() {
		electionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);

		numberOfVotingCardSets = secureRandom.nextInt(1, 10);
		votingCardSetIds = IntStream.range(0, numberOfVotingCardSets)
				.mapToObj(i -> random.genRandomString(ID_LENGTH, base16Alphabet))
				.toList();
	}

	@Test
	@DisplayName("compute voting card sets with invalid election event id throws FailedValidationException")
	void computeInvalidParamsThrows() {
		assertThrows(FailedValidationException.class, () -> verificationCardSetComputeService.computeVerificationCardSet("", "", ""));
	}

	@Test
	@DisplayName("compute voting card sets with valid input does not throw")
	void computeVotingCardSetsHappyPath() {

		when(setupComponentVerificationDataPayloadFileRepositoryMock.getCount(eq(electionEventId), anyString())).thenReturn(1);
		configureWebClientFactoryMock();

		final SetupComponentVerificationDataPayloadGenerator setupComponentVerificationDataPayloadGenerator = new SetupComponentVerificationDataPayloadGenerator();
		when(setupComponentVerificationDataPayloadFileRepositoryMock.retrieve(eq(electionEventId), anyString(), anyInt()))
				.then(p -> setupComponentVerificationDataPayloadGenerator.generate(electionEventId, p.getArgument(1), 4));

		assertDoesNotThrow(
				() -> verificationCardSetComputeService.computeVerificationCardSet(electionEventId, random.genRandomString(ID_LENGTH, base16Alphabet),
						random.genRandomString(ID_LENGTH, base16Alphabet)));
		verify(encryptedLongReturnCodeSharesComputeServiceMock, times(1)).computeGenEncLongCodeShares(eq(electionEventId),
				anyString(),
				any());
		verify(configurationEntityStatusServiceMock, times(1)).update(eq(Status.COMPUTING.name()), anyString(), any());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void configureWebClientFactoryMock() {
		final WebClient webClient = mock(WebClient.class);
		final WebClient.RequestHeadersUriSpec requestHeadersUriSpec = mock(WebClient.RequestHeadersUriSpec.class);
		final WebClient.ResponseSpec responseSpec = mock(WebClient.ResponseSpec.class);
		final Flux<Integer> flux = mock(Flux.class);
		final Mono<List<Integer>> mono = mock(Mono.class);
		final List<Integer> broadcastChunkIds = List.of();

		when(webClientFactoryMock.getWebClient(anyString())).thenReturn(webClient);
		when(webClient.get()).thenReturn(requestHeadersUriSpec);
		when(requestHeadersUriSpec.uri(any(Function.class))).thenReturn(requestHeadersUriSpec);
		when(requestHeadersUriSpec.accept(any())).thenReturn(requestHeadersUriSpec);
		when(requestHeadersUriSpec.retrieve()).thenReturn(responseSpec);
		when(responseSpec.bodyToFlux(Integer.class)).thenReturn(flux);
		when(flux.collectList()).thenReturn(mono);
		when(mono.block()).thenReturn(broadcastChunkIds);
	}
}
