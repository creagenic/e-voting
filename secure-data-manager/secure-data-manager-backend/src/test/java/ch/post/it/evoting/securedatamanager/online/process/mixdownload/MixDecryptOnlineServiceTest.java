/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.mixdownload;

import static ch.post.it.evoting.securedatamanager.shared.Constants.VOTER_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.nio.file.Path;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Function;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.WebClient;

import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.domain.generators.ControlComponentBallotBoxPayloadGenerator;
import ch.post.it.evoting.domain.generators.ControlComponentShufflePayloadGenerator;
import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.domain.tally.MixDecryptOnlinePayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.evotinglibraries.domain.tally.ControlComponentBallotBoxPayload;
import ch.post.it.evoting.securedatamanager.shared.WebClientFactory;
import ch.post.it.evoting.securedatamanager.shared.process.BallotBoxService;
import ch.post.it.evoting.securedatamanager.shared.process.ControlComponentBallotBoxPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.shared.process.ControlComponentShufflePayloadFileRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@ExtendWith(MockitoExtension.class)
@DisplayName("Use MixDecryptOnlineService to ")
class MixDecryptOnlineServiceTest extends TestGroupSetup {

	private static final int N = 2;
	private static final SecureRandom RANDOM = new SecureRandom();

	private final String electionEventId = "426b2de832ac4cf384af0f68bb2b5d20";
	private final String ballotId = "0fca7e83f8cf41acb2a48b67b2e37a71";
	private final String ballotBoxId = "64ea41b3881e4bef81a2cddab7597ecb";
	private final int l = RANDOM.nextInt(5) + 1;

	private final BallotBoxService ballotBoxService = mock(BallotBoxService.class);
	private final ControlComponentShufflePayloadFileRepository controlComponentShufflePayloadFileRepository = mock(
			ControlComponentShufflePayloadFileRepository.class);
	private final ControlComponentBallotBoxPayloadFileRepository controlComponentBallotBoxPayloadFileRepository = mock(
			ControlComponentBallotBoxPayloadFileRepository.class);

	private final WebClientFactory webClientFactory = mock(WebClientFactory.class);
	private final MixDecryptOnlineService sut = new MixDecryptOnlineService(ballotBoxService, webClientFactory,
			controlComponentShufflePayloadFileRepository, controlComponentBallotBoxPayloadFileRepository, true);

	@Nested
	@DisplayName("Test startOnlineMixing calls")
	class StartOnlineMixing {

		@Test
		@DisplayName("startOnlineMixing with null arguments throws a NullPointerException")
		void startOnlineMixingWithNullArgumentsThrows() {
			assertThrows(NullPointerException.class, () -> sut.startOnlineMixing(null, ballotBoxId));
			assertThrows(NullPointerException.class, () -> sut.startOnlineMixing(electionEventId, null));
		}

		@Test
		@DisplayName("startOnlineMixing with voter portal not enabled throws an IllegalStateException")
		void startOnlineMixingWithisVoterPortalEnabledFalseThrows() {
			final MixDecryptOnlineService mixDecryptOnlineService = new MixDecryptOnlineService(ballotBoxService, webClientFactory,
					controlComponentShufflePayloadFileRepository, controlComponentBallotBoxPayloadFileRepository, false);
			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> mixDecryptOnlineService.startOnlineMixing(electionEventId, ballotBoxId));
			assertEquals(VOTER_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE, exception.getMessage());
		}

		@Test
		@SuppressWarnings("unchecked")
		@DisplayName("startOnlineMixing response is unsuccessful")
		void startOnlineMixingTest_response_unsuccessful() {
			when(ballotBoxService.getDateTo(ballotBoxId)).thenReturn(LocalDateTime.now().minusDays(1));

			final WebClient webClient = mock(WebClient.class);
			final WebClient.RequestBodyUriSpec requestBodyUriSpec = mock(WebClient.RequestBodyUriSpec.class);
			final WebClient.ResponseSpec responseSpec = mock(WebClient.ResponseSpec.class);
			final Mono<ResponseEntity<Void>> mono = mock(Mono.class);
			when(webClientFactory.getWebClient(anyString())).thenReturn(webClient);
			when(webClient.put()).thenReturn(requestBodyUriSpec);
			when(requestBodyUriSpec.uri(any(Function.class))).thenReturn(requestBodyUriSpec);
			when(requestBodyUriSpec.accept(any())).thenReturn(requestBodyUriSpec);
			when(requestBodyUriSpec.retrieve()).thenReturn(responseSpec);
			when(responseSpec.toBodilessEntity()).thenReturn(mono);
			when(mono.block()).thenThrow(IllegalStateException.class);

			assertThrows(IllegalStateException.class, () -> sut.startOnlineMixing(electionEventId, ballotBoxId));

			verify(ballotBoxService, times(0)).updateBallotBoxStatus(ballotBoxId, BallotBoxStatus.MIXING);
		}

		@Test
		@DisplayName("happyPath")
		@SuppressWarnings("unchecked")
		void startOnlineMixingTest_happyPath() {
			when(ballotBoxService.getDateTo(ballotBoxId)).thenReturn(LocalDateTime.now().minusDays(1));

			final WebClient webClient = mock(WebClient.class);
			final WebClient.RequestBodyUriSpec requestBodyUriSpec = mock(WebClient.RequestBodyUriSpec.class);
			final WebClient.ResponseSpec responseSpec = mock(WebClient.ResponseSpec.class);
			final Mono<ResponseEntity<Void>> mono = mock(Mono.class);
			when(webClientFactory.getWebClient(anyString())).thenReturn(webClient);
			when(webClient.put()).thenReturn(requestBodyUriSpec);
			when(requestBodyUriSpec.uri(any(Function.class))).thenReturn(requestBodyUriSpec);
			when(requestBodyUriSpec.accept(any())).thenReturn(requestBodyUriSpec);
			when(requestBodyUriSpec.retrieve()).thenReturn(responseSpec);
			when(responseSpec.toBodilessEntity()).thenReturn(mono);
			when(mono.block()).thenReturn(new ResponseEntity<>(HttpStatus.OK));

			when(ballotBoxService.updateBallotBoxStatus(any(), any())).thenReturn(BallotBoxStatus.MIXING);

			sut.startOnlineMixing(electionEventId, ballotBoxId);

			verify(ballotBoxService, times(1)).updateBallotBoxStatus(ballotBoxId, BallotBoxStatus.MIXING);
		}
	}

	@Nested
	@DisplayName("Test startOnlineMixing calls")
	class GetOnlineStatus {

		@Test
		@DisplayName("getOnlineStatus with null arguments throws a NullPointerException")
		void getOnlineStatusWithNullArgumentsThrows() {
			assertThrows(NullPointerException.class, () -> sut.getOnlineStatus(null, ballotBoxId));
			assertThrows(NullPointerException.class, () -> sut.getOnlineStatus(electionEventId, null));
		}

		@Test
		@DisplayName("getOnlineStatus with voter portal not enabled throws an IllegalStateException")
		void getOnlineStatusWithisVoterPortalEnabledFalseThrows() {
			final MixDecryptOnlineService mixDecryptOnlineService = new MixDecryptOnlineService(ballotBoxService, webClientFactory,
					controlComponentShufflePayloadFileRepository, controlComponentBallotBoxPayloadFileRepository, false);
			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> mixDecryptOnlineService.getOnlineStatus(electionEventId, ballotBoxId));
			assertEquals(VOTER_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE, exception.getMessage());
		}

		@Test
		@DisplayName("happy path")
		@SuppressWarnings({ "unchecked", "rawtypes" })
		void getOnlineStatus_happyPath() {

			final BallotBoxStatus ballotBoxStatus = BallotBoxStatus.MIXED;

			final WebClient webClient = mock(WebClient.class);
			final WebClient.RequestHeadersUriSpec requestHeadersUriSpec = mock(WebClient.RequestHeadersUriSpec.class);
			final WebClient.ResponseSpec responseSpec = mock(WebClient.ResponseSpec.class);
			final Mono<BallotBoxStatus> mono = mock(Mono.class);
			when(webClientFactory.getWebClient(anyString())).thenReturn(webClient);
			when(webClient.get()).thenReturn(requestHeadersUriSpec);
			when(requestHeadersUriSpec.uri(any(Function.class))).thenReturn(requestHeadersUriSpec);
			when(requestHeadersUriSpec.accept(any())).thenReturn(requestHeadersUriSpec);
			when(requestHeadersUriSpec.retrieve()).thenReturn(responseSpec);
			when(responseSpec.bodyToMono(BallotBoxStatus.class)).thenReturn(mono);
			when(mono.block()).thenReturn(ballotBoxStatus);

			assertEquals(ballotBoxStatus, sut.getOnlineStatus(electionEventId, ballotBoxId));
		}
	}

	@Nested
	@DisplayName("Test downloadOnlineMixnetPayloads calls")
	class DownloadOnlineMixnetPayloads {

		private final int N_NODE_IDS = 4;

		private MixDecryptOnlinePayload mixDecryptOnlinePayload;

		@BeforeEach
		void createData() {
			final int numberOfSelections = l + 1;
			final List<ControlComponentBallotBoxPayload> controlComponentBallotBoxPayloads = new ControlComponentBallotBoxPayloadGenerator(
					gqGroup).generate(electionEventId, ballotBoxId,
					numberOfSelections, l);
			final List<ControlComponentShufflePayload> controlComponentShufflePayloads = new ControlComponentShufflePayloadGenerator(
					gqGroup).generate(
					electionEventId, ballotBoxId, N, l);

			mixDecryptOnlinePayload = new MixDecryptOnlinePayload(electionEventId, ballotBoxId, controlComponentBallotBoxPayloads,
					controlComponentShufflePayloads);
		}

		@Test
		@DisplayName("downloadOnlineMixnetPayloads with null arguments throws a NullPointerException")
		void downloadOnlineMixnetPayloadsWithNullArgumentsThrows() {
			assertThrows(NullPointerException.class, () -> sut.downloadOnlineMixnetPayloads(null, ballotBoxId));
			assertThrows(NullPointerException.class, () -> sut.downloadOnlineMixnetPayloads(electionEventId, null));
		}

		@Test
		@DisplayName("downloadOnlineMixnetPayloads with voter portal not enabled throws an IllegalStateException")
		void downloadOnlineMixnetPayloadsWithIsVoterPortalEnabledFalseThrows() {
			final MixDecryptOnlineService mixDecryptOnlineService = new MixDecryptOnlineService(ballotBoxService, webClientFactory,
					controlComponentShufflePayloadFileRepository, controlComponentBallotBoxPayloadFileRepository, false);
			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> mixDecryptOnlineService.downloadOnlineMixnetPayloads(electionEventId, ballotBoxId));
			assertEquals(VOTER_PORTAL_CONNECTION_NOT_ENABLED_MESSAGE, exception.getMessage());
		}

		@Test
		@DisplayName("ballotBoxService.hasStatus(ballotBoxId, BallotBoxStatus.MIXED) return false")
		void ballotBoxService_hasStatus_return_false() {
			when(ballotBoxService.hasStatus(any(), any())).thenReturn(false);

			final IllegalArgumentException uncheckedIOException =
					assertThrows(IllegalArgumentException.class, () -> sut.downloadOnlineMixnetPayloads(electionEventId, ballotBoxId));

			assertEquals("Ballot box is not mixed [ballotBoxId : 64ea41b3881e4bef81a2cddab7597ecb]", uncheckedIOException.getMessage());
		}

		@Test
		@DisplayName("messageBrokerOrchestratorClient.downloadMixDecryptOnline raises IOException")
		void messageBrokerOrchestratorClient_downloadMixDecryptOnline_raises_IOException() {
			final IllegalArgumentException uncheckedIOException =
					assertThrows(IllegalArgumentException.class, () -> sut.downloadOnlineMixnetPayloads(electionEventId, ballotBoxId));

			assertEquals("Ballot box is not mixed [ballotBoxId : 64ea41b3881e4bef81a2cddab7597ecb]", uncheckedIOException.getMessage());
		}

		@Test
		@DisplayName("happy path")
		@SuppressWarnings({ "unchecked", "rawtypes" })
		void downloadOnlineMixnetPayloadsTest() {

			when(ballotBoxService.hasStatus(any(), any())).thenReturn(true);
			when(ballotBoxService.getBallotId(any())).thenReturn(ballotId);
			when(ballotBoxService.updateBallotBoxStatus(any(), any())).thenReturn(BallotBoxStatus.DOWNLOADED);

			final WebClient webClient = mock(WebClient.class);
			final WebClient.RequestHeadersUriSpec requestHeadersUriSpec = mock(WebClient.RequestHeadersUriSpec.class);
			final WebClient.ResponseSpec responseSpec = mock(WebClient.ResponseSpec.class);
			final Flux<MixDecryptOnlinePayload> flux = mock(Flux.class);
			final Mono<MixDecryptOnlinePayload> mono = mock(Mono.class);
			when(webClientFactory.getWebClient(anyString())).thenReturn(webClient);
			when(webClient.get()).thenReturn(requestHeadersUriSpec);
			when(requestHeadersUriSpec.uri(any(Function.class))).thenReturn(requestHeadersUriSpec);
			when(requestHeadersUriSpec.accept(any())).thenReturn(requestHeadersUriSpec);
			when(requestHeadersUriSpec.retrieve()).thenReturn(responseSpec);
			when(responseSpec.bodyToMono(MixDecryptOnlinePayload.class)).thenReturn(mono);
			when(mono.block()).thenReturn(mixDecryptOnlinePayload);

			when(controlComponentShufflePayloadFileRepository.savePayload(any(), any())).thenReturn(
					Path.of("/controlComponentShufflePayloadFile"));

			assertNotNull(sut);

			sut.downloadOnlineMixnetPayloads(electionEventId, ballotBoxId);

			verify(ballotBoxService).hasStatus(any(), any());
			verify(ballotBoxService).getBallotId(any());
			verify(controlComponentShufflePayloadFileRepository, times(N_NODE_IDS)).savePayload(any(), any());
			verify(ballotBoxService).updateBallotBoxStatus(any(), any());
		}
	}
}
