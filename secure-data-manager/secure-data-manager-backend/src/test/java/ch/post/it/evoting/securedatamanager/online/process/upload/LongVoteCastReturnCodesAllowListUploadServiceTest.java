/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.online.process.upload;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Base64;
import java.util.Collections;
import java.util.function.Function;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.configuration.setupvoting.SetupComponentLVCCAllowListPayload;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.online.process.OnlinePathResolver;
import ch.post.it.evoting.securedatamanager.shared.WebClientFactory;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentLVCCAllowListPayloadFileRepository;
import ch.post.it.evoting.securedatamanager.shared.process.SetupComponentLVCCAllowListPayloadService;

import reactor.core.publisher.Mono;

@DisplayName("A LongVoteCastReturnCodesAllowListUploadService")
@ExtendWith(MockitoExtension.class)
class LongVoteCastReturnCodesAllowListUploadServiceTest {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final String ELECTION_EVENT_ID = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final String VERIFICATION_CARD_SET_ID = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
	private static final Hash hash = HashFactory.createHash();
	private static SetupComponentLVCCAllowListPayloadService setupComponentLVCCAllowListPayloadService;
	private final WebClientFactory webClientFactory = mock(WebClientFactory.class);

	@BeforeAll
	static void setUpAll(
			@TempDir
			final Path tempDir) throws IOException {
		final OnlinePathResolver pathResolver = new OnlinePathResolver(tempDir, Path.of(""));

		final SetupComponentLVCCAllowListPayloadFileRepository setupComponentLVCCAllowListPayloadFileRepository = new SetupComponentLVCCAllowListPayloadFileRepository(
				objectMapper, pathResolver);

		setupComponentLVCCAllowListPayloadService =
				new SetupComponentLVCCAllowListPayloadService(setupComponentLVCCAllowListPayloadFileRepository);

		setupComponentLVCCAllowListPayloadService.save(validLongVoteCastReturnCodesAllowListPayload());
	}

	@DisplayName("uploads successfully and does not throw.")
	@Test
	@SuppressWarnings({ "unchecked", "rawtypes" })
	void uploadHappyPath() {

		final WebClient webClient = mock(WebClient.class);
		final WebClient.RequestBodyUriSpec requestBodyUriSpec = mock(WebClient.RequestBodyUriSpec.class);
		final WebClient.RequestHeadersSpec requestHeadersSpec = mock(WebClient.RequestHeadersSpec.class);
		final WebClient.ResponseSpec responseSpec = mock(WebClient.ResponseSpec.class);
		final Mono<ResponseEntity<Void>> mono = mock(Mono.class);
		when(webClientFactory.getWebClient(anyString())).thenReturn(webClient);
		when(webClient.post()).thenReturn(requestBodyUriSpec);
		when(requestBodyUriSpec.uri(any(Function.class))).thenReturn(requestBodyUriSpec);
		when(requestBodyUriSpec.body(any(), any(Class.class))).thenReturn(requestHeadersSpec);
		when(requestHeadersSpec.accept(any())).thenReturn(requestHeadersSpec);
		when(requestHeadersSpec.retrieve()).thenReturn(responseSpec);
		when(responseSpec.toBodilessEntity()).thenReturn(mono);
		when(mono.block()).thenReturn(new ResponseEntity<>(HttpStatus.OK));

		final LongVoteCastReturnCodesAllowListUploadService longVoteCastReturnCodesAllowListUploadService = new LongVoteCastReturnCodesAllowListUploadService(
				setupComponentLVCCAllowListPayloadService, webClientFactory, true);

		assertDoesNotThrow(() -> longVoteCastReturnCodesAllowListUploadService.upload(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
	}

	@DisplayName("uploading throws upon unsuccessful response.")
	@Test
	@SuppressWarnings({ "unchecked", "rawtypes" })
	void uploadUnsuccessfulThrows() {

		final WebClient webClient = mock(WebClient.class);
		final WebClient.RequestBodyUriSpec requestBodyUriSpec = mock(WebClient.RequestBodyUriSpec.class);
		final WebClient.RequestHeadersSpec requestHeadersSpec = mock(WebClient.RequestHeadersSpec.class);
		final WebClient.ResponseSpec responseSpec = mock(WebClient.ResponseSpec.class);
		final Mono<ResponseEntity<Void>> mono = mock(Mono.class);
		when(webClientFactory.getWebClient(anyString())).thenReturn(webClient);
		when(webClient.post()).thenReturn(requestBodyUriSpec);
		when(requestBodyUriSpec.uri(any(Function.class))).thenReturn(requestBodyUriSpec);
		when(requestBodyUriSpec.body(any(), any(Class.class))).thenReturn(requestHeadersSpec);
		when(requestHeadersSpec.accept(any())).thenReturn(requestHeadersSpec);
		when(requestHeadersSpec.retrieve()).thenReturn(responseSpec);
		when(responseSpec.toBodilessEntity()).thenReturn(mono);
		when(mono.block()).thenThrow(IllegalStateException.class);

		final LongVoteCastReturnCodesAllowListUploadService longVoteCastReturnCodesAllowListUploadService = new LongVoteCastReturnCodesAllowListUploadService(
				setupComponentLVCCAllowListPayloadService, webClientFactory, true);

		final IllegalStateException illegalStateException = assertThrows(IllegalStateException.class,
				() -> longVoteCastReturnCodesAllowListUploadService.upload(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
	}

	@DisplayName("provided with different inputs behaves as expected.")
	@Test
	void differentInputsThrowing() {

		final LongVoteCastReturnCodesAllowListUploadService longVoteCastReturnCodesAllowListUploadService = new LongVoteCastReturnCodesAllowListUploadService(
				mock(SetupComponentLVCCAllowListPayloadService.class), webClientFactory, true);

		assertAll(
				() -> assertThrows(NullPointerException.class,
						() -> longVoteCastReturnCodesAllowListUploadService.upload(null, VERIFICATION_CARD_SET_ID)),
				() -> assertThrows(FailedValidationException.class,
						() -> longVoteCastReturnCodesAllowListUploadService.upload("invalidElectionEventId", VERIFICATION_CARD_SET_ID)),
				() -> assertThrows(NullPointerException.class,
						() -> longVoteCastReturnCodesAllowListUploadService.upload(ELECTION_EVENT_ID, null)),
				() -> assertThrows(FailedValidationException.class,
						() -> longVoteCastReturnCodesAllowListUploadService.upload(ELECTION_EVENT_ID, "invalidVerificationCardSetId")));
	}

	private static SetupComponentLVCCAllowListPayload validLongVoteCastReturnCodesAllowListPayload() {
		final SetupComponentLVCCAllowListPayload setupComponentLVCCAllowListPayload =
				new SetupComponentLVCCAllowListPayload(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID,
						Collections.singletonList(Base64.getEncoder().encodeToString(new byte[] { 1 })));

		final byte[] payloadHash = hash.recursiveHash(setupComponentLVCCAllowListPayload);

		setupComponentLVCCAllowListPayload.setSignature(new CryptoPrimitivesSignature(payloadHash));

		return setupComponentLVCCAllowListPayload;
	}
}
