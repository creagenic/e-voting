/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.generate;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.shared.VotingCardSetServiceTestSpringConfig;
import ch.post.it.evoting.securedatamanager.shared.process.BallotBoxRepository;
import ch.post.it.evoting.securedatamanager.shared.process.Status;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetServiceTestBase;

@ExtendWith(MockitoExtension.class)
@SpringJUnitConfig(VotingCardSetServiceTestSpringConfig.class)
class VotingCardSetGenerateBallotConfigServiceTest extends VotingCardSetServiceTestBase {

	private static final String ELECTION_EVENT_ID = "A3D790FD1AC543F9B0A05CA79A20C9E2";

	@Autowired
	private BallotBoxRepository ballotBoxRepository;

	@Autowired
	private VotingCardSetRepository votingCardSetRepository;

	@Autowired
	private BallotDataGeneratorService ballotDataGeneratorService;

	@Autowired
	private VotingCardSetGenerateBallotService votingCardSetGenerateBallotService;

	static Stream<Arguments> invalidParameters() {
		return Stream.of(
				Arguments.of("", VOTING_CARD_SET_ID),
				Arguments.of(ELECTION_EVENT_ID, "")
		);
	}

	@ParameterizedTest
	@MethodSource("invalidParameters")
	void generateWithInvalidParametersThrows(final String electionEventId, final String votingCardSetId) {
		assertThrows(FailedValidationException.class, () -> votingCardSetGenerateBallotService.generateBallotData(electionEventId, votingCardSetId));
	}

	static Stream<Arguments> nullParameters() {
		return Stream.of(
				Arguments.of(null, VOTING_CARD_SET_ID),
				Arguments.of(ELECTION_EVENT_ID, null)
		);
	}

	@ParameterizedTest
	@MethodSource("nullParameters")
	void generateWithNullParametersThrows(final String electionEventId, final String votingCardSetId) {
		assertThrows(NullPointerException.class, () -> votingCardSetGenerateBallotService.generateBallotData(electionEventId, votingCardSetId));
	}

	@Test
	void generateWhenBallotBoxIdIsEmptyFails() {
		when(votingCardSetRepository.getBallotBoxId(VOTING_CARD_SET_ID)).thenReturn("");

		assertFalse(votingCardSetGenerateBallotService.generateBallotData(ELECTION_EVENT_ID, VOTING_CARD_SET_ID));
	}

	@Test
	void generateWhenBallotIdIsEmptyFails() {
		when(votingCardSetRepository.getBallotBoxId(VOTING_CARD_SET_ID)).thenReturn(BALLOT_BOX_ID);
		when(ballotBoxRepository.getBallotId(BALLOT_BOX_ID)).thenReturn("");

		assertFalse(votingCardSetGenerateBallotService.generateBallotData(ELECTION_EVENT_ID, VOTING_CARD_SET_ID));
	}

	@Test
	void generateWhenUnsuccessfulBallotDataGenerationFails() {
		when(votingCardSetRepository.getBallotBoxId(VOTING_CARD_SET_ID)).thenReturn(BALLOT_BOX_ID);
		when(ballotBoxRepository.getBallotId(BALLOT_BOX_ID)).thenReturn(BALLOT_ID);
		when(ballotDataGeneratorService.generate(BALLOT_ID, ELECTION_EVENT_ID)).thenReturn(false);

		assertFalse(votingCardSetGenerateBallotService.generateBallotData(ELECTION_EVENT_ID, VOTING_CARD_SET_ID));
	}

	@Test
	void generateSuccessful() {
		when(votingCardSetRepository.getBallotBoxId(VOTING_CARD_SET_ID)).thenReturn(BALLOT_BOX_ID);
		when(ballotBoxRepository.getBallotId(BALLOT_BOX_ID)).thenReturn(BALLOT_ID);
		when(ballotDataGeneratorService.generate(BALLOT_ID, ELECTION_EVENT_ID)).thenReturn(true);
		when(ballotBoxRepository.find(BALLOT_BOX_ID)).thenReturn(getBallotBoxWithStatus(Status.LOCKED));

		assertTrue(votingCardSetGenerateBallotService.generateBallotData(ELECTION_EVENT_ID, VOTING_CARD_SET_ID));
	}
}
