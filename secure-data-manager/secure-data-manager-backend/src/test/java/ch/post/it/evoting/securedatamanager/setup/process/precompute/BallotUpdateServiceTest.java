/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.process.precompute;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.StringReader;
import java.io.UncheckedIOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.stream.Stream;

import jakarta.json.Json;
import jakarta.json.JsonArray;
import jakarta.json.JsonArrayBuilder;
import jakarta.json.JsonObject;
import jakarta.json.JsonObjectBuilder;
import jakarta.json.JsonReader;
import jakarta.json.JsonString;
import jakarta.json.JsonValue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.election.Ballot;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionOption;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.election.generators.PrimesMappingTableGenerator;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;

class BallotUpdateServiceTest {
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final String electionEventId = "df004676c0914e1ba69558f466663cec";
	private static String JSON_BALLOT_1;
	private static String JSON_BALLOT_3;
	private static ObjectMapper objectMapper;
	private JsonObject ballotObject1;
	private JsonObject ballotObject3;
	private PrimesMappingTable primesMappingTable;
	private BallotUpdateService ballotUpdateService;

	@BeforeAll
	static void setAll() throws IOException {
		objectMapper = DomainObjectMapper.getNewInstance();

		final ClassLoader classLoader = BallotUpdateServiceTest.class.getClassLoader();
		JSON_BALLOT_1 = new String(Objects.requireNonNull(classLoader.getResource("ballot.json")).openStream().readAllBytes(),
				StandardCharsets.UTF_8)
				.replaceAll("(\"representation\": \"\\d+\")", "\"representation\": \"1\"");

		JSON_BALLOT_3 = new String(Objects.requireNonNull(classLoader.getResource("ballot_largerRepresentations.json")).openStream().readAllBytes(),
				StandardCharsets.UTF_8)
				.replaceAll("(\"representation\": \"\\d+\")", "\"representation\": \"1\"");
	}

	@BeforeEach
	void setup() throws IOException, URISyntaxException {
		try (final JsonReader jsonReader = Json.createReader(new StringReader(JSON_BALLOT_1))) {

			ballotObject1 = jsonReader.readObject();
		}
		try (final JsonReader jsonReader = Json.createReader(new StringReader(JSON_BALLOT_3))) {

			ballotObject3 = ensureNoDuplicatedAttributeIds(jsonReader);
		}

		final PrimesMappingTableGenerator primesMappingTableGenerator = new PrimesMappingTableGenerator();
		primesMappingTable = primesMappingTableGenerator.generate();

		ballotUpdateService = new BallotUpdateService(objectMapper);
	}

	@Test
	void happyPathForOneBallot() {

		final Ballot ballot = toBallot(ballotObject1);
		final Map<String, String> mapRepresentation = new TreeMap<>();

		// Extract original representation values of ballot1
		ballot.getOrderedElectionOptions().forEach(option -> mapRepresentation.put(option.getId(), option.getRepresentation()));

		final JsonObject updatedBallotObject = ballotUpdateService.updateOptionsRepresentation(electionEventId, ballotObject1, primesMappingTable);

		assertNotNull(updatedBallotObject);

		final Ballot updatedBallot = toBallot(updatedBallotObject);

		assertTrue(optionStream(updatedBallot).noneMatch(option -> option.getRepresentation().equals(mapRepresentation.get(option.getId()))),
				"Not all representation of ballot have been set.");

		final List<Long> representationValues = optionStream(updatedBallot)
				.map(ElectionOption::getRepresentation)
				.map(Long::valueOf)
				.toList();

		assertEquals(representationValues, representationValues.stream().sorted(Comparator.comparing(Long::valueOf)).toList(),
				"The representation values are not ascending.");
	}

	@Test
	void actualVotingOptionNotInPrimesMappingTable() {
		final IllegalStateException ex = assertThrows(IllegalStateException.class,
				() -> ballotUpdateService.updateOptionsRepresentation(electionEventId, ballotObject3, primesMappingTable));

		final String messagePrefix = "The actual voting option does not have a corresponding encoded voting option. [actualVotingOption:";

		assertTrue(ex.getMessage().startsWith(messagePrefix));
	}

	private Stream<ElectionOption> optionStream(final Ballot ballot) {
		return ballot.contests().stream().flatMap(c -> c.options().stream());
	}

	private Ballot toBallot(final JsonObject ballotObject) {
		final Ballot ballot;
		try {
			ballot = objectMapper.readValue(ballotObject.toString(), Ballot.class);
		} catch (final IOException e) {
			throw new UncheckedIOException("Cannot deserialize the ballot box json string to a valid Ballot object.", e);
		}
		return ballot;
	}

	private JsonObject ensureNoDuplicatedAttributeIds(final JsonReader jsonReader) {
		final Random random = RandomFactory.createRandom();
		final JsonObject ballot = jsonReader.readObject();

		final JsonArrayBuilder contestsBuilder = Json.createArrayBuilder();
		ballot.getJsonArray("contests").stream()
				.map(JsonValue::asJsonObject)
				.forEach(contest -> {
					final JsonArrayBuilder questionsBuilder = Json.createArrayBuilder();
					final JsonArrayBuilder relatedBuilder = Json.createArrayBuilder();
					contest.getJsonArray("questions").stream()
							.map(JsonValue::asJsonObject)
							.forEach(question -> {
								final JsonString attributeId = Json.createValue(random.genRandomString(ID_LENGTH, base16Alphabet));
								questionsBuilder.add(createQuestion(question, attributeId));
								relatedBuilder.add(attributeId);
							});
					final JsonArray related = relatedBuilder.build();

					final JsonArrayBuilder optionsBuilder = Json.createArrayBuilder();
					final JsonArrayBuilder attributesBuilder = Json.createArrayBuilder();
					contest.getJsonArray("options").stream()
							.map(JsonValue::asJsonObject)
							.forEach(option -> {
								final JsonString attributeId = Json.createValue(random.genRandomString(ID_LENGTH, base16Alphabet));
								final JsonValue randomRelated = related.get(random.genRandomInteger(related.size()));

								optionsBuilder.add(createOption(option, attributeId));
								attributesBuilder.add(createAttribute(attributeId, Json.createArrayBuilder().add(randomRelated).build()));
							});
					related.forEach(
							questionAttributeId -> attributesBuilder.add(createAttribute(questionAttributeId, Json.createArrayBuilder().build())));

					contestsBuilder.add(createContest(contest, questionsBuilder.build(), optionsBuilder.build(), attributesBuilder.build()));
				});

		return createBallot(ballot, contestsBuilder.build());
	}

	private JsonObject createBallot(final JsonObject ballot, final JsonArray contests) {
		final JsonObjectBuilder ballotBuilder = Json.createObjectBuilder();
		ballotBuilder.add("id", ballot.getString("id"));
		ballotBuilder.add("defaultTitle", ballot.getString("defaultTitle"));
		ballotBuilder.add("defaultDescription", ballot.getString("defaultDescription"));
		ballotBuilder.add("alias", ballot.getString("alias"));
		ballotBuilder.add("electionEvent", ballot.getJsonObject("electionEvent"));
		ballotBuilder.add("status", ballot.getString("status"));
		ballotBuilder.add("contests", contests);
		return ballotBuilder.build();
	}

	private JsonObject createContest(final JsonObject contest, final JsonArray questions, final JsonArray options,
			final JsonArray attributes) {
		final JsonObjectBuilder contestBuilder = Json.createObjectBuilder();
		contestBuilder.add("id", contest.getString("id"));
		contestBuilder.add("defaultTitle", contest.getString("defaultTitle"));
		contestBuilder.add("alias", contest.getString("alias"));
		contestBuilder.add("defaultDescription", contest.getString("defaultDescription"));
		contestBuilder.add("electionEvent", contest.getJsonObject("electionEvent"));
		contestBuilder.add("template", contest.getString("template"));
		contestBuilder.add("fullBlank", contest.getString("fullBlank"));
		contestBuilder.add("options", options);
		contestBuilder.add("attributes", attributes);
		contestBuilder.add("questions", questions);
		return contestBuilder.build();
	}

	private JsonObject createQuestion(final JsonObject question, final JsonString attributeId) {
		final JsonObjectBuilder questionBuilder = Json.createObjectBuilder();
		questionBuilder.add("id", question.getString("id"));
		questionBuilder.add("max", question.getString("max"));
		questionBuilder.add("min", question.getString("min"));
		questionBuilder.add("accumulation", question.getString("accumulation"));
		questionBuilder.add("writeIn", question.getBoolean("writeIn"));
		questionBuilder.add("blankAttributes", question.getJsonArray("blankAttributes"));
		questionBuilder.add("writeInAttributes", question.getJsonArray("writeInAttributes"));
		questionBuilder.add("questionNumber", question.getString("questionNumber"));
		questionBuilder.add("variantBallot", question.getBoolean("variantBallot"));
		questionBuilder.add("ballotIdentification", question.getString("ballotIdentification"));
		questionBuilder.add("fusions", question.getJsonArray("fusions"));
		questionBuilder.add("electionGroupIdentification", question.getString("electionGroupIdentification"));
		questionBuilder.add("primarySecondaryType", question.getString("primarySecondaryType"));
		questionBuilder.add("implicitlyEligibleCandidateIdentifications", question.getJsonArray("implicitlyEligibleCandidateIdentifications"));
		questionBuilder.add("implicitlyDiscreetCandidateIdentifications", question.getJsonArray("implicitlyDiscreetCandidateIdentifications"));
		questionBuilder.add("attribute", attributeId);
		return questionBuilder.build();
	}

	private JsonObject createOption(final JsonObject option, final JsonString attributeId) {
		final JsonObjectBuilder optionBuilder = Json.createObjectBuilder();
		optionBuilder.add("id", option.getString("id"));
		optionBuilder.add("representation", option.getString("representation"));
		optionBuilder.add("semantics", option.getString("semantics"));
		optionBuilder.add("attribute", attributeId);
		return optionBuilder.build();
	}

	private JsonObject createAttribute(final JsonValue attributeId, final JsonArray related) {
		final JsonObjectBuilder attributeBuilder = Json.createObjectBuilder();
		attributeBuilder.add("id", attributeId);
		attributeBuilder.add("alias", Json.createValue("5b37171d736c4d13baa072f7f5cd5a4f"));
		attributeBuilder.add("correctness", Json.createValue("true"));
		attributeBuilder.add("related", related);
		return attributeBuilder.build();
	}

}
