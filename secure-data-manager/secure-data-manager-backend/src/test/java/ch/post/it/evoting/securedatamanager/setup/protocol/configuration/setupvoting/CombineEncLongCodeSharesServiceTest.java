/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory.createElGamal;
import static ch.post.it.evoting.cryptoprimitives.hashing.HashFactory.createHash;
import static ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory.createBase64;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientKeyPair;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.domain.generators.ControlComponentCodeSharesPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionEventContext;
import ch.post.it.evoting.evotinglibraries.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.ElectionEventContextPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.ControlComponentCodeShare;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.setup.process.SetupKeyPairService;
import ch.post.it.evoting.securedatamanager.setup.process.generate.EncryptedNodeLongReturnCodeSharesChunk;
import ch.post.it.evoting.securedatamanager.setup.process.generate.EncryptedSingleNodeLongReturnCodeSharesChunk;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;

@DisplayName("combineEncLongCodeShares called with")
class CombineEncLongCodeSharesServiceTest {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final ElectionEventService electionEventService = mock(ElectionEventService.class);
	private static CombineEncLongCodeSharesService combineEncLongCodeSharesService;
	private static String electionEventId;
	private static String verificationCardSetId;
	private static VerificationCardSetContext verificationCardSetContext;
	private static EncryptedNodeLongReturnCodeSharesChunk.Builder encryptedNodeLongReturnCodeSharesChunkBuilder;
	private static EncryptedNodeLongReturnCodeSharesChunk encryptedNodeLongReturnCodeSharesChunk;

	@BeforeAll
	static void setUpAll() {
		final CombineEncLongCodeSharesAlgorithm combineEncLongCodeSharesAlgorithm = new CombineEncLongCodeSharesAlgorithm(createHash(),
				createElGamal(), createBase64());
		final SetupKeyPairService setupKeyPairService = mock(SetupKeyPairService.class);
		final ElectionEventContextPayloadService electionEventContextPayloadService = mock(ElectionEventContextPayloadService.class);
		combineEncLongCodeSharesService = new CombineEncLongCodeSharesService(electionEventService, setupKeyPairService,
				combineEncLongCodeSharesAlgorithm, electionEventContextPayloadService);

		final ElectionEventContextPayloadGenerator electionEventContextPayloadGenerator = new ElectionEventContextPayloadGenerator();
		final ElectionEventContextPayload electionEventContextPayload = electionEventContextPayloadGenerator.generate();
		final ElectionEventContext electionEventContext = electionEventContextPayload.getElectionEventContext();
		electionEventId = electionEventContext.electionEventId();
		verificationCardSetContext = electionEventContext
				.verificationCardSetContexts()
				.getFirst();
		verificationCardSetId = verificationCardSetContext.getVerificationCardSetId();

		when(electionEventService.exists(electionEventId)).thenReturn(true);
		when(electionEventContextPayloadService.load(electionEventId)).thenReturn(electionEventContextPayload);
		when(electionEventContextPayloadService.loadPrimesMappingTable(electionEventId, verificationCardSetId))
				.thenReturn(verificationCardSetContext.getPrimesMappingTable());

		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(electionEventContextPayload.getEncryptionGroup());
		final ElGamalMultiRecipientKeyPair setupKeyPair = elGamalGenerator.genRandomKeyPair(
				electionEventContext.maximumNumberOfVotingOptions());
		when(setupKeyPairService.load(electionEventId)).thenReturn(setupKeyPair);
	}

	@BeforeEach
	void setUp() {
		final GqGroup gqGroup = verificationCardSetContext.getPrimesMappingTable().getEncryptionGroup();
		final List<ControlComponentCodeSharesPayload> controlComponentCodeSharesPayloads = new ControlComponentCodeSharesPayloadGenerator(gqGroup)
				.generate(electionEventId, verificationCardSetId, 1, verificationCardSetContext.getPrimesMappingTable().getNumberOfVotingOptions());
		final int chunkId = controlComponentCodeSharesPayloads.getFirst().getChunkId();
		final List<String> verificationCardIds = controlComponentCodeSharesPayloads.getFirst().getControlComponentCodeShares().stream()
				.map(ControlComponentCodeShare::verificationCardId)
				.toList();
		final List<EncryptedSingleNodeLongReturnCodeSharesChunk> nodeReturnCodesValues = controlComponentCodeSharesPayloads.stream()
				.map(payload -> {
					final List<ElGamalMultiRecipientCiphertext> exponentiatedEncryptedConfirmationKeys = payload.getControlComponentCodeShares()
							.stream()
							.map(ControlComponentCodeShare::exponentiatedEncryptedConfirmationKey)
							.toList();
					final List<ElGamalMultiRecipientCiphertext> exponentiatedEncryptedPartialChoiceReturnCodes = payload.getControlComponentCodeShares()
							.stream()
							.map(ControlComponentCodeShare::exponentiatedEncryptedPartialChoiceReturnCodes)
							.toList();
					return new EncryptedSingleNodeLongReturnCodeSharesChunk.Builder()
							.setNodeId(payload.getNodeId())
							.setChunkId(payload.getChunkId())
							.setVerificationCardIds(verificationCardIds)
							.setExponentiatedEncryptedConfirmationKeys(exponentiatedEncryptedConfirmationKeys)
							.setExponentiatedEncryptedPartialChoiceReturnCodes(exponentiatedEncryptedPartialChoiceReturnCodes)
							.build();
				})
				.toList();
		encryptedNodeLongReturnCodeSharesChunkBuilder = new EncryptedNodeLongReturnCodeSharesChunk.Builder()
				.setElectionEventId(electionEventId)
				.setVerificationCardSetId(verificationCardSetId)
				.setVerificationCardIds(verificationCardIds)
				.setChunkId(chunkId)
				.setNodeReturnCodesValues(nodeReturnCodesValues);
		encryptedNodeLongReturnCodeSharesChunk = encryptedNodeLongReturnCodeSharesChunkBuilder.build();
	}

	private static Stream<Arguments> provideNullParameters() {
		return Stream.of(
				Arguments.of(null, verificationCardSetId, encryptedNodeLongReturnCodeSharesChunk),
				Arguments.of(electionEventId, null, encryptedNodeLongReturnCodeSharesChunk),
				Arguments.of(electionEventId, verificationCardSetId, null)
		);
	}

	@ParameterizedTest
	@MethodSource("provideNullParameters")
	@DisplayName("null parameters throws NullPointerException")
	void combineEncLongCodeSharesWithNullParametersThrows(final String electionEventId, final String verificationCardSetId,
			final EncryptedNodeLongReturnCodeSharesChunk encryptedNodeLongReturnCodeSharesChunk) {
		assertThrows(NullPointerException.class,
				() -> combineEncLongCodeSharesService.combineEncLongCodeShares(electionEventId, verificationCardSetId,
						encryptedNodeLongReturnCodeSharesChunk));
	}

	@Test
	@DisplayName("invalid election event id throws FailedValidationException")
	void combineEncLongCodeSharesWithInvalidElectionEventIdThrows() {
		assertThrows(FailedValidationException.class,
				() -> combineEncLongCodeSharesService.combineEncLongCodeShares("InvalidElectionEventId", verificationCardSetId,
						encryptedNodeLongReturnCodeSharesChunk));
	}

	@Test
	@DisplayName("invalid verification card set id throws FailedValidationException")
	void combineEncLongCodeSharesWithInvalidVerificationCardSetIdThrows() {
		assertThrows(FailedValidationException.class,
				() -> combineEncLongCodeSharesService.combineEncLongCodeShares(electionEventId, "InvalidVerificationCardSetId",
						encryptedNodeLongReturnCodeSharesChunk));
	}

	@Test
	@DisplayName("encrypted node long return code shares chunk of other election event throws IllegalArgumentException")
	void combineEncLongCodeSharesWithOtherElectionEventChunkThrows() {
		final String otherElectionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);
		final EncryptedNodeLongReturnCodeSharesChunk nonConsistentChunk = encryptedNodeLongReturnCodeSharesChunkBuilder
				.setElectionEventId(otherElectionEventId)
				.build();

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> combineEncLongCodeSharesService.combineEncLongCodeShares(electionEventId, verificationCardSetId, nonConsistentChunk));

		final String expected = String.format(
				"The encrypted node long return code shares chunk does not correspond to the expected election event id. [expected: %s, actual: %s]",
				electionEventId, nonConsistentChunk.getElectionEventId());
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("encrypted node long return code shares chunk of other verification card set throws IllegalArgumentException")
	void combineEncLongCodeSharesWithOtherVerificationCardSetChunkThrows() {
		final String otherVerificationCardSetId = random.genRandomString(ID_LENGTH, base16Alphabet);
		final EncryptedNodeLongReturnCodeSharesChunk nonConsistentChunk = encryptedNodeLongReturnCodeSharesChunkBuilder
				.setVerificationCardSetId(otherVerificationCardSetId)
				.build();

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> combineEncLongCodeSharesService.combineEncLongCodeShares(electionEventId, verificationCardSetId, nonConsistentChunk));

		final String expected = String.format(
				"The encrypted node long return code shares chunk does not correspond to the expected verification card set id. [expected: %s, actual: %s]",
				verificationCardSetId, nonConsistentChunk.getVerificationCardSetId());
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("non existent election event id throws IllegalArgumentException")
	void combineEncLongCodeSharesWithNonExistentElectionEventIdThrows() {
		final String nonExistentElectionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);

		when(electionEventService.exists(nonExistentElectionEventId)).thenReturn(false);

		assertThrows(IllegalArgumentException.class,
				() -> combineEncLongCodeSharesService.combineEncLongCodeShares(nonExistentElectionEventId, verificationCardSetId,
						encryptedNodeLongReturnCodeSharesChunk));
	}

	@Test
	@DisplayName("non existent verification card set id throws IllegalArgumentException")
	void combineEncLongCodeSharesWithNonExistentVerificationCardSetIdThrows() {
		final String nonExistentVerificationCardSetId = random.genRandomString(ID_LENGTH, base16Alphabet);
		final EncryptedNodeLongReturnCodeSharesChunk nonExistentVerificationCardSetIdChunk = encryptedNodeLongReturnCodeSharesChunkBuilder
				.setVerificationCardSetId(nonExistentVerificationCardSetId)
				.build();

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> combineEncLongCodeSharesService.combineEncLongCodeShares(electionEventId, nonExistentVerificationCardSetId,
						nonExistentVerificationCardSetIdChunk));
		final String expected = String.format("The given verification card set id does not exist. [verificationCardSetId: %s]",
				nonExistentVerificationCardSetId);
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}
}
