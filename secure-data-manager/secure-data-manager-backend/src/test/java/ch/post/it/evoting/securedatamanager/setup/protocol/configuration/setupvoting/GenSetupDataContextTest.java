/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTableEntry;
import ch.post.it.evoting.evotinglibraries.domain.election.generators.PrimesMappingTableGenerator;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

@DisplayName("GenSetupDataInput constructed with")
class GenSetupDataContextTest {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private Map<String, List<PrimesMappingTableEntrySubset>> optionsInformation;
	private Map<String, List<PrimesMappingTableEntrySubset>> invalidOptionsInformation;

	@BeforeEach
	void setUp() {
		final List<String> verificationCardSetIds = List.of(
				random.genRandomString(ID_LENGTH, base16Alphabet),
				random.genRandomString(ID_LENGTH, base16Alphabet)
		);
		final PrimesMappingTableGenerator primesMappingTableGenerator = new PrimesMappingTableGenerator();
		final GroupVector<PrimesMappingTableEntry, GqGroup> pTableVcs0 = primesMappingTableGenerator.generate(4, 1).getPTable();
		final GroupVector<PrimesMappingTableEntry, GqGroup> pTableVcs1 = primesMappingTableGenerator.generate(2, 2).getPTable();
		optionsInformation = Map.of(
				verificationCardSetIds.get(0), pTableVcs0.stream()
						.map(entry -> new PrimesMappingTableEntrySubset(entry.actualVotingOption(), entry.semanticInformation(),
								entry.correctnessInformation()))
						.toList(),
				verificationCardSetIds.get(1), pTableVcs1.stream()
						.map(entry -> new PrimesMappingTableEntrySubset(entry.actualVotingOption(), entry.semanticInformation(),
								entry.correctnessInformation()))
						.toList()
		);
		invalidOptionsInformation = Map.of(
				verificationCardSetIds.get(0), pTableVcs0.stream()
						.map(entry -> new PrimesMappingTableEntrySubset(entry.actualVotingOption(), entry.semanticInformation(),
								entry.correctnessInformation()))
						.toList(),
				"invalid UUID", pTableVcs1.stream()
						.map(entry -> new PrimesMappingTableEntrySubset(entry.actualVotingOption(), entry.semanticInformation(),
								entry.correctnessInformation()))
						.toList()
		);
	}

	@Test
	@DisplayName("any null parameter throws NullPointerException")
	void anyNullParamThrows() {
		assertThrows(NullPointerException.class, () -> new GenSetupDataContext(null));
	}

	@Test
	@DisplayName("invalid options information throws FailedValidationException")
	void invalidOptionsInformationThrows() {
		assertThrows(FailedValidationException.class, () -> new GenSetupDataContext(invalidOptionsInformation));

	}

	@Test
	@DisplayName("valid parameters does not throw")
	void validParamsDoesNotThrow() {
		assertDoesNotThrow(() -> new GenSetupDataContext(optionsInformation));
	}

}
