/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import static ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalFactory.createElGamal;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.election.Ballot;
import ch.post.it.evoting.evotinglibraries.domain.election.generators.BallotGenerator;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.PrimesMappingTableAlgorithms;
import ch.post.it.evoting.securedatamanager.setup.process.precompute.BallotConfigService;
import ch.post.it.evoting.securedatamanager.shared.process.BallotBoxService;
import ch.post.it.evoting.securedatamanager.shared.process.VotingCardSetRepository;

@DisplayName("genSetupData called with")
class GenSetupDataServiceTest {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private static GenSetupDataService genSetupDataService;
	private static String electionEventId;
	private static String seed;

	@BeforeAll
	static void setUpAll() {
		final GetElectionEventEncryptionParametersAlgorithm getElectionEventEncryptionParametersAlgorithm = new GetElectionEventEncryptionParametersAlgorithm(
				createElGamal());
		final GenSetupDataAlgorithm genSetupDataAlgorithm = new GenSetupDataAlgorithm(random, new PrimesMappingTableAlgorithms(),
				getElectionEventEncryptionParametersAlgorithm);
		final BallotConfigService ballotConfigService = mock(BallotConfigService.class);
		final BallotBoxService ballotBoxService = mock(BallotBoxService.class);
		final VotingCardSetRepository votingCardSetRepository = mock(VotingCardSetRepository.class);
		genSetupDataService = new GenSetupDataService(ballotConfigService, ballotBoxService, votingCardSetRepository, genSetupDataAlgorithm);

		electionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);
		seed = "NE_20231124_TT05";

		final List<String> votingCardSetIds = IntStream.range(0, 4)
				.mapToObj(i -> random.genRandomString(ID_LENGTH, base16Alphabet))
				.toList();
		when(votingCardSetRepository.findAllVotingCardSetIds(electionEventId)).thenReturn(votingCardSetIds);

		votingCardSetIds.forEach(votingCardSetId -> when(votingCardSetRepository.getVerificationCardSetId(votingCardSetId))
				.thenReturn(random.genRandomString(ID_LENGTH, base16Alphabet)));
		when(votingCardSetRepository.getBallotBoxId(anyString())).thenReturn(random.genRandomString(ID_LENGTH, base16Alphabet));
		when(ballotBoxService.getBallotId(anyString())).thenReturn(random.genRandomString(ID_LENGTH, base16Alphabet));

		final BallotGenerator ballotGenerator = new BallotGenerator();
		final Ballot ballot = ballotGenerator.generate();
		when(ballotConfigService.getBallot(anyString(), anyString())).thenReturn(ballot);
	}

	private static Stream<Arguments> provideNullParameters() {
		return Stream.of(
				Arguments.of(null, seed),
				Arguments.of(electionEventId, null)
		);
	}

	@ParameterizedTest
	@MethodSource("provideNullParameters")
	@DisplayName("null parameters throws NullPointerException")
	void genSetupDataWithNullParametersThrows(final String electionEventId, final String seed) {
		assertThrows(NullPointerException.class, () -> genSetupDataService.genSetupData(electionEventId, seed));
	}

	@Test
	@DisplayName("invalid election event id throws FailedValidationException")
	void genSetupDataWithInvalidElectionEventIdThrows() {
		assertThrows(FailedValidationException.class, () -> genSetupDataService.genSetupData("InvalidElectionEventId", seed));
	}

	@Test
	@DisplayName("invalid seed throws FailedValidationException")
	void genSetupDataWithInvalidSeedThrows() {
		assertThrows(FailedValidationException.class, () -> genSetupDataService.genSetupData(electionEventId, "InvalidSeed_^``"));
	}

	@Test
	@DisplayName("valid parameters does not throw")
	void genSetupDataWithValidParametersDoesNotThrow() {
		assertDoesNotThrow(() -> genSetupDataService.genSetupData(electionEventId, seed));
	}

}
