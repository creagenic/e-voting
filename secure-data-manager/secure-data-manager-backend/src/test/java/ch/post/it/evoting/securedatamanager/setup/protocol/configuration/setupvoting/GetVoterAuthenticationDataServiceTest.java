/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.setup.protocol.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.BASE64_ENCODED_HASH_OUTPUT_LENGTH;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.BIRTH_DATE;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.EXTENDED_AUTHENTICATION_FACTORS;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.google.common.base.Throwables;
import com.google.common.collect.MoreCollectors;

import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base64Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.ElectionSetupUtils;
import ch.post.it.evoting.evotinglibraries.domain.election.VerificationCardSetContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.ElectionEventContextPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.Configuration;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ContestType;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingconfig.ExtendedAuthenticationKeysDefinitionType;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;

@DisplayName("getVoterAuthenticationData called with")
class GetVoterAuthenticationDataServiceTest {

	private static final Random random = RandomFactory.createRandom();

	private static GetVoterAuthenticationDataService getVoterAuthenticationDataService;
	private static String electionEventId;
	private static String verificationCardSetId;
	private static Configuration configuration;
	private static List<String> startVotingKeys;
	private static List<String> extendedAuthenticationFactors;

	@BeforeAll
	static void setUpAll() {
		final GetVoterAuthenticationDataAlgorithm getVoterAuthenticationDataAlgorithm = mock(GetVoterAuthenticationDataAlgorithm.class);
		final ElectionEventContextPayloadService electionEventContextPayloadService = mock(ElectionEventContextPayloadService.class);
		getVoterAuthenticationDataService = new GetVoterAuthenticationDataService(electionEventContextPayloadService,
				getVoterAuthenticationDataAlgorithm);

		final ElectionEventContextPayloadGenerator electionEventContextPayloadGenerator = new ElectionEventContextPayloadGenerator();
		final ElectionEventContextPayload electionEventContextPayload = electionEventContextPayloadGenerator.generate();
		electionEventId = electionEventContextPayload.getElectionEventContext().electionEventId();
		verificationCardSetId = electionEventContextPayload.getElectionEventContext().verificationCardSetContexts().getFirst()
				.getVerificationCardSetId();
		configuration = new Configuration().withContest(
				new ContestType().withExtendedAuthenticationKeys(new ExtendedAuthenticationKeysDefinitionType().withKeyName(BIRTH_DATE)));

		final int numberOfEligibleVoters = electionEventContextPayload.getElectionEventContext()
				.verificationCardSetContexts()
				.stream()
				.filter(verificationCardSetContext -> verificationCardSetContext.getVerificationCardSetId().equals(verificationCardSetId))
				.map(VerificationCardSetContext::getNumberOfVotingCards)
				.collect(MoreCollectors.onlyElement());
		startVotingKeys = IntStream.range(0, numberOfEligibleVoters)
				.mapToObj(ignored -> ElectionSetupUtils.genStartVotingKey())
				.toList();
		extendedAuthenticationFactors = IntStream.range(0, numberOfEligibleVoters)
				.mapToObj(ignored -> String.join("", random.genUniqueDecimalStrings(4, 2)))
				.toList();

		when(electionEventContextPayloadService.load(electionEventId)).thenReturn(electionEventContextPayload);

		when(getVoterAuthenticationDataAlgorithm.getVoterAuthenticationData(any(), any()))
				.thenReturn(new GetVoterAuthenticationDataOutput(
						List.of(random.genRandomString(ID_LENGTH, Base16Alphabet.getInstance())),
						List.of(random.genRandomString(BASE64_ENCODED_HASH_OUTPUT_LENGTH, Base64Alphabet.getInstance())))
				);
	}

	private static Stream<Arguments> provideNullParameters() {
		final List<String> startVotingKeysWithNull = new ArrayList<>(startVotingKeys);
		startVotingKeysWithNull.add(null);

		final List<String> extendedAuthenticationFactorsWithNull = new ArrayList<>(extendedAuthenticationFactors);
		extendedAuthenticationFactorsWithNull.add(null);

		return Stream.of(
				Arguments.of(null, verificationCardSetId, configuration, startVotingKeys, extendedAuthenticationFactors),
				Arguments.of(electionEventId, null, configuration, startVotingKeys, extendedAuthenticationFactors),
				Arguments.of(electionEventId, verificationCardSetId, null, startVotingKeys, extendedAuthenticationFactors),
				Arguments.of(electionEventId, verificationCardSetId, configuration, null, extendedAuthenticationFactors),
				Arguments.of(electionEventId, verificationCardSetId, configuration, startVotingKeysWithNull, extendedAuthenticationFactors),
				Arguments.of(electionEventId, verificationCardSetId, configuration, startVotingKeys, null),
				Arguments.of(electionEventId, verificationCardSetId, configuration, startVotingKeys, extendedAuthenticationFactorsWithNull)
		);
	}

	@ParameterizedTest
	@MethodSource("provideNullParameters")
	@DisplayName("null parameters throws NullPointerException")
	void getVoterAuthenticationDataWithNullParametersThrows(final String electionEventId, final String verificationCardSetId,
			final Configuration configuration, final List<String> startVotingKeys, final List<String> extendedAuthenticationFactors) {
		assertThrows(NullPointerException.class,
				() -> getVoterAuthenticationDataService.getVoterAuthenticationData(electionEventId, verificationCardSetId, configuration,
						startVotingKeys, extendedAuthenticationFactors));
	}

	@Test
	@DisplayName("invalid election event id throws FailedValidationException")
	void getVoterAuthenticationDataWithInvalidElectionEventIdThrows() {
		assertThrows(FailedValidationException.class,
				() -> getVoterAuthenticationDataService.getVoterAuthenticationData("InvalidElectionEventId", verificationCardSetId, configuration,
						startVotingKeys, extendedAuthenticationFactors));
	}

	@Test
	@DisplayName("invalid verification card set id throws FailedValidationException")
	void getVoterAuthenticationDataWithInvalidVerificationCardSetIdThrows() {
		assertThrows(FailedValidationException.class,
				() -> getVoterAuthenticationDataService.getVoterAuthenticationData(electionEventId, "InvalidVerificationCardSetId", configuration,
						startVotingKeys, extendedAuthenticationFactors));
	}

	@Test
	@DisplayName("multiple extended authentication factors throws IllegalStateException")
	void getVoterAuthenticationDataWithMultipleExtendedAuthenticationFactorsThrows() {
		final ExtendedAuthenticationKeysDefinitionType multipleExtendedAuthenticationFactors = new ExtendedAuthenticationKeysDefinitionType()
				.withKeyName(EXTENDED_AUTHENTICATION_FACTORS.keySet());
		final Configuration invalidConfiguration = new Configuration();
		invalidConfiguration.setContest(new ContestType().withExtendedAuthenticationKeys(multipleExtendedAuthenticationFactors));

		final IllegalStateException exception = assertThrows(IllegalStateException.class,
				() -> getVoterAuthenticationDataService.getVoterAuthenticationData(electionEventId, verificationCardSetId, invalidConfiguration,
						startVotingKeys, extendedAuthenticationFactors));

		final String expected = String.format("There must be a single extended authentication key name. [size: %s]",
				multipleExtendedAuthenticationFactors.getKeyName().size());
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("invalid extended authentication factor throws IllegalStateException")
	void getVoterAuthenticationDataWithInvalidExtendedAuthenticationFactorThrows() {
		final ExtendedAuthenticationKeysDefinitionType invalidExtendedAuthenticationFactor = new ExtendedAuthenticationKeysDefinitionType()
				.withKeyName("invalid");
		final Configuration invalidConfiguration = new Configuration();
		invalidConfiguration.setContest(new ContestType().withExtendedAuthenticationKeys(invalidExtendedAuthenticationFactor));

		final IllegalStateException exception = assertThrows(IllegalStateException.class,
				() -> getVoterAuthenticationDataService.getVoterAuthenticationData(electionEventId, verificationCardSetId, invalidConfiguration,
						startVotingKeys, extendedAuthenticationFactors));

		final String expected = String.format("Unsupported extended authentication factor. [name: %s]",
				invalidExtendedAuthenticationFactor.getKeyName().getFirst());
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("valid parameters does not throw")
	void getVoterAuthenticationDataWithValidParametersDoesNotThrow() {
		assertDoesNotThrow(() -> getVoterAuthenticationDataService.getVoterAuthenticationData(electionEventId, verificationCardSetId, configuration,
				startVotingKeys, extendedAuthenticationFactors));
	}

}
