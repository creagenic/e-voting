/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.tally.BallotBoxStatus;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;

class BallotBoxServiceTest {

	private static final Random RANDOM = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final String BALLOT_BOX_READY = "{\"result\": [{\"id\": \"96e\", \"ballot\" : { " + "\"id\": \"96f\"}, \"status\": \"READY\"}]}";
	private static final String BALLOT_BOX_ID = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
	private static final String BALLOT_ID = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
	private static final String ELECTION_EVENT_ID = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);

	private static PathResolver pathResolver;
	private static BallotBoxService ballotBoxService;
	private static BallotBoxRepository ballotBoxRepositoryMock;
	private static ConfigurationEntityStatusService statusServiceMock;

	@BeforeAll
	static void setUpAll() {
		pathResolver = mock(PathResolver.class);
		statusServiceMock = mock(ConfigurationEntityStatusService.class);
		ballotBoxRepositoryMock = mock(BallotBoxRepository.class);

		ballotBoxService = new BallotBoxService(DomainObjectMapper.getNewInstance(), ballotBoxRepositoryMock, statusServiceMock, true);
	}

	@AfterEach
	void tearDown() {
		reset(pathResolver, statusServiceMock, ballotBoxRepositoryMock);
	}

	@Test
	void getBallotBoxesId() {
		// given
		when(ballotBoxRepositoryMock.list(anyMap())).thenReturn(BALLOT_BOX_READY);
		when(statusServiceMock.updateWithSynchronizedStatus(BallotBoxStatus.SIGNED.name(), BALLOT_BOX_ID, ballotBoxRepositoryMock,
				SynchronizeStatus.PENDING))
				.thenReturn("");

		// when
		final List<String> ballotBoxes = ballotBoxService.getBallotBoxesId("12e590cc85ad49af96b15ca761dfe49d");

		// then
		assertNotNull(ballotBoxes);
		assertEquals(1, ballotBoxes.size());
		assertEquals("96e",ballotBoxes.getFirst());
	}

	@Test
	void sign() {
		when(ballotBoxRepositoryMock.list(anyMap())).thenReturn(BALLOT_BOX_READY);
		when(statusServiceMock.updateWithSynchronizedStatus(BallotBoxStatus.SIGNED.name(), BALLOT_BOX_ID, ballotBoxRepositoryMock,
				SynchronizeStatus.PENDING))
				.thenReturn("");

		assertDoesNotThrow(() -> ballotBoxService.sign(BALLOT_BOX_ID));
	}

}
