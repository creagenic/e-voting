/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.shared.process;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static ch.post.it.evoting.securedatamanager.shared.Constants.CONFIG_FILE_NAME_SETUP_COMPONENT_VERIFICATION_CARD_KEYSTORES_PAYLOAD;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.io.TempDir;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.configuration.SetupComponentVerificationCardKeystoresPayload;
import ch.post.it.evoting.domain.configuration.VerificationCardKeystore;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.securedatamanager.setup.process.SetupPathResolver;

@DisplayName("A SetupComponentVerificationCardKeystoresPayloadService")
class SetupComponentVerificationCardKeystoresPayloadServiceTest {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final Hash hash = HashFactory.createHash();

	private static final String ELECTION_EVENT_ID = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final String MISSING_ELECTION_EVENT_ID = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final String EXISTING_ELECTION_EVENT_ID = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final String VERIFICATION_CARD_SET_ID = random.genRandomString(ID_LENGTH, base16Alphabet);
	private static final String INVALID_ID = "invalidId";

	private static ObjectMapper objectMapper;
	private static PathResolver pathResolver;
	private static SetupComponentVerificationCardKeystoresPayloadService setupComponentVerificationCardKeystoresPayloadService;

	@BeforeAll
	static void setUpAll(
			@TempDir
			final Path tempDir) throws IOException {
		objectMapper = DomainObjectMapper.getNewInstance();

		final PathResolver pathResolver = new SetupPathResolver(tempDir, Path.of(""), Path.of(""), Path.of(""), Path.of(""));

		final SetupComponentVerificationCardKeystoresPayloadFileRepository setupComponentVerificationCardKeystoresPayloadFileRepository =
				new SetupComponentVerificationCardKeystoresPayloadFileRepository(objectMapper, pathResolver);

		final SetupComponentVerificationCardKeystoresPayload setupComponentVerificationCardKeystoresPayload = validSetupComponentVerificationCardKeystoresPayload(
				ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID);
		setupComponentVerificationCardKeystoresPayloadFileRepository.save(setupComponentVerificationCardKeystoresPayload);

		final SetupComponentVerificationCardKeystoresPayloadUploadRepository setupComponentVerificationCardKeystoresPayloadUploadRepositoryMock =
				mock(SetupComponentVerificationCardKeystoresPayloadUploadRepository.class);
		setupComponentVerificationCardKeystoresPayloadService = new SetupComponentVerificationCardKeystoresPayloadService(
				setupComponentVerificationCardKeystoresPayloadFileRepository, setupComponentVerificationCardKeystoresPayloadUploadRepositoryMock);
	}

	private static SetupComponentVerificationCardKeystoresPayload validSetupComponentVerificationCardKeystoresPayload(final String electionEventId,
			final String verificationCardSetId) {

		// Create payload.
		final List<VerificationCardKeystore> verificationCardKeystores = List.of(
				new VerificationCardKeystore(random.genRandomString(ID_LENGTH, base16Alphabet),
						"dmVyaWZpY2F0aW9uQ2FyZEtleXN0b3JlMQ=="),
				new VerificationCardKeystore(random.genRandomString(ID_LENGTH, base16Alphabet),
						"dmVyaWZpY2F0aW9uQ2FyZEtleXN0b3JlMg=="),
				new VerificationCardKeystore(random.genRandomString(ID_LENGTH, base16Alphabet),
						"dmVyaWZpY2F0aW9uQ2FyZEtleXN0b3JlMw==")
		);

		final SetupComponentVerificationCardKeystoresPayload setupComponentVerificationCardKeystoresPayload = new SetupComponentVerificationCardKeystoresPayload(
				electionEventId, verificationCardSetId,
				verificationCardKeystores);

		final byte[] payloadHash = hash.recursiveHash(setupComponentVerificationCardKeystoresPayload);
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature(payloadHash);
		setupComponentVerificationCardKeystoresPayload.setSignature(signature);

		return setupComponentVerificationCardKeystoresPayload;
	}

	@Nested
	@DisplayName("saving")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class SaveTest {

		private SetupComponentVerificationCardKeystoresPayload setupComponentVerificationCardKeystoresPayload;

		private SetupComponentVerificationCardKeystoresPayloadService setupComponentVerificationCardKeystoresPayloadServiceTemp;

		@BeforeAll
		void setUpAll(
				@TempDir
				final Path tempDir) throws IOException {

			pathResolver = new SetupPathResolver(tempDir, Path.of(""), Path.of(""), Path.of(""), Path.of(""));

			final SetupComponentVerificationCardKeystoresPayloadFileRepository setupComponentVerificationCardKeystoresPayloadFileRepositoryTemp =
					new SetupComponentVerificationCardKeystoresPayloadFileRepository(objectMapper, pathResolver);

			final SetupComponentVerificationCardKeystoresPayloadUploadRepository setupComponentVerificationCardKeystoresPayloadUploadRepositoryMock =
					mock(SetupComponentVerificationCardKeystoresPayloadUploadRepository.class);

			setupComponentVerificationCardKeystoresPayloadServiceTemp = new SetupComponentVerificationCardKeystoresPayloadService(
					setupComponentVerificationCardKeystoresPayloadFileRepositoryTemp,
					setupComponentVerificationCardKeystoresPayloadUploadRepositoryMock);
		}

		@BeforeEach
		void setUp() {
			setupComponentVerificationCardKeystoresPayload = validSetupComponentVerificationCardKeystoresPayload(ELECTION_EVENT_ID,
					VERIFICATION_CARD_SET_ID);
		}

		@Test
		@DisplayName("a valid payload does not throw")
		void saveValidPayload() {
			assertDoesNotThrow(() -> setupComponentVerificationCardKeystoresPayloadServiceTemp.save(setupComponentVerificationCardKeystoresPayload));

			assertTrue(Files.exists(pathResolver.resolveVerificationCardSetPath(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID)
					.resolve(CONFIG_FILE_NAME_SETUP_COMPONENT_VERIFICATION_CARD_KEYSTORES_PAYLOAD)));
		}

		@Test
		@DisplayName("a null setup component verification card keystores payload throws")
		void saveNullPayload() {
			assertThrows(NullPointerException.class, () -> setupComponentVerificationCardKeystoresPayloadServiceTemp.save(null));
		}
	}

	@Nested
	@DisplayName("calling exist")
	@TestInstance(TestInstance.Lifecycle.PER_CLASS)
	class ExistTest {

		@Test
		@DisplayName("for valid election event and verification card set returns true")
		void existValidElectionEvent() {
			assertTrue(setupComponentVerificationCardKeystoresPayloadService.exist(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
		}

		@Test
		@DisplayName("for null input throws NullPointerException")
		void existNullInput() {
			assertThrows(NullPointerException.class,
					() -> setupComponentVerificationCardKeystoresPayloadService.exist(null, VERIFICATION_CARD_SET_ID));
			assertThrows(NullPointerException.class,
					() -> setupComponentVerificationCardKeystoresPayloadService.exist(EXISTING_ELECTION_EVENT_ID, null));
		}

		@Test
		@DisplayName("for invalid input throws FailedValidationException")
		void existInvalidInput() {
			assertThrows(FailedValidationException.class,
					() -> setupComponentVerificationCardKeystoresPayloadService.exist(INVALID_ID, VERIFICATION_CARD_SET_ID));
			assertThrows(FailedValidationException.class,
					() -> setupComponentVerificationCardKeystoresPayloadService.exist(EXISTING_ELECTION_EVENT_ID, INVALID_ID));
		}

		@Test
		@DisplayName("for non existing election event returns false")
		void existNonExistingElectionEvent() {
			assertFalse(setupComponentVerificationCardKeystoresPayloadService.exist(MISSING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
		}

	}

	@Nested
	@DisplayName("loading")
	@TestInstance(TestInstance.Lifecycle.PER_METHOD)
	class LoadTest {

		@Test
		@DisplayName("existing election event and verification card set returns expected setup component verification card keystores payload")
		void loadExistingElectionEventValidSignature() {
			final SetupComponentVerificationCardKeystoresPayload setupComponentVerificationCardKeystoresPayload = setupComponentVerificationCardKeystoresPayloadService.load(
					ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID);

			assertEquals(ELECTION_EVENT_ID, setupComponentVerificationCardKeystoresPayload.getElectionEventId());
		}

		@Test
		@DisplayName("null input throws NullPointerException")
		void loadNullInput() {
			assertThrows(NullPointerException.class,
					() -> setupComponentVerificationCardKeystoresPayloadService.load(null, VERIFICATION_CARD_SET_ID));
			assertThrows(NullPointerException.class, () -> setupComponentVerificationCardKeystoresPayloadService.load(ELECTION_EVENT_ID, null));
		}

		@Test
		@DisplayName("invalid input throws FailedValidationException")
		void loadInvalidInput() {
			assertThrows(FailedValidationException.class,
					() -> setupComponentVerificationCardKeystoresPayloadService.load(INVALID_ID, VERIFICATION_CARD_SET_ID));
			assertThrows(FailedValidationException.class,
					() -> setupComponentVerificationCardKeystoresPayloadService.load(ELECTION_EVENT_ID, INVALID_ID));
		}

		@Test
		@DisplayName("existing election event and verification card set but with missing payload throws IllegalStateException")
		void loadMissingPayload() {
			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> setupComponentVerificationCardKeystoresPayloadService.load(MISSING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));

			final String errorMessage = String.format(
					"Requested setup component verification card keystores payload is not present. [electionEventId: %s, verificationCardSetId: %s]",
					MISSING_ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID);
			assertEquals(errorMessage, Throwables.getRootCause(exception).getMessage());
		}

	}

}
