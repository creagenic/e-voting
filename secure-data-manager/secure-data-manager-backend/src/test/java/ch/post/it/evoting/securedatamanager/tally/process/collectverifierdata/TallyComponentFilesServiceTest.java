/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.process.collectverifierdata;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SignatureException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import javax.xml.datatype.XMLGregorianCalendar;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.junit.jupiter.MockitoExtension;

import ch.ech.xmlns.ech_0110._4.Delivery;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.xml.XmlNormalizer;
import ch.post.it.evoting.evotinglibraries.xml.hashable.HashableEch0110Factory;
import ch.post.it.evoting.evotinglibraries.xml.hashable.HashableEch0222Factory;
import ch.post.it.evoting.evotinglibraries.xml.hashable.HashableResultsFactory;
import ch.post.it.evoting.evotinglibraries.xml.xmlns.evotingdecrypt.Results;
import ch.post.it.evoting.securedatamanager.shared.Constants;
import ch.post.it.evoting.securedatamanager.shared.process.BallotBoxRepository;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;
import ch.post.it.evoting.securedatamanager.shared.process.EvotingConfigService;
import ch.post.it.evoting.securedatamanager.tally.process.TallyComponentVotesFileRepository;
import ch.post.it.evoting.securedatamanager.tally.process.TallyComponentVotesService;
import ch.post.it.evoting.securedatamanager.tally.process.TallyPathResolver;

@ExtendWith(MockitoExtension.class)
class TallyComponentFilesServiceTest {

	private static final String ELECTION_EVENT_ID = "BB4E536C3C02A992FD337BDD1F40FCBE";
	private static final String CONTEST_IDENTIFICATION = "Post_E2E_DEV";
	private static final XmlNormalizer XML_NORMALIZER = new XmlNormalizer();
	private static final Hash hash = HashFactory.createHash();

	private static final TallyPathResolver pathResolverMock = mock(TallyPathResolver.class);
	private static final BallotBoxRepository ballotBoxRepositoryMock = mock(BallotBoxRepository.class);
	private static final SignatureKeystore<Alias> signatureKeystoreMock = mock(SignatureKeystore.class);
	private static final ElectionEventService electionEventServiceMock = mock(ElectionEventService.class);

	private static Path testFolderPath;
	private static EvotingConfigService evotingConfigService;
	private static TallyComponentDecryptService tallyComponentDecryptService;
	private static TallyComponentEch0110Service tallyComponentEch0110Service;
	private static TallyComponentEch0222Service tallyComponentEch0222Service;
	private static TallyComponentFilesService tallyComponentFilesService;

	@BeforeAll
	static void setUpAll(
			@TempDir
			final Path tempDir) throws SignatureException, URISyntaxException, IOException {

		when(signatureKeystoreMock.generateSignature(any(), any())).thenReturn("signature".getBytes(StandardCharsets.UTF_8));
		when(signatureKeystoreMock.verifySignature(any(), any(), any(), any())).thenReturn(true);

		testFolderPath = Paths.get(
				Objects.requireNonNull(TallyComponentFilesServiceTest.class.getResource("/TallyComponentFilesServiceTest/")).toURI());

		when(pathResolverMock.resolveConfigurationPath()).thenReturn(testFolderPath);
		when(pathResolverMock.resolveElectionEventPath(ELECTION_EVENT_ID)).thenReturn(tempDir);

		record BallotBox(String ballotId, String ballotBoxId) {
		}

		final List<BallotBox> ballotBoxes = List.of(
				new BallotBox("8F21286191782B442B44F449EA432B65", "8318B5DEBBBA7AFC09A1D7DBFDBD65B3"),
				new BallotBox("758E591E2CEB67D02101A65FE14072C1", "43F9F6E598837CEB254C9830417F912F"),
				new BallotBox("A578C04473ED1B3EB8037CEFC41F40FC", "E1328A455652329955D42283CDD4EF86"),
				new BallotBox("D3BFE04CC5EB4437F54F82C51855DB12", "BAF31AAE0B00BC1C57E46750AE2C5C9A"));

		ballotBoxes.forEach(
				ballotBox -> when(pathResolverMock.resolveBallotBoxPath(ELECTION_EVENT_ID, ballotBox.ballotId, ballotBox.ballotBoxId)).thenReturn(
						testFolderPath.resolve(Constants.BALLOT_BOXES).resolve(ballotBox.ballotBoxId)));

		when(electionEventServiceMock.findElectionEventId()).thenReturn(ELECTION_EVENT_ID);
		evotingConfigService = new EvotingConfigService(electionEventServiceMock,
				new TallyEvotingConfigFileRepository(pathResolverMock, electionEventServiceMock, signatureKeystoreMock));

		tallyComponentDecryptService = new TallyComponentDecryptService(evotingConfigService,
				new TallyComponentDecryptFileRepository(pathResolverMock, signatureKeystoreMock));

		tallyComponentEch0110Service = new TallyComponentEch0110Service(
				tallyComponentDecryptService,
				evotingConfigService,
				new TallyComponentEch0110FileRepository(pathResolverMock, signatureKeystoreMock),
				XML_NORMALIZER);

		tallyComponentEch0222Service = new TallyComponentEch0222Service(
				tallyComponentDecryptService,
				evotingConfigService,
				new TallyComponentEch0222FileRepository(pathResolverMock, signatureKeystoreMock),
				XML_NORMALIZER);

		final TallyComponentVotesService tallyComponentVotesService = new TallyComponentVotesService(
				new TallyComponentVotesFileRepository(pathResolverMock, DomainObjectMapper.getNewInstance()));

		when(ballotBoxRepositoryMock.listByElectionEvent(ELECTION_EVENT_ID)).thenReturn(Files.readString(testFolderPath.resolve("ballotBoxes.json")));

		tallyComponentFilesService = new TallyComponentFilesService(tallyComponentDecryptService, tallyComponentEch0110Service,
				tallyComponentEch0222Service, evotingConfigService, ballotBoxRepositoryMock, tallyComponentVotesService);
	}

	@Test
	void happyPath() {
		assertDoesNotThrow(() -> tallyComponentFilesService.generate(ELECTION_EVENT_ID));

		assertTrue(sameEvotingDecrypt());
		assertTrue(sameEch0110());
		assertTrue(sameEch0222());
	}

	private boolean sameEvotingDecrypt() {

		final Results evotingDecryptXML = new TallyComponentDecryptService(
				evotingConfigService,
				new TallyComponentDecryptFileRepository(pathResolverMock, signatureKeystoreMock))
				.load(ELECTION_EVENT_ID, CONTEST_IDENTIFICATION);

		final Results evotingDecryptXMLRegenerated = tallyComponentDecryptService.load(ELECTION_EVENT_ID, CONTEST_IDENTIFICATION);

		final Hashable hashableEvotingDecryptXML = HashableResultsFactory.fromResults(evotingDecryptXML);
		final Hashable hashableEvotingDecryptXMLRegenerated = HashableResultsFactory.fromResults(evotingDecryptXMLRegenerated);

		return Arrays.equals(hash.recursiveHash(hashableEvotingDecryptXML), hash.recursiveHash(hashableEvotingDecryptXMLRegenerated));
	}

	private boolean sameEch0110() {

		final Delivery eCH0110XML = new TallyComponentEch0110Service(
				tallyComponentDecryptService,
				evotingConfigService,
				new TallyComponentEch0110FileRepository(pathResolverMock, signatureKeystoreMock),
				XML_NORMALIZER)
				.load(ELECTION_EVENT_ID, CONTEST_IDENTIFICATION);

		final Delivery regenerated = tallyComponentEch0110Service.load(ELECTION_EVENT_ID, CONTEST_IDENTIFICATION);

		// Ignore timestamp fields (use original timestamps in re-generated file).
		final XMLGregorianCalendar eCH0110OriginalMessageDate = eCH0110XML.getDeliveryHeader().getMessageDate();
		regenerated.getDeliveryHeader().withMessageDate(eCH0110OriginalMessageDate);
		final XMLGregorianCalendar eCH0110OriginalCreationDateTime = eCH0110XML.getResultDelivery().getReportingBody().getCreationDateTime();
		regenerated.getResultDelivery().getReportingBody().withCreationDateTime(eCH0110OriginalCreationDateTime);

		final Hashable hashableECH0110XML = HashableEch0110Factory.fromDelivery(eCH0110XML);
		final Hashable hashableECH0110XMLRegenerated = HashableEch0110Factory.fromDelivery(regenerated);

		return Arrays.equals(hash.recursiveHash(hashableECH0110XML), hash.recursiveHash(hashableECH0110XMLRegenerated));
	}

	private boolean sameEch0222() {
		final ch.ech.xmlns.ech_0222._1.Delivery eCH0222XML = new TallyComponentEch0222Service(
				tallyComponentDecryptService,
				evotingConfigService,
				new TallyComponentEch0222FileRepository(pathResolverMock, signatureKeystoreMock),
				XML_NORMALIZER)
				.load(ELECTION_EVENT_ID, CONTEST_IDENTIFICATION);

		final ch.ech.xmlns.ech_0222._1.Delivery regenerated = tallyComponentEch0222Service.load(ELECTION_EVENT_ID, CONTEST_IDENTIFICATION);

		// Ignore timestamp fields (use original timestamps in re-generated file).
		final XMLGregorianCalendar eCH0222OriginalMessageDate = eCH0222XML.getDeliveryHeader().getMessageDate();
		regenerated.getDeliveryHeader().withMessageDate(eCH0222OriginalMessageDate);
		final XMLGregorianCalendar eCH0222OriginalCreationDateTime = eCH0222XML.getRawDataDelivery().getReportingBody().getCreationDateTime();
		regenerated.getRawDataDelivery().getReportingBody().withCreationDateTime(eCH0222OriginalCreationDateTime);

		final Hashable hashableECH0222XML = HashableEch0222Factory.fromDelivery(eCH0222XML);
		final Hashable hashableECH0222XMLRegenerated = HashableEch0222Factory.fromDelivery(regenerated);

		return Arrays.equals(hash.recursiveHash(hashableECH0222XML), hash.recursiveHash(hashableECH0222XMLRegenerated));
	}

}
