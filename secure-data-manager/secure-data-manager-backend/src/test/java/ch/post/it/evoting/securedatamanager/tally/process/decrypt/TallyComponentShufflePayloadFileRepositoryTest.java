/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.process.decrypt;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.nio.file.Files;
import java.nio.file.Path;
import java.security.SecureRandom;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mockito;

import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.mixnet.VerifiableShuffle;
import ch.post.it.evoting.evotinglibraries.domain.SerializationTestData;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.TallyComponentShufflePayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.VerifiablePlaintextDecryption;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.securedatamanager.shared.Constants;
import ch.post.it.evoting.securedatamanager.tally.process.TallyPathResolver;

@DisplayName("Use TallyComponentShufflePayloadPersistenceService to ")
class TallyComponentShufflePayloadFileRepositoryTest {

	private static final String FILE_NAME = "tallyComponentShufflePayload_4";
	private static final String electionEventId = "f28493b098604663b6a6969f53f51b56";
	private static final String ballotId = "f0642fdfe4864f4985ac07c057da54b7";
	private static final String ballotBoxId = "672b1b49ed0341a3baa953d611b90b74";

	private static TallyComponentShufflePayload expectedTallyComponentShufflePayload;
	private static TallyComponentShufflePayloadFileRepository tallyComponentShufflePayloadFileRepository;
	private static TallyPathResolver payloadResolver;

	@TempDir
	Path tempDir;

	@BeforeAll
	static void setUpAll() {
		// Create ciphertexts list.
		final GqGroup gqGroup = SerializationTestData.getGqGroup();
		final int nbrMessage = 4;

		final VerifiableShuffle verifiableShuffle = SerializationTestData.getVerifiableShuffle(nbrMessage);
		final VerifiablePlaintextDecryption verifiablePlaintextDecryption = SerializationTestData.getVerifiablePlaintextDecryption(nbrMessage);

		// Generate random bytes for signature content and create payload signature.
		final byte[] randomBytes = new byte[10];
		new SecureRandom().nextBytes(randomBytes);
		final CryptoPrimitivesSignature signature = new CryptoPrimitivesSignature(randomBytes);

		expectedTallyComponentShufflePayload = new TallyComponentShufflePayload(gqGroup, electionEventId, ballotBoxId, verifiableShuffle,
				verifiablePlaintextDecryption, signature);

		payloadResolver = Mockito.mock(TallyPathResolver.class);
		tallyComponentShufflePayloadFileRepository = new TallyComponentShufflePayloadFileRepository(FILE_NAME, DomainObjectMapper.getNewInstance(),
				payloadResolver);
	}

	@Test
	@DisplayName("read TallyComponentShufflePayload file")
	void readMixnetPayload() {
		// Mock payloadResolver path and write payload
		when(payloadResolver.resolveBallotBoxPath(electionEventId, ballotId, ballotBoxId)).thenReturn(tempDir);
		tallyComponentShufflePayloadFileRepository.savePayload(electionEventId, ballotId, ballotBoxId, expectedTallyComponentShufflePayload);

		// Read payload and check
		final TallyComponentShufflePayload actualMixnetPayload = tallyComponentShufflePayloadFileRepository.getPayload(electionEventId, ballotId,
				ballotBoxId);

		assertEquals(expectedTallyComponentShufflePayload, actualMixnetPayload);
	}

	@Test
	@DisplayName("save TallyComponentShufflePayload file")
	void saveMixnetPayload() {
		// Mock payloadResolver path
		when(payloadResolver.resolveBallotBoxPath(electionEventId, ballotId, ballotBoxId)).thenReturn(tempDir);
		final Path payloadPath = tempDir.resolve(FILE_NAME + Constants.JSON);

		assertFalse(Files.exists(payloadPath), "The tally component shuffle payload file should not exist at this point");

		// Write payload
		assertNotNull(tallyComponentShufflePayloadFileRepository.savePayload(electionEventId, ballotId, ballotBoxId,
				expectedTallyComponentShufflePayload));

		assertTrue(Files.exists(payloadPath), "The tally component shuffle payload file should exist at this point");
	}

}
