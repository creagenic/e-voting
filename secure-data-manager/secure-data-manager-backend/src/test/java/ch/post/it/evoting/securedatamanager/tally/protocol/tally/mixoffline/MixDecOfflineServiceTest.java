/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.securedatamanager.tally.protocol.tally.mixoffline;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.domain.generators.ControlComponentShufflePayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.election.generators.PrimesMappingTableGenerator;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.PrimesMappingTableAlgorithms;
import ch.post.it.evoting.securedatamanager.shared.process.BallotBoxService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventContextPayloadService;
import ch.post.it.evoting.securedatamanager.shared.process.ElectionEventService;
import ch.post.it.evoting.securedatamanager.tally.process.decrypt.IdentifierValidationService;

@DisplayName("mixDecOffline called with")
class MixDecOfflineServiceTest extends TestGroupSetup {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private static String electionEventId;
	private static String ballotId;
	private static String ballotBoxId;
	private static PrimesMappingTable primesMappingTable;
	private static ControlComponentShufflePayload controlComponentShufflePayload;
	private static List<char[]> electoralBoardPasswords;
	private MixDecOfflineService mixDecOfflineService;
	private ElectionEventService electionEventService;
	private BallotBoxService ballotBoxService;

	@BeforeAll
	static void setUpSuite() {
		electionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);
		ballotId = random.genRandomString(ID_LENGTH, base16Alphabet);
		ballotBoxId = random.genRandomString(ID_LENGTH, base16Alphabet);
		primesMappingTable = new PrimesMappingTableGenerator(gqGroup).generate(1);
		electoralBoardPasswords = List.of("Password_ElectoralBoard1".toCharArray(), "Password_ElectoralBoard2".toCharArray());

		final int N = 2;
		final int l = 5;
		final int nodeId = NODE_IDS.last();
		final ControlComponentShufflePayloadGenerator controlComponentShufflePayloadGenerator = new ControlComponentShufflePayloadGenerator(gqGroup);
		controlComponentShufflePayload = controlComponentShufflePayloadGenerator.generate(electionEventId, ballotBoxId, nodeId, N, l);
	}

	@BeforeEach
	void setUp() {
		final MixDecOfflineAlgorithm mixDecOfflineAlgorithm = mock(MixDecOfflineAlgorithm.class);
		final ElectionEventContextPayloadService electionEventContextPayloadService = mock(ElectionEventContextPayloadService.class);
		electionEventService = mock(ElectionEventService.class);
		ballotBoxService = mock(BallotBoxService.class);

		final IdentifierValidationService identifierValidationService = new IdentifierValidationService(electionEventService, ballotBoxService);
		final PrimesMappingTableAlgorithms primesMappingTableAlgorithms = mock(PrimesMappingTableAlgorithms.class);
		when(primesMappingTableAlgorithms.getDelta(any())).thenReturn(1);

		mixDecOfflineService = new MixDecOfflineService(electionEventContextPayloadService, mixDecOfflineAlgorithm, identifierValidationService,
				primesMappingTableAlgorithms);

		when(electionEventService.exists(electionEventId)).thenReturn(true);
		when(ballotBoxService.getBallotBoxesId(electionEventId)).thenReturn(List.of(ballotBoxId));
		when(ballotBoxService.getBallotId(ballotBoxId)).thenReturn(ballotId);
	}

	private static Stream<Arguments> provideNullParameters() {
		final List<char[]> electoralBoardPasswordsWithNull = new ArrayList<>(electoralBoardPasswords);
		electoralBoardPasswordsWithNull.add(null);
		return Stream.of(
				Arguments.of(null, ballotId, ballotBoxId, primesMappingTable, controlComponentShufflePayload, electoralBoardPasswords),
				Arguments.of(electionEventId, null, ballotBoxId, primesMappingTable, controlComponentShufflePayload, electoralBoardPasswords),
				Arguments.of(electionEventId, ballotId, null, primesMappingTable, controlComponentShufflePayload, electoralBoardPasswords),
				Arguments.of(electionEventId, ballotId, ballotBoxId, null, controlComponentShufflePayload, electoralBoardPasswords),
				Arguments.of(electionEventId, ballotId, ballotBoxId, primesMappingTable, null, electoralBoardPasswords),
				Arguments.of(electionEventId, ballotId, ballotBoxId, primesMappingTable, controlComponentShufflePayload, null),
				Arguments.of(electionEventId, ballotId, ballotBoxId, primesMappingTable, controlComponentShufflePayload,
						electoralBoardPasswordsWithNull)
		);
	}

	@ParameterizedTest
	@MethodSource("provideNullParameters")
	@DisplayName("null parameters throws NullPointerException")
	void mixDecOfflineWithNullParametersThrows(final String electionEventId, final String ballotId, final String ballotBoxId,
			final PrimesMappingTable primesMappingTable, final ControlComponentShufflePayload controlComponentShufflePayload,
			final List<char[]> electoralBoardPasswords) {
		assertThrows(NullPointerException.class,
				() -> mixDecOfflineService.mixDecOffline(electionEventId, ballotId, ballotBoxId, primesMappingTable, controlComponentShufflePayload,
						electoralBoardPasswords));
	}

	@Test
	@DisplayName("invalid election event id throws FailedValidationException")
	void mixDecOfflineWithInvalidElectionEventIdThrows() {
		assertThrows(FailedValidationException.class,
				() -> mixDecOfflineService.mixDecOffline("InvalidElectionEventId", ballotId, ballotBoxId, primesMappingTable,
						controlComponentShufflePayload, electoralBoardPasswords));
	}

	@Test
	@DisplayName("invalid ballot id throws FailedValidationException")
	void mixDecOfflineWithInvalidBallotIdThrows() {
		assertThrows(FailedValidationException.class,
				() -> mixDecOfflineService.mixDecOffline(electionEventId, "InvalidBallotId", ballotBoxId, primesMappingTable,
						controlComponentShufflePayload, electoralBoardPasswords));
	}

	@Test
	@DisplayName("invalid ballot box id throws FailedValidationException")
	void mixDecOfflineWithInvalidBallotBoxIdThrows() {
		assertThrows(FailedValidationException.class,
				() -> mixDecOfflineService.mixDecOffline(electionEventId, ballotId, "InvalidBallotBoxId", primesMappingTable,
						controlComponentShufflePayload, electoralBoardPasswords));
	}

	@Test
	@DisplayName("wrong number of electoral board members passwords throws IllegalArgumentException")
	void mixDecOfflineWithWrongNumberOfElectoralBoardMembersPasswordsThrows() {
		final List<char[]> tooFewElectoralBoardMembersPasswords = List.of("Password_ElectoralBoard1".toCharArray());

		assertThrows(IllegalArgumentException.class,
				() -> mixDecOfflineService.mixDecOffline(electionEventId, ballotId, ballotBoxId, primesMappingTable, controlComponentShufflePayload,
						tooFewElectoralBoardMembersPasswords));
	}

	@Test
	@DisplayName("non existent election event id throws IllegalArgumentException")
	void mixDecryptWhenNonExistingElectionEventIdThrowsIllegalArgumentException() {
		final String nonExistingElectionEventId = "0123456789abcdef0123456789abcdef";

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> mixDecOfflineService.mixDecOffline(nonExistingElectionEventId, ballotId, ballotBoxId, primesMappingTable,
						controlComponentShufflePayload, electoralBoardPasswords));

		final String expected = String.format("The given election event ID does not exist. [electionEventId: %s]", nonExistingElectionEventId);
		assertEquals(expected, exception.getMessage());
	}

	@Test
	@DisplayName("non existent ballot box id throws IllegalArgumentException")
	void mixDecryptWhenNonExistingBallotBoxIdThrowsIllegalArgumentException() {
		final String nonExistingBallotBoxId = "0123456789abcdef0123456789abcdef";

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> mixDecOfflineService.mixDecOffline(electionEventId, ballotId, nonExistingBallotBoxId, primesMappingTable,
						controlComponentShufflePayload, electoralBoardPasswords));

		final String expected = String.format(
				"The given ballot box ID does not belong to the given election event ID. [ballotBoxId: %s, electionEventId: %s]",
				nonExistingBallotBoxId, electionEventId);
		assertEquals(expected, exception.getMessage());
	}

	@Test
	@DisplayName("non existent ballot id throws IllegalArgumentException")
	void mixDecryptWhenNonExistingBallotIdThrowsIllegalArgumentException() {
		final String nonExistingBallotId = "0123456789abcdef0123456789abcdef";

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> mixDecOfflineService.mixDecOffline(electionEventId, nonExistingBallotId, ballotBoxId, primesMappingTable,
						controlComponentShufflePayload, electoralBoardPasswords));

		final String expectedMessage = String.format(
				"The given ballot ID does not belong to the given ballot box ID. [ballotId: %s, ballotBoxId: %s]", nonExistingBallotId, ballotBoxId);
		assertEquals(expectedMessage, exception.getMessage());
	}
}
