/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {bootstrapSdmApplication} from '@sdm/shared-feature-shell';
import {appRoutes} from './app.routes';

import '@angular/localize/init';

bootstrapSdmApplication(appRoutes);
