/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
const fs = require('fs');
const path = require('path');
const { contextBridge, ipcRenderer } = require('electron');
const { join } = require('path');
const {
  convertToRawSettings,
  convertToUISettings,
  getModeConfig,
} = require('./utils');

function readRawSettingsJson() {
  const file = path.join(__dirname, './settings.json');
  const rawSettings = fs.readFileSync(file, 'utf8');
  return JSON.parse(rawSettings);
}

/**
 * Provides a list of Settings.
 * @returns {Settings[]}
 */
function getUISettingsList() {
  const rawSettingsList = readRawSettingsJson();
  const settingsList = [];
  for (let index in rawSettingsList) {
    settingsList.push(convertToUISettings(rawSettingsList[index]));
  }
  return settingsList;
}

function saveUISettings(UISettings, startSDM) {
  const convertedRawSettings = convertToRawSettings(UISettings);

  const rawSettingsList = readRawSettingsJson();
  // If id is not set, set it to the next available id
  if (UISettings.id === -1) {
    convertedRawSettings.id = rawSettingsList.length;
  }
  rawSettingsList[convertedRawSettings.id] = convertedRawSettings;

  const file = path.join(__dirname, `./settings.json`);
  fs.writeFile(file, JSON.stringify(rawSettingsList), (err) => {
    if (err) console.log(err);
    if (startSDM) {
      writeApplicationProperties(convertedRawSettings);
    }
  });
}

function deleteUISettings(id) {
  const rawSettingsList = readRawSettingsJson();
  let newIndex = 0;
  let result = [];
  for (let index in rawSettingsList) {
    if (rawSettingsList[index].id !== id) {
      rawSettingsList[index].id = newIndex;
      result[newIndex] = rawSettingsList[index];
      newIndex++;
    }
  }

  const file = path.join(__dirname, `./settings.json`);
  fs.writeFile(file, JSON.stringify(result), (err) => {
    if (err) console.log(err);
  });
}

function writeApplicationProperties(rawSettings) {
  let properties = '';
  const debugConfig = readDebugConfig();
  rawSettings.properties.forEach((property) => {
    const debugProperty = debugConfig.properties.filter(
      (debugProperty) => debugProperty.key === property.key,
    );

    properties += getPropertyLine(
      property.key,
      debugProperty.length === 1 ? debugProperty[0].value : property.value,
    );
  });

  debugConfig.properties.forEach((property) => {
    const propertyFound = rawSettings.properties.find(
      (rawProperty) => rawProperty.key === property.key,
    );
    if (!propertyFound) {
      properties += getPropertyLine(property.key, property.value);
    }
  });

  const file = join(
    __dirname,
    '..',
    '..',
    `./application-profile_${rawSettings.id}.properties`,
  );
  fs.writeFile(file, properties, (err) => {
    if (err) console.log(err);
    startupSdmBackend(rawSettings.mode, rawSettings.id);
  });
}

function getPropertyLine(key, value) {
  if (typeof value === 'string') {
    value = value.replace(/\\/g, '\\\\');
  }
  return key + '=' + value + '\n';
}

function startupSdmBackend(mode, id) {
  const debugConfig = readDebugConfig();
  const reply = ipcRenderer.sendSync('startup-sdm-backend', {
    mode: mode,
    profileId: id,
    debug: debugConfig.debug,
    useWindow: debugConfig.useWindow,
  });
  if (reply.pid !== null) {
    checkBackend(mode);
  } else {
    console.error('Backend not started.');
  }
}

function checkBackend(mode) {
  const modeConfig = getModeConfig(mode);
  setTimeout(() => {
    fetch(`${modeConfig.backendUrl}/actuator/health`, { method: 'GET' }).then(
      (response) => {
        // network error in the 4xx–5xx range
        if (!response.ok) {
          console.error('Backend server error response (4xx–5xx).', response);
          // recursive check
          checkBackend(mode);
        }
        response.json().then((data) => {
          if (data.status === 'UP') {
            console.log('Backend server is ready.');
            startupSdmFrontend(mode);
          } else {
            console.error('Ready but not UP.', data);
          }
        });
      },
      (error) => {
        console.error(
          'Backend server is not reachable (error on fetch).',
          error,
        );
        // recursive check
        checkBackend(mode);
      },
    );
  }, 3000);
}

function startupSdmFrontend(mode) {
  const debugConfig = readDebugConfig();
  ipcRenderer.sendSync('startup-sdm-frontend', {
    mode: mode,
    useWindow: debugConfig.useWindow,
  });
}

function readDebugConfig() {
  const file = path.join(__dirname, './debug-config.json');
  const debug = fs.readFileSync(file, 'utf8');
  return JSON.parse(debug);
}

function isValidPath(path) {
  return fs.existsSync(path);
}

// Exposed API
contextBridge.exposeInMainWorld('SettingsApi', {
  getSettingsList: getUISettingsList,
  saveSettings: saveUISettings,
  deleteSettings: deleteUISettings,
  isValidPath: isValidPath,
});
