/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
const getModeConfig = (mode) => {
  switch (mode) {
    case 'LAUNCHER':
      return {
        mode: 'LAUNCHER',
        app: 'sdm-launcher',
        clientUrl: 'http://localhost:4203/',
      };
    case 'SETUP':
      return {
        mode: 'SETUP',
        app: 'sdm-setup',
        clientUrl: 'http://localhost:4200/',
        backendUrl: 'http://localhost:8090',
        debugPort: '6090',
      };
    case 'ONLINE':
      return {
        mode: 'ONLINE',
        app: 'sdm-online',
        clientUrl: 'http://localhost:4202/',
        backendUrl: 'http://localhost:8092',
        debugPort: '6092',
      };
    case 'TALLY':
      return {
        mode: 'TALLY',
        app: 'sdm-tally',
        clientUrl: 'http://localhost:4201/',
        backendUrl: 'http://localhost:8091',
        debugPort: '6091',
      };
  }
  throw Error('Application mode is not valid.');
};

// Map a raw settings to a UI settings
const convertRawSettingsToUISettings = (rawSettings) => {
  let directTrustLocation = '';
  let directTrustPwdLocation = '';
  if (rawSettings.mode === 'SETUP' || rawSettings.mode === 'TALLY') {
    directTrustLocation = getValueFromKey(
      rawSettings.properties,
      'direct.trust.keystore.location',
    );
    directTrustPwdLocation = getValueFromKey(
      rawSettings.properties,
      'direct.trust.keystore.password.location',
    );
  }
  let outputFolder = '';
  if (rawSettings.mode === 'SETUP' || rawSettings.mode === 'ONLINE') {
    outputFolder = getValueFromKey(
        rawSettings.properties,
        'sdm.output.folder.path',
    );
  }

  return {
    id: rawSettings.id,
    mode: rawSettings.mode,
    electionEventSeed: getValueFromKey(
      rawSettings.properties,
      'sdm.election.event.seed',
    ),
    onlineHost: getValueFromKey(rawSettings.properties, 'voter.portal.host'),
    twoWaySSLLocation: getValueFromKey(
      rawSettings.properties,
      'voting-server.connection.client-keystore-location',
    ),
    twoWaySSLPwdLocation: getValueFromKey(
      rawSettings.properties,
      'voting-server.connection.client-keystore-password-location',
    ),
    outputFolder: outputFolder,
    externalConfigurationFolder: getValueFromKey(
      rawSettings.properties,
      'sdm.external.configuration.folder.path',
    ),
    printFolder: getValueFromKey(
      rawSettings.properties,
      'sdm.output.print.folder.path',
    ),
    verifierFolder: getValueFromKey(
      rawSettings.properties,
      'sdm.output.verifier.folder.path',
    ),
    workspaceFolder: getValueFromKey(rawSettings.properties, 'sdm.workspace'),
    exportPwd: getValueFromKey(
      rawSettings.properties,
      'import.export.zip.password',
    ),
    verifierDatasetPwd: getValueFromKey(
      rawSettings.properties,
      'verifier.export.zip.password',
    ),
    databasePwd: getValueFromKey(rawSettings.properties, 'database.password'),
    choiceCodeGenerationChunkSize: getValueFromKey(
      rawSettings.properties,
      'choiceCodeGenerationChunkSize',
    ),
    directTrustLocation: directTrustLocation,
    directTrustPwdLocation: directTrustPwdLocation,
  };
};

function getValueFromKey(properties, key) {
  let property = properties.find((prop) => prop.key === key);
  if (property) {
    return property.value;
  }
  return null;
}

// Map a UI settings to a raw settings
const convertUISettingsToRawSettings = (UISettings) => {
  const rawSettings = {};

  rawSettings.id = UISettings.id;
  rawSettings.mode = UISettings.mode;

  // Properties
  rawSettings.properties = [];

  // Common properties
  rawSettings.properties.push({
    key: 'sdm.election.event.seed',
    value: UISettings.electionEventSeed,
  });
  rawSettings.properties.push({
    key: 'sdm.workspace',
    value: UISettings.workspaceFolder,
  });
  rawSettings.properties.push({
    key: 'sdm.output.folder.path',
    value: UISettings.outputFolder,
  });
  rawSettings.properties.push({
    key: 'import.export.zip.password',
    value: UISettings.exportPwd,
  });
  rawSettings.properties.push({
    key: 'verifier.export.zip.password',
    value: UISettings.verifierDatasetPwd,
  });
  rawSettings.properties.push({
    key: 'database.password',
    value: UISettings.databasePwd,
  });

  if (UISettings.mode === 'ONLINE') {
    // Only for Online
    rawSettings.properties.push({ key: 'server.port', value: '8092' });
    rawSettings.properties.push({ key: 'role.isSetup', value: 'false' });
    rawSettings.properties.push({ key: 'role.isTally', value: 'false' });
    rawSettings.properties.push({ key: 'voter.portal.enabled', value: 'true' });
    rawSettings.properties.push({
      key: 'voter.portal.host',
      value: UISettings.onlineHost,
    });

    if (
      UISettings.twoWaySSLLocation &&
      UISettings.twoWaySSLLocation.trim() !== '' &&
      UISettings.twoWaySSLPwdLocation &&
      UISettings.twoWaySSLPwdLocation.trim() !== ''
    ) {
      rawSettings.properties.push({
        key: 'voting-server.connection.client-keystore-location',
        value: UISettings.twoWaySSLLocation,
      });
      rawSettings.properties.push({
        key: 'voting-server.connection.client-keystore-password-location',
        value: UISettings.twoWaySSLPwdLocation,
      });
    }
  } else {
    // Common to Setup & Tally
    rawSettings.properties.push({
      key: 'voter.portal.enabled',
      value: 'false',
    });
    rawSettings.properties.push({
      key: 'voter.portal.host',
      value: 'http://dummy',
    });
    rawSettings.properties.push({
      key: 'sdm.output.verifier.folder.path',
      value: UISettings.verifierFolder,
    });

    if (UISettings.mode === 'SETUP') {
      // Only for Setup
      rawSettings.properties.push({ key: 'server.port', value: '8090' });
      rawSettings.properties.push({ key: 'role.isSetup', value: 'true' });
      rawSettings.properties.push({ key: 'role.isTally', value: 'false' });
      rawSettings.properties.push({
        key: 'direct.trust.keystore.location',
        value: UISettings.directTrustLocation,
      });
      rawSettings.properties.push({
        key: 'direct.trust.keystore.password.location',
        value: UISettings.directTrustPwdLocation,
      });
      rawSettings.properties.push({
        key: 'choiceCodeGenerationChunkSize',
        value: UISettings.choiceCodeGenerationChunkSize,
      });
      rawSettings.properties.push({
        key: 'sdm.external.configuration.folder.path',
        value: UISettings.externalConfigurationFolder,
      });
      rawSettings.properties.push({
        key: 'sdm.output.print.folder.path',
        value: UISettings.printFolder,
      });
    } else {
      // Only for Tally
      rawSettings.properties.push({ key: 'server.port', value: '8091' });
      rawSettings.properties.push({ key: 'role.isSetup', value: 'false' });
      rawSettings.properties.push({ key: 'role.isTally', value: 'true' });
      rawSettings.properties.push({
        key: 'direct.trust.keystore.location',
        value: UISettings.directTrustLocation,
      });
      rawSettings.properties.push({
        key: 'direct.trust.keystore.password.location',
        value: UISettings.directTrustPwdLocation,
      });
    }
  }

  return rawSettings;
};

module.exports = {
  getModeConfig: getModeConfig,
  convertToUISettings: convertRawSettingsToUISettings,
  convertToRawSettings: convertUISettingsToRawSettings,
};
