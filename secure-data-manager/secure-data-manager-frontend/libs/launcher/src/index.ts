/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
export * from './lib/select-settings/select-settings.component';
export * from './lib/manage-settings/manage-settings.component';
