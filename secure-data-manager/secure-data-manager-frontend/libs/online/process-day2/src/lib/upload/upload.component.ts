/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {PageActionsComponent} from '@sdm/shared-ui-components';
import {RouterLinkNextDirective} from '@sdm/shared-ui-directives';
import {ProgressComponent} from '@sdm/shared-feature-progress';
import {UploadService} from './upload.service';
import {filter, mergeMap, take} from 'rxjs';
import {ClipboardModule} from 'ngx-clipboard';
import {WorkflowStateService} from '@sdm/shared-ui-services';
import {WorkflowStatus, WorkflowStep} from '@sdm/shared-util-types';

@Component({
  selector: 'sdm-upload',
  standalone: true,
  imports: [
    CommonModule,
    TranslateModule,
    ProgressComponent,
    PageActionsComponent,
    RouterLinkNextDirective,
    ClipboardModule,
  ],
  templateUrl: './upload.component.html',
})
export class UploadComponent {
  day: number = 1;
  voterPortalUrl: string = '';
  copied: boolean = false;

  constructor(
    readonly route: ActivatedRoute,
    private readonly uploadService: UploadService,
    private readonly workflowStates: WorkflowStateService,
  ) {
    this.day = this.route.snapshot.data['day'];

    // if the upload configuration 2 is complete when the component loads, then retrieve the voter portal URL.
    this.workflowStates
        .get(WorkflowStep.UploadConfiguration2)
        .pipe(
            take(1),
            filter((state) => state.status === WorkflowStatus.Complete),
            mergeMap(() => this.uploadService.getVoterPortalUrl()),
        )
        .subscribe((voterPortalUrl) => {
          this.voterPortalUrl = voterPortalUrl;
        });
  }

  get showVoterPortalUrl(): boolean {
    return this.day === 2 && this.voterPortalUrl !== '';
  }

  upload(): void {
    if(this.day === 1) {
      this.uploadService.upload(this.day).subscribe();
    } else if(this.day === 2) {
      this.uploadService.upload(this.day)
          .pipe(mergeMap(() => this.uploadService.getVoterPortalUrl()))
          .subscribe((voterPortalUrl) => this.voterPortalUrl = voterPortalUrl);
    }
  }

  setCopied(): void {
    this.copied = true;
    setTimeout(() => (this.copied = false), 10000);
  }
}
