/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {Component, TemplateRef} from '@angular/core';
import {takeUntilDestroyed} from '@angular/core/rxjs-interop';
import {FormsModule} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from '@ngx-translate/core';
import {BallotBoxListComponent} from '@sdm/shared-feature-ballot-box-list';
import {ExportModalComponent} from '@sdm/shared-feature-data-exchange';
import {WorkflowStateService} from '@sdm/shared-ui-services';
import {BallotBox, WorkflowState, WorkflowStatus, WorkflowStep,} from '@sdm/shared-util-types';
import {filter, map, merge, Observable, switchMap, take, tap} from 'rxjs';
import {MixAndDownloadService} from './mix-and-download.service';
import {PageActionsComponent} from '@sdm/shared-ui-components';

@Component({
	selector: 'sdm-mix-and-download',
	standalone: true,
	imports: [
		CommonModule,
		FormsModule,
		TranslateModule,
		BallotBoxListComponent,
		PageActionsComponent,
	],
	templateUrl: './mix-and-download.component.html',
})
export class MixAndDownloadComponent {
	readonly WorkflowStatus = WorkflowStatus;
	mixAndDownloadStatus$: Observable<WorkflowStatus>;
	mixableBallotBoxes = new Set<BallotBox>();
	exportableBallotBoxes = new Set<BallotBox>();
	selectedBallotBoxes: BallotBox[] = [];
	exchangeIndex = '';
	isFullExportDone = false;
	private ballotBoxById = new Map<BallotBox['id'], BallotBox>();

	constructor(
		private readonly mixAndDownloadService: MixAndDownloadService,
		private readonly workflowStates: WorkflowStateService,
		private readonly modalService: NgbModal,
		private readonly activatedRoute: ActivatedRoute,
	) {
		this.exchangeIndex = this.activatedRoute.snapshot.data['exchangeIndex'];

		this.workflowStates
			.get(WorkflowStep.ExportToTally5)
			.pipe(
				take(1),
				filter((state) => state.status === WorkflowStatus.Complete)
			).subscribe(() => this.isFullExportDone = true);

		this.mixAndDownloadStatus$ = this.workflowStates
			.get(WorkflowStep.MixAndDownload)
			.pipe(map((state) => state.status));

		this.mixAndDownloadService.getBallotBoxes()
			.pipe(
				tap((ballotBoxes) => {
					this.registerBallotBoxes(ballotBoxes);
				}),
				switchMap(ballotBoxes => this.getAllDownloadedBallotBoxStates(ballotBoxes)),
				tap((ballot) => console.log(ballot)),
				takeUntilDestroyed()
			)
			.subscribe((state) => {
				this.moveBallotBoxDependingOnState(state);
			});
	}

	get hasSelection(): boolean {
		return this.selectedBallotBoxes.length > 0;
	}

	openModal(modalTemplate: TemplateRef<null>) {
		this.modalService
			.open(modalTemplate, {
				ariaLabelledBy: 'confirmation-modal-title',
			})
			.result.then((wasMixAndDownloadConfirmed) => {
			if (!wasMixAndDownloadConfirmed) return;
			this.mixAndDownloadService.mixAndDownload(this.selectedBallotBoxes);
			this.selectedBallotBoxes = [];
		});
	}

	export() {
		const modalRef = this.modalService.open(ExportModalComponent);
		modalRef.componentInstance.exchangeIndex = this.exchangeIndex;
	}

	private registerBallotBoxes(ballotBoxes: BallotBox[]) {
		ballotBoxes.forEach((ballotBox) => {
			this.ballotBoxById.set(ballotBox.id, ballotBox);
		});
	}

	private getAllDownloadedBallotBoxStates(ballotBoxes: BallotBox[]): Observable<WorkflowState> {
		const ballotBoxStates = ballotBoxes.map((ballotBox) =>
			this.workflowStates.get(WorkflowStep.DownloadBallotBox, ballotBox.id)
		);

		return merge(...ballotBoxStates);
	}

	private moveBallotBoxDependingOnState(state: WorkflowState) {
		const ballotBox = this.ballotBoxById.get(state.contextId ?? '');
		if (!ballotBox) return;

		if (WorkflowStatus.Complete !== state.status) {
			this.mixableBallotBoxes.add(ballotBox);
		} else {
			this.mixableBallotBoxes.delete(ballotBox);
			this.exportableBallotBoxes.add(ballotBox);
		}
	}

}
