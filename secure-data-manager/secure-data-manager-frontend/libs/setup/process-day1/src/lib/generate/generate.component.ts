/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {ProgressComponent} from '@sdm/shared-feature-progress';
import {PageActionsComponent} from '@sdm/shared-ui-components';
import {RouterLinkNextDirective} from '@sdm/shared-ui-directives';
import {GenerateService} from './generate.service';

@Component({
	selector: 'sdm-generate',
	standalone: true,
	imports: [
	  CommonModule,
	  TranslateModule,
	  ProgressComponent,
	  PageActionsComponent,
	  RouterLinkNextDirective,
	],
	templateUrl: './generate.component.html',
})
export class GenerateComponent {
	constructor(private readonly generateService: GenerateService) {}
  
	generate() {
	  this.generateService.generate();
	}
}
