/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {HttpClient} from '@angular/common/http';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {NgbProgressbar} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from '@ngx-translate/core';
import {RouterLinkNextDirective} from '@sdm/shared-ui-directives';
import {ToastService} from '@sdm/shared-ui-services';
import {ProgressComponent} from '@sdm/shared-feature-progress';
import {MockComponent, MockDirective, MockModule, MockProvider,} from 'ng-mocks';
import {PreComputeComponent} from './pre-compute.component';

describe('PreComputationComponent', () => {
	let component: PreComputeComponent;
	let fixture: ComponentFixture<PreComputeComponent>;

	beforeEach(async () => {
		jest.useFakeTimers();

		await TestBed.configureTestingModule({
			imports: [
				PreComputeComponent,
				MockModule(TranslateModule),
				MockDirective(RouterLinkNextDirective),
				MockComponent(NgbProgressbar),
				MockComponent(ProgressComponent),
			],
			providers: [MockProvider(HttpClient), MockProvider(ToastService)],
		}).compileComponents();

		fixture = TestBed.createComponent(PreComputeComponent);
		component = fixture.componentInstance;

		fixture.detectChanges();
	});

	it('should be created', () => {
		expect(component).toBeTruthy();
	});
});
