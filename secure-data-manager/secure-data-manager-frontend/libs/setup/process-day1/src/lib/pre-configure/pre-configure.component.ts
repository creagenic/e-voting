/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {Component, DestroyRef} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {PageActionsComponent, PolicyComponent,} from '@sdm/shared-ui-components';
import {RouterLinkNextDirective} from '@sdm/shared-ui-directives';
import {
	AuthorizationSummary,
	ConfigurationSummary,
	ContestSummary,
	ElectionSummary,
	QuestionSummary,
	VoteSummary,
	WorkflowStatus,
	WorkflowStep,
} from '@sdm/shared-util-types';
import {NgxMaskDirective} from 'ngx-mask';
import {PreConfigureService} from './pre-configure.service';
import {ProgressComponent} from '@sdm/shared-feature-progress';
import {SummaryService, WorkflowStateService} from '@sdm/shared-ui-services';
import {MarkdownPipe} from 'ngx-markdown';
import {ScrollbarComponent} from '@sdm/shared-feature-scrollbar';
import {catchError, EMPTY, switchMap} from 'rxjs';
import {takeUntilDestroyed} from '@angular/core/rxjs-interop';

@Component({
  selector: 'sdm-preconfigure',
  standalone: true,
  imports: [
    CommonModule,
    TranslateModule,
    NgxMaskDirective,
    PolicyComponent,
    PageActionsComponent,
    RouterLinkNextDirective,
    ProgressComponent,
    MarkdownPipe,
    FormsModule,
    ScrollbarComponent,
  ],
  templateUrl: './pre-configure.component.html',
})
export class PreConfigureComponent {
  configurationSummary: ConfigurationSummary | null = null;
  contests: ContestSummary[] = [];
  elections: ElectionSummary[] = [];
  votes: VoteSummary[] = [];
  regularAuthorizations: AuthorizationSummary[] = [];
  testAuthorizations: AuthorizationSummary[] = [];
  testAuthorizationsByElectionId = new Map<string, AuthorizationSummary[]>();
  regularAuthorizationsByElectionId = new Map<string, AuthorizationSummary[]>();
  testAuthorizationsByVoteId = new Map<string, AuthorizationSummary[]>();
  regularAuthorizationsByVoteId = new Map<string, AuthorizationSummary[]>();
  questionsByVoteId = new Map<string, QuestionSummary[]>();

  loadingSummaryError = false;
  workflowStatus: WorkflowStatus = WorkflowStatus.Idle;
  loadingSummary = false;
  isContestConfirmed = false;
  protected readonly WorkflowStatus = WorkflowStatus;

  constructor(
    private readonly translate: TranslateService,
    private readonly preConfigureService: PreConfigureService,
    private readonly summaryService: SummaryService,
    private readonly workflowStates: WorkflowStateService,
    private readonly destroyRef: DestroyRef,
  ) {
    this.loadConfigurationSummary();
  }

  reload() {
    this.configurationSummary = null;
    this.loadConfigurationSummary();
  }

  get eventName() {
    if (!this.configurationSummary) {
      return '';
    }

    return (
      this.configurationSummary.contestDescription[
        this.translate.currentLang
      ] ||
      this.configurationSummary.contestDescription[this.translate.defaultLang]
    );
  }

  preConfigure() {
    this.preConfigureService.preConfigureElectionEvent();
  }

  getElectionGroupPosition(electionId: string): number {
    const electionGroup = this.configurationSummary?.electionGroups.find(
      (electionGroup) =>
        electionGroup.elections.find(
          (election) => election.electionId === electionId,
        ),
    );

    if (!electionGroup) return -1;

    return electionGroup.electionGroupPosition;
  }

  private loadConfigurationSummary() {
    this.workflowStates
      .get(WorkflowStep.PreConfigure)
      .pipe(
        switchMap((state) => {
          this.workflowStatus = state.status;

          if (state.status === WorkflowStatus.Ready) {
            this.loadingSummary = true;
            this.loadingSummaryError = false;
            return this.preConfigureService.previewSummary();
          }
          if (state.status === WorkflowStatus.Complete) {
            this.loadingSummary = true;
            this.loadingSummaryError = false;
            return this.summaryService.getSummary();
          }

          return EMPTY;
        }),
        catchError(() => {
          this.loadingSummaryError = true;
          this.loadingSummary = false;
          return EMPTY;
        }),
        takeUntilDestroyed(this.destroyRef),
      )
      .subscribe((configurationSummary) => {
        this.loadingSummary = false;
        this.setConfigurationSummary(configurationSummary);
      });
  }

  private sortByName(a: AuthorizationSummary, b: AuthorizationSummary): number {
    return a.authorizationName.localeCompare(b.authorizationName);
  }

  private setConfigurationSummary(configurationSummary: ConfigurationSummary) {
    this.configurationSummary = configurationSummary;

    this.elections = this.configurationSummary.electionGroups
      .sort((a, b) => a.electionGroupPosition - b.electionGroupPosition)
      .flatMap((electionGroup) => electionGroup.elections)
      .sort((a, b) => a.electionPosition - b.electionPosition);

    this.votes = this.configurationSummary.votes.sort(
      (a, b) => a.votePosition - b.votePosition,
    );

    const electionContests: ContestSummary[] = this.elections.map(
      (election) => {
        return {
          id: election.electionId,
          position: this.getElectionGroupPosition(election.electionId),
          election: election,
        };
      },
    );

    const voteContests: ContestSummary[] = this.votes.map((vote) => {
      return {
        id: vote.voteId,
        position: vote.votePosition,
        vote: vote,
      };
    });

    this.contests = [...electionContests, ...voteContests].sort(
      (a, b) => a.position - b.position,
    );

    const authorizations = this.configurationSummary.authorizations.sort(
      (a, b) => this.sortByName(a, b),
    );

    this.regularAuthorizations = authorizations.filter(
      (authorization) => !authorization.isTest,
    );

    this.testAuthorizations = authorizations.filter(
      (authorization) => authorization.isTest,
    );

    this.configurationSummary.electionGroups.forEach((electionGroup) => {
      const electionGroupTestAuthorizations = electionGroup.authorizations
        .filter((authorization) => authorization.isTest)
        .sort((a, b) => this.sortByName(a, b));

      electionGroup.elections.forEach((election) => {
        this.testAuthorizationsByElectionId.set(
          election.electionId,
          electionGroupTestAuthorizations,
        );
      });

      const electionGroupRegularAuthorizations = electionGroup.authorizations
        .filter((authorization) => !authorization.isTest)
        .sort((a, b) => this.sortByName(a, b));

      electionGroup.elections.forEach((election) => {
        this.regularAuthorizationsByElectionId.set(
          election.electionId,
          electionGroupRegularAuthorizations,
        );
      });
    });

    this.votes.forEach((vote) => {
      const voteTestAuthorizations = vote.authorizations
        .filter((authorization) => authorization.isTest)
        .sort((a, b) => this.sortByName(a, b));

      this.testAuthorizationsByVoteId.set(vote.voteId, voteTestAuthorizations);

      const voteRegularAuthorizations = vote.authorizations
        .filter((authorization) => !authorization.isTest)
        .sort((a, b) => this.sortByName(a, b));

      this.regularAuthorizationsByVoteId.set(
        vote.voteId,
        voteRegularAuthorizations,
      );

      const voteQuestions: QuestionSummary[] = vote.ballots.flatMap(
        (ballot) => ballot.questions,
      );
      this.questionsByVoteId.set(vote.voteId, voteQuestions);
    });
  }
}
