/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
export * from './lib/state/dummy.actions';
export * from './lib/state/dummy.reducer';
export * from './lib/state/dummy.provider';
