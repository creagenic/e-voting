/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Injectable} from '@angular/core';
import {Observable, of} from "rxjs";

@Injectable({
	providedIn: 'root'
})
export class DummyService {

	dummy(): Observable<string> {
		return of('');
	}
}
