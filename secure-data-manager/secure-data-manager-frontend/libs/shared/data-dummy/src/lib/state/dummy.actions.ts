/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {createActionGroup, emptyProps, props} from '@ngrx/store';

export const DummyActions = createActionGroup({
	source: 'Dummy',
	events: {
		'Dummy': props<{ dummyString: string }>(),
		'Dummy Completed': props<{
			dummyString: string;
		}>(),
		'Dummy Error': emptyProps(),
	},
});
