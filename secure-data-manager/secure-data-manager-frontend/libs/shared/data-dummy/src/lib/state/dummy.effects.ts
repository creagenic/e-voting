/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {inject} from '@angular/core';
import {catchError, exhaustMap, map, of, throwIfEmpty} from 'rxjs';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {DummyService} from '../services/dummy.service';
import {DummyActions} from './dummy.actions';

const executeDummy = createEffect(
	(
		actions$ = inject(Actions),
		dummyService = inject(DummyService),
	) => {
		return actions$.pipe(
			ofType(DummyActions.dummy),
			exhaustMap(({dummyString}) =>
				dummyService.dummy().pipe(
					map(() =>
						DummyActions.dummyCompleted({dummyString}),
					),
					throwIfEmpty(),
					catchError(() => of(DummyActions.dummyError())),
				),
			),
		);
	},
	{functional: true},
);

export const dummyEffects = {
	executeDummy,
};
