/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {makeEnvironmentProviders} from '@angular/core';
import {provideState} from '@ngrx/store';
import {provideEffects} from '@ngrx/effects';
import {dummyFeature} from "./dummy.reducer";
import {dummyEffects} from "./dummy.effects";

export const provideDummyState = () =>
	makeEnvironmentProviders([
		provideState(dummyFeature),
		provideEffects(dummyEffects),
	]);
