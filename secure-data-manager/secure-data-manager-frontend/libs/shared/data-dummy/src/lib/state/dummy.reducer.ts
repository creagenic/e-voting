/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {createFeature, createReducer, on} from '@ngrx/store';
import {ElectionEvent} from '@sdm/shared-util-types';
import {DummyActions} from './dummy.actions';

interface State {
  electionEvent: ElectionEvent | null;
  dummyString: string;
}

const initialState: State = {
  electionEvent: null,
  dummyString: 'dummy',
};

export const dummyFeature = createFeature({
  name: 'state',
  reducer: createReducer(
    initialState,
    on(DummyActions.dummy, (state, { dummyString }) => ({
      ...state,
      dummyString: dummyString,
    })),
  ),
});

export const { selectDummyString, selectElectionEvent } = dummyFeature;
