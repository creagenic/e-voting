/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
export * from './lib/data-collection/data-collection.service';
export * from './lib/data-collection/data-collection.component';
