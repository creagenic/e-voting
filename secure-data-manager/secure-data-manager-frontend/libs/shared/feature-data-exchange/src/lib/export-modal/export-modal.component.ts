/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { ExportInformationComponent } from '../export-information/export-information.component';
import {
  WorkflowExceptionCode,
  WorkflowStatus,
  WorkflowStepUtil,
} from '@sdm/shared-util-types';
import { WorkflowStateService } from '@sdm/shared-ui-services';
import { DataExchangeService } from '../data-exchange.service';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

@Component({
  selector: 'sdm-export-modal',
  standalone: true,
  imports: [CommonModule, ExportInformationComponent, TranslateModule],
  templateUrl: './export-modal.component.html',
})
export class ExportModalComponent {
  @Input() exchangeIndex = '5'; // Hard coded for export five, to clean later
  exportStatus!: WorkflowStatus;
  errorFeedbackCode!: WorkflowExceptionCode;

  constructor(
    public readonly activeModal: NgbActiveModal,
    private readonly workflowStates: WorkflowStateService,
    private readonly dataExchangeService: DataExchangeService,
  ) {
    this.workflowStates
      .get(WorkflowStepUtil.getExportStep(parseInt(this.exchangeIndex)))
      .pipe(takeUntilDestroyed())
      .subscribe((state) => {
        this.exportStatus = state.status;
        this.errorFeedbackCode =
          state.exceptionCode ?? WorkflowExceptionCode.None;
      });

    this.dataExchangeService.export(this.exchangeIndex).subscribe();
  }

  get hasErrorFeedback(): boolean {
    return (
      this.errorFeedbackCode !== null &&
      this.errorFeedbackCode !== WorkflowExceptionCode.None &&
      this.errorFeedbackCode !== WorkflowExceptionCode.Default
    );
  }

  protected readonly WorkflowStatus = WorkflowStatus;
}
