/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {CommonModule} from "@angular/common";
import {TranslateModule, TranslateService} from "@ngx-translate/core";

@Component({
	selector: 'sdm-semantic-information',
	standalone: true,
	template: `
		<ng-container *ngFor="let segment of segments">
			<p [class.fst-italic]="isSpecialCase">{{ segment }}</p>
		</ng-container>
	`,
	imports: [CommonModule, TranslateModule],
})
export class SemanticInformationComponent {
	@Input() text?: string;
	protected isSpecialCase = false;


	constructor(private readonly translate: TranslateService) {

	}

	get segments(): string[] {
		if (this.text) {
			const parsedValues = this.text.split('|').slice(1);
			const firstSegment = parsedValues[0];

			// Define mapping for special cases
			const specialCases = {
				'EMPTY_CANDIDATE_POSITION-': 'ballotBoxTestVoteList.emptyCandidatePosition',
				'WRITE_IN_POSITION-': 'ballotBoxTestVoteList.writeInPosition'
			};

			// Check if first segment matches any special case
			this.isSpecialCase = false;
			for (const [prefix, translationKey] of Object.entries(specialCases)) {
				if (firstSegment.startsWith(prefix)) {
					this.isSpecialCase = true;
					parsedValues[0] = this.translate.instant(translationKey);
					break;
				}
			}

			return parsedValues;
			
		} else {
			return [];
		}
	}
}
