/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Injectable} from '@angular/core';
import {environment} from '@sdm/shared-ui-config';
import {HttpClient} from '@angular/common/http';
import {filter, mergeMap, Observable} from 'rxjs';
import {ConfigurationSummary, WorkflowStatus, WorkflowStep} from '@sdm/shared-util-types';
import {WorkflowStateService} from "../workflow-states/workflow-state.service";

@Injectable({
	providedIn: 'root',
})
export class SummaryService {

	constructor(private httpClient: HttpClient,
				private readonly workflowStates: WorkflowStateService) {
	}

	getSummary(): Observable<ConfigurationSummary> {
		return this.httpClient.get<ConfigurationSummary>(
			`${environment.backendPath}/sdm-shared/summary`,
		);
	}

	getHeaderSummary(): Observable<ConfigurationSummary> {
		return this.workflowStates
			// Depending on the sdm mode, only one will be not EMPTY.
			.getAllMerged([WorkflowStep.PreConfigure, WorkflowStep.ImportFromSetup1, WorkflowStep.ImportFromOnline5])
			.pipe(
				filter((state) => state.status === WorkflowStatus.Complete),
				mergeMap(() => this.getSummary())
			);
	}
}
