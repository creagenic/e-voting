/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {TestBed} from '@angular/core/testing';
import {TranslateService} from '@ngx-translate/core';
import {MockProvider} from 'ng-mocks';
import {ToastrService} from 'ngx-toastr';
import {ActiveToast} from 'ngx-toastr/toastr/toastr.service';

import {ToastService, ToastType} from './toast.service';
import {Component} from '@angular/core';

const toastTypes: (keyof Pick<ToastService, ToastType>)[] = [
  'success',
  'error',
  'warning',
  'info',
];

describe('ToastService', () => {
  let mockTranslateService: TranslateService;
  let mockToastrService: ToastrService;
  let toastService: ToastService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ToastService,
        MockProvider(TranslateService, {
          instant: jest.fn(),
        }),
        MockProvider(ToastrService, {
          success: jest.fn(),
          error: jest.fn(),
          warning: jest.fn(),
          info: jest.fn(),
          remove: jest.fn(),
        }),
      ],
    });

    mockTranslateService = TestBed.inject(TranslateService);
    mockToastrService = TestBed.inject(ToastrService);

    toastService = TestBed.inject(ToastService);
  });

  toastTypes.forEach((toastType) => {
    describe(toastType, () => {
      it('should open toast with translated message', () => {
        const translatedMessage = 'mock message';

        (mockTranslateService.instant as jest.Mock).mockReturnValue(
          translatedMessage,
        );

        toastService[toastType]('mock.message.key');

        expect(mockToastrService[toastType]).toHaveBeenNthCalledWith(
          1,
          translatedMessage,
          undefined,
          undefined,
        );
      });

      it('should open toast with translated title and message', () => {
        const translatedTitle = 'mock title';
        const translatedMessage = 'mock message';

        (mockTranslateService.instant as jest.Mock).mockImplementation(
          (key: string) =>
            key.includes('title') ? translatedTitle : translatedMessage,
        );

        toastService[toastType]({
          title: 'mock.title.key',
          message: 'mock.message.key',
        });

        expect(mockToastrService[toastType]).toHaveBeenNthCalledWith(
          1,
          translatedMessage,
          translatedTitle,
          undefined,
        );
      });

      it('should open toast with provided options', () => {
        const mockOptions = { timeOut: 1000 };

        toastService[toastType]('mock message', mockOptions);

        expect(mockToastrService[toastType]).toHaveBeenNthCalledWith(
          1,
          undefined,
          undefined,
          mockOptions,
        );
      });
    });
  });

  describe('remove', () => {
    it('should close the provided toast', () => {
      const mockToast = { toastId: 0 } as ActiveToast<Component>;

      toastService.remove(mockToast);

      expect(mockToastrService.remove).toHaveBeenNthCalledWith(
        1,
        mockToast.toastId,
      );
    });
  });
});
