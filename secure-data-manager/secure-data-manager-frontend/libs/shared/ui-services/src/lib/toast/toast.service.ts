/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {IndividualConfig} from 'ngx-toastr/toastr/toastr-config';
import {ActiveToast} from 'ngx-toastr/toastr/toastr.service';

type TranslateParams = string | [string, object];

type TitleAndMessageParams = {
  title: TranslateParams;
  message: TranslateParams;
};

type ToastOptions = Partial<IndividualConfig>;

export type OpenToastFn = (
  translationParams: TranslateParams | TitleAndMessageParams,
  options?: ToastOptions,
) => ActiveToast<Component>;

export type ToastType = 'success' | 'error' | 'warning' | 'info';

@Injectable({
  providedIn: 'root',
})
export class ToastService {
  success = this.getOpenToastFn('success');
  error = this.getOpenToastFn('error');
  warning = this.getOpenToastFn('warning');
  info = this.getOpenToastFn('info');

  constructor(
    private readonly toastr: ToastrService,
    private readonly translate: TranslateService,
  ) {}

  remove({ toastId }: ActiveToast<Component>): void {
    this.toastr.remove(toastId);
  }

  private getOpenToastFn(type: ToastType): OpenToastFn {
    return (...toastArgs) => this.openToast(type, ...toastArgs);
  }

  private openToast(
    type: string,
    params: TranslateParams | TitleAndMessageParams,
    options?: ToastOptions,
  ): ActiveToast<Component> {
    let translatedTitle, translatedMessage;

    if (typeof params === 'object' && 'title' in params) {
      const { title, message } = params;
      translatedTitle = this.getTranslation(title);
      translatedMessage = this.getTranslation(message);
    } else {
      translatedMessage = this.getTranslation(params as TranslateParams);
    }

    return this.toastr[type as keyof Pick<ToastrService, ToastType>](
      translatedMessage,
      translatedTitle,
      options,
    );
  }

  private getTranslation(params: TranslateParams): string {
    if (typeof params === 'string') {
      return this.translate.instant(params);
    }

    return this.translate.instant(...params);
  }
}
