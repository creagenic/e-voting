/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
let fileIndex = 0;

export class MockFile extends File {
  constructor(extension = '.png') {
    fileIndex++;
    super([], `file-${fileIndex}${extension}`);
  }
}
