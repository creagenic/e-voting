/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

export interface DatasetInfo {
	outputFolder: string;
	filenames: string[];
}
