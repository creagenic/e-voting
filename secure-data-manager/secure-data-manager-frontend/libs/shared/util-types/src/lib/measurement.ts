/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
export interface Measurement {
  statistic: string;
  value: number;
}
