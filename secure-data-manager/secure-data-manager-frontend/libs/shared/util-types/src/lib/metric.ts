/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Measurement} from './measurement';

export interface Metric {
  info: string;
  name: string;
  description: string;
  measurements: Measurement[];
  availableTags: [];
}
