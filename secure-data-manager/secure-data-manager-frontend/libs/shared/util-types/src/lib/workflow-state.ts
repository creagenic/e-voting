/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import { WorkflowStatus } from './workflow-status';
import { WorkflowStep } from './workflow-step';
import { WorkflowExceptionCode } from './workflow-exception-code';

export interface WorkflowState {
  step: WorkflowStep;
  contextId?: string;
  status: WorkflowStatus;
  startDate: string | null;
  endDate: string | null;
  exceptionCode?: WorkflowExceptionCode;
}
