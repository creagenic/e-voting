/*
 * (c) Copyright 2023 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DirectTrustToolBackendApplication {

	public static void main(final String[] args) {
		SpringApplication.run(DirectTrustToolBackendApplication.class, args);
	}

}
