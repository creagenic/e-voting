/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend.process;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;

@Repository
public class ComponentStorageRepository {
	private static final String PROPERTY_FILE = "session.properties";
	private final FileRepository fileRepository;
	private final ObjectMapper mapper;

	public ComponentStorageRepository(final FileRepository fileRepository) {
		this.fileRepository = fileRepository;
		this.mapper = new ObjectMapper();
	}

	public void createNewStorage(final String sessionId, final Set<Alias> wantedComponents) {
		validateUUID(sessionId);
		checkNotNull(wantedComponents);
		checkArgument(!wantedComponents.isEmpty());

		final Path sessionDirectory = Path.of(sessionId);
		wantedComponents.forEach(component -> fileRepository.createDirectory(sessionDirectory.resolve(component.name())));
	}

	public void deleteStorage(final String sessionId) {
		validateUUID(sessionId);
		final Path sessionDirectory = Path.of(sessionId);
		fileRepository.removeDirectory(sessionDirectory);
	}

	public void putGlobalStorageKey(final String sessionId, final String key, final String value) {
		validateUUID(sessionId);
		checkNotNull(key);
		checkNotNull(value);

		final Map<String, String> map = readGlobalMap(sessionId);
		map.put(key, value);
		writeGlobalMap(sessionId, map);
	}

	public String getGlobalStorageKey(final String sessionId, final String key) {
		validateUUID(sessionId);
		checkNotNull(key);
		return readGlobalMap(sessionId).get(key);
	}

	public Map<String, String> readGlobalMap(final String sessionId) {
		final Path propertyFile = Path.of(sessionId).resolve(PROPERTY_FILE);

		return fileRepository.readFile(propertyFile)
				.map(bytes -> new String(bytes, StandardCharsets.UTF_8))
				.map(s -> {
					try {
						return mapper.readValue(s, new TypeReference<HashMap<String, String>>() {
						});
					} catch (final JsonProcessingException e) {
						throw new ComponentStorageRepositoryException(e);
					}
				})
				.orElse(new HashMap<>());
	}

	public void writeGlobalMap(final String sessionId, final Map<String, String> map) {
		final Path propertyFile = Path.of(sessionId).resolve(PROPERTY_FILE);
		try {
			fileRepository.writeFile(propertyFile, mapper.writeValueAsBytes(map));
		} catch (final JsonProcessingException e) {
			throw new ComponentStorageRepositoryException(e);
		}
	}

	public Set<Alias> selectedComponents(final String sessionId) {
		validateUUID(sessionId);
		return Arrays.stream(Alias.values())
				.filter(component -> fileRepository.isPresent(Path.of(sessionId).resolve(component.name())))
				.collect(Collectors.toSet());
	}

	public boolean isPresent(final Key key) {
		return fileRepository.isPresent(getKeyLocation(key));
	}

	public Optional<char[]> getCharArray(final Key key) {
		return getBytes(key).map(ByteBuffer::wrap)
				.map(StandardCharsets.UTF_8::decode)
				.map(charBuffer -> Arrays.copyOfRange(charBuffer.array(), charBuffer.arrayOffset(), charBuffer.limit()));
	}

	public Optional<byte[]> getBytes(final Key key) {
		return fileRepository.readFile(getKeyLocation(key));
	}

	public void putCharArray(final Key key, final char[] value) {
		final ByteBuffer byteBuffer = StandardCharsets.UTF_8.encode(CharBuffer.wrap(value));
		putBytes(key, Arrays.copyOfRange(byteBuffer.array(), byteBuffer.arrayOffset(), byteBuffer.limit()));
	}

	public void putBytes(final Key key, final byte[] value) {
		fileRepository.writeFile(getKeyLocation(key), value);
	}

	private Path getKeyLocation(final Key key) {
		return Path.of(key.sessionId).resolve(key.component.name()).resolve(key.type.getKeyLabel());
	}

	public enum Type {
		KEYSTORE("keystore"),
		PUBLIC_KEY("public-key"),
		PASSWORD("password.txt");

		private final String keyLabel;

		Type(final String keyLabel) {
			this.keyLabel = keyLabel;
		}

		public String getKeyLabel() {
			return keyLabel;
		}
	}

	public record Key(String sessionId, Alias component, Type type) {
		public Key {
			validateUUID(sessionId);
			checkNotNull(component);
			checkNotNull(type);
		}
	}

	public static class ComponentStorageRepositoryException extends RuntimeException {
		public ComponentStorageRepositoryException(final Throwable cause) {
			super(cause);
		}
	}
}
