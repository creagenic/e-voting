/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend.process.keystoredownload;

import static ch.post.it.evoting.directtrusttool.backend.RouteConstants.BASE_PATH;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Base64;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(BASE_PATH + "/key-store-download")
public class KeystoreDownloadController {

	private final KeystoreDownloadService keystoreDownloadService;

	public KeystoreDownloadController(final KeystoreDownloadService keystoreDownloadService) {
		this.keystoreDownloadService = keystoreDownloadService;
	}

	@GetMapping(value = "{sessionId}", produces = "application/json")
	public String downloadKeyStores(
			@PathVariable
			final String sessionId,
			@RequestParam(required = false, defaultValue = "")
			final String suffix) {
		validateUUID(sessionId);
		checkNotNull(suffix);
		return new String(Base64.getEncoder().encode(keystoreDownloadService.downloadKeyStores(sessionId, suffix)));
	}

	@GetMapping(value = "fingerprints/{sessionId}", produces = "application/json")
	public Map<String, String> getFingerprints(
			@PathVariable
			final String sessionId
	) {
		validateUUID(sessionId);
		return keystoreDownloadService.extractFingerprints(sessionId);
	}
}
