/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend.process.keystoredownload;

import static ch.post.it.evoting.directtrusttool.backend.process.ComponentStorageRepository.Type.KEYSTORE;
import static ch.post.it.evoting.directtrusttool.backend.process.ComponentStorageRepository.Type.PASSWORD;
import static ch.post.it.evoting.directtrusttool.backend.process.ComponentStorageRepository.Type.PUBLIC_KEY;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static java.lang.String.format;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.util.HexFormat;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.directtrusttool.backend.process.ComponentStorageRepository;
import ch.post.it.evoting.directtrusttool.backend.process.PemConverterService;
import ch.post.it.evoting.directtrusttool.backend.process.Zipper;
import ch.post.it.evoting.directtrusttool.backend.process.session.Phase;
import ch.post.it.evoting.directtrusttool.backend.process.session.SessionService;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;

@Service
public class KeystoreDownloadService {

	private static final Logger LOGGER = LoggerFactory.getLogger(KeystoreDownloadService.class);
	private static final String FOLDER_PREFIX = "direct_trust_";
	private static final String KEYSTORE_PREFIX = "signing_keystore";
	private static final String KEYSTORE_EXTENSION = "p12";
	private static final String PASSWORD_PREFIX = "signing_pw";
	private static final String PASSWORD_EXTENSION = "txt";
	private final ComponentStorageRepository componentStorageRepository;
	private final PemConverterService pemConverterService;
	private final SessionService sessionService;

	public KeystoreDownloadService(final ComponentStorageRepository componentStorageRepository, final PemConverterService pemConverterService,
			final SessionService sessionService) {
		this.componentStorageRepository = componentStorageRepository;
		this.pemConverterService = pemConverterService;
		this.sessionService = sessionService;
	}

	public byte[] downloadKeyStores(final String sessionId, final String userCustomSuffix) {
		validateUUID(sessionId);
		checkState(sessionService.getSessionPhase(sessionId).equals(Phase.DOWNLOAD_KEYSTORE));
		checkNotNull(userCustomSuffix);
		checkState(userCustomSuffix.matches("^\\w*$"));

		final Map<String, byte[]> filesMap = componentStorageRepository.selectedComponents(sessionId).stream().flatMap(
						component -> Stream.of(
								createDownloableEntry(sessionId, KEYSTORE_PREFIX, component, userCustomSuffix, KEYSTORE_EXTENSION, KEYSTORE),
								createDownloableEntry(sessionId, PASSWORD_PREFIX, component, userCustomSuffix, PASSWORD_EXTENSION, PASSWORD)))
				.collect(Collectors.toUnmodifiableMap(Map.Entry::getKey, Map.Entry::getValue));

		LOGGER.info("Files created successfully. Ready to create zip and download keystore. [sessionId: {}, userCustomSuffix: {}]", sessionId,
				userCustomSuffix);

		return Zipper.zip(filesMap);
	}

	/**
	 * Extracts the fingerprints for the direct trust certificates.
	 *
	 * @return a map with the alias as keys and fingerprints as values.
	 */
	public Map<String, String> extractFingerprints(final String sessionId) {
		validateUUID(sessionId);
		checkState(sessionService.getSessionPhase(sessionId).equals(Phase.DOWNLOAD_KEYSTORE));

		final Map<String, String> fingerprints = componentStorageRepository.selectedComponents(sessionId).stream()
				.filter(Alias::hasPrivateKey)
				.collect(Collectors.toUnmodifiableMap(
						Alias::name,
						component -> extractFingerprintForComponent(sessionId, component)));

		LOGGER.info("Fingerprints extracted successfully. [sessionId: {}]", sessionId);

		return fingerprints;
	}

	private Map.Entry<String, byte[]> createDownloableEntry(final String sessionId, final String prefix, final Alias component,
			final String customSuffix, final String extension, final ComponentStorageRepository.Type type) {
		final byte[] data = componentStorageRepository.getBytes(new ComponentStorageRepository.Key(sessionId, component, type)).orElseThrow();
		final String folderName = FOLDER_PREFIX + component.get() + "/";
		final String fileName = format("%s_%s_%s.%s", prefix, component.get(), customSuffix, extension).replaceAll("_(?=\\.)", "");
		return Map.entry(folderName + fileName, data);
	}

	private String extractFingerprintForComponent(final String sessionId, final Alias component) {
		final byte[] certificateBytes = componentStorageRepository.getBytes(
						new ComponentStorageRepository.Key(sessionId, component, PUBLIC_KEY))
				.orElseThrow(() -> new NoSuchElementException("Missing public key."));
		final Certificate certificate = pemConverterService.fromPem(new String(certificateBytes, StandardCharsets.UTF_8));

		final byte[] encodedCertificate;
		try {
			encodedCertificate = certificate.getEncoded();
		} catch (final CertificateEncodingException e) {
			throw new IllegalStateException(String.format("Failed to get encoded certificate. [alias: %s]", component.get()), e);
		}

		final MessageDigest messageDigest;
		try {
			messageDigest = MessageDigest.getInstance("SHA-256");
		} catch (final NoSuchAlgorithmException e) {
			throw new IllegalStateException("Failed to get message digest instance.", e);
		}

		final byte[] digest = messageDigest.digest(encodedCertificate);
		final String sha256Fingerprint = HexFormat.of().formatHex(digest);

		return sha256Fingerprint.toLowerCase(Locale.ENGLISH);
	}
}
