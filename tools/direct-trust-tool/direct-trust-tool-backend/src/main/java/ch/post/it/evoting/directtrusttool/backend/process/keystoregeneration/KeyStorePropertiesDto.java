/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend.process.keystoregeneration;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.time.LocalDate;
import java.util.Set;

import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.validations.KeystoreOrganisationValidation;

public record KeyStorePropertiesDto(LocalDate validUntil, String country, String state, String locality,
									String organisation, Set<Alias> wantedComponents) {

	public KeyStorePropertiesDto {
		checkNotNull(validUntil);
		checkNotNull(country);
		checkNotNull(state);
		checkNotNull(locality);
		checkNotNull(organisation);
		checkNotNull(wantedComponents);
		checkState(!country.isBlank());
		State.isValidLabel(state);
		checkState(!locality.isBlank());
		KeystoreOrganisationValidation.validate(organisation);
		checkState(!wantedComponents.isEmpty());
	}
}
