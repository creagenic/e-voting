/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend.process.keystoregeneration;

import static ch.post.it.evoting.directtrusttool.backend.RouteConstants.BASE_PATH;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Set;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;

@RestController
@RequestMapping(BASE_PATH + "/key-store-generation")
public class KeystoreGenerationController {

	private final KeystoreGenerationService keystoreGenerationService;
	private final PropertiesService propertiesService;

	public KeystoreGenerationController(final KeystoreGenerationService keystoreGenerationService, final PropertiesService propertiesService) {
		this.keystoreGenerationService = keystoreGenerationService;
		this.propertiesService = propertiesService;
	}

	@PostMapping(value = "{sessionId}", consumes = "application/json")
	public void generateKeyStores(
			@PathVariable
			final String sessionId,
			@RequestBody
			final KeyStorePropertiesDto properties) {
		validateUUID(sessionId);
		checkNotNull(properties);
		keystoreGenerationService.generateKeyStores(sessionId, properties);
	}

	@GetMapping(value = "available/components", produces = "application/json")
	public Set<Alias> getAvailableComponents() {
		return propertiesService.getAvailableComponents();
	}

	@GetMapping(value = "available/states", produces = "application/json")
	public Set<String> getAvailableStates() {
		return propertiesService.getAvailableStates();
	}

	@GetMapping(value = "default/certificate", produces = "application/json")
	public CertificateDefaultValueDto getCertificateDefaultValue() {
		return propertiesService.getCertificateDefaultValue();
	}
}
