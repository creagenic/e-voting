/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend.process.keystoregeneration;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.signing.AuthorityInformation;
import ch.post.it.evoting.directtrusttool.backend.process.ComponentStorageRepository;
import ch.post.it.evoting.directtrusttool.backend.process.PemConverterService;
import ch.post.it.evoting.directtrusttool.backend.process.session.Phase;
import ch.post.it.evoting.directtrusttool.backend.process.session.SessionService;

@Service
public class KeystoreGenerationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(KeystoreGenerationService.class);
	private final ComponentStorageRepository componentStorageRepository;
	private final SessionService sessionService;
	private final PemConverterService pemConverterService;

	public KeystoreGenerationService(
			final ComponentStorageRepository componentStorageRepository,
			final SessionService sessionService,
			final PemConverterService pemConverterService) {
		this.componentStorageRepository = componentStorageRepository;
		this.sessionService = sessionService;
		this.pemConverterService = pemConverterService;
	}

	public void generateKeyStores(final String sessionId, final KeyStorePropertiesDto properties) {
		validateUUID(sessionId);
		checkNotNull(properties);
		checkState(sessionService.getSessionPhase(sessionId).equals(Phase.GENERATE_KEYSTORE));

		componentStorageRepository.createNewStorage(sessionId, properties.wantedComponents());

		LOGGER.info("Storage created successfully. [sessionId: {}]", sessionId);

		final KeystoreCreator keystoreCreator = new KeystoreCreator(properties.validUntil(),
				AuthorityInformation.builder()
						.setCommonName("") // the name will be overwritten with component name
						.setCountry(properties.country())
						.setState(properties.state())
						.setLocality(properties.locality())
						.setOrganisation(properties.organisation())
						.build());

		componentStorageRepository.selectedComponents(sessionId).forEach(component -> {
			final KeystoreCreator.Output output = keystoreCreator.generateKeyStore(component);
			componentStorageRepository.putCharArray(
					new ComponentStorageRepository.Key(sessionId, component, ComponentStorageRepository.Type.PASSWORD), output.password());
			componentStorageRepository.putBytes(new ComponentStorageRepository.Key(sessionId, component, ComponentStorageRepository.Type.KEYSTORE),
					output.keyStore());
			Optional.ofNullable(output.publicKey())
					.ifPresent(bytes -> componentStorageRepository.putBytes(
							new ComponentStorageRepository.Key(sessionId, component, ComponentStorageRepository.Type.PUBLIC_KEY),
							pemConverterService.toPem(bytes).getBytes(StandardCharsets.UTF_8)));
		});

		LOGGER.info("Generated keystore successfully. [sessionId: {}]", sessionId);

		sessionService.setPhase(sessionId, Phase.SHARING_PUBLIC_KEYS);
	}
}
