/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend.process.keystoregeneration;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;

@Service
public class PropertiesService {
	private final String defaultCountry;
	private final State defaultState;
	private final String defaultLocality;
	private final String defaultOrganization;
	private final Set<Alias> availableComponents;
	private final Set<String> availableStates;

	public PropertiesService(
			@Value("${app.certificate.country:}")
			final String defaultCountry,
			@Value("${app.certificate.state:AARGAU}")
			final String defaultState,
			@Value("${app.certificate.locality:}")
			final String defaultLocality,
			@Value("${app.certificate.organization:}")
			final String defaultOrganization,
			@Value("${app.component.available:#{null}}")
			final List<String> availableComponents) {

		this.defaultCountry = defaultCountry;
		this.defaultState = State.valueOf(defaultState);
		this.defaultLocality = defaultLocality;
		this.defaultOrganization = defaultOrganization;

		if (availableComponents == null || availableComponents.isEmpty()) {
			this.availableComponents = Set.of(Alias.values());
		} else {
			this.availableComponents = availableComponents.stream()
					.map(Alias::getByComponentName)
					.collect(Collectors.toSet());
		}

		this.availableStates = State.getLabels();
	}

	public Set<Alias> getAvailableComponents() {
		return availableComponents;
	}

	public Set<String> getAvailableStates() {
		return availableStates;
	}

	public CertificateDefaultValueDto getCertificateDefaultValue() {
		return new CertificateDefaultValueDto(
				defaultCountry,
				defaultState.getLabel(),
				defaultLocality,
				defaultOrganization
		);
	}
}
