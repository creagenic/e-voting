/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend.process.publickeyssharing;

import static com.google.common.base.Preconditions.checkState;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.Map;

import ch.post.it.evoting.cryptoprimitives.utils.VerificationResult;
import ch.post.it.evoting.evotinglibraries.direct.trust.KeystoreValidator;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;

public class KeystoreKeyImporter {

	private KeystoreKeyImporter() {
		// utility class
	}

	public static byte[] importPublicKeys(final byte[] keyStoreBytes, final char[] password, final Alias component,
			final Map<Alias, Certificate> componentKeys) {
		try {
			final KeyStore keyStore = KeyStore.getInstance("PKCS12");

			try (final ByteArrayInputStream keyStoreStream = new ByteArrayInputStream(keyStoreBytes)) {
				keyStore.load(keyStoreStream, password.clone());
			}

			componentKeys.entrySet().stream()
					.filter(componentAndPublicKey -> {
						try {
							return !keyStore.containsAlias(componentAndPublicKey.getKey().get());
						} catch (final KeyStoreException e) {
							throw new KeystoreImportException(e);
						}
					})
					.forEach(componentAndPublicKey -> saveCertificateInKeyStore(
							componentAndPublicKey.getKey(),
							componentAndPublicKey.getValue(),
							keyStore));

			final Alias signingAlias = component.hasPrivateKey() ? component : null;
			final VerificationResult result = KeystoreValidator.validateKeystore(keyStore, signingAlias, password.clone());
			checkState(result.isVerified(), "Keystore validation failed. [component: %s, errorMessages: %s]", component.get(),
					result.isVerified() ? "" : result.getErrorMessages());

			try (final ByteArrayOutputStream updatedKeyStoreStream = new ByteArrayOutputStream()) {
				keyStore.store(updatedKeyStoreStream, password.clone());
				return updatedKeyStoreStream.toByteArray();
			}
		} catch (final IOException | KeyStoreException | NoSuchAlgorithmException | CertificateException e) {
			throw new KeystoreImportException(e);
		} finally {
			Arrays.fill(password, '0');
		}
	}

	private static void saveCertificateInKeyStore(final Alias component, final Certificate key, final KeyStore keyStore) {
		try {
			keyStore.setCertificateEntry(component.get(), key);
		} catch (final KeyStoreException e) {
			throw new KeystoreImportException(e);
		}
	}

	public static class KeystoreImportException extends RuntimeException {
		public KeystoreImportException(final Throwable cause) {
			super(cause);
		}
	}
}
