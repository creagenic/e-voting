/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend.process.publickeyssharing;

import static ch.post.it.evoting.directtrusttool.backend.RouteConstants.BASE_PATH;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartRequest;

@RestController
@RequestMapping(BASE_PATH + "/public-keys")
public class PublicKeysController {

	private final PublicKeysService publicKeysService;

	public PublicKeysController(final PublicKeysService publicKeysService) {
		this.publicKeysService = publicKeysService;
	}

	@GetMapping(value = "{sessionId}")
	public String downloadPublicKeys(
			@PathVariable
			final String sessionId) {
		validateUUID(sessionId);
		return new String(Base64.getEncoder().encode(publicKeysService.downloadPublicKeys(sessionId)));
	}

	@PostMapping(value = "{sessionId}", consumes = "multipart/form-data")
	public void importPublicKeys(
			@PathVariable
			final String sessionId,
			final MultipartRequest multipartRequest) {
		validateUUID(sessionId);
		checkNotNull(multipartRequest);

		final Map<String, String> componentKeys = multipartRequest.getFileMap().entrySet().stream()
				.collect(Collectors.toUnmodifiableMap(Map.Entry::getKey, kv -> {
					try {
						return new String(kv.getValue().getBytes(), StandardCharsets.UTF_8);
					} catch (final IOException e) {
						throw new UncheckedIOException(e);
					}
				}));

		publicKeysService.importPublicKeys(sessionId, componentKeys);
	}
}
