/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend.process.publickeyssharing;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.security.cert.Certificate;
import java.util.Arrays;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.directtrusttool.backend.process.ComponentStorageRepository;
import ch.post.it.evoting.directtrusttool.backend.process.PemConverterService;
import ch.post.it.evoting.directtrusttool.backend.process.Zipper;
import ch.post.it.evoting.directtrusttool.backend.process.session.Phase;
import ch.post.it.evoting.directtrusttool.backend.process.session.SessionService;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;

@Service
public class PublicKeysService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PublicKeysService.class);
	private static final String KEY_FILE_EXTENSION = "pem";

	private final ComponentStorageRepository componentStorageRepository;
	private final SessionService sessionService;
	private final PemConverterService pemConverterService;

	public PublicKeysService(final ComponentStorageRepository componentStorageRepository, final SessionService sessionService,
			final PemConverterService pemConverterService) {
		this.componentStorageRepository = componentStorageRepository;
		this.sessionService = sessionService;
		this.pemConverterService = pemConverterService;
	}

	public byte[] downloadPublicKeys(final String sessionId) {
		validateUUID(sessionId);
		checkState(sessionService.getSessionPhase(sessionId).equals(Phase.SHARING_PUBLIC_KEYS));

		final Map<String, byte[]> filesMap = componentStorageRepository.selectedComponents(sessionId).stream()
				.flatMap(component -> {
					final ComponentStorageRepository.Key key = new ComponentStorageRepository.Key(sessionId, component,
							ComponentStorageRepository.Type.PUBLIC_KEY);
					final String name = component.name() + "." + KEY_FILE_EXTENSION;
					return componentStorageRepository.getBytes(key).map(bytes -> Map.entry(name, bytes)).stream();
				})
				.collect(Collectors.toUnmodifiableMap(Map.Entry::getKey, Map.Entry::getValue));

		LOGGER.info("Files created successfully. Ready to create zip and download public keys. [sessionId: {}]", sessionId);

		return Zipper.zip(filesMap);
	}

	public void importPublicKeys(final String sessionId, final Map<String, String> componentKeysAsPem) {
		validateUUID(sessionId);
		checkState(sessionService.getSessionPhase(sessionId).equals(Phase.SHARING_PUBLIC_KEYS));
		checkNotNull(componentKeysAsPem);
		checkArgument(componentKeysAsPem.size() == Alias.values().length - 1);

		// Convert the PEM map to X509 map
		final Map<Alias, Certificate> componentKeys = componentKeysAsPem.entrySet().stream()
				.collect(Collectors.toUnmodifiableMap(
						e -> Alias.valueOf(e.getKey().replace("." + KEY_FILE_EXTENSION, "")),
						e -> pemConverterService.fromPem(e.getValue())));

		componentStorageRepository.selectedComponents(sessionId).forEach(component -> {
			final byte[] keystore = componentStorageRepository.getBytes(
							new ComponentStorageRepository.Key(sessionId, component, ComponentStorageRepository.Type.KEYSTORE))
					.orElseThrow(() -> new NoSuchElementException("Missing keystore."));
			final char[] password = componentStorageRepository.getCharArray(
							new ComponentStorageRepository.Key(sessionId, component, ComponentStorageRepository.Type.PASSWORD))
					.orElseThrow(() -> new NoSuchElementException("Missing password for keystore."));

			final byte[] updatedKeystore;
			try {
				updatedKeystore = KeystoreKeyImporter.importPublicKeys(keystore, password.clone(), component, componentKeys);
			} finally {
				Arrays.fill(password, '0');
			}

			componentStorageRepository.putBytes(new ComponentStorageRepository.Key(sessionId, component, ComponentStorageRepository.Type.KEYSTORE),
					updatedKeystore);
		});

		LOGGER.info("Imported public keys successfully. [sessionId: {}]", sessionId);

		sessionService.setPhase(sessionId, Phase.DOWNLOAD_KEYSTORE);
	}
}
