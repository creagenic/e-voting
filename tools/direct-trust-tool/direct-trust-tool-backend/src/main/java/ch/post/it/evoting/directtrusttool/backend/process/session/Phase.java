/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.backend.process.session;

public enum Phase {
	GENERATE_KEYSTORE,
	SHARING_PUBLIC_KEYS,
	DOWNLOAD_KEYSTORE
}