/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.cli;

import java.security.cert.CertificateException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ch.post.it.evoting.directtrusttool.backend.process.ComponentStorageRepository;
import ch.post.it.evoting.directtrusttool.backend.process.FileRepository;
import ch.post.it.evoting.directtrusttool.backend.process.PemConverterService;
import ch.post.it.evoting.directtrusttool.backend.process.keystoredownload.KeystoreDownloadService;
import ch.post.it.evoting.directtrusttool.backend.process.keystoregeneration.KeystoreGenerationService;
import ch.post.it.evoting.directtrusttool.backend.process.publickeyssharing.PublicKeysService;
import ch.post.it.evoting.directtrusttool.backend.process.session.SessionService;

@Configuration
public class DirectTrustToolCliConfig {

	@Bean
	public FileRepository fileRepository(
			@Value("${app.directory.output}")
			final String outputDirectory) {
		return new FileRepository(outputDirectory);
	}

	@Bean
	public ComponentStorageRepository componentStorageRepository(final FileRepository fileRepository) {
		return new ComponentStorageRepository(fileRepository);
	}

	@Bean
	public SessionService sessionService(final ComponentStorageRepository componentStorageRepository) {
		return new SessionService(componentStorageRepository);
	}

	@Bean
	public PemConverterService pemConverterService() throws CertificateException {
		return new PemConverterService();
	}

	@Bean
	public KeystoreDownloadService keystoreDownloadService(
			final ComponentStorageRepository componentStorageRepository,
			final PemConverterService pemConverterService,
			final SessionService sessionService) {
		return new KeystoreDownloadService(componentStorageRepository, pemConverterService, sessionService);
	}

	@Bean
	public PublicKeysService publicKeysService(
			final ComponentStorageRepository componentStorageRepository,
			final PemConverterService pemConverterService,
			final SessionService sessionService) {
		return new PublicKeysService(componentStorageRepository, sessionService, pemConverterService);
	}

	@Bean
	public KeystoreGenerationService keystoreGenerationService(
			final ComponentStorageRepository componentStorageRepository,
			final PemConverterService pemConverterService,
			final SessionService sessionService) {
		return new KeystoreGenerationService(componentStorageRepository, sessionService, pemConverterService);
	}
}
