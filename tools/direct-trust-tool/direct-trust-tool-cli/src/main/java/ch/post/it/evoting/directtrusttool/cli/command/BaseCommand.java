/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.cli.command;

import org.springframework.stereotype.Component;

import picocli.CommandLine;

@Component
@CommandLine.Command(subcommands = {
		KeyStoreGenerationCommand.class,
		DownloadPublicKeysCommand.class,
		ImportPublicKeysCommand.class,
		DownloadKeystoresCommand.class,
		ClearCommand.class
})
public class BaseCommand {
}
