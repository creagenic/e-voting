/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.cli.command;

import java.nio.file.Path;

import org.springframework.stereotype.Component;

import ch.post.it.evoting.directtrusttool.backend.process.keystoredownload.KeystoreDownloadService;
import ch.post.it.evoting.directtrusttool.cli.FileService;

import picocli.CommandLine;

@Component
@CommandLine.Command(
		name = "download-keystores",
		description = "Download the generated keystores.",
		mixinStandardHelpOptions = true)
public class DownloadKeystoresCommand implements Runnable {

	@CommandLine.Option(
			names = { "--session-id" },
			description = "The UUID of the wanted session.",
			defaultValue = "00000000000000000000000000000000"
	)
	private String sessionId;

	@CommandLine.Option(
			names = { "--suffix" },
			description = "The custom suffix you want to add to the generated keystores.",
			defaultValue = ""
	)
	private String suffix;

	@CommandLine.Option(
			names = { "--output" },
			description = "The path to save the generated keystores.",
			required = true
	)
	private Path output;

	private final KeystoreDownloadService keystoreDownloadService;
	private final FileService fileService;

	public DownloadKeystoresCommand(final KeystoreDownloadService keystoreDownloadService, final FileService fileService) {
		this.keystoreDownloadService = keystoreDownloadService;
		this.fileService = fileService;
	}

	@Override
	public void run() {
		final byte[] downloadedKeystoresAsZip = keystoreDownloadService.downloadKeyStores(sessionId, suffix);
		fileService.saveByteArrayAsZip(downloadedKeystoresAsZip, output);
	}
}

