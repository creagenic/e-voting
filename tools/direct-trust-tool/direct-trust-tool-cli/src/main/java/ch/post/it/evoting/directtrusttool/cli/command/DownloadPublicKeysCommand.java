/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.cli.command;

import java.nio.file.Path;

import org.springframework.stereotype.Component;

import ch.post.it.evoting.directtrusttool.backend.process.publickeyssharing.PublicKeysService;
import ch.post.it.evoting.directtrusttool.cli.FileService;

import picocli.CommandLine;

@Component
@CommandLine.Command(
		name = "download-public-keys",
		description = "Download the public keys of the generated keystores.",
		mixinStandardHelpOptions = true)
public class DownloadPublicKeysCommand implements Runnable {

	@CommandLine.Option(
			names = { "--session-id" },
			description = "The UUID of the wanted session.",
			defaultValue = "00000000000000000000000000000000"
	)
	private String sessionId;

	@CommandLine.Option(
			names = { "--output" },
			description = "The path to save the exporter public keys.",
			required = true
	)
	private Path output;

	private final PublicKeysService publicKeysService;
	private final FileService fileService;

	public DownloadPublicKeysCommand(final PublicKeysService publicKeysService, final FileService fileService) {
		this.publicKeysService = publicKeysService;
		this.fileService = fileService;
	}

	@Override
	public void run() {
		final byte[] downloadedPublicKeysAsZip = publicKeysService.downloadPublicKeys(sessionId);
		fileService.saveByteArrayAsZip(downloadedPublicKeysAsZip, output);
	}
}

