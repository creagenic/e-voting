/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.cli.command;

import java.nio.file.Path;

import org.springframework.stereotype.Component;

import ch.post.it.evoting.directtrusttool.backend.process.publickeyssharing.PublicKeysService;
import ch.post.it.evoting.directtrusttool.cli.FileService;

import picocli.CommandLine;

@Component
@CommandLine.Command(
		name = "import-public-keys",
		description = "Add the public keys to the keystores.",
		mixinStandardHelpOptions = true)
public class ImportPublicKeysCommand implements Runnable {

	@CommandLine.Option(
			names = { "--session-id" },
			description = "The UUID of the wanted session.",
			defaultValue = "00000000000000000000000000000000"
	)
	private String sessionId;

	@CommandLine.Option(
			names = { "--public-key-path" },
			description = "The path where is the complete set of keys from all components to import.",
			required = true
	)
	private Path publicKeysPaths;

	private final PublicKeysService publicKeysService;
	private final FileService fileService;

	public ImportPublicKeysCommand(final PublicKeysService publicKeysService, final FileService fileService) {
		this.publicKeysService = publicKeysService;
		this.fileService = fileService;
	}

	@Override
	public void run() {
		publicKeysService.importPublicKeys(sessionId, fileService.getAllFilesContentAsString(publicKeysPaths));
	}
}

