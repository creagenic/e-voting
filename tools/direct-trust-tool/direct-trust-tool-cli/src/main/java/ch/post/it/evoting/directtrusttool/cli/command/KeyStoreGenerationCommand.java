/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.cli.command;

import java.time.LocalDate;
import java.util.Set;

import org.springframework.stereotype.Component;

import ch.post.it.evoting.directtrusttool.backend.process.keystoregeneration.KeyStorePropertiesDto;
import ch.post.it.evoting.directtrusttool.backend.process.keystoregeneration.KeystoreGenerationService;
import ch.post.it.evoting.directtrusttool.cli.command.converter.ComponentConverter;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;

import picocli.CommandLine;

@Component
@CommandLine.Command(
		name = "keystores-generation",
		description = "Generate the wanted keystores.",
		mixinStandardHelpOptions = true)
public class KeyStoreGenerationCommand implements Runnable {

	@CommandLine.Option(
			names = { "--session-id" },
			description = "The UUID of the wanted session.",
			defaultValue = "00000000000000000000000000000000"
	)
	private String sessionId;

	@CommandLine.Option(
			names = { "--components" },
			description = "The component(s) for which generate a keystore.",
			converter = ComponentConverter.class,
			required = true,
			split = ","
	)
	private Set<Alias> components;

	@CommandLine.Option(
			names = { "--valid-until" },
			description = "The date until which the keystore is valid. Format: yyyy-MM-dd.",
			required = true
	)
	private LocalDate validUntil;

	@CommandLine.Option(
			names = { "--country" },
			description = "The country for which generate a keystore.",
			required = true
	)
	private String country;

	@CommandLine.Option(
			names = { "--state" },
			description = "The state for which generate a keystore.",
			required = true
	)
	private String state;

	@CommandLine.Option(
			names = { "--locality" },
			description = "The locality for which generate a keystore.",
			required = true
	)
	private String locality;

	@CommandLine.Option(
			names = { "--organization" },
			description = "The organization for which generate a keystore.",
			required = true
	)
	private String organization;

	private final KeystoreGenerationService keystoreGenerationService;

	public KeyStoreGenerationCommand(final KeystoreGenerationService keystoreGenerationService) {
		this.keystoreGenerationService = keystoreGenerationService;
	}

	@Override
	public void run() {
		keystoreGenerationService.generateKeyStores(
				sessionId,
				new KeyStorePropertiesDto(
						validUntil,
						country,
						state,
						locality,
						organization,
						components
				)
		);
	}
}

