/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.directtrusttool.cli.command;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.reset;
import static org.mockito.BDDMockito.then;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import ch.post.it.evoting.directtrusttool.backend.process.keystoregeneration.KeyStorePropertiesDto;
import ch.post.it.evoting.directtrusttool.backend.process.keystoregeneration.KeystoreGenerationService;
import ch.post.it.evoting.directtrusttool.cli.DirectTrustToolCliApplication;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;

import picocli.CommandLine;

@SpringBootTest(webEnvironment = NONE, classes = DirectTrustToolCliApplication.class)
class KeyStoreGenerationCommandTest {

	@MockBean
	KeystoreGenerationService keystoreGenerationService;

	@Autowired
	CommandLine.IFactory factory;

	@Autowired
	KeyStoreGenerationCommand keyStoreGenerationCommand;

	@AfterEach
	void tearDown() {
		reset(keystoreGenerationService);
	}

	@ParameterizedTest
	@EnumSource(Alias.class)
	void testAllAliasesWorkIndividually(final Alias alias) {
		// given
		final KeyStorePropertiesDto expected = KeyStorePropertiesDtoBuilder.builder()
				.addAlias(alias)
				.build();

		// when
		final int exitCode = new CommandLine(keyStoreGenerationCommand, factory)
				.execute(
						"--components", aliasSetToCliString(expected.wantedComponents()),
						"--valid-until", expected.validUntil().format(DateTimeFormatter.ISO_LOCAL_DATE),
						"--country", expected.country(),
						"--state", expected.state(),
						"--locality", expected.locality(),
						"--organization", expected.organisation()
				);

		// then
		assertEquals(0, exitCode);
		then(keystoreGenerationService).should().generateKeyStores("00000000000000000000000000000000", expected);
	}

	@Test
	void testAllAliasesWorkSimultaneously() {
		// given
		final KeyStorePropertiesDto expected = KeyStorePropertiesDtoBuilder.builder()
				.aliases(Set.of(Alias.values()))
				.build();

		// when
		final int exitCode = new CommandLine(keyStoreGenerationCommand, factory)
				.execute(
						"--components", aliasSetToCliString(expected.wantedComponents()),
						"--valid-until", expected.validUntil().format(DateTimeFormatter.ISO_LOCAL_DATE),
						"--country", expected.country(),
						"--state", expected.state(),
						"--locality", expected.locality(),
						"--organization", expected.organisation()
				);

		// then
		assertEquals(0, exitCode);
		then(keystoreGenerationService).should().generateKeyStores("00000000000000000000000000000000", expected);
	}

	@Test
	void testWithCustomSessionNumber() {
		// given
		final KeyStorePropertiesDto expected = KeyStorePropertiesDtoBuilder.builder()
				.aliases(Set.of(Alias.values()))
				.build();

		// when
		final int exitCode = new CommandLine(keyStoreGenerationCommand, factory)
				.execute(
						"--session-id", "11111111111111111111111111111111",
						"--components", aliasSetToCliString(expected.wantedComponents()),
						"--valid-until", expected.validUntil().format(DateTimeFormatter.ISO_LOCAL_DATE),
						"--country", expected.country(),
						"--state", expected.state(),
						"--locality", expected.locality(),
						"--organization", expected.organisation()
				);

		// then
		assertEquals(0, exitCode);
		then(keystoreGenerationService).should().generateKeyStores("11111111111111111111111111111111", expected);
	}

	static String aliasSetToCliString(final Set<Alias> aliases) {
		return aliases.stream().map(Alias::get).reduce((a, b) -> a + "," + b).orElse("");
	}

	static class KeyStorePropertiesDtoBuilder {
		private final LocalDate date = LocalDate.now();
		private final String country = "testCountry";
		private final String state = "Aargau";
		private final String locality = "testLocality";
		private final String organization = "DT_CT_20001212_TT01";
		private Set<Alias> aliases = new HashSet<>();

		private KeyStorePropertiesDtoBuilder() {
			// use static method
		}

		public KeyStorePropertiesDtoBuilder aliases(final Set<Alias> aliases) {
			this.aliases = aliases;
			return this;
		}

		public KeyStorePropertiesDtoBuilder addAlias(final Alias alias) {
			this.aliases.add(alias);
			return this;
		}

		public KeyStorePropertiesDto build() {
			return new KeyStorePropertiesDto(date, country, state, locality, organization, aliases);
		}

		public static KeyStorePropertiesDtoBuilder builder() {
			return new KeyStorePropertiesDtoBuilder();
		}
	}
}