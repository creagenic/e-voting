/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.integrationtests;

import static ch.post.it.evoting.cryptoprimitives.math.RandomFactory.createRandom;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static java.lang.String.format;
import static java.nio.file.Files.list;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchService;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.directtrusttool.backend.process.session.Phase;
import ch.post.it.evoting.directtrusttool.backend.process.session.SessionService;
import ch.post.it.evoting.directtrusttool.cli.DirectTrustToolCliApplication;
import ch.post.it.evoting.evotinglibraries.direct.trust.KeystoreValidator;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;

@SpringBootTest(webEnvironment = NONE, classes = DirectTrustToolCliApplication.class)
@ActiveProfiles("test")
public class DirectTrustToolCliApplicationIT {

	@Value("${app.directory.output}")
	Path sessionDirectory;

	@TempDir
	Path tempDir;

	@Autowired
	DirectTrustToolCliApplication application;

	@Autowired
	SessionService sessionService;

	@Test
	void testHappyPath() throws IOException, InterruptedException {
		final String sessionId = createRandom().genRandomString(ID_LENGTH, Base16Alphabet.getInstance());

		// DTT process
		keystoreGeneration(sessionId);
		downloadPublicKeys(sessionId);
		importPublicKeys(sessionId);
		downloadKeystores(sessionId);
		clear(sessionId);

		// Validation of the key stores
		validateKeystores(sessionId);
	}

	private void keystoreGeneration(final String sessionId) {
		// precondition
		assertEquals(Phase.GENERATE_KEYSTORE, sessionService.getSessionPhase(sessionId));

		// given
		final String components = Arrays.stream(Alias.values()).map(Alias::get).reduce((a, b) -> a + "," + b).orElse("");
		final LocalDate date = LocalDate.now().plusDays(1);
		final String country = "testCountry";
		final String state = "Aargau";
		final String locality = "testLocality";
		final String organization = "DT_CT_20001212_TT01";

		// when
		application.run(
				"keystores-generation",
				"--session-id", sessionId,
				"--components", components,
				"--valid-until", date.format(DateTimeFormatter.ISO_LOCAL_DATE),
				"--country", country,
				"--state", state,
				"--locality", locality,
				"--organization", organization
		);

		// then
		assertTrue(Files.exists(sessionDirectory));
		assertTrue(Files.exists(sessionDirectory.resolve(sessionId)));
		assertEquals(Phase.SHARING_PUBLIC_KEYS, sessionService.getSessionPhase(sessionId));
	}

	private void downloadPublicKeys(final String sessionId) throws IOException {
		// precondition
		assertEquals(Phase.SHARING_PUBLIC_KEYS, sessionService.getSessionPhase(sessionId));

		// given
		final Path publicKeysZip = tempDir.resolve("public-keys.zip");
		final long expectedCount = Arrays.stream(Alias.values()).filter(Alias::hasPrivateKey).count();

		// when / then
		application.run(
				"download-public-keys",
				"--session-id", sessionId,
				"--output", publicKeysZip.toString()
		);
		assertTrue(Files.exists(publicKeysZip));

		unzip(publicKeysZip, tempDir.resolve("public-keys"));

		assertTrue(Files.exists(tempDir.resolve("public-keys")));
		try (final Stream<Path> list = list(tempDir.resolve("public-keys"))) {
			assertEquals(expectedCount, list.count());
		}
		assertEquals(Phase.SHARING_PUBLIC_KEYS, sessionService.getSessionPhase(sessionId));
	}

	private void importPublicKeys(final String sessionId) {
		// precondition
		assertEquals(Phase.SHARING_PUBLIC_KEYS, sessionService.getSessionPhase(sessionId));

		// givens
		final Path publicKeysPath = tempDir.resolve("public-keys");

		// when
		application.run(
				"import-public-keys",
				"--session-id", sessionId,
				"--public-key-path", publicKeysPath.toString()
		);

		// then
		assertTrue(Files.exists(sessionDirectory));
		assertTrue(Files.exists(sessionDirectory.resolve(sessionId)));
		assertEquals(Phase.DOWNLOAD_KEYSTORE, sessionService.getSessionPhase(sessionId));
	}

	private void downloadKeystores(final String sessionId) throws IOException {
		// precondition
		assertEquals(Phase.DOWNLOAD_KEYSTORE, sessionService.getSessionPhase(sessionId));

		// givens
		final String suffix = "Test";
		final Path publicKeysZip = tempDir.resolve("out").resolve("keystores.zip");

		// when
		application.run(
				"download-keystores",
				"--session-id", sessionId,
				"--suffix", suffix,
				"--output", publicKeysZip.toString()
		);

		// then
		assertTrue(Files.exists(publicKeysZip));

		unzip(publicKeysZip, tempDir.resolve("keystores"));
		try (final Stream<Path> components = list(tempDir.resolve("keystores"))) {
			assertEquals(Alias.values().length, components.count());
		}
		try (final Stream<Path> components = list(tempDir.resolve("keystores"))) {
			components.forEach(path -> {
				try (final Stream<Path> contents = list(path)) {
					assertEquals(2, contents.count());
				} catch (final IOException e) {
					throw new RuntimeException(e);
				}
			});
		}

		assertEquals(Phase.DOWNLOAD_KEYSTORE, sessionService.getSessionPhase(sessionId));
	}

	private void clear(final String sessionId) throws IOException, InterruptedException {
		// precondition
		assertEquals(Phase.DOWNLOAD_KEYSTORE, sessionService.getSessionPhase(sessionId));

		// givens
		final long expectedCount = Alias.values().length * 2L;

		// when
		application.run(
				"clear",
				"--session-id", sessionId
		);

		// then
		try (final WatchService watchService = FileSystems.getDefault().newWatchService()) {
			sessionDirectory.register(watchService, StandardWatchEventKinds.ENTRY_DELETE);
			watchService.poll(5, TimeUnit.SECONDS);
		}
		assertTrue(Files.exists(sessionDirectory));
		assertTrue(Files.notExists(sessionDirectory.resolve(sessionId)));
		assertEquals(Phase.GENERATE_KEYSTORE, sessionService.getSessionPhase(sessionId));
	}

	private void validateKeystores(final String sessionId) throws IOException {
		// given
		final String suffix = "Test";
		final Path keystoresPath = tempDir.resolve("keystores");
		final Map<String, String> passwordsMap = Arrays.stream(Alias.values())
				.map(Alias::get)
				.map(aliasName -> Map.entry(aliasName, keystoresPath.resolve(format("direct_trust_%s", aliasName))
						.resolve(format("signing_pw_%s_%s.txt", aliasName, suffix))))
				.map(entry -> {
					try {
						return Map.entry(entry.getKey(), Files.readString(entry.getValue()));
					} catch (final IOException e) {
						throw new RuntimeException(e);
					}
				})
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		Arrays.stream(Alias.values())
				.forEach(alias -> {
					final Path keystorePath = keystoresPath.resolve(format("direct_trust_%s", alias.get()))
							.resolve(format("signing_keystore_%s_%s.p12", alias.get(), suffix));
					try {
						final char[] password = Files.readString(
								keystoresPath.resolve(format("direct_trust_%s", alias.get()))
										.resolve(format("signing_pw_%s_%s.txt", alias.get(), suffix))).toCharArray();
						final KeyStore keyStore = KeyStore.getInstance(keystorePath.toFile(), password);
						final Alias signingAlias = alias.hasPrivateKey() ? alias : null;
						assertTrue(KeystoreValidator.validateKeystore(keyStore, signingAlias, password).isVerified());
					} catch (final KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException e) {
						throw new RuntimeException(e);
					}
				});
	}

	public static void unzip(final Path zipFilePath, final Path targetDir) throws IOException {
		try (final ZipInputStream zis = new ZipInputStream(Files.newInputStream(zipFilePath))) {
			for (ZipEntry entry; (entry = zis.getNextEntry()) != null; ) {
				final Path resolvedPath = targetDir.resolve(entry.getName()).normalize();
				Files.createDirectories(resolvedPath.getParent());
				Files.copy(zis, resolvedPath);
			}
		}
	}
}
