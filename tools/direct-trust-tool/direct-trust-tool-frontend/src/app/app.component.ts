/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, OnInit} from '@angular/core';
import {SessionService} from "./session/session.service";
import {Phase} from "./app.module";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  protected readonly Phase = Phase;

  constructor(protected session: SessionService) {
  }

  ngOnInit(): void {
  }

  get keystoreGenerationState() {
    return this.session.currentPhase === Phase.GENERATE_KEYSTORE ? ActivationState.selected : ActivationState.disabled;
  }

  get publicKeySharingState() {
    return this.session.currentPhase === Phase.SHARING_PUBLIC_KEYS ? ActivationState.selected : ActivationState.disabled;
  }

  get keystoreDownloadState() {
    return this.session.currentPhase === Phase.DOWNLOAD_KEYSTORE ? ActivationState.selected : ActivationState.disabled;
  }

  resetData() {
    if (confirm("Are you sure that you want to reset the data? All data will be lost.")) {
      this.session.reset();
    }
  }
}


enum ActivationState {
  selected = 'active',
  disabled = 'disabled'
}
