/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import {PublicKeysSharingComponent} from "./public-keys-sharing/public-keys-sharing.component";
import {KeystoreDownloadComponent} from "./keystore-download/keystore-download.component";
import {KeystoreGenerationSetup} from "./keystore-generation/keystore-generation.component";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    KeystoreGenerationSetup,
    PublicKeysSharingComponent,
    KeystoreDownloadComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}

export const API_BASE_PATH = 'http://localhost:8080/api/direct-trust'

export interface EvotingComponent {
  key: string,
  label: string,
  hasKey: boolean
}

export const EvotingComponents: { [char: string]: EvotingComponent } = {
  CANTON: {key: "CANTON", label: "Canton", hasKey: true},
  SDM_CONFIG: {key: "SDM_CONFIG", label: "SDM config", hasKey: true},
  SDM_TALLY: {key: "SDM_TALLY", label: "SDM tally", hasKey: true},
  VOTING_SERVER: {key: "VOTING_SERVER", label: "Voting Server", hasKey: true},
  CONTROL_COMPONENT_1: {key: "CONTROL_COMPONENT_1", label: "Control component 1", hasKey: true},
  CONTROL_COMPONENT_2: {key: "CONTROL_COMPONENT_2", label: "Control component 2", hasKey: true},
  CONTROL_COMPONENT_3: {key: "CONTROL_COMPONENT_3", label: "Control component 3", hasKey: true},
  CONTROL_COMPONENT_4: {key: "CONTROL_COMPONENT_4", label: "Control component 4", hasKey: true},
  PRINTING_COMPONENT: {key: "PRINTING_COMPONENT", label: "Printing component", hasKey: true},
  VERIFIER: {key: "VERIFIER", label: "Verifier", hasKey: true},
}

export enum Phase {
  GENERATE_KEYSTORE,
  SHARING_PUBLIC_KEYS,
  DOWNLOAD_KEYSTORE
}
