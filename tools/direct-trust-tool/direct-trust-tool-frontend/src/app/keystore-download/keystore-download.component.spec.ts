/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {ComponentFixture, TestBed} from '@angular/core/testing';

import {KeystoreDownloadComponent} from './keystore-download.component';

describe('KeystoreDownloadComponent', () => {
  let component: KeystoreDownloadComponent;
  let fixture: ComponentFixture<KeystoreDownloadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [KeystoreDownloadComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(KeystoreDownloadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
