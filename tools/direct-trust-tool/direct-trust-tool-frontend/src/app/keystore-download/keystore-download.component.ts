/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SessionService} from '../session/session.service';
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators} from '@angular/forms';
import {API_BASE_PATH} from '../app.module';
import {HttpClient} from '@angular/common/http';
import {switchMap} from 'rxjs';

@Component({
  selector: 'app-keystore-download',
  standalone: true,
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  templateUrl: './keystore-download.component.html',
  styleUrl: './keystore-download.component.css'
})
export class KeystoreDownloadComponent {

  downloadForm: FormGroup;
  private fingerprints!: { [component: string]: string };

  constructor(private fb: FormBuilder, private http: HttpClient, public session: SessionService) {

    this.downloadForm = this.fb.group({
      customSuffix: ['', [Validators.pattern(/^\w*$/)]],
    });

    this.session.getSession().pipe(
      switchMap(sessionId => this.http.get(`${API_BASE_PATH}/key-store-download/fingerprints/${sessionId}`, {responseType: "json"}))
    ).subscribe(value => {
      this.fingerprints = <{ [component: string]: string }>value;
    });
  }

  downloadKeyStores() {
    this.session.getSession().subscribe(sessionId => {

      const suffix: string = this.downloadForm.get('customSuffix')?.value;
      let url = `${API_BASE_PATH}/key-store-download/${sessionId}`;
      if (suffix.trim().length > 0) {
        url += `?suffix=${suffix}`;
      }

      this.http.get(url, {responseType: "text"})
        .subscribe(value => {
          const src = `data:text/csv;base64,${value}`;
          const link = document.createElement("a")
          link.href = src;
          link.download = suffix ? `key_stores_${suffix}.zip` : 'key_stores.zip';
          link.click();
          link.remove();
        });
    });
  }

  getFingerprint(component: string): string {
    if(!this.fingerprints) {
      return '-';
    }
    return this.fingerprints[component]?? '-';
  }
}
