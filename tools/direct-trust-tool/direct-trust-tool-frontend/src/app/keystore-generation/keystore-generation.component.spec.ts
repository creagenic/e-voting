/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {ComponentFixture, TestBed} from '@angular/core/testing';

import {KeystoreGenerationSetup} from './keystore-generation.component';

describe('ComponentSelectionComponent', () => {
  let component: KeystoreGenerationSetup;
  let fixture: ComponentFixture<KeystoreGenerationSetup>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [KeystoreGenerationSetup]
    })
      .compileComponents();

    fixture = TestBed.createComponent(KeystoreGenerationSetup);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
