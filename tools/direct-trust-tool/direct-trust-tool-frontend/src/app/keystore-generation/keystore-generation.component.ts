/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AbstractControl, FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, ValidationErrors, ValidatorFn, Validators} from "@angular/forms";
import {HttpClient} from '@angular/common/http';
import {SessionService} from '../session/session.service';
import {API_BASE_PATH, EvotingComponent, EvotingComponents} from '../app.module';
import {map, switchMap} from 'rxjs';
import {OrganisationValidator} from './organisation.validator';

@Component({
  selector: 'app-keystore-generation-setup',
  standalone: true,
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  templateUrl: './keystore-generation.component.html',
  styleUrl: './keystore-generation.component.css'
})
export class KeystoreGenerationSetup {

  generationForm: FormGroup;
  generationInProgress: boolean = false;
  organisationPolicies: string[] = ['prefix', 'contentPrefix', 'date', 'suffix', 'delimiter'];
  organisationPoliciesText: Map<string, string> = new Map<string, string>(
    [['prefix', 'A fixed prefix DT.'],
    ['contentPrefix', 'CT: cantonal abbreviation, for example SG, TG...'],
    ['date', 'Date in \'YYYYMMDD\' format.'],
    ['suffix', 'XY01: Test (TT) / Prod (PP), followed by ascending sequence number.'],
    ['delimiter', 'An underscore \'_\' delimiter between each part.']]
  );

  constructor(private fb: FormBuilder, private http: HttpClient, protected session: SessionService) {
    const defaultValidDate = new Date();
    defaultValidDate.setFullYear(defaultValidDate.getFullYear() + 4);

    this.generationForm = this.fb.group({
      components: this.fb.group({}),
      validUntil: [defaultValidDate.toISOString().substring(0, 10), [Validators.required]],
      country: ['', [Validators.required]],
      state: ['', [Validators.required]],
      locality: ['', [Validators.required]],
      organisation: ['', [Validators.required, OrganisationValidator.validate()]],
    });

    this.loadAvailableComponents();
    this.loadAvailableStates();
    this.loadCertificateDefaultValue();
  }

  _availableStates: string[] | undefined;

  get availableStates() {
    return this._availableStates;
  }

  _availableComponents: EvotingComponent[] | undefined;

  get availableComponents() {
    return this._availableComponents;
  }

  get organisation() {
    return this.generationForm.get('organisation');
  }

  generateKeystores() {
    const formComponents: FormGroup = this.generationForm.get('components') as FormGroup;
    const selectedComponents: string[] = Object.entries(formComponents.getRawValue())
      .filter((kv) => kv[1] === true)
      .map((kv) => kv[0]);

    const dto = <KeyStoreProperties>{
      validUntil: this.generationForm.get('validUntil')?.value,
      country: this.generationForm.get('country')?.value,
      state: this.generationForm.get('state')?.value,
      locality: this.generationForm.get('locality')?.value,
      organisation: this.generationForm.get('organisation')?.value,
      wantedComponents: selectedComponents
    };

    this.session.getSession().pipe(
      switchMap(sessionId => this.http.post(`${API_BASE_PATH}/key-store-generation/${sessionId}`, dto)),
    ).subscribe(() => this.session.update());

    this.generationInProgress = true;
  }

  hasError(policy: string): boolean {
    if(this.organisation?.value === '') {
      return true;
    }

    const errors = this.organisation?.errors;
    if (!errors?.['organisation']) return false;

    return errors['organisation'][policy];
  }

  private loadAvailableComponents() {
    // get the list of available components
    this.http.get(`${API_BASE_PATH}/key-store-generation/available/components`).subscribe((value) => {
        const availableComponents = (<string[]>value)
          .map(component => EvotingComponents[component])
          .sort((a, b) => a.label.localeCompare(b.label));

        this.generationForm.setControl('components', this.fb.group(availableComponents
          .reduce((control, value) => ({...control, [value.key]: [true]}), {}), {validators: requireCheckboxesToBeCheckedValidator()}
        ));
        this._availableComponents = availableComponents;
      }
    );
  }

  private loadAvailableStates() {
    // get the list of available states
    this.http.get(`${API_BASE_PATH}/key-store-generation/available/states`).subscribe((value) => {
      this._availableStates = (<string[]>value)
          .sort((a, b) => a.localeCompare(b));
      }
    );
  }

  private loadCertificateDefaultValue() {
    this.http.get(`${API_BASE_PATH}/key-store-generation/default/certificate`).pipe(
      map(value => <CertificateDefaultValue>value),
    ).subscribe(defaultValue => {
      this.generationForm.patchValue({
        country: defaultValue.country,
        state: defaultValue.state,
        locality: defaultValue.locality,
        organisation: defaultValue.organisation
      });
    });
  }
}

function requireCheckboxesToBeCheckedValidator(minRequired = 1): ValidatorFn {
  return function validate(controlGroup: AbstractControl): ValidationErrors | null {
    const formGroup = <FormGroup>controlGroup;
    let checked = 0;

    Object.keys(formGroup.controls).forEach(key => {
      if (formGroup.controls[key].value) {
        checked++
      }
    });

    if (checked >= minRequired) {
      return null;
    } else {
      return {notEnough: true}
    }
  };
}


interface CertificateDefaultValue {
  country: string;
  state: string;
  locality: string;
  organisation: string;
}

interface KeyStoreProperties {
  validUntil: string
  country: string;
  state: string;
  locality: string;
  organisation: string;
  wantedComponents: string[];
}
