/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {SessionService} from "../session/session.service";
import {EvotingComponents} from "../app.module";

@Component({
  selector: 'app-public-key-sharing',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  templateUrl: './public-keys-sharing.component.html',
  styleUrl: './public-keys-sharing.component.css'
})
export class PublicKeysSharingComponent {

  importForm: FormGroup;
  importInProgress: boolean = false;
  error: boolean = false;
  selectedFiles: FileList | undefined;
  wantedNumberOfKey: number | undefined;

  constructor(private fb: FormBuilder, public phase: SessionService) {
    this.importForm = this.fb.group({
      // eslint-disable-next-line @typescript-eslint/unbound-method
      publicKeyLocation: ['', [Validators.required]]
    });
    this.wantedNumberOfKey = Object.keys(EvotingComponents).length - 1;
  }

  selectFiles(event: Event): void {
    const target = event.target as HTMLInputElement;
    this.selectedFiles = target.files as FileList;
  }

  importPublicKeys() {
    if (this.selectedFiles) {
      const formData: FormData = new FormData();
      for (let i = 0; i < this.selectedFiles.length; i++) {
        const file: File = this.selectedFiles[i];
        formData.append(file.name, file);
      }
      this.phase.importKey(formData);
    }
  }

  downloadPublicKeys() {
    this.phase.downloadPublicKeys()
  }
}
