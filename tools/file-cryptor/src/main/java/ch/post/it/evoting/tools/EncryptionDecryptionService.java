/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.tools;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.evotinglibraries.domain.encryption.StreamedEncryptionDecryptionService;
import ch.post.it.evoting.evotinglibraries.domain.validations.PasswordValidation;

@Service
public class EncryptionDecryptionService {
	private static final Logger LOGGER = LoggerFactory.getLogger(EncryptionDecryptionService.class);

	private final StreamedEncryptionDecryptionService streamedEncryptionDecryptionService;

	private final Mode mode;
	private final char[] password;

	private final Path sourcePath;
	private final Path targetPath;

	private final byte[] associatedData = {};

	public EncryptionDecryptionService(
			final StreamedEncryptionDecryptionService streamedEncryptionDecryptionService,
			@Value("${mode}")
			final Mode mode,
			@Value("${password}")
			final char[] password,
			@Value("${source.file-path}")
			final Path sourceFilePath,
			@Value("${target.file-path}")
			final Path targetFilePath) {
		this.streamedEncryptionDecryptionService = streamedEncryptionDecryptionService;
		this.mode = checkNotNull(mode, "The mode is required.");
		PasswordValidation.validate(password, "file-cryptor");
		this.password = password;
		this.sourcePath = checkNotNull(sourceFilePath, "The source file path is required.");
		this.targetPath = checkNotNull(targetFilePath, "The target file path is required.");
	}

	@SuppressWarnings("java:S1301")
		// By choice, we prefer the usage of switch statement over if-else clause.
	void run() {
		checkState(Files.exists(sourcePath), "The given source file does not exist. [path: %s]", sourcePath);
		checkState(!Files.exists(targetPath), "The given target file already exists. [path: %s]", targetPath);

		try (final InputStream inputStream = Files.newInputStream(sourcePath);
				final OutputStream outputStream = Files.newOutputStream(targetPath)) {
			switch (mode) {
			case ENCRYPT -> {
				LOGGER.info("Encrypting file. Please wait... [path: {}]", sourcePath);
				streamedEncryptionDecryptionService.encrypt(outputStream, inputStream, password, associatedData);
				LOGGER.info("File successfully encrypted. [path: {}]", targetPath);
			}
			case DECRYPT -> {
				LOGGER.info("Decrypting file. Please wait... [path: {}]", sourcePath);
				final InputStream decryptedStream = streamedEncryptionDecryptionService.decrypt(inputStream, password, associatedData);
				decryptedStream.transferTo(outputStream);
				LOGGER.info("File successfully decrypted. [path: {}]", targetPath);
			}
			}
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	public enum Mode {
		ENCRYPT,
		DECRYPT
	}

}
