/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.tools.xmlsignature;

import java.io.IOException;
import java.nio.file.Path;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.google.common.annotations.VisibleForTesting;

import ch.post.it.evoting.cryptoprimitives.signing.SignatureFactory;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureGeneration;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureVerification;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.tools.xmlsignature.keystore.KeystoreRepository;

@Component
public class XmlSignatureCommandLine {

	private static final Logger LOGGER = LoggerFactory.getLogger(XmlSignatureCommandLine.class);
	private static final String USAGE_TEXT = "Usage : java -Ddirect.trust.keystore.location=<direct-trust-keystoreFile> "
			+ "-Ddirect.trust.keystore.password.location=<direct-trust-passFile> -jar xml-signature.jar <"
			+ String.join("|", Arrays.stream(SupportedFileType.values()).map(SupportedFileType::name).toList())
			+ "> <"
			+ String.join("|", Arrays.stream(Action.values()).map(Action::name).toList())
			+ "> <filePath>";
	private final KeystoreRepository keystoreRepository;

	public XmlSignatureCommandLine(final KeystoreRepository keystoreRepository) {
		this.keystoreRepository = keystoreRepository;
	}

	public void run(final String... args) {
		final boolean isExistingFileType = args.length > 0 && Arrays.stream(SupportedFileType.values()).anyMatch(s -> s.name().equals(args[0]));
		final boolean isExistingAction = args.length > 1 && Arrays.stream(Action.values()).anyMatch(a -> a.name().equals(args[1]));
		if (!isExistingFileType || !isExistingAction || args.length != 3) {
			LOGGER.error(USAGE_TEXT);
			new Result(false, USAGE_TEXT).logAsJson();
			return;
		}

		try {
			// Extract arguments.
			final SupportedFileType fileType = SupportedFileType.valueOf(args[0]);
			final Action action = Action.valueOf(args[1]);
			final Path filePath = Path.of(args[2]);

			final SignatureGeneration signatureGeneration = getSignatureGeneration(keystoreRepository, fileType.getSigningAlias());
			final SignatureVerification signatureVerification = getSignatureVerification(keystoreRepository);

			final XmlSigner<?> xmlSigner = fileType.getSignerClass()
					.getDeclaredConstructor(SignatureGeneration.class, SignatureVerification.class)
					.newInstance(signatureGeneration, signatureVerification);

			// Apply action by signer.
			final Result result = action.apply(xmlSigner, filePath);

			result.logAsJson();

			if (result.signatureVerified()) {
				LOGGER.info("The action has finished successfully. [fileType: {}, action: {}, filePath: {}]", fileType, action, filePath);
			} else {
				LOGGER.error("The action failed. [fileType: {}, action: {}, filePath: {}]", fileType, action, filePath);
			}
		} catch (final Exception e) {
			LOGGER.error("Unable to process the requested action.", e);
			new Result(false, e.getMessage()).logAsJson();
		}
	}

	@VisibleForTesting
	protected SignatureGeneration getSignatureGeneration(final KeystoreRepository keystoreRepository, final Alias signingAlias) {
		if (signingAlias != null) {
			char[] password = {};
			try {
				password = Arrays.copyOf(keystoreRepository.getKeystorePassword(), keystoreRepository.getKeystorePassword().length);
				final KeyStore keyStore = KeyStore.getInstance("PKCS12");
				keyStore.load(keystoreRepository.getKeyStore(), password);

				final PrivateKey privateKey = (PrivateKey) keyStore.getKey(signingAlias.get(), password);
				final X509Certificate certificate = (X509Certificate) keyStore.getCertificate(signingAlias.get());

				return SignatureFactory.getInstance().createSignatureGeneration(privateKey, certificate);
			} catch (final KeyStoreException | CertificateException | IOException | NoSuchAlgorithmException | UnrecoverableKeyException e) {
				throw new IllegalArgumentException(e);
			} finally {
				Arrays.fill(password, '0');
			}
		} else {
			return (message, additionalContextData) -> {
				throw new IllegalStateException("No signing alias provided");
			};
		}
	}

	@VisibleForTesting
	protected SignatureVerification getSignatureVerification(final KeystoreRepository keystoreRepository) {
		char[] password = {};
		try {
			password = Arrays.copyOf(keystoreRepository.getKeystorePassword(), keystoreRepository.getKeystorePassword().length);
			final KeyStore keyStore = KeyStore.getInstance("PKCS12");
			keyStore.load(keystoreRepository.getKeyStore(), password);

			return SignatureFactory.getInstance().createSignatureVerification(keyStore);
		} catch (final KeyStoreException | CertificateException | IOException | NoSuchAlgorithmException e) {
			throw new IllegalArgumentException(e);
		} finally {
			Arrays.fill(password, '0');
		}
	}
}
