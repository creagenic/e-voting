/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.tools.xmlsignature;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.security.SignatureException;
import java.util.function.BiConsumer;
import java.util.function.Function;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureGeneration;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureVerification;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;

public abstract class XmlSigner<T> {
	private final BiConsumer<T, byte[]> signatureSetter;
	private final Function<T, byte[]> signatureGetter;
	private final Function<T, Hashable> hashGetter;
	private final Function<T, Hashable> additionalContextDataHashGetter;
	private final Class<T> clazz;
	private final Alias verifyAlias;
	private final URL schema;
	private final SignatureGeneration signatureGeneration;
	private final SignatureVerification signatureVerification;

	protected XmlSigner(final SignatureGeneration signatureGeneration,
			final SignatureVerification signatureVerification,
			final BiConsumer<T, byte[]> signatureSetter,
			final Function<T, byte[]> signatureGetter,
			final Function<T, Hashable> hashGetter,
			final Function<T, Hashable> additionalContextDataHashGetter,
			final Alias verifyAlias,
			final Class<T> clazz,
			final URL schema) {
		this.signatureGeneration = checkNotNull(signatureGeneration);
		this.signatureVerification = checkNotNull(signatureVerification);
		this.signatureSetter = checkNotNull(signatureSetter);
		this.signatureGetter = checkNotNull(signatureGetter);
		this.hashGetter = checkNotNull(hashGetter);
		this.additionalContextDataHashGetter = checkNotNull(additionalContextDataHashGetter);
		this.clazz = checkNotNull(clazz);
		this.verifyAlias = checkNotNull(verifyAlias);
		this.schema = schema;
	}

	public void sign(final Path path) throws SignatureException {
		final Path realPath = validatePath(path);

		final T document = load(realPath);

		final Hashable hash = hashGetter.apply(document);
		final Hashable additionalHash = additionalContextDataHashGetter.apply(document);

		final byte[] signature = signatureGeneration.genSignature(hash, additionalHash);

		signatureSetter.accept(document, signature);

		save(realPath, document);
	}

	public boolean verify(final Path path) throws SignatureException {
		final Path realPath = validatePath(path);

		final T document = load(realPath);

		final Hashable hash = hashGetter.apply(document);
		final Hashable additionalHash = additionalContextDataHashGetter.apply(document);
		final byte[] signature = signatureGetter.apply(document);

		return signatureVerification.verifySignature(verifyAlias.get(), hash, additionalHash, signature);
	}

	private Path validatePath(final Path path) {
		checkNotNull(path);

		final Path realPath;
		try {
			realPath = path.toRealPath(LinkOption.NOFOLLOW_LINKS);
		} catch (final IOException e) {
			throw new IllegalStateException(String.format("The xml file does not exist or an I/O error occurred. [path: %s]", path));
		}

		final String fileName = realPath.getFileName().toString();
		checkArgument(fileName.endsWith(".xml"), "The file is not an xml. [path: %s]", realPath);

		return realPath;
	}

	private T load(final Path path) {
		try (final InputStream fis = Files.newInputStream(path)) {
			final JAXBContext jaxbContext = JAXBContext.newInstance(this.clazz);
			final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			if (this.schema != null) {
				unmarshaller.setSchema(loadSchema(this.schema));
			}

			final SAXParserFactory spf = SAXParserFactory.newInstance();
			spf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			spf.setFeature("http://xml.org/sax/features/external-general-entities", false);
			spf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
			spf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			spf.setNamespaceAware(true);
			final Source xmlSource = new SAXSource(spf.newSAXParser().getXMLReader(), new InputSource(fis));

			return unmarshaller.unmarshal(xmlSource, this.clazz).getValue();
		} catch (final FileNotFoundException | JAXBException | ParserConfigurationException | SAXException e) {
			throw new IllegalStateException(String.format("Unable to load the file. [filePath: %s]", path), e);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	private Schema loadSchema(final URL schemaUrl) {
		final SchemaFactory schemaFactory = SchemaFactory.newDefaultInstance();
		try {
			schemaFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			schemaFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "file,nested");

			return schemaFactory.newSchema(schemaUrl);
		} catch (final SAXException e) {
			throw new IllegalStateException(String.format("Could not create new schema. [schemaUrl: %s]", schemaUrl), e);
		}

	}

	private void save(final Path path, final T document) {
		try {
			final JAXBContext jaxbContext = JAXBContext.newInstance(this.clazz);
			jaxbContext.createMarshaller().marshal(document, path.toFile());
		} catch (final JAXBException e) {
			throw new IllegalStateException(String.format("Unable to save the file. [filePath: %s]", path), e);
		}
	}

}
