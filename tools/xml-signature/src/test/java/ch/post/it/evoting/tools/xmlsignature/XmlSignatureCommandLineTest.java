/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.tools.xmlsignature;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SignatureException;
import java.util.Objects;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.LoggerFactory;

import ch.post.it.evoting.cryptoprimitives.signing.SignatureGeneration;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureVerification;
import ch.post.it.evoting.tools.xmlsignature.keystore.KeystoreRepository;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;

class XmlSignatureCommandLineTest {

	public static final String EVOTING_PRINT_XML_PATH = "/evoting-print.xml";
	public static final String CONFIGURATION_ANONYMIZED_XML_PATH = "/configuration-anonymized.xml";
	public static final String USAGE_ERROR_LOG_PREFIX = "Usage : java -Ddirect.trust.keystore.location=<direct-trust-keystoreFile> ";
	private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(XmlSignatureCommandLine.class);
	private static final String SUCCESS_LOG_PREFIX = "The action has finished successfully.";
	private static final String ERROR_LOG_PREFIX = "Unable to process the requested action.";
	private final SignatureGeneration signatureGeneration = mock(SignatureGeneration.class);

	private final SignatureVerification signatureVerification = mock(SignatureVerification.class);
	private final KeystoreRepository keystoreRepository = mock(KeystoreRepository.class);
	private final XmlSignatureCommandLine xmlSignatureCommandLine = spy(new XmlSignatureCommandLine(keystoreRepository));
	private ListAppender<ILoggingEvent> logAppender;

	@BeforeEach
	void setUp() {
		doReturn(signatureGeneration).when(xmlSignatureCommandLine).getSignatureGeneration(any(), any());
		doReturn(signatureVerification).when(xmlSignatureCommandLine).getSignatureVerification(any());

		logAppender = new ListAppender<>();
		logAppender.start();
		LOGGER.addAppender(logAppender);
	}

	@Test
	void runWithUnknownSignerLogsError() {
		xmlSignatureCommandLine.run("unknown", Action.VERIFY.name(), "path/to/file");

		assertTrue(logAppender.list.stream()
				.map(ILoggingEvent::getMessage)
				.anyMatch(message -> message.startsWith(USAGE_ERROR_LOG_PREFIX)));
	}

	@Test
	void runWithUnknownActionLogsError() {
		xmlSignatureCommandLine.run(SupportedFileType.CONFIG.name(), "unknown", "path/to/file");

		assertTrue(logAppender.list.stream()
				.map(ILoggingEvent::getMessage)
				.anyMatch(message -> message.startsWith(USAGE_ERROR_LOG_PREFIX)));
	}

	@Test
	void runWithTooManyArgumentsLogsError() {
		xmlSignatureCommandLine.run(SupportedFileType.CONFIG.name(), Action.VERIFY.name(), "path/to/file", "extra-argument");

		assertTrue(logAppender.list.stream()
				.map(ILoggingEvent::getMessage)
				.anyMatch(message -> message.startsWith(USAGE_ERROR_LOG_PREFIX)));
	}

	@Test
	void runWithTwoArgumentsLogsError() {
		xmlSignatureCommandLine.run(SupportedFileType.CONFIG.name(), Action.VERIFY.name());

		assertTrue(logAppender.list.stream()
				.map(ILoggingEvent::getMessage)
				.anyMatch(message -> message.startsWith(USAGE_ERROR_LOG_PREFIX)));
	}

	@Test
	void runWithOneArgumentLogsError() {
		xmlSignatureCommandLine.run(SupportedFileType.CONFIG.name());

		assertTrue(logAppender.list.stream()
				.map(ILoggingEvent::getMessage)
				.anyMatch(message -> message.startsWith(USAGE_ERROR_LOG_PREFIX)));
	}

	@Test
	void runWithNoArgumentsLogsError() {
		xmlSignatureCommandLine.run();

		assertTrue(logAppender.list.stream()
				.map(ILoggingEvent::getMessage)
				.anyMatch(message -> message.startsWith(USAGE_ERROR_LOG_PREFIX)));
	}

	@Test
	void signWithSignatureExceptionLogsError() throws SignatureException, URISyntaxException {
		when(signatureGeneration.genSignature(any(), any())).thenThrow(SignatureException.class);

		final Path filePath = Paths.get(Objects.requireNonNull(this.getClass().getResource(CONFIGURATION_ANONYMIZED_XML_PATH)).toURI());

		xmlSignatureCommandLine.run(SupportedFileType.CONFIG.name(), Action.SIGN.name(), filePath.toString());

		assertTrue(logAppender.list.stream()
				.map(ILoggingEvent::getMessage)
				.anyMatch(message -> message.startsWith(ERROR_LOG_PREFIX)));
	}

	private static Stream<Arguments> signArgumentProvider() {
		return Stream.of(
				Arguments.of(SupportedFileType.CONFIG, Action.SIGN, CONFIGURATION_ANONYMIZED_XML_PATH, SUCCESS_LOG_PREFIX),
				Arguments.of(SupportedFileType.CONFIG, Action.SIGN, EVOTING_PRINT_XML_PATH, ERROR_LOG_PREFIX),
				Arguments.of(SupportedFileType.PRINT, Action.SIGN, CONFIGURATION_ANONYMIZED_XML_PATH, ERROR_LOG_PREFIX),
				Arguments.of(SupportedFileType.PRINT, Action.SIGN, EVOTING_PRINT_XML_PATH, SUCCESS_LOG_PREFIX)
		);
	}

	@ParameterizedTest
	@MethodSource("signArgumentProvider")
	void signTest(final SupportedFileType signer, final Action action, final String xmlPath, final String expectedLog)
			throws SignatureException, URISyntaxException {
		when(signatureGeneration.genSignature(any(), any())).thenReturn("signature".getBytes(StandardCharsets.UTF_8));

		final Path filePath = Paths.get(Objects.requireNonNull(this.getClass().getResource(xmlPath)).toURI());

		xmlSignatureCommandLine.run(signer.name(), action.name(), filePath.toString());

		assertTrue(logAppender.list.stream()
				.map(ILoggingEvent::getMessage)
				.anyMatch(message -> message.startsWith(expectedLog)));
	}

	private static Stream<Arguments> verifyArgumentProvider() {
		final String failLog = "The action failed.";

		return Stream.of(
				Arguments.of(SupportedFileType.CONFIG, Action.VERIFY, CONFIGURATION_ANONYMIZED_XML_PATH, SUCCESS_LOG_PREFIX, true),
				Arguments.of(SupportedFileType.CONFIG, Action.VERIFY, CONFIGURATION_ANONYMIZED_XML_PATH, failLog, false),
				Arguments.of(SupportedFileType.CONFIG, Action.VERIFY, EVOTING_PRINT_XML_PATH, ERROR_LOG_PREFIX, true),
				Arguments.of(SupportedFileType.CONFIG, Action.VERIFY, EVOTING_PRINT_XML_PATH, ERROR_LOG_PREFIX, false),
				Arguments.of(SupportedFileType.PRINT, Action.VERIFY, CONFIGURATION_ANONYMIZED_XML_PATH, ERROR_LOG_PREFIX, true),
				Arguments.of(SupportedFileType.PRINT, Action.VERIFY, CONFIGURATION_ANONYMIZED_XML_PATH, ERROR_LOG_PREFIX, false),
				Arguments.of(SupportedFileType.PRINT, Action.VERIFY, EVOTING_PRINT_XML_PATH, SUCCESS_LOG_PREFIX, true),
				Arguments.of(SupportedFileType.PRINT, Action.VERIFY, EVOTING_PRINT_XML_PATH, failLog, false)
		);
	}

	@ParameterizedTest
	@MethodSource("verifyArgumentProvider")
	void verifyTest(final SupportedFileType signer, final Action action, final String xmlPath, final String expectedLog,
			final boolean signatureVerified)
			throws SignatureException, URISyntaxException, IOException {
		when(signatureVerification.verifySignature(any(), any(), any(), any())).thenReturn(signatureVerified);

		final Path filePath = Paths.get(Objects.requireNonNull(this.getClass().getResource(xmlPath)).toURI());

		xmlSignatureCommandLine.run(signer.name(), action.name(), filePath.toString());

		assertTrue(logAppender.list.stream()
				.map(ILoggingEvent::getMessage)
				.anyMatch(message -> message.startsWith(expectedLog)));
	}
}