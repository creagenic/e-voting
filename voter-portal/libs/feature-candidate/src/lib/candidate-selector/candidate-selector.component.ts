/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {
	Component,
	Input,
	OnChanges, OnDestroy,
	OnInit,
	SimpleChanges,
} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {
	Candidate,
	Contest,
	ContestUserData,
	ElectionContest,
} from '@vp/util-types';
import { Store } from '@ngrx/store';
import { getContests, getContestsAndContestsUserData } from '@vp/ui-state';
import {filter, retry, takeUntil} from 'rxjs/operators';
import {Subject} from "rxjs";

@Component({
	selector: 'vp-candidate-selector',
	templateUrl: './candidate-selector.component.html',
})
export class CandidateSelectorComponent implements OnInit, OnChanges, OnDestroy {
	@Input() electionContest!: ElectionContest;
	@Input() contestUserData!: ContestUserData;
	@Input() candidateIndex!: number;
	@Input() candidate!: Candidate;
	candidateCumul!: number;
	isAliasSelected!: boolean;
	primaryElectionContest?: Contest;
	selectedInPrimary = false;
	implicitlyEligible = false;
	private destroy$ = new Subject<void>;

	constructor(
		private readonly store: Store,
		private readonly modal: NgbActiveModal
	) {}

	get candidatesUserData() {
		return this.contestUserData.candidates ?? [];
	}

	get candidateMaxAllowedCumul() {
		return this.candidate.allRepresentations?.length || 1;
	}

	get isCumulAllowed() {
		return this.candidateMaxAllowedCumul > 1;
	}

	get isCandidateSelected() {
		return this.candidateCumul > 0;
	}

	get isWriteIn(): boolean {
		return this.candidate.isWriteIn;
	}

	get isSecondaryElection(): boolean {
		return (
			this.electionContest.candidatesQuestion?.primarySecondaryType ===
			'SECONDARY'
		);
	}

	get isSelectedInPrimaryElection(): boolean {
		return this.selectedInPrimary;
	}

	get isImplicitlyEligible(): boolean {
		return this.implicitlyEligible;
	}

	get isSelectedOnCurrentPosition() {
		return (
			this.candidate.id ===
			this.candidatesUserData[this.candidateIndex]?.candidateId
		);
	}

	get isCumulMessageShown() {
		return (
			!this.candidate.isBlank && this.isCumulAllowed && this.candidateCumul > 0
		);
	}

	get isEligibilityMessageShown(): boolean {
		return (
			this.isSecondaryElection &&
			!this.isSelectedInPrimaryElection &&
			!this.isImplicitlyEligible &&
			!this.isWriteIn
		);
	}

	get isSelectable(): boolean {
		return (
			!this.isSelectedOnCurrentPosition &&
			!this.isAliasSelected &&
			(!this.isCandidateSelected ||
				(this.isCumulAllowed &&
					this.candidateCumul < this.candidateMaxAllowedCumul)) &&
			(!this.isSecondaryElection ||
				this.isSelectedInPrimaryElection ||
				this.isImplicitlyEligible ||
				this.isWriteIn)
		);
	}

	private get selectedAlias(): boolean {
		const candidateAliases =
			this.electionContest.candidatesQuestion?.fusions.reduce(
				(aliases, fusion) => {
					return this.candidate.alias && fusion.includes(this.candidate.alias)
						? [...aliases, ...fusion]
						: aliases;
				},
				[]
			);

		return this.candidatesUserData.some(({ candidateId }) => {
			const candidate = this.electionContest.getCandidate(candidateId);
			const hasAlias = !!candidate?.alias;
			const isSameCandidate = candidate && candidate.id === this.candidate.id;
			return (
				hasAlias &&
				!isSameCandidate &&
				candidateAliases?.includes(candidate.alias as string)
			);
		});
	}

	ngOnInit() {
		this.isAliasSelected = this.selectedAlias;
		this.candidateCumul = this.candidatesUserData.filter(({ candidateId }) => {
			return candidateId === this.candidate.id;
		}).length;

		if (
			this.electionContest.candidatesQuestion?.primarySecondaryType !==
			'SECONDARY'
		) {
			return;
		}

		this.store.select(getContests).pipe(takeUntil(this.destroy$)).subscribe((contests) => {
			this.primaryElectionContest = contests?.find((contest) => {
				return (
					contest.candidatesQuestion?.primarySecondaryType === 'PRIMARY' &&
					contest.candidatesQuestion?.electionGroupIdentification ===
						this.electionContest.candidatesQuestion?.electionGroupIdentification
				);
			});
		});

		let primaryElectionCandidateIndex = -1;
		this.store
			.select(getContestsAndContestsUserData)
			.pipe(
				filter(
					() =>
						this.electionContest.candidatesQuestion?.primarySecondaryType ===
						'SECONDARY'
				),
				takeUntil(this.destroy$)
			)
			.subscribe(({ contests, contestsUserData }) => {
				if (!contests || !contestsUserData) return;

				// 1. find corresponding primary election (same electionGroupIdentification
				if (!this.primaryElectionContest) return;

				const primaryElectionIndex = contests.findIndex((contest) => {
					return contest.id === this.primaryElectionContest?.id;
				});

				if (primaryElectionIndex < 0) return;

				// 2. Get the candidates of the primary election selected by the user
				const primaryElectionUserSelection =
					contestsUserData[primaryElectionIndex].candidates;

				if (!primaryElectionUserSelection) return;

				// 3. Find the candidate in the primary election
				const candidateInPrimaryElection = this.primaryElectionContest.lists
					?.flatMap((candidateList) => {
						return candidateList.candidates;
					})
					.find((candidate) => {
						if (!candidate.alias || !this.candidate?.alias) return false;

						return (
							candidate.alias.split('|')[1] ===
							this.candidate?.alias.split('|')[1]
						);
					});

				if (!candidateInPrimaryElection) return;

				// 4. Find the index of the candidate selection
				primaryElectionCandidateIndex = primaryElectionUserSelection.findIndex(
					(candidateUserData) => {
						return (
							candidateUserData.candidateId === candidateInPrimaryElection.id ||
							(candidateUserData.writeIn &&
								candidateInPrimaryElection.isWriteIn)
						);
					}
				);
			});

		this.selectedInPrimary = primaryElectionCandidateIndex >= 0;
	}

	ngOnChanges(changes: SimpleChanges) {
		if (!changes['electionContest'] && !changes['candidate']) return;

		this.implicitlyEligible =
			!!this.electionContest.candidatesQuestion?.implicitlyEligibleCandidateIdentifications.find(
				(candidateAttribute) => {
					return candidateAttribute === this.candidate.attribute;
				}
			) ||
			!!this.electionContest.candidatesQuestion?.implicitlyDiscreetCandidateIdentifications.find(
				(candidateAttribute) => {
					return candidateAttribute === this.candidate.attribute;
				}
			);
	}

	ngOnDestroy() {
		this.destroy$.next();
		this.destroy$.complete();
	}

	selectCandidate() {
		this.modal.close(this.candidate);
	}
}
