/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {
	Component,
	ElementRef,
	Input,
	OnDestroy,
	OnInit,
	ViewChild,
} from '@angular/core';
import {
	AbstractControl,
	FormControl,
	ValidationErrors,
	ValidatorFn,
	Validators,
} from '@angular/forms';
import {Store} from '@ngrx/store';
import {FAQService} from '@vp/feature-faq';
import {
	getContestsAndContestsUserData,
	getDefinedWriteInAlphabet,
} from '@vp/ui-state';
import {Candidate, CandidateUserData, ElectionContest, FAQSection, Nullable} from '@vp/util-types';
import {Subject} from 'rxjs';
import {takeUntil} from "rxjs/operators";

@Component({
	selector: 'vp-candidate-write-in',
	templateUrl: './candidate-write-in.component.html',
})
export class CandidateWriteInComponent implements OnInit, OnDestroy {
	@ViewChild('writeIn', {read: ElementRef}) writeInInput:
		| ElementRef<HTMLInputElement>
		| undefined;
	@Input() writeInControl!: FormControl;
	@Input() electionContest!: ElectionContest;
	@Input() candidate?: Nullable<Candidate>;
	@Input() position!: number;
	@Input() showErrors = false;
	initialWriteIn = '';
	writeInMaxLength = 95;
	private writeInAlphabet: RegExp | undefined;
	private primaryCandidatePresent: boolean | undefined;
	private destroy$ = new Subject<void>;

	constructor(
		private readonly faqService: FAQService,
		private readonly store: Store
	) {
	}

	private get writeInValidator(): ValidatorFn {
		return (control: AbstractControl): ValidationErrors | null => {
			// Control value length is checked to prevent regex DoS attack
			const writeInFormat = /^\S+\s\S.*$/;
			if (
				!control.value ||
				control.value.length > this.writeInMaxLength ||
				!writeInFormat.test(control.value)
			) {
				return {incorrectFormat: true};
			}

			if (this.writeInAlphabet && !this.writeInAlphabet.test(control.value)) {
				return {incorrectCharacters: true};
			}

			if (this.electionContest.candidatesQuestion?.primarySecondaryType === "SECONDARY" &&
				!this.primaryCandidatePresent) {
				return {notSelectedInPrimary: true};
			}

			return null;
		};
	}

	ngOnInit() {
		this.store
			.pipe(getDefinedWriteInAlphabet, takeUntil(this.destroy$))
			.subscribe((alphabet) => {
				this.writeInAlphabet = new RegExp(`^[${alphabet.substring(1)}]+$`);
			});

		this.initialWriteIn = this.writeInControl.value;

		this.writeInControl.setValidators([
			this.writeInValidator,
			Validators.maxLength(this.writeInMaxLength),
		]);
		setTimeout(() => {
			this.writeInControl.updateValueAndValidity();
			this.writeInInput?.nativeElement.focus();
		});

		if (this.electionContest.candidatesQuestion?.primarySecondaryType !== "SECONDARY") {
			return;
		}

		const electionGroupIdentification = this.electionContest.candidatesQuestion?.electionGroupIdentification;

		this.store
			.select(getContestsAndContestsUserData)
			.pipe(takeUntil(this.destroy$))
			.subscribe(({contests, contestsUserData}) => {
				if (!contests || !contestsUserData) return;

				const primaryElectionContestIndex = contests.findIndex((contest) => {
					return (contest.candidatesQuestion?.primarySecondaryType === "PRIMARY" &&
						contest.candidatesQuestion.electionGroupIdentification === electionGroupIdentification);
				})

				if (primaryElectionContestIndex < 0) return;

				const contestUserData = contestsUserData[primaryElectionContestIndex]

				this.primaryCandidatePresent = !!contestUserData.candidates?.find((candidateUserData) => {
					return candidateUserData.writeIn === this.writeInControl.value
				})
			});
	}

	ngOnDestroy() {
		this.destroy$.next();
		this.destroy$.complete();

		this.writeInControl.clearValidators();
		setTimeout(() => this.writeInControl.updateValueAndValidity());
	}

	setWriteIn(writeIn: string) {
		this.writeInControl.setValue(this.normalizeSpaces(writeIn.trim()));
	}

	showWriteInsFAQ() {
		this.faqService.showFAQ(FAQSection.HowToUseWriteIns);
	}

	preventFormSubmission(): false {
		return false;
	}

	private normalizeSpaces(input: string) {
		const words = input.split(/\s+/);
		return words.join(' ');
	}
}
