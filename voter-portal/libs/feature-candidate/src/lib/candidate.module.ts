/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {
	NgbAccordionBody,
	NgbAccordionButton,
	NgbAccordionCollapse,
	NgbAccordionDirective,
	NgbAccordionHeader,
	NgbAccordionItem,
	NgbTooltipModule,
} from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { UiComponentsModule } from '@vp/ui-components';
import { CandidateDetailsComponent } from './candidate-details/candidate-details.component';
import { CandidateSelectionModalComponent } from './candidate-selection-modal/candidate-selection-modal.component';
import { CandidateSelectorComponent } from './candidate-selector/candidate-selector.component';
import { CandidateWriteInComponent } from './candidate-write-in/candidate-write-in.component';
import { CandidatesComponent } from './candidates/candidates.component';
import { UiDirectivesModule } from '@vp/ui-directives';

@NgModule({
	imports: [
		CommonModule,
		TranslateModule,
		RouterModule,
		FormsModule,
		NgbTooltipModule,
		UiComponentsModule,
		UiDirectivesModule,
		ReactiveFormsModule,
		NgbAccordionDirective,
		NgbAccordionItem,
		NgbAccordionHeader,
		NgbAccordionButton,
		NgbAccordionCollapse,
		NgbAccordionBody,
	],
	declarations: [
		CandidateSelectionModalComponent,
		CandidateSelectorComponent,
		CandidatesComponent,
		CandidateDetailsComponent,
		CandidateWriteInComponent,
	],
	exports: [CandidateSelectionModalComponent, CandidatesComponent],
})
export class CandidateModule {}
