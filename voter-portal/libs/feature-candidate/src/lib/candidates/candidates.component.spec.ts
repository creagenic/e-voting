/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {ElectionContest} from '@vp/util-types';
import {TranslateTestingModule} from 'ngx-translate-testing';

import {CandidatesComponent} from './candidates.component';

describe('CandidatesComponent', () => {
	let component: CandidatesComponent;
	let fixture: ComponentFixture<CandidatesComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [CandidatesComponent],
			imports: [TranslateTestingModule.withTranslations({})],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(CandidatesComponent);
		component = fixture.componentInstance;
		component.candidateSelection = [];
		component.electionContest = {} as ElectionContest;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
