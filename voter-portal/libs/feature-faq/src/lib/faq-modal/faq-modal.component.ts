/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {TranslateService} from '@ngx-translate/core';
import {ConfigurationService} from '@vp/ui-services';
import {AdditionalFAQs, ExtendedFactor, FAQSection, FAQSectionContent,} from '@vp/util-types';
import {Observable, startWith} from 'rxjs';
import {filter, map} from 'rxjs/operators';

@Component({
	selector: 'vp-faq',
	templateUrl: './faq-modal.component.html',
})
export class FAQModalComponent implements AfterViewInit, OnInit {
	additionalFAQs$!: Observable<FAQSectionContent[]>;
	@Input() activeFAQSection: FAQSection | undefined;

	readonly FAQSection = FAQSection;
	readonly ExtendedFactor = ExtendedFactor;
	readonly allowedCharacters =
		"ÀÁÂÃÄÅ àáâãäå ÈÉÊË èéêë ÌÍÎÏ ìíîï ÒÓÔÕÖ òóôõö ÙÚÛÜ ùúûü Ææ Çç Œœ Þþ Ññ Øø Šš Ýý Ÿÿ Žž ¢ () ð Ð ß ' , - . /";

	public constructor(
		public readonly configuration: ConfigurationService,
		public readonly activeModal: NgbActiveModal,
		private readonly translate: TranslateService
	) {}

	ngOnInit(): void {
		this.additionalFAQs$ = this.translate.onLangChange.pipe(
			startWith({ lang: this.translate.currentLang }),
			map(({ lang }) => {
				const { additionalFAQs } = this.configuration;
				return additionalFAQs && additionalFAQs[lang as keyof AdditionalFAQs];
			}),
			filter((faqs): faqs is FAQSectionContent[] => !!faqs)
		);
	}

	ngAfterViewInit() {
		if (this.activeFAQSection) {
			const activeSectionButton = document.querySelector<HTMLButtonElement>(
				`#${this.activeFAQSection}-header > .accordion-button`
			);
			activeSectionButton?.focus();
		}
	}
}
