/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {NgbAccordionModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from '@ngx-translate/core';
import {UiPipesModule} from '@vp/ui-pipes';
import {FAQModalComponent} from './faq-modal/faq-modal.component';
import {UiDirectivesModule} from '@vp/ui-directives';

@NgModule({
	imports: [
		CommonModule,
		TranslateModule,
		NgbAccordionModule,
		UiPipesModule,
		UiDirectivesModule,
	],
	declarations: [FAQModalComponent],
	exports: [FAQModalComponent],
})
export class FeatureFaqModule {}
