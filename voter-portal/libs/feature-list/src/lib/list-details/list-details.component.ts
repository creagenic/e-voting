/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {CandidateList, CandidateListDetails, Nullable} from '@vp/util-types';

@Component({
	selector: 'vp-list-details',
	templateUrl: './list-details.component.html',
})
export class ListDetailsComponent implements OnChanges {
	@Input() list: Nullable<CandidateList>;
	@Input() listPlaceholder: Nullable<string>;
	@Input() headingLevel: Nullable<number>;
	@Input() headingDisplayLevel: Nullable<number>;
	@Input() screenReaderLabel: Nullable<string>;
	@Input() showAllDetails: Nullable<boolean>;
	listDetails: string[] = [];

	get headingClass(): string {
		const headingLevel = this.headingDisplayLevel ?? this.headingLevel;
		return headingLevel ? `h${headingLevel}` : '';
	}

	ngOnChanges({ list }: SimpleChanges) {
		const newList = list?.currentValue;
		this.listDetails = newList ? this.getListDetails(newList) : [];
	}

	private getListDetails(list: CandidateList): string[] {
		const listDetailKeys: (keyof CandidateListDetails)[] = [
			'listType_apparentment',
			'listType_sousApparentment',
			'listType_attribute2',
			'listType_attribute3',
		];

		return listDetailKeys.reduce((listDetails, detailKey) => {
			const detail = list.details ? list.details[detailKey] : undefined;
			return detail ? [...listDetails, detail] : listDetails;
		}, [] as string[]);
	}
}
