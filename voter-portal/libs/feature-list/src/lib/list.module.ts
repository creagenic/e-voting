/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from '@ngx-translate/core';
import {UiComponentsModule} from '@vp/ui-components';
import {ListDetailsComponent} from './list-details/list-details.component';
import {ListSelectionModalComponent} from './list-selection-modal/list-selection-modal.component';
import {ListSelectorComponent} from './list-selector/list-selector.component';
import {ListComponent} from './list/list.component';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule,
		NgbCollapseModule,
		TranslateModule,
		UiComponentsModule,
	],
	declarations: [
		ListSelectionModalComponent,
		ListSelectorComponent,
		ListComponent,
		ListDetailsComponent,
	],
	exports: [ListSelectionModalComponent, ListComponent],
})
export class ListModule {}
