/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {
	Component,
	Input,
	OnChanges,
	SimpleChanges,
	TemplateRef,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {
	CandidateList,
	Contest,
	ContestUserData,
	ElectionContest,
} from '@vp/util-types';

interface IListTemplateContext {
	list: CandidateList | null;
}

@Component({
	selector: 'vp-list',
	templateUrl: './list.component.html',
})
export class ListComponent implements OnChanges {
	@Input() electionContest!: ElectionContest;
	@Input() contestUserData: ContestUserData | undefined;
	@Input() rightColumnTemplate: TemplateRef<IListTemplateContext> | undefined;
	list!: CandidateList | null;

	constructor(private readonly translate: TranslateService) {}

	@Input() set contest(value: Contest | undefined) {
		if (value) {
			this.electionContest = new ElectionContest(value);
		}
	}

	get listPlaceholder(): string {
		if (!this.contestUserData) {
			return this.translate.instant('listandcandidates.list');
		}

		return (
			this.electionContest.blankList?.details?.text ||
			this.translate.instant('listandcandidates.nolistchosen')
		);
	}

	get screenReaderLabel(): string | null {
		return this.contestUserData
			? this.translate.instant('listandcandidates.selectedlist')
			: null;
	}

	ngOnChanges({ contestUserData }: SimpleChanges) {
		const newContestUserData: ContestUserData = contestUserData?.currentValue;
		if (newContestUserData) {
			this.list = this.electionContest.getList(newContestUserData.listId);
		}
	}

	getTemplateContext(): IListTemplateContext {
		return { list: this.list };
	}
}
