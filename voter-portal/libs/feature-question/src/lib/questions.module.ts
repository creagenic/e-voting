/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {UiComponentsModule} from '@vp/ui-components';
import {UiPipesModule} from '@vp/ui-pipes';
import {QuestionsComponent} from './questions/questions.component';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		TranslateModule,
		UiComponentsModule,
		UiPipesModule,
	],
	declarations: [QuestionsComponent],
	exports: [QuestionsComponent],
})
export class QuestionsModule {}
