/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormGroup} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {CandidateUserData, Contest, TemplateType} from '@vp/util-types';
import {Observable, switchMap} from 'rxjs';
import {startWith} from 'rxjs/operators';

@Component({
	selector: 'vp-choose-contest-container',
	templateUrl: './choose-contest-container.component.html',
})
export class ChooseContestContainerComponent implements OnInit {
	@Input() contest!: Contest;
	@Input() contestFormGroup!: FormGroup;
	@Input() formSubmitted: boolean | undefined;
	readonly TemplateType = TemplateType;
	accordionSubtitle$: Observable<string> | undefined;

	constructor(private readonly translate: TranslateService) {}

	get candidates(): FormArray {
		return this.contestFormGroup.get('candidates') as FormArray;
	}

	ngOnInit() {
		if (this.contest.template === TemplateType.ListsAndCandidates) {
			this.accordionSubtitle$ = this.candidates.valueChanges.pipe(
				startWith(this.candidates.value as CandidateUserData[]),
				switchMap((candidatesUserData: CandidateUserData[]) => {
					const selectedCandidates = candidatesUserData.filter(
						({ candidateId }) => !!candidateId
					);

					return this.translate.stream('choose.candidates.subtitle', {
						nbChoices: selectedCandidates.length,
						maxChoices: this.contest?.candidatesQuestion?.maxChoices,
					});
				})
			);
		}
	}
}
