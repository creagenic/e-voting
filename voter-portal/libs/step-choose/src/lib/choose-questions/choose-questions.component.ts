/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import { Component, Input } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { Contest } from '@vp/util-types';

@Component({
	selector: 'vp-choose-questions',
	templateUrl: './choose-questions.component.html',
})
export class ChooseQuestionsComponent {
	@Input() contest: Contest | undefined;
	@Input() contestFormGroup: FormGroup | undefined;

	getQuestionFormGroup(questionIndex: number): FormGroup | undefined {
		const contestQuestions = this.contestFormGroup?.get(
			'questions'
		) as FormArray;
		return contestQuestions?.controls[questionIndex] as FormGroup;
	}

	getChosenOption(questionIndex: number): string | undefined {
		return this.getQuestionFormGroup(questionIndex)?.get('chosenOption')?.value;
	}

	resetRadioButtons(questionIndex: number): void {
		const question = document.getElementById(
			'option-' + this.getChosenOption(questionIndex)
		) as HTMLElement;
		question.focus();
		this.getQuestionFormGroup(questionIndex)?.get('chosenOption')?.reset();
	}
}
