/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ChooseComponent} from './choose/choose.component';
import {BackAction, RouteData, VotingStep} from '@vp/util-types';
import {confirmDeactivationIfNeeded, isUserAuthenticated,} from '@vp/ui-guards';

const routes: Routes = [
	{
		path: VotingStep.Choose,
		data: {
			reachableSteps: [VotingStep.Review],
			backAction: BackAction.ShowCancelVoteDialog,
		} as RouteData,
		canMatch: [isUserAuthenticated],
		canDeactivate: [confirmDeactivationIfNeeded],
		component: ChooseComponent,
	},
];

@NgModule({
	declarations: [],
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class ChooseRoutingModule {}
