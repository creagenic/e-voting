/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {Store} from '@ngrx/store';
import {ProcessCancellationService} from '@vp/ui-services';
import {ChooseActions, getContests, getContestsAndContestsUserData,} from '@vp/ui-state';
import {CandidatesQuestion, Contest, Question, TemplateType,} from '@vp/util-types';
import {debounceTime, take} from 'rxjs/operators';
import {Subscription, switchMap} from 'rxjs';

@Component({
	selector: 'vp-choose',
	templateUrl: './choose.component.html',
})
export class ChooseComponent implements OnInit, OnDestroy {
	ballotFormGroup!: FormGroup;
	contests$ = this.store.select(getContests);
	private subscription!: Subscription;

	constructor(
		private readonly store: Store,
		private readonly cancelProcessService: ProcessCancellationService,
		private readonly fb: FormBuilder
	) {}

	ngOnInit() {
		this.subscription = this.store
			.select(getContestsAndContestsUserData)
			.pipe(
				take(1),
				switchMap(({ contests, contestsUserData }) => {
					this.ballotFormGroup = this.fb.group({
						contests: this.getContestsFormArray(contests),
					});

					if (contestsUserData) {
						this.ballotFormGroup.patchValue({ contests: contestsUserData });
					}

					this.store.dispatch(
						ChooseActions.userDataChanges({
							ballotUserData: this.ballotFormGroup.value,
						})
					);

					return this.ballotFormGroup.valueChanges;
				}),
				debounceTime(400)
			)
			.subscribe((ballotUserData) => {
				this.store.dispatch(
					ChooseActions.userDataChanges({
						ballotUserData,
					})
				);
			});
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}

	confirmCancel() {
		this.cancelProcessService.cancelVote();
	}

	review() {
		if (this.ballotFormGroup.invalid) {
			const firstInvalid =
				document.querySelector<HTMLInputElement>('input.ng-invalid');
			if (firstInvalid) {
				firstInvalid.focus();
			}
			return;
		}

		this.store.dispatch(ChooseActions.reviewClicked());
	}

	getContestFormGroup(index: number): FormGroup {
		const contestFromArray = this.ballotFormGroup.get('contests') as FormArray;
		return contestFromArray.controls[index] as FormGroup;
	}

	private getContestsFormArray(contests: Contest[] | undefined): FormArray {
		const contestsFormGroups = contests?.map((contest) => {
			if (contest.template === TemplateType.Options) {
				return this.fb.group({
					questions: this.getOptionsFormArray(contest.questions),
				});
			}

			const listAndCandidatesFormGroup: FormGroup = this.fb.group({
				candidates: this.getCandidatesFormArray(contest.candidatesQuestion),
			});

			if (contest.listQuestion && contest.listQuestion.maxChoices > 0) {
				listAndCandidatesFormGroup.setControl('listId', this.fb.control(null));
			}

			return listAndCandidatesFormGroup;
		});

		return this.fb.array(contestsFormGroups || []);
	}

	private getCandidatesFormArray(
		candidateQuestions: CandidatesQuestion | undefined
	): FormArray {
		const candidateFormGroups = Array.from(
			{ length: candidateQuestions?.maxChoices || 0 },
			() => {
				return this.fb.group({
					candidateId: this.fb.control(null),
					writeIn: this.fb.control(null),
				});
			}
		);

		return this.fb.array(candidateFormGroups);
	}

	private getOptionsFormArray(questions: Question[] | undefined): FormArray {
		const questionFormGroups = questions?.map((question) => {
			return this.fb.group({
				id: this.fb.control(question.id),
				chosenOption: this.fb.control(null),
			});
		});

		return this.fb.array(questionFormGroups ?? []);
	}
}
