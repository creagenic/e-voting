/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {provideMockStore} from '@ngrx/store/testing';
import {ConfigurationService} from '@vp/ui-services';
import {ExtendedFactor, VoterPortalConfig} from '@vp/util-types';
import {MockProvider} from 'ng-mocks';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {ReactiveFormsModule} from '@angular/forms';

import {LegalTermsComponent} from './legal-terms.component';

describe('LegalTermsComponent', () => {
	let component: LegalTermsComponent;
	let fixture: ComponentFixture<LegalTermsComponent>;
	let voterPortalConfig: VoterPortalConfig;

	beforeEach(async () => {
		voterPortalConfig = {
			identification: ExtendedFactor.YearOfBirth,
			contestsCapabilities: {
				writeIns: true,
			},
			requestTimeout: {
				authenticateVoter: 30000,
				sendVote: 120000,
				confirmVote: 120000
			},
			header: {
				logoPath: {desktop: '', mobile: ''},
				logoHeight: {desktop: 0, mobile: 0},
			},
		};

		await TestBed.configureTestingModule({
			imports: [
				ReactiveFormsModule,
				RouterTestingModule,
				HttpClientTestingModule,
				TranslateTestingModule.withTranslations({}),
			],
			declarations: [LegalTermsComponent],
			providers: [
				provideMockStore({}),
				MockProvider(ConfigurationService, voterPortalConfig),
			],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(LegalTermsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
