/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {ContestAndContestUserData} from '@vp/util-types';

@Component({
	selector: 'vp-review-candidate-list',
	templateUrl: './review-candidate-list.component.html',
})
export class ReviewCandidateListComponent {
	@Input() contestAndValues: ContestAndContestUserData | undefined;

	get showList() {
		return (
			this.contestAndValues?.contest?.listQuestion &&
			this.contestAndValues.contest.listQuestion.maxChoices > 0
		);
	}
}
