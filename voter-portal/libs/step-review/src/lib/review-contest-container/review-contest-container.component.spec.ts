/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {DebugElement} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {provideMockStore} from '@ngrx/store/testing';
import {UiComponentsModule} from '@vp/ui-components';
import {MockContest, MockContestUserData, MockQuestions,} from '@vp/util-testing';
import {ContestAndContestUserData, ContestUserData, Question, TemplateType,} from '@vp/util-types';
import {MockComponent, MockModule} from 'ng-mocks';
import {ReviewCandidateListComponent} from '../review-candidate-list/review-candidate-list.component';
import {ReviewQuestionsComponent} from '../review-questions/review-questions.component';

import {ReviewContestContainerComponent} from './review-contest-container.component';

describe('ContestReviewContainerComponent', () => {
	let component: ReviewContestContainerComponent;
	let fixture: ComponentFixture<ReviewContestContainerComponent>;
	let accordionComponent: DebugElement;
	let questionListComponent: DebugElement;
	let questions: Question[];
	let contestAndValues: ContestAndContestUserData;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [
				ReviewContestContainerComponent,
				MockComponent(ReviewQuestionsComponent),
				MockComponent(ReviewCandidateListComponent),
			],
			providers: [provideMockStore({})],
			imports: [MockModule(UiComponentsModule)],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(ReviewContestContainerComponent);
		component = fixture.componentInstance;

		questions = MockQuestions([
			'Choose Container Question A',
			'Choose Container Question B',
		]);
		contestAndValues = component.contestAndValues = {
			contest: MockContest(questions),
			contestUserData: MockContestUserData(questions),
		};

		fixture.detectChanges();

		accordionComponent = fixture.debugElement.query(
			By.css('vp-contest-accordion')
		);
		questionListComponent = fixture.debugElement.query(
			By.css('vp-review-questions')
		);
	});

	function setContestUserData(newContestUserData: ContestUserData) {
		component.contestAndValues = {
			...contestAndValues,
			contestUserData: newContestUserData,
		};
		fixture.detectChanges();
	}

	it('should pass the provided contest title as the title of the accordion component', () => {
		expect(accordionComponent.componentInstance.title).toContain(
			component.contestAndValues?.contest?.title
		);
	});

	it('should pass the provided contest and contest user data to the question list component', () => {
		expect(questionListComponent.componentInstance.contestAndValues).toBe(
			component.contestAndValues
		);
	});

	it('should not pass a card class to the accordion component if not all questions are blank', () => {
		setContestUserData(
			MockContestUserData(questions, { blankAnswerOnly: false })
		);

		accordionComponent = fixture.debugElement.query(
			By.css('vp-contest-accordion')
		);
		expect(accordionComponent.componentInstance.cardClass).toBeFalsy();
	});

	it('should not show the question list component if the provided contest has candidate choices', () => {
		contestAndValues.contest.template = TemplateType.ListsAndCandidates;

		fixture.detectChanges();

		questionListComponent = fixture.debugElement.query(
			By.css('vp-choose-questions')
		);
		expect(questionListComponent).toBeFalsy();
	});
});
