/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Store} from '@ngrx/store';
import {ConfirmationService} from '@vp/ui-confirmation';
import {ProcessCancellationService} from '@vp/ui-services';
import {getContestsAndContestsUserData, getLoading, ReviewActions,} from '@vp/ui-state';
import {ConfirmationModalConfig, ContestAndContestUserData,} from '@vp/util-types';
import {Observable, partition} from 'rxjs';
import {map} from 'rxjs/operators';
import {SendVoteModalComponent} from '../send-vote-modal/send-vote-modal.component';

@Component({
	selector: 'vp-review',
	templateUrl: './review.component.html',
})
export class ReviewComponent {
	contestsAndValues$: Observable<ContestAndContestUserData[] | undefined>;
	isLoading$: Observable<boolean> = this.store.select(getLoading);

	constructor(
		private readonly store: Store,
		private readonly modalService: NgbModal,
		private readonly cancelProcessService: ProcessCancellationService,
		private readonly confirmationService: ConfirmationService
	) {
		this.contestsAndValues$ = this.store
			.select(getContestsAndContestsUserData)
			.pipe(
				map(({ contests, contestsUserData }) =>
					contests?.map((contest, i) => {
						const contestUserData = contestsUserData
							? contestsUserData[i]
							: { questions: [] };
						return { contest, contestUserData };
					})
				)
			);
	}

	confirmCancel() {
		this.cancelProcessService.cancelVote();
	}

	confirmSeal(): void {
		const sealingModalConfig: ConfirmationModalConfig = {
			title: 'review.confirm.title',
			content: [
				'review.confirm.questionsealvote',
				'review.confirm.hintconfirm',
			],
			confirmLabel: 'review.confirm.yes',
			cancelLabel: 'review.confirm.no',
			modalOptions: { size: 'lg' },
		};

		const [sealingConfirmed$, sealingRejected$] = partition(
			this.confirmationService.confirm(sealingModalConfig),
			(wasSealingConfirmed) => wasSealingConfirmed
		);

		sealingConfirmed$.subscribe(() => this.seal());
		sealingRejected$.subscribe(() =>
			this.store.dispatch(ReviewActions.sealVoteCanceled())
		);
	}

	private seal() {
		this.store.dispatch(ReviewActions.sealVoteClicked());

		this.modalService.open(SendVoteModalComponent, {
			size: 'lg',
			backdrop: 'static',
			keyboard: false,
		});
	}
}
