/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {ContestVerifyData} from '@vp/util-types';

@Component({
	selector: 'vp-verify-candidate-list',
	templateUrl: './verify-candidate-list.component.html',
})
export class VerifyCandidateListComponent {
	@Input() contestVerifyData: ContestVerifyData | undefined;

	get hasList(): boolean {
		const listMaxChoices =
			this.contestVerifyData?.contest.listQuestion?.maxChoices ?? 0;
		return listMaxChoices > 0;
	}

	getCandidateChoiceCode(candidateIndex: number): string {
		const choiceCodeIndex = this.hasList ? candidateIndex + 1 : candidateIndex;
		return this.contestVerifyData?.choiceReturnCodes[choiceCodeIndex] ?? '';
	}

	getListChoiceCode(): string {
		return this.contestVerifyData?.choiceReturnCodes[0] ?? '';
	}
}
