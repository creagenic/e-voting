/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Component, Input} from '@angular/core';
import {ContestVerifyData, TemplateType} from '@vp/util-types';

@Component({
	selector: 'vp-verify-contest-container',
	templateUrl: './verify-contest-container.component.html',
})
export class VerifyContestContainerComponent {
	readonly TemplateType = TemplateType;

	@Input() contestVerifyData: ContestVerifyData | undefined;
}
