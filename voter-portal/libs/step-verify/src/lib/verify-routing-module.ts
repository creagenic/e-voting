/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {VerifyComponent} from './verify/verify.component';
import {BackAction, RouteData, VotingStep} from '@vp/util-types';
import {confirmDeactivationIfNeeded, isUserAuthenticated,} from '@vp/ui-guards';

const routes: Routes = [
	{
		path: VotingStep.Verify,
		data: {
			reachableSteps: [],
			backAction: BackAction.ShowLeaveProcessDialog,
		} as RouteData,
		canMatch: [isUserAuthenticated],
		canDeactivate: [confirmDeactivationIfNeeded],
		component: VerifyComponent,
	},
];

@NgModule({
	declarations: [],
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class VerifyRoutingModule {}
