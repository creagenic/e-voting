/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { CandidateModule } from '@vp/feature-candidate';
import { ListModule } from '@vp/feature-list';
import { QuestionsModule } from '@vp/feature-question';
import { UiComponentsModule } from '@vp/ui-components';
import { NgxMaskDirective, provideNgxMask } from 'ngx-mask';
import { VerifyCandidateListComponent } from './verify-candidate-list/verify-candidate-list.component';
import { VerifyContestContainerComponent } from './verify-contest-container/verify-contest-container.component';
import { VerifyQuestionsComponent } from './verify-questions/verify-questions.component';
import { VerifyRoutingModule } from './verify-routing-module';
import { VerifyComponent } from './verify/verify.component';
import { UiDirectivesModule } from '@vp/ui-directives';

@NgModule({
	imports: [
		// Warning, the order of the dependencies is important !
		// If the NgxMaskDirective is put 'at the wrong place', the input place older of textarea is not display when user input something.
		CandidateModule,
		CommonModule,
		ListModule,
		NgxMaskDirective,
		QuestionsModule,
		ReactiveFormsModule,
		TranslateModule,
		UiComponentsModule,
		UiDirectivesModule,
		VerifyRoutingModule,
	],
	declarations: [
		VerifyComponent,
		VerifyContestContainerComponent,
		VerifyQuestionsComponent,
		VerifyCandidateListComponent,
	],
	providers: [provideNgxMask({ validation: false })],
})
export class VerifyModule {}
