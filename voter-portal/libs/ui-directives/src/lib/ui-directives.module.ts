/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ConditionallyDescribedByDirective} from './conditionally-described-by.directive';
import {MaskDirective} from './mask.directive';
import {MultilineInputDirective} from './multiline-input.directive';
import {TranslationListDirective} from './translation-list.directive';

@NgModule({
	imports: [CommonModule],
	declarations: [
		TranslationListDirective,
		ConditionallyDescribedByDirective,
		MaskDirective,
		MultilineInputDirective,
	],
	exports: [
		TranslationListDirective,
		ConditionallyDescribedByDirective,
		MaskDirective,
		MultilineInputDirective,
	],
})
export class UiDirectivesModule {}
