/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import * as AngularCore from '@angular/core';
import { ProviderToken } from '@angular/core';
import { Router } from '@angular/router';
import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Store } from '@ngrx/store';
import { getConfig, getIsAuthenticated } from '@vp/ui-state';
import { take } from 'rxjs/operators';
import { RandomElectionEventId } from '@vp/util-testing';
import { MockProvider } from 'ng-mocks';
import { BackendConfig } from '@vp/util-types';
import { isUserAuthenticated } from './is-user-authenticated.guard';

describe('isUserAuthenticated', () => {
	let router: Router;
	let store: MockStore;

	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				provideMockStore({}),
				MockProvider(Router, {
					createUrlTree: jest
						.fn()
						.mockImplementation((commands) => commands.join('/')),
				}),
			],
		});

		router = TestBed.inject(Router);
		store = TestBed.inject(MockStore);
	});

	it('should be authenticated', (done) => {
		store.overrideSelector(getIsAuthenticated, true);

		TestBed.runInInjectionContext(() =>
			isUserAuthenticated()
				.pipe(take(1))
				.subscribe((authenticated) => {
					expect(authenticated).toBe(true);
					done();
				}),
		);
	});

	it('should not be authenticated', (done) => {
		const mockElectionEventId = RandomElectionEventId();

		store.overrideSelector(getIsAuthenticated, false);
		store.overrideSelector(getConfig, {
			electionEventId: mockElectionEventId,
		} as BackendConfig);

		TestBed.runInInjectionContext(() =>
			isUserAuthenticated()
				.pipe(take(1))
				.subscribe((urlTree) => {
					expect(urlTree).toBe(`legal-terms/${mockElectionEventId}`);
					done();
				}),
		);
	});

	it('should not be authenticated, no config', (done) => {
		store.overrideSelector(getIsAuthenticated, false);
		store.overrideSelector(getConfig, { electionEventId: '' } as BackendConfig);

		TestBed.runInInjectionContext(() =>
			isUserAuthenticated()
				.pipe(take(1))
				.subscribe((urlTree) => {
					expect(urlTree).toBe('');
					done();
				}),
		);
	});
});
