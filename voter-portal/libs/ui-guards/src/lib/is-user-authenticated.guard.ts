/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {inject} from '@angular/core';
import {Router, UrlTree} from '@angular/router';
import {Store} from '@ngrx/store';
import {getConfig, getIsAuthenticated} from '@vp/ui-state';
import {merge, Observable, partition} from 'rxjs';
import {map, mergeMap} from 'rxjs/operators';
import {VotingStep} from '@vp/util-types';

export const isUserAuthenticated = (): Observable<boolean | UrlTree> => {
	const store = inject(Store);
	const router = inject(Router);

	const [canLoad$, cannotLoad$] = partition(
		store.select(getIsAuthenticated),
		Boolean
	);

	const redirectToLandingPage$ = cannotLoad$.pipe(
		mergeMap(() => store.select(getConfig)),
		map((config) => {
			if (config.electionEventId) {
				return router.createUrlTree([
					VotingStep.LegalTerms,
					config.electionEventId,
				]);
			}
			return router.createUrlTree(['']);
		})
	);

	return merge(canLoad$, redirectToLandingPage$);
};
