/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {TestBed} from '@angular/core/testing';
import {DomSanitizer} from '@angular/platform-browser';
import {MarkdownPipe} from './markdown.pipe';

describe('MarkdownPipe', () => {
	let pipe: MarkdownPipe;

	beforeEach(() => {
		TestBed.configureTestingModule({
			declarations: [MarkdownPipe],
			providers: [
				MarkdownPipe,
				{
					provide: DomSanitizer,
					useValue: {
						sanitize: (val: string) => val,
					},
				},
			],
		});
		pipe = TestBed.inject(MarkdownPipe);
	});

	it('create an instance', () => {
		expect(pipe).toBeTruthy();
	});
});
