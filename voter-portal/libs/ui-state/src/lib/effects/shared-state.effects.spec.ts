/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {TestBed} from '@angular/core/testing';
import {provideMockActions} from '@ngrx/effects/testing';
import {Action} from '@ngrx/store';
import {provideMockStore} from '@ngrx/store/testing';
import {BackendService} from '@vp/data-access';
import {AuthenticateVoterResponse, BackendError, Ballot, ExtendedFactor, Voter, VoterPortalConfig, VotingCardState,} from '@vp/util-types';
import {cold, hot} from 'jasmine-marbles';
import {Observable} from 'rxjs';
import {StartVotingActions} from '../actions/shared-state.actions';

import {SharedStateEffects} from './shared-state.effects';
import {MockProvider} from "ng-mocks";
import {ConfigurationService} from "@vp/ui-services";

describe('SharedStateEffects', () => {
	let actions$: Observable<Action>;
	let effects: SharedStateEffects;
	let backendService: BackendService;
	let voterPortalConfig: VoterPortalConfig;

	beforeEach(() => {
		voterPortalConfig = {
			identification: ExtendedFactor.YearOfBirth,
			contestsCapabilities: {
				writeIns: true,
			},
			requestTimeout: {
				authenticateVoter: 30000,
				sendVote: 120000,
				confirmVote: 120000
			},
			header: {
				logoPath: {desktop: '', mobile: ''},
				logoHeight: {desktop: 0, mobile: 0},
			},
		};

		TestBed.configureTestingModule({
			providers: [
				SharedStateEffects,
				provideMockActions(() => actions$),
				provideMockStore(),
				BackendService,
				MockProvider(ConfigurationService, voterPortalConfig),
			],
		});

		effects = TestBed.inject(SharedStateEffects);
		backendService = TestBed.inject(BackendService);
	});

	describe('authenticateVoter$', () => {
		let authenticateVoter: jest.Mock;

		beforeEach(() => {
			backendService.authenticateVoter = authenticateVoter = jest.fn();

			// stream to simulate user clicking "Start" after entering authentication data
			actions$ = hot('-a', {
				a: StartVotingActions.startClicked({voter: {} as Voter}),
			});
		});

		it('should load the ballot when the voting card is in its initial state', () => {
			const mockBallot = {id: 'mockBallot'} as Ballot;
			const mockAuthenticateVoterResponse: AuthenticateVoterResponse = {
				votingCardState: VotingCardState.Initial,
				ballot: mockBallot,
			};

			authenticateVoter.mockReturnValueOnce(
				cold('--a|', {a: mockAuthenticateVoterResponse})
			);

			const expected = hot('---a', {
				a: StartVotingActions.ballotLoaded({ballot: mockBallot}),
			});

			expect(effects.authenticateVoter$).toBeObservable(expected);
		});

		it('should load the ballot and the choice return codes when the vote is sent', () => {
			const mockBallot = {id: 'mockBallot'} as Ballot;
			const mockChoiceReturnCodes = ['mockChoiceReturnCode'];
			const mockAuthenticateVoterResponse: AuthenticateVoterResponse = {
				votingCardState: VotingCardState.Sent,
				ballot: mockBallot,
				choiceReturnCodes: mockChoiceReturnCodes,
			};

			authenticateVoter.mockReturnValueOnce(
				cold('--a|', {a: mockAuthenticateVoterResponse})
			);

			const expected = hot('---a', {
				a: StartVotingActions.choiceReturnCodesLoaded({
					ballot: mockBallot,
					choiceReturnCodes: mockChoiceReturnCodes,
				}),
			});

			expect(effects.authenticateVoter$).toBeObservable(expected);
		});

		it('should load the vote cast return code when the vote is confirmed', () => {
			const mockVoteCastReturnCode = 'mockVoteCastReturnCode';
			const mockAuthenticateVoterResponse: AuthenticateVoterResponse = {
				votingCardState: VotingCardState.Confirmed,
				voteCastReturnCode: mockVoteCastReturnCode,
			};

			authenticateVoter.mockReturnValueOnce(
				cold('--a|', {a: mockAuthenticateVoterResponse})
			);

			const expected = hot('---a', {
				a: StartVotingActions.voteCastReturnCodeLoaded({
					voteCastReturnCode: mockVoteCastReturnCode,
				}),
			});

			expect(effects.authenticateVoter$).toBeObservable(expected);
		});

		it('should error', () => {
			const mockBackendError = new BackendError();

			authenticateVoter.mockReturnValueOnce(
				cold('--a|', {a: mockBackendError})
			);

			const expected = hot('---a', {
				a: StartVotingActions.authenticationFailed({
					error: mockBackendError,
				}),
			});

			expect(effects.authenticateVoter$).toBeObservable(expected);
		});
	});
});
