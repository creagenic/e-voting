/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Action, createReducer, on} from '@ngrx/store';
import {BackendConfig, BackendError, Ballot, BallotUserData,} from '@vp/util-types';
import {
	ChooseActions,
	LanguageSelectorActions,
	LegalTermsActions,
	ReviewActions,
	SharedActions,
	StartVotingActions,
	VerifyActions,
} from '../actions/shared-state.actions';

export const SHARED_FEATURE_KEY = 'sharedState';

export interface SharedState {
	isAuthenticated: boolean;
	config: BackendConfig;
	error?: BackendError | null; // last known error (if any)
	startVotingKey?: string;
	ballot?: Ballot | null;
	ballotUserData?: BallotUserData | null;
	choiceReturnCodes?: string[] | null;
	confirmationKey?: string | null;
	voteCastReturnCode?: string | null;
	loading: boolean;
	sentButNotCast: boolean; //User sent vote in last session.
	cast: boolean; //User casted vote in last session
}

export const initialState: SharedState = {
	// set initial required properties
	isAuthenticated: false,
	loading: false,
	error: null,
	config: {
		lib: '',
		lang: '',
		electionEventId: '',
	},
	sentButNotCast: false,
	cast: false,
};

const stateReducer = createReducer(
	initialState,
	on(LanguageSelectorActions.languageClicked, (state, { lang }) => ({
		...state,
		config: { ...state.config, lang },
	})),
	on(LanguageSelectorActions.ballotTranslated, (state, { ballot }) => ({
		...state,
		ballot,
	})),
	on(LanguageSelectorActions.ballotTranslationFailed, (state, { error }) => ({
		...state,
		error,
	})),
	on(LegalTermsActions.agreeClicked, (state, { config }) => ({
		...state,
		config,
	})),
	on(StartVotingActions.startClicked, (state) => ({
		...state,
		isAuthenticated: false,
		ballot: null,
		sentButNotCast: false,
		cast: false,
		error: null,
		loading: true,
	})),
	on(StartVotingActions.ballotLoaded, (state, { ballot }) => ({
		...state,
		ballot,
		isAuthenticated: true,
		ballotUserData: null,
		error: null,
		loading: false,
	})),
	on(
		StartVotingActions.choiceReturnCodesLoaded,
		(state, { choiceReturnCodes, ballot }) => ({
			...state,
			ballot,
			isAuthenticated: true,
			choiceReturnCodes,
			ballotUserData: null,
			error: null,
			sentButNotCast: true,
			loading: false,
		})
	),
	on(
		StartVotingActions.voteCastReturnCodeLoaded,
		(state, { voteCastReturnCode }) => ({
			...state,
			voteCastReturnCode,
			isAuthenticated: true,
			cast: true,
			error: null,
			loading: false,
		})
	),
	on(StartVotingActions.authenticationFailed, (state, { error }) => ({
		...state,
		error,
		loading: false,
	})),
	on(ChooseActions.userDataChanges, (state, { ballotUserData }) => ({
		...state,
		ballotUserData,
	})),
	on(ReviewActions.sealVoteClicked, (state) => ({
		...state,
		choiceReturnCodes: null,
		loading: true,
		error: null,
	})),
	on(ReviewActions.sealedVoteLoaded, (state, { choiceReturnCodes }) => ({
		...state,
		choiceReturnCodes,
		loading: false,
		error: null,
	})),
	on(ReviewActions.sealedVoteLoadFailed, (state, { error }) => ({
		...state,
		choiceReturnCodes: null,
		loading: false,
		error: error,
	})),
	on(VerifyActions.castVoteClicked, (state, { confirmationKey }) => ({
		...state,
		confirmationKey,
		loading: true,
		error: null,
	})),
	on(VerifyActions.castVoteLoaded, (state, { voteCastReturnCode }) => ({
		...state,
		voteCastReturnCode,
		loading: false,
		error: null,
	})),
	on(VerifyActions.castVoteLoadFailed, (state, { error }) => ({
		...state,
		error,
		loading: false,
	})),
	on(SharedActions.loggedOut, (state) => ({
		...initialState,
		config: state.config,
		error: state.error,
	})),
	on(SharedActions.serverErrorCleared, (state) => ({
		...state,
		loading: false,
		error: null,
	}))
);

export function reducer(state: SharedState | undefined, action: Action) {
	return stateReducer(state, action);
}
