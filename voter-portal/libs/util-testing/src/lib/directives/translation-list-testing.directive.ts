/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
	selector: '[vpTranslationList]',
})
export class TranslationListTestingDirective {
	constructor(
		private readonly templateRef: TemplateRef<{ key: string }>,
		private readonly viewContainer: ViewContainerRef
	) {}

	@Input() set vpTranslationList(key: string | undefined) {
		if (key) {
			this.viewContainer.createEmbeddedView(this.templateRef, { key });
		}
	}
}
