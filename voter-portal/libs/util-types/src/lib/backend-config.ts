/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
export interface BackendConfig {
	lib: string;
	lang: string;
	electionEventId: string;
}
