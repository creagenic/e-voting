/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';

export interface ConfirmationModalConfig {
	content: string | string[];
	title?: string;
	confirmLabel?: string;
	cancelLabel?: string;
	modalOptions?: NgbModalOptions;
}
