/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';

export const routes: Routes = [
	{
		path: 'page-not-found',
		component: PageNotFoundComponent,
	},
	{ path: '**', redirectTo: 'page-not-found' },
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes, {
			preloadingStrategy: PreloadAllModules,
			useHash: true,
			anchorScrolling: 'enabled',
			onSameUrlNavigation: 'reload',
		}),
	],
	exports: [RouterModule],
})
export class AppRoutingModule {}
