/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
import {LocationStrategy} from '@angular/common';
import {Component, HostListener, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {TranslateService} from '@ngx-translate/core';
import {ProcessCancellationService} from '@vp/ui-services';
import {getIsAuthenticated, SharedActions} from '@vp/ui-state';
import {Observable, partition} from 'rxjs';
import {filter, take} from 'rxjs/operators';

declare global {
	interface Window {
		Cypress?: object;
		store?: Store;
	}
}

@Component({
	selector: 'vp-root',
	templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
	constructor(
		private readonly store: Store,
		private readonly translate: TranslateService,
		private readonly cancellationService: ProcessCancellationService,
		private readonly location: LocationStrategy
	) {}

	get isCurrentlyAuthenticated$(): Observable<boolean> {
		return this.store.select(getIsAuthenticated).pipe(take(1));
	}

	ngOnInit() {
		if (window.Cypress) {
			window.store = this.store;
		}

		this.location.onPopState(() => {
			this.cancellationService.backButtonPressed = true;
			return false;
		});

		this.store.dispatch(SharedActions.serverErrorCleared());

		this.isCurrentlyAuthenticated$.pipe(filter(Boolean)).subscribe(() => {
			this.store.dispatch(SharedActions.loggedOut());
		});
	}

	@HostListener('window:beforeunload', ['$event']) beforeUnload(
		$event: BeforeUnloadEvent
	) {
		const [isAuthenticated$, isNotAuthenticated$] = partition(
			this.isCurrentlyAuthenticated$,
			Boolean
		);

		isAuthenticated$.subscribe(() => {
			$event.returnValue = this.translate.instant(
				'common.questionnavigateaway'
			);
		});

		isNotAuthenticated$.subscribe(() => {
			$event.preventDefault();
		});
	}
}
