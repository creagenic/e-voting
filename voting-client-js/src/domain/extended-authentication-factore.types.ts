/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

export class ExtendedAuthenticationFactor {
	constructor(
		public name: string,
		public length: number,
		public regex: string
	) {}
}