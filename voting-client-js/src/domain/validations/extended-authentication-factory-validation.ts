/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {FailedValidationError} from "./failed-validation-error";
import {checkArgument, checkNotNull} from "crypto-primitives-ts/lib/esm/validation/preconditions";
import {ExtendedAuthenticationFactor} from "../extended-authentication-factore.types";

const BIRTH_YEAR: string = "birthYear";
const BIRTH_DATE: string = "birthDate";
const BIRTH_YEAR_LENGTH: number = 4;
const BIRTH_YEAR_REGEX: string = "^\\d{4}$";
const BIRTH_DATE_LENGTH: number = 8;
const BIRTH_DATE_REGEX: string = "^\\d{4}-\\d{2}-\\d{2}$";

const EXTENDED_AUTHENTICATION_FACTORS: Record<string, ExtendedAuthenticationFactor> = {
	[BIRTH_YEAR]: new ExtendedAuthenticationFactor(BIRTH_YEAR, BIRTH_YEAR_LENGTH, BIRTH_YEAR_REGEX),
	[BIRTH_DATE]: new ExtendedAuthenticationFactor(BIRTH_DATE, BIRTH_DATE_LENGTH, BIRTH_DATE_REGEX)
};

/**
 * * Validates the provided extended authentication factor using the provided extended authentication factor length.
 *
 * @param extendedAuthenticationFactor       the extended authentication factor. Must be non-null and be a sequence of
 *                                           {@code extendedAuthenticationFactorLength} digits.
 * @param extendedAuthenticationFactorLength the extended authentication factor length. Must be part of the supported
 *                                           {@code EXTENDED_AUTHENTICATION_FACTORS}.
 *
 * @returns {string} - the validated input string.
 */
export function validateEA(extendedAuthenticationFactor: string, extendedAuthenticationFactorLength: number): string {
	checkNotNull(extendedAuthenticationFactor);

	checkArgument(
		Object.values(EXTENDED_AUTHENTICATION_FACTORS)
			.map(factor => factor.length)
			.includes(extendedAuthenticationFactorLength),
		`Unsupported extended authentication factor length provided. [length: ${extendedAuthenticationFactorLength}]`
	);

	const regex = new RegExp(`^.{${extendedAuthenticationFactorLength}}$`);
	if (!regex.test(extendedAuthenticationFactor)) {
		throw new FailedValidationError("The extended authentication factor must be a digit of correct size.");
	}

	return extendedAuthenticationFactor;
}
