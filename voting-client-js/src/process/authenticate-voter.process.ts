/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {getKey} from "../protocol/voting-phase/authenticate-voter/get-key.algorithm";
import {ZqElement} from "crypto-primitives-ts/lib/esm/math/zq_element";
import {parseBallot} from "../domain/model/ballot-parser-api";
import {SessionService} from "../session.service";
import {PrimitivesParams} from "../domain/primitives-params.types";
import {VotingServerService} from "../voting-server.service";
import {parsePrimitivesParams} from "../domain/primitives-params-parser";
import {VoterPortalInputError} from "../domain/voter-portal-input-error";
import {AuthenticationChallenge} from "../protocol/voting-phase/authenticate-voter/get-authentication-challenge.types";
import {validateUUID} from "../domain/validations/validations";
import {getAuthenticationChallenge} from "../protocol/voting-phase/authenticate-voter/get-authentication-challenge.algorithm";
import {AuthenticateVoterRequestPayload, AuthenticateVoterResponse, AuthenticateVoterResponsePayload} from "../domain/authenticate-voter.types";
import {validateSVK} from "../domain/validations/start-voting-key-validation";

export class AuthenticateVoterProcess {

	private static START_VOTING_KEY_INVALID_ERROR_CODE: string = "START_VOTING_KEY_INVALID";

	private sessionService: SessionService;
	private votingServerService: VotingServerService;

	constructor() {
		this.sessionService = SessionService.getInstance();
		this.votingServerService = new VotingServerService();
	}

	/**
	 * This process implements the authenticateVoter phase of the protocol:
	 *  - Receives the StartVotingKey and ExtendedAuthenticationFactor from the Voter Portal.
	 *  - Calls the GetAuthenticationChallenge algorithm.
	 *  - Sends an AuthenticationChallenge to the Voting Server.
	 *  - Parses the AuthenticateVoterResponsePayload received from the Voting Server.
	 *  - Calls the GetKey algorithm if the verification card state is "INITIAL" or "SENT".
	 *  - Returns an AuthenticateVoterResponse to the Voter Portal.
	 *
	 * @param {string} startVotingKey - the voter start voting key.
	 * @param {string} extendedAuthenticationFactor - the voter extended authentication factor (i.e. birthdate).
	 * @param {string} electionEventId - the election event id.
	 * @param {string} lang - the supported language code.
	 * @returns {Promise<AuthenticateVoterResponse>}, the authenticateVoter response expected by the Voter-Portal.
	 */
	public async authenticateVoter(
		startVotingKey: string,
		extendedAuthenticationFactor: string,
		electionEventId: string,
		lang: string
	): Promise<AuthenticateVoterResponse> {

		this.validateStartVotingKeyInput(startVotingKey);

		// Call GetAuthenticationChallenge algorithm.
		const authenticationChallenge: AuthenticationChallenge = await getAuthenticationChallenge(
			{
				electionEventId: electionEventId,
				extendedAuthenticationFactorLength: extendedAuthenticationFactor.length
			},
			"authenticateVoter",
			startVotingKey,
			extendedAuthenticationFactor
		);

		// Store authentication challenge inputs in session.
		this.sessionService.startVotingKey(startVotingKey);
		this.sessionService.extendedAuthenticationFactor(extendedAuthenticationFactor);

		// Prepare and post payload to the Voting Server.
		const authenticateVoterRequestPayload: AuthenticateVoterRequestPayload = {
			electionEventId: electionEventId,
			authenticationChallenge: authenticationChallenge
		};
		const authenticateVoterResponsePayload: AuthenticateVoterResponsePayload = await this.votingServerService.authenticateVoter(authenticateVoterRequestPayload);

		// If the verification card is already CONFIRMED (happens in a re-login), there is nothing left to calculate.
		const verificationCardState: string = authenticateVoterResponsePayload.verificationCardState;

		// Prepare the response
		const authenticateVoterResponse: AuthenticateVoterResponse = {
			votingCardState: verificationCardState
		};

		if (verificationCardState === "INITIAL" || verificationCardState === "SENT") {
			// Store ids in session.
			this.sessionService.voterAuthenticationData(authenticateVoterResponsePayload.voterAuthenticationData);

			// Parse the primitives params.
			const primitivesParams: PrimitivesParams = parsePrimitivesParams(authenticateVoterResponsePayload.votingClientPublicKeys, authenticateVoterResponsePayload.primesMappingTable);
			this.sessionService.primitivesParams(primitivesParams);

			// Call GetKey algorithm.
			const verificationCardSecretKey: ZqElement = await getKey(
				{
					encryptionGroup: primitivesParams.encryptionGroup,
					electionEventId: validateUUID(authenticateVoterResponsePayload.voterAuthenticationData.electionEventId),
					verificationCardSetId: authenticateVoterResponsePayload.voterAuthenticationData.verificationCardSetId,
					verificationCardId: authenticateVoterResponsePayload.verificationCardKeystore.verificationCardId,
					primesMappingTable: primitivesParams.primesMappingTable,
					electionPublicKey: primitivesParams.electionPublicKey,
					choiceReturnCodesEncryptionPublicKey: primitivesParams.choiceReturnCodesEncryptionPublicKey
				},
				startVotingKey,
				authenticateVoterResponsePayload.verificationCardKeystore.verificationCardKeystore
			);

			// Store the verification card secret key in session.
			this.sessionService.verificationCardSecretKey(verificationCardSecretKey);

			// The ballotTexts in the payload is escaped as string, we must convert it before returning its value.
			const parsedBallotTexts = JSON.parse(authenticateVoterResponsePayload.voterMaterial.ballotTexts);
			// The ballot must be parsed to integrate the ballotTexts and prepare a specific ballot for the Voter-Portal.
			authenticateVoterResponse.ballot = await parseBallot(authenticateVoterResponsePayload.voterMaterial.ballot, parsedBallotTexts, lang);

			// In the SENT state, we also have the Choice Return Codes to return.
			if (verificationCardState === "SENT") {
				authenticateVoterResponse.choiceReturnCodes = authenticateVoterResponsePayload.voterMaterial.shortChoiceReturnCodes;
			}
		}

		// In the CONFIRMED state, we do not need to perform getKey algorithm. Only the Cast Return Code is needed.
		if (verificationCardState === "CONFIRMED") {
			authenticateVoterResponse.voteCastReturnCode = authenticateVoterResponsePayload.voterMaterial.shortVoteCastReturnCode;
		}

		return authenticateVoterResponse;
	}


	/**
	 * Throws a Voter Portal input error if the start voting key is not valid.
	 * @param {string} startVotingKey - the voter start voting key.
	 */
	private validateStartVotingKeyInput(startVotingKey: string): void {
		try {
			validateSVK(startVotingKey);
		} catch (_error) {
			throw new VoterPortalInputError(AuthenticateVoterProcess.START_VOTING_KEY_INVALID_ERROR_CODE, _error.message);
		}
	}
}



