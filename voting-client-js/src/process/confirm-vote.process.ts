/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
/* global require */

import {GqElement} from "crypto-primitives-ts/lib/esm/math/gq_element";
import {SessionService} from "../session.service";
import {VotingServerService} from "../voting-server.service";
import {createConfirmMessage} from "../protocol/voting-phase/confirm-vote/create-confirm-message.algorithm";
import {VoterPortalInputError} from "../domain/voter-portal-input-error";
import {AuthenticationChallenge} from "../protocol/voting-phase/authenticate-voter/get-authentication-challenge.types";
import {getAuthenticationChallenge} from "../protocol/voting-phase/authenticate-voter/get-authentication-challenge.algorithm";
import {validateBallotCastingKey} from "../domain/validations/validations";
import {serializeGqGroup, serializeGroupElement} from "../domain/primitives-serializer";
import {ConfirmVoteRequestPayload, ConfirmVoteResponse, ConfirmVoteResponsePayload} from "../domain/confirm-vote.types";

export class ConfirmVoteProcess {

	private static CONFIRMATION_KEY_INCORRECT_ERROR_CODE: string = "CONFIRMATION_KEY_INCORRECT";

	private sessionService: SessionService;
	private votingServerService: VotingServerService;

	constructor() {
		this.sessionService = SessionService.getInstance();
		this.votingServerService = new VotingServerService();
	}

	/**
	 * This process implements the confirmVote phase of the protocol:
	 *  - Receives the BallotCastingKey from the Voter Portal.
	 *  - Calls the CreateConfirmMessage algorithm.
	 *  - Calls the GetAuthenticationChallenge algorithm.
	 *  - Sends a ConfirmVoteRequestPayload to the Voting Server.
	 *  - Returns a ConfirmVoteResponse to the Voter Portal.
	 *
	 * @param {string} ballotCastingKey - the voter ballot casting key.
	 *
	 * @returns {Promise<ConfirmVoteResponse>} - the confirmVote response expected by the Voter-Portal.
	 */
	public async confirmVote(ballotCastingKey: string): Promise<ConfirmVoteResponse> {

		this.validateBallotCastingKeyInput(ballotCastingKey);

		const primitivesParams = this.sessionService.primitivesParams();
		const voterAuthenticationData = this.sessionService.voterAuthenticationData();

		// Call CreateConfirmMessage algorithm
		const confirmationKey: GqElement = createConfirmMessage(
			{
				encryptionGroup: primitivesParams.encryptionGroup
			},
			ballotCastingKey,
			this.sessionService.verificationCardSecretKey()
		);

		const extendedAuthenticationFactor : string = this.sessionService.extendedAuthenticationFactor();

		// Call GetAuthenticationChallenge algorithm
		const authenticationChallenge: AuthenticationChallenge = await getAuthenticationChallenge(
			{
				electionEventId: voterAuthenticationData.electionEventId,
				extendedAuthenticationFactorLength: extendedAuthenticationFactor.length
			},
			"confirmVote",
			this.sessionService.startVotingKey(),
			extendedAuthenticationFactor
		);

		// Prepare and post payload to the Voting Server
		const confirmVoteRequestPayload: ConfirmVoteRequestPayload = {
			contextIds: {
				electionEventId: voterAuthenticationData.electionEventId,
				verificationCardSetId: voterAuthenticationData.verificationCardSetId,
				verificationCardId: voterAuthenticationData.verificationCardId
			},
			authenticationChallenge: authenticationChallenge,
			confirmationKey: serializeGroupElement(confirmationKey),
			encryptionGroup: JSON.parse(serializeGqGroup(primitivesParams.encryptionGroup))
		};
		const confirmVoteResponsePayload: ConfirmVoteResponsePayload = await this.votingServerService.confirmVote(confirmVoteRequestPayload);

		return {
			voteCastReturnCode: confirmVoteResponsePayload.shortVoteCastReturnCode
		};
	}

	/**
	 * Throws a Voter Portal input error if the ballot casting key is not valid.
	 * @param {string} ballotCastingKey - the voter ballot casting key.
	 */
	private validateBallotCastingKeyInput(ballotCastingKey: string) {
		try {
			validateBallotCastingKey(ballotCastingKey);
		} catch (_error) {
			throw new VoterPortalInputError(ConfirmVoteProcess.CONFIRMATION_KEY_INCORRECT_ERROR_CODE, _error.message);
		}
	}

}
