/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {GqGroup} from "crypto-primitives-ts/lib/esm/math/gq_group";

/**
 * @property {GqGroup} encryptionGroup - (p, q, g), the encryption group.
 * @property {number} numberOfAllowedWriteInsPlusOne - &delta;, the number of allowed write ins + 1.
 */
export interface EncodeWriteInsContext {
  encryptionGroup: GqGroup;
  numberOfAllowedWriteInsPlusOne: number;
}

/**
 * @property {GqGroup} encryptionGroup - (p, q, g), the encryption group.
 */
export interface WriteInToQuadraticResidueContext {
	encryptionGroup: GqGroup;
}

/**
 * @property {GqGroup} encryptionGroup - (p, q, g), the encryption group.
 */
export interface WriteInToIntegerContext {
	encryptionGroup: GqGroup;
}
