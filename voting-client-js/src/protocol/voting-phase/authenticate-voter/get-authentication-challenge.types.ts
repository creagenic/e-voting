/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

/**
 * @property {string} derivedVoterIdentifier - credentialID_id, The derived voter identifier.
 * @property {string} derivedAuthenticationChallenge - hhAuth_id, The derived authentication challenge.
 * @property {string} authenticationNonce - nonce, The authentication nonce.
 */
export interface AuthenticationChallenge {
	derivedVoterIdentifier: string;
	derivedAuthenticationChallenge: string;
	authenticationNonce: string;
}

/**
 * @property {string} electionEventId - ee, the election event id.
 * @property {string} extendedAuthenticationFactorLength - l_EA, the extended authentication factor length.
 */
export interface AuthenticationChallengeContext {
	electionEventId: string;
	extendedAuthenticationFactorLength: number;
}