/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {getKey} from "../../../../src/protocol/voting-phase/authenticate-voter/get-key.algorithm";
import {ZqElement} from "crypto-primitives-ts/lib/esm/math/zq_element";
import {PrimitivesParams} from "../../../../src/domain/primitives-params.types";
import {parsePrimitivesParams} from "../../../../src/domain/primitives-params-parser";

import authenticateVoterResponseJson from "../../../tools/data/authenticate-voter-response.json";

describe("Get key algorithm", function (): void {

	test("should return a private key", async function (): Promise<void> {
		const primitivesParams: PrimitivesParams = parsePrimitivesParams(
			authenticateVoterResponseJson.votingClientPublicKeys,
			authenticateVoterResponseJson.primesMappingTable
		);

		const expectedValue: string = "1142402876189003794064028201924441359008322856926761937364886533670411270629401167579864989422565467609713431742006768010726801011090918905054860207526804159818402032303242248787778800410721905918915445689123290094823209224825696034168181467868822097066518108606187809446249843391038394933851576203390405138696938565334055797201208699466421283540297875832693107959866133805413597159190242918554829194975400724853234012168513837880148837483858486250830533191880745605510453574271105568597527250585169206738768561063109684473915715029661991695426063117226173217258292749213697749037727408754612291680756745268430611896124834549151852819054888325704042296494692972834653057327653446048632722264573414725860838532793466528371424563205634092709711163795610804042104621191381226951758463628017557652787387811726931424497357688800486774750580936358506255563171615721325253361918238664414712780358140878540146050190497105816502808385"

		const key: ZqElement = await getKey(
			{
				encryptionGroup: primitivesParams.encryptionGroup,
				electionEventId: authenticateVoterResponseJson.voterAuthenticationData.electionEventId,
				verificationCardSetId: authenticateVoterResponseJson.voterAuthenticationData.verificationCardSetId,
				verificationCardId: authenticateVoterResponseJson.verificationCardKeystore.verificationCardId,
				primesMappingTable: primitivesParams.primesMappingTable,
				electionPublicKey: primitivesParams.electionPublicKey,
				choiceReturnCodesEncryptionPublicKey: primitivesParams.choiceReturnCodesEncryptionPublicKey
			},
			"ptbni9gqn4kz2yk5s429rrmt",
			authenticateVoterResponseJson.verificationCardKeystore.verificationCardKeystore
		);

		expect(key.value.toString()).toBe(expectedValue);
	}, 30000);

});


