/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */

import {ZqGroup} from "crypto-primitives-ts/lib/esm/math/zq_group";
import {GqElement} from "crypto-primitives-ts/lib/esm/math/gq_element";
import {ZqElement} from "crypto-primitives-ts/lib/esm/math/zq_element";
import {PrimitivesParams} from "../../../../src/domain/primitives-params.types";
import {byteArrayToInteger} from "crypto-primitives-ts/lib/esm/conversions";
import {ImmutableBigInteger} from "crypto-primitives-ts/lib/esm/immutable_big_integer";
import {ImmutableUint8Array} from "crypto-primitives-ts/lib/esm/immutable_uint8Array";
import {createConfirmMessage} from "../../../../src/protocol/voting-phase/confirm-vote/create-confirm-message.algorithm";
import {parsePrimitivesParams} from "../../../../src/domain/primitives-params-parser";

import authenticateVoterResponseJson from "../../../tools/data/authenticate-voter-response.json";
import verificationCardSecretKeyBytes from "../../../tools/data/verificationCardSecretKeyBytes.json";

describe("Create confirm message algorithm", function (): void {

	const primitivesParams: PrimitivesParams = parsePrimitivesParams(authenticateVoterResponseJson.votingClientPublicKeys, authenticateVoterResponseJson.primesMappingTable);
	const k_id: ImmutableBigInteger = byteArrayToInteger(ImmutableUint8Array.from(verificationCardSecretKeyBytes));
	const verificationCardSecretKey: ZqElement = ZqElement.create(k_id, ZqGroup.sameOrderAs(primitivesParams.encryptionGroup));

	test("should generate a confirmation key", function (): void {

		const confirmationKey: GqElement = createConfirmMessage(
			{
				encryptionGroup: primitivesParams.encryptionGroup
			},
			"186077054",
			verificationCardSecretKey
		);

		expect("633277726668645620227289993942530817756060101178214684867209460490535994812253110145427986334770622074641226507736932012161304940445928494351422825350528980199190326827067701748192966720854778528937542445822811039189735164344340363142105834803079499741660070456563371137815945196362790921065475604969839673441345217777468308503924611043063291266890928554571568842123449113715774722930258653268436591621589068986205393698734628620685252274850413349619590271696953935657571841347772435738112910510253938216062283631482284339998448185033442556426721459051702680260336558130810811797202949417673097621102992604849524783702599109750676423626771332264279206537866136198457223739131026571264545966867927248975449971814152003781212308107158050910476215660835569813710792871399502333550551932102198664474880176012926648448125960175460192675471923064898170204641106621138370761431592333556926064058254069985472180554693124547137782995")
			.toBe(confirmationKey.value.toString());
	});

	describe("should fail with", function (): void {

		const bck_length_error: string = "The ballot casting key length must be 9";
		const bck_numeric_error: string = "The ballot casting key must be a numeric value";

		const parameters = [
			{
				description: "a shorter ballot casting key",
				bck: "12345678",
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup
				},
				error: new Error(bck_length_error)
			},
			{
				description: "a longer ballot casting key",
				bck: "1234567890",
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup
				},
				error: new Error(bck_length_error)
			},
			{
				description: "a zero ballot casting key",
				bck: "000000000",
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup
				},
				error: new Error("The ballot casting key must contain at least one non-zero element")
			},
			{
				description: "a non-numeric ballot casting key",
				bck: "1A3456789",
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup
				},
				error: new Error(bck_numeric_error)
			},
			{
				description: "a space starting ballot casting key",
				bck: " 23456789",
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup
				},
				error: new Error(bck_numeric_error)
			},
			{
				description: "a space ending ballot casting key",
				bck: "12345678 ",
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup
				},
				error: new Error(bck_numeric_error)
			},
			{
				description: "a null ballot casting key",
				bck: null,
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup
				},
				error: new Error()
			},
			{
				description: "a null verification card secret key",
				bck: "123456789",
				vcsk: null,
				context: {
					encryptionGroup: primitivesParams.encryptionGroup
				},
				error: new Error()
			},
			{
				description: "a null encryption params",
				bck: "123456789",
				vcsk: verificationCardSecretKey,
				context: {
					encryptionGroup: null
				},
				error: new Error()
			}
		];

		parameters.forEach((parameter): void => {
			it(parameter.description, function (): void {
				expect(function () {
					createConfirmMessage(
						parameter.context,
						parameter.bck,
						parameter.vcsk
					);
				}).toThrow(parameter.error);
			});
		});
	});

});


