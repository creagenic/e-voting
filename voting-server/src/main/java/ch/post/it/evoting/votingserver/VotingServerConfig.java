/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver;

import java.io.IOException;
import java.time.Duration;

import jakarta.jms.ConnectionFactory;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.boot.autoconfigure.jms.JmsProperties;
import org.springframework.boot.context.properties.PropertyMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.destination.DestinationResolver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import ch.post.it.evoting.cryptoprimitives.hashing.Argon2;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Factory;
import ch.post.it.evoting.cryptoprimitives.hashing.Argon2Profile;
import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.hashing.HashFactory;
import ch.post.it.evoting.cryptoprimitives.math.Base64;
import ch.post.it.evoting.cryptoprimitives.math.BaseEncodingFactory;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.cryptoprimitives.symmetric.Symmetric;
import ch.post.it.evoting.cryptoprimitives.symmetric.SymmetricFactory;
import ch.post.it.evoting.cryptoprimitives.utils.KeyDerivation;
import ch.post.it.evoting.cryptoprimitives.utils.KeyDerivationFactory;
import ch.post.it.evoting.evotinglibraries.direct.trust.SignatureKeystoreFactory;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.PrimesMappingTableAlgorithms;
import ch.post.it.evoting.votingserver.messaging.AggregatorJob;
import ch.post.it.evoting.votingserver.messaging.MessageErrorHandler;
import ch.post.it.evoting.votingserver.process.KeystoreRepository;

@Configuration
@EnableJpaRepositories
public class VotingServerConfig {

	@Bean
	public ObjectMapper objectMapper() {
		final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
		objectMapper.registerModule(new JavaTimeModule());
		return objectMapper;
	}

	@Bean
	public Hash hash() {
		return HashFactory.createHash();
	}

	@Bean
	public KeyDerivation keyDerivation() {
		return KeyDerivationFactory.createKeyDerivation();
	}

	@Bean
	public Base64 base64() {
		return BaseEncodingFactory.createBase64();
	}

	@Bean
	public SignatureKeystore<Alias> keystoreService(final KeystoreRepository repository) throws IOException {
		return SignatureKeystoreFactory.createSignatureKeystore(repository.getKeyStore(), repository.getKeystorePassword(),
				repository.getKeystoreAlias());
	}

	@Bean
	public Symmetric symmetric() {
		return SymmetricFactory.createSymmetric();
	}

	@Bean
	public Argon2 argon2() {
		return Argon2Factory.createArgon2(Argon2Profile.LESS_MEMORY);
	}

	@Bean
	public PrimesMappingTableAlgorithms primesMappingTableAlgorithms() {
		return new PrimesMappingTableAlgorithms();
	}

	@Bean
	@Primary
	public JmsTemplate jmsTemplate(
			final ConnectionFactory connectionFactory,
			final JmsProperties jmsProperties,
			final ObjectProvider<DestinationResolver> destinationResolver,
			final ObjectProvider<MessageConverter> messageConverter) {

		final PropertyMapper map = PropertyMapper.get();
		final JmsTemplate template = new JmsTemplate(connectionFactory);
		template.setPubSubDomain(jmsProperties.isPubSubDomain());
		map.from(destinationResolver::getIfUnique).whenNonNull().to(template::setDestinationResolver);
		map.from(messageConverter::getIfUnique).whenNonNull().to(template::setMessageConverter);
		mapTemplateProperties(jmsProperties.getTemplate(), template);
		return template;
	}

	@Bean(name = "multicastJmsTemplate")
	public JmsTemplate multicastJmsTemplate(final JmsTemplate jmsTemplate) {
		final JmsTemplate multicastJmsTemplate = new JmsTemplate();
		BeanUtils.copyProperties(jmsTemplate, multicastJmsTemplate);
		multicastJmsTemplate.setPubSubDomain(true);
		return multicastJmsTemplate;
	}

	@Bean
	public JmsListenerContainerFactory<DefaultMessageListenerContainer> customFactory(
			final MessageErrorHandler errorHandler,
			final DefaultJmsListenerContainerFactoryConfigurer configurer,
			final ConnectionFactory connectionFactory) {

		final DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
		configurer.configure(factory, connectionFactory);
		factory.setErrorHandler(errorHandler);

		return factory;
	}

	@Bean
	public JmsListenerContainerFactory<DefaultMessageListenerContainer> multicastConnectionFactory(
			final MessageErrorHandler errorHandler,
			final ConnectionFactory connectionFactory,
			final DefaultJmsListenerContainerFactoryConfigurer configurer) {

		final DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
		// The configurer has to be called before other factory changes.
		configurer.configure(factory, connectionFactory);
		factory.setPubSubDomain(true);
		factory.setErrorHandler(errorHandler);

		return factory;
	}

	// Taken from Spring autoconfiguration. See JmsAutoConfiguration.class.
	private void mapTemplateProperties(final JmsProperties.Template properties, final JmsTemplate template) {
		final PropertyMapper map = PropertyMapper.get();
		map.from(properties::getDefaultDestination).whenNonNull().to(template::setDefaultDestinationName);
		map.from(properties::getDeliveryDelay).whenNonNull().as(Duration::toMillis).to(template::setDeliveryDelay);
		map.from(properties::determineQosEnabled).to(template::setExplicitQosEnabled);
		map.from(properties::getDeliveryMode)
				.whenNonNull()
				.as(JmsProperties.DeliveryMode::getValue)
				.to(template::setDeliveryMode);
		map.from(properties::getPriority).whenNonNull().to(template::setPriority);
		map.from(properties::getTimeToLive).whenNonNull().as(Duration::toMillis).to(template::setTimeToLive);
		map.from(properties::getReceiveTimeout)
				.whenNonNull()
				.as(Duration::toMillis)
				.to(template::setReceiveTimeout);
	}

	@Bean
	JobDetail aggregatorJob() {
		return JobBuilder.newJob(AggregatorJob.class)
				.withIdentity("aggregator-job")
				.storeDurably()
				.build();
	}

	@Bean
	Trigger aggregatorTrigger(final JobDetail aggregatorJob,
			@Value("${aggregator-job.interval:10000}")
			final int interval) {
		return TriggerBuilder.newTrigger()
				.withIdentity("aggregator-job-trigger")
				.forJob(aggregatorJob)
				.startNow()
				.withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInMilliseconds(interval).repeatForever())
				.build();
	}
}
