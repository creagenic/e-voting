/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.cardmanagementapi;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.time.LocalDateTime;

import ch.post.it.evoting.votingserver.process.VerificationCardState;

public record UsedVotingCardDTO(String electionEventId, String verificationCardSetId, String verificationCardId, String votingCardId,
								VerificationCardState votingCardState, LocalDateTime votingCardStateDate) {

	public UsedVotingCardDTO {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		validateUUID(verificationCardId);
		validateUUID(votingCardId);
		checkNotNull(votingCardState);
		checkNotNull(votingCardStateDate);
	}

}
