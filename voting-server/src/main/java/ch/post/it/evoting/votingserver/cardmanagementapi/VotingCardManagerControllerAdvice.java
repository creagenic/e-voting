/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.cardmanagementapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import ch.post.it.evoting.votingserver.process.VerificationCardNotFoundException;
import ch.post.it.evoting.votingserver.process.VotingCardDto;

@ControllerAdvice(assignableTypes = { VotingCardManagerController.class })
public class VotingCardManagerControllerAdvice {

	private static final Logger LOG = LoggerFactory.getLogger(VotingCardManagerControllerAdvice.class);

	@ExceptionHandler(value = { IllegalStateException.class })
	public ResponseEntity<VotingCardDto> handleIllegalStateException(final ServerHttpRequest serverRequest, final IllegalStateException ex) {
		LOG.error("Failed to process request. [request: {}]", serverRequest.getURI(), ex);
		return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
	}

	@ExceptionHandler(value = { TooManyVerificationCardsException.class })
	public ResponseEntity<VotingCardDto> handleTooManyVotingCardsException(final ServerHttpRequest serverRequest,
			final TooManyVerificationCardsException ex) {
		LOG.debug("Failed to process request due too many voting cards found. [request: {}]", serverRequest.getURI(), ex);
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = { VerificationCardNotFoundException.class })
	public ResponseEntity<VotingCardDto> handleVerificationCardNotFoundException(final ServerHttpRequest serverRequest,
			final VerificationCardNotFoundException ex) {
		LOG.debug("Failed to process request due too many voting cards found. [request: {}]", serverRequest.getURI(), ex);
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
}
