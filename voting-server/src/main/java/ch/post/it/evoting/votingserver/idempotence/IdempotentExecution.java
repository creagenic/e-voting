/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.idempotence;

import static com.google.common.base.Preconditions.checkNotNull;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Version;

@Entity
@IdClass(IdempotentExecutionId.class)
public class IdempotentExecution {

	@Id
	private String context;

	@Id
	private String executionKey;

	@Version
	private Long changeControlId;

	protected IdempotentExecution() {
	}

	public IdempotentExecution(final String context, final String executionKey) {
		this.context = checkNotNull(context);
		this.executionKey = checkNotNull(executionKey);
	}

	public String getContext() {
		return context;
	}

	public String getExecutionKey() {
		return executionKey;
	}
}
