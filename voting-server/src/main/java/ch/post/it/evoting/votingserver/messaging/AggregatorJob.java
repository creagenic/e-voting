/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messaging;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static com.google.common.base.Preconditions.checkState;

import java.util.List;
import java.util.Map;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

@Component
@DisallowConcurrentExecution
public class AggregatorJob extends QuartzJobBean {

	private static final Logger LOGGER = LoggerFactory.getLogger(AggregatorJob.class);
	private final AggregatorService aggregatorService;

	public AggregatorJob(final AggregatorService aggregatorService) {
		this.aggregatorService = aggregatorService;
	}

	@Override
	protected void executeInternal(final JobExecutionContext context) {
		LOGGER.debug("Executing Aggregator Job...");
		final Map<String, List<InProgressMessage>> messages = aggregatorService.findMessagesForAggregationGroupedByCorrelationId();

		if (!messages.isEmpty()) {
			LOGGER.info("The aggregator job found messages to aggregate. [messages size: {}]", messages.size());
		}

		messages.forEach((correlationId, inProgressMessages) -> {
			checkState(inProgressMessages.size() == NODE_IDS.size(),
					"Not all nodes have responded yet. [correlationId: %s, inProgressMessages size: %s]", correlationId, inProgressMessages.size());
			aggregatorService.aggregateInProgressMessage(correlationId, inProgressMessages.getFirst().getResponseMessageType());
		});
		LOGGER.debug("Aggregator Job executed.");
	}
}
