/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messaging;

import java.util.List;

import jakarta.persistence.LockModeType;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface InProgressMessageRepository extends CrudRepository<InProgressMessage, InProgressMessageId> {
	@Transactional
	Integer countByCorrelationIdAndResponsePayloadIsNotNull(final String correlationId);

	@Transactional
	@Lock(LockModeType.PESSIMISTIC_WRITE)
	List<InProgressMessage> findAllByCorrelationIdAndResponsePayloadIsNotNull(final String correlationId);

	@Transactional
	void deleteAllByCorrelationId(final String correlationId);

	@Transactional
	@Query("SELECT m FROM InProgressMessage m WHERE m.correlationId IN (SELECT DISTINCT u.correlationId FROM InProgressMessage u WHERE u.responsePayload IS NOT NULL AND u.detectedForAggregation = false GROUP BY u.correlationId HAVING COUNT(u.correlationId) = ?1)")
	List<InProgressMessage> findAggregateReadyMessages(int size);
}
