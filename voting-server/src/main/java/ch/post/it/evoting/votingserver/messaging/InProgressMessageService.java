/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messaging;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class InProgressMessageService {
	private final InProgressMessageRepository inProgressMessageRepository;

	public InProgressMessageService(final InProgressMessageRepository inProgressMessageRepository) {
		this.inProgressMessageRepository = inProgressMessageRepository;
	}

	@Transactional
	public void storeRequest(final String correlationId, final int nodeId) {
		checkNotNull(correlationId);
		checkArgument(NODE_IDS.contains(nodeId));

		final InProgressMessage inProgressMessage = new InProgressMessage(correlationId, nodeId);
		inProgressMessageRepository.save(inProgressMessage);
	}

	@Transactional
	public void storeResponse(final String correlationId, final int nodeId, final byte[] payload, final String responseMessageType) {
		checkNotNull(correlationId);
		checkArgument(NODE_IDS.contains(nodeId));
		checkNotNull(payload);

		final byte[] payloadCopy = Arrays.copyOf(payload, payload.length);

		final InProgressMessage inProgressMessage = inProgressMessageRepository.findById(new InProgressMessageId(correlationId, nodeId))
				.orElseThrow(() -> new IllegalArgumentException(
						String.format("Unable to find the InProgressMessage. [correlationId: %s, nodeId: %s]", correlationId, nodeId)));

		inProgressMessage.setResponsePayload(payloadCopy);
		inProgressMessage.setResponseMessageType(responseMessageType);
		inProgressMessageRepository.save(inProgressMessage);
	}

	@Transactional
	public int countAllInProgressMessagesWithResponsePayload(final String correlationId) {
		checkNotNull(correlationId);
		return inProgressMessageRepository.countByCorrelationIdAndResponsePayloadIsNotNull(correlationId);
	}

	@Transactional
	public List<InProgressMessage> getAllNodesResponses(final String correlationId) {
		checkNotNull(correlationId);
		return inProgressMessageRepository.findAllByCorrelationIdAndResponsePayloadIsNotNull(correlationId);
	}

	@Transactional
	public void removeInProgressMessage(final String correlationId, final int nodeId) {
		checkNotNull(correlationId);
		checkArgument(NODE_IDS.contains(nodeId));

		final InProgressMessageId inProgressMessageId = new InProgressMessageId(correlationId, nodeId);
		inProgressMessageRepository.deleteById(inProgressMessageId);
	}

	@Transactional
	public void removeInProgressMessages(final String correlationId) {
		checkNotNull(correlationId);
		inProgressMessageRepository.deleteAllByCorrelationId(correlationId);
	}

	@Transactional
	public void setDetectedForAggregation(final String correlationId) {
		checkNotNull(correlationId);
		inProgressMessageRepository.findAllByCorrelationIdAndResponsePayloadIsNotNull(correlationId).forEach(inProgressMessage -> {
			inProgressMessage.setDetectedForAggregation(true);
			inProgressMessageRepository.save(inProgressMessage);
		});
	}

	@Transactional
	public List<InProgressMessage> findAggregateReadyMessages() {
		return inProgressMessageRepository.findAggregateReadyMessages(NODE_IDS.size());
	}
}
