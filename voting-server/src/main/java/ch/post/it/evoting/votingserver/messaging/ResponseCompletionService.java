/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messaging;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.util.concurrent.TimeUnit;

import jakarta.jms.JMSException;
import jakarta.jms.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

@Service
public class ResponseCompletionService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ResponseCompletionService.class);
	private static final String BROADCAST_TOPIC_EXCHANGE = "broadcast-topic-exchange";
	private static final String HEADER_MESSAGE_TYPE = "message_type";

	private final Serializer serializer;
	private final Cache<String, CachedElement<?>> cache;
	private final JmsTemplate multicastJmsTemplate;

	@Value("${responseCompletion.defaultTimeout:120}")
	private long defaultCompletionTimout;

	public ResponseCompletionService(
			final Serializer serializer,
			@Qualifier("multicastJmsTemplate")
			final JmsTemplate multicastJmsTemplate,
			@Value("${responseCompletionService.cache-timeout:10}")
			final long cacheTimeout) {
		this.serializer = serializer;
		this.multicastJmsTemplate = multicastJmsTemplate;
		this.cache = Caffeine.newBuilder()
				.expireAfterWrite(cacheTimeout, TimeUnit.MINUTES)
				.removalListener((key, value, cause) -> {
					if (cause.wasEvicted()) {
						LOGGER.debug("response completion evicted from cache. [correlationId : {}]", key);
					}
				})
				.build();
	}

	public void notifyCompleted(final String correlationId) {
		checkNotNull(correlationId);
		notifyResponseCompleted(correlationId, new Empty());
	}

	public <T> void notifyException(final String correlationId, final RuntimeException exception) {
		checkNotNull(correlationId);
		checkNotNull(exception);

		@SuppressWarnings("unchecked")
		final CachedElement<T> cachedElement = (CachedElement<T>) cache.getIfPresent(correlationId);

		if (cachedElement != null) {
			cachedElement.completableFuture().completeExceptionally(exception);
			cache.invalidate(correlationId);
		} else {
			//broadcast to other instances
			produceMessageToBroadcastQueue(correlationId, exception);
		}
	}

	public <T> void notifyResponseCompleted(final String correlationId, final T response) {
		checkNotNull(correlationId);
		checkNotNull(response);

		@SuppressWarnings("unchecked")
		final CachedElement<T> cachedElement = (CachedElement<T>) cache.getIfPresent(correlationId);

		if (cachedElement != null) {
			cachedElement.completableFuture().complete(response);
			cache.invalidate(correlationId);
		} else {
			//broadcast to other instances
			produceMessageToBroadcastQueue(correlationId, response);
		}
	}

	public <T> ResponseCompletionCompletableFuture<T> registerForResponseCompletion(final String correlationId, final Class<T> clazz) {
		checkNotNull(correlationId);
		checkNotNull(clazz);

		if (cache.getIfPresent(correlationId) != null) {
			throw new IllegalStateException(
					String.format("a registration for given correlationId already exist. [correlationId: %s]", correlationId));
		}

		@SuppressWarnings("unchecked")
		final ResponseCompletionCompletableFuture<T> responseCompletionCompletableFuture = (ResponseCompletionCompletableFuture<T>) cache.get(
				correlationId,
				string -> new CachedElement<>(new ResponseCompletionCompletableFuture<>(defaultCompletionTimout), clazz, null)).completableFuture();

		return responseCompletionCompletableFuture;
	}

	public <T> ResponseCompletionCompletableFuture<T> registerForResponseCompletion(final String correlationId,
			final TypeReference<T> typeReference) {
		checkNotNull(correlationId);
		checkNotNull(typeReference);

		if (cache.getIfPresent(correlationId) != null) {
			throw new IllegalStateException(
					String.format("a registration for given correlationId already exist. [correlationId: %s]", correlationId));
		}

		@SuppressWarnings("unchecked")
		final ResponseCompletionCompletableFuture<T> responseCompletionCompletableFuture = (ResponseCompletionCompletableFuture<T>) cache.get(
						correlationId,
						string -> new CachedElement<>(new ResponseCompletionCompletableFuture<>(defaultCompletionTimout), null, typeReference))
				.completableFuture();

		return responseCompletionCompletableFuture;
	}

	public ResponseCompletionCompletableFuture<Empty> registerForCompletion(final String correlationId) {
		checkNotNull(correlationId);
		return registerForResponseCompletion(correlationId, Empty.class);
	}

	public void unregisterResponseCompletion(final String correlationId) {
		checkNotNull(correlationId);
		cache.invalidate(correlationId);
	}

	private <T> void produceMessageToBroadcastQueue(final String correlationId, final T message) {
		checkNotNull(correlationId);
		checkNotNull(message);

		final byte[] payload = serializer.serialize(message);

		multicastJmsTemplate.convertAndSend(BROADCAST_TOPIC_EXCHANGE, payload, jmsMessage -> {
			jmsMessage.setJMSCorrelationID(correlationId);
			jmsMessage.setStringProperty(HEADER_MESSAGE_TYPE, message.getClass().getName());
			return jmsMessage;
		});
		LOGGER.info("response has been broadcast for other instances. [correlationId: {}]", correlationId);
	}

	@JmsListener(destination = BROADCAST_TOPIC_EXCHANGE, containerFactory = "multicastConnectionFactory")
	private <T> void consumeMessageFromBroadcastQueue(final Message message) throws JMSException {
		checkNotNull(message);
		final String correlationId = checkNotNull(message.getJMSCorrelationID());
		final String messageType = checkNotNull(message.getStringProperty(HEADER_MESSAGE_TYPE));

		@SuppressWarnings("unchecked")
		final CachedElement<T> cachedElement = (CachedElement<T>) cache.getIfPresent(correlationId);

		if (cachedElement != null) {
			if (!messageType.contains("Exception")) {
				final T o;
				final byte[] messageBytes = message.getBody(byte[].class);
				if (cachedElement.clazz != null) {
					o = serializer.deserialize(messageBytes, cachedElement.clazz);
				} else {
					o = serializer.deserialize(messageBytes, cachedElement.typeReference);
				}
				cachedElement.completableFuture().complete(o);
				cache.invalidate(correlationId);
				LOGGER.info("Response has been handled after been received from broadcast message. [correlationId: {}]", correlationId);
			} else {
				final byte[] messageBytes = message.getBody(byte[].class);
				final RuntimeException o = (RuntimeException) serializer.deserialize(messageBytes, getExceptionType(messageType));

				cachedElement.completableFuture().completeExceptionally(o);
				cache.invalidate(correlationId);
				LOGGER.info("Response (exceptionally) has been handled after been received from broadcast message. [correlationId: {}]",
						correlationId);
			}
		} else {
			LOGGER.info("Response has been received from broadcast message but no one is registered for in this instance. [correlationId: {}]",
					correlationId);
		}
	}

	private Class<?> getExceptionType(final String messageType) {
		try {
			final Class<?> aClass = Class.forName(messageType);
			if (RuntimeException.class.isAssignableFrom(aClass)) {
				return aClass;
			} else {
				throw new IllegalArgumentException("Exception is not a descendant of RuntimeException");
			}
		} catch (final ClassNotFoundException e) {
			throw new IllegalArgumentException("Class not found.", e);
		}
	}

	private record CachedElement<T>(ResponseCompletionCompletableFuture<T> completableFuture, Class<T> clazz, TypeReference<T> typeReference) {
		public CachedElement {
			checkNotNull(completableFuture);
			checkState(clazz != null ^ typeReference != null,
					"Either clazz or typeReference (xor) should by specified to deserialize the T element.");
		}
	}

	@SuppressWarnings("Java:S2094")
	record Empty() {
	}
}
