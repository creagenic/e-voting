/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.messaging;

import java.io.IOException;
import java.io.UncheckedIOException;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class Serializer {

	private final ObjectMapper objectMapper;

	public Serializer(final ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	public <T> T deserialize(final byte[] bytes, final Class<T> clazz) {
		try {
			return objectMapper.readValue(bytes, clazz);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	public <T> T deserialize(final byte[] bytes, final TypeReference<T> typeRef) {
		try {
			return objectMapper.readValue(bytes, typeRef);
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	public <T> byte[] serialize(final T payload) {
		try {
			return objectMapper.writeValueAsBytes(payload);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException(e);
		}
	}
}
