/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static com.google.common.base.Preconditions.checkNotNull;

import ch.post.it.evoting.evotinglibraries.domain.election.Ballot;

/**
 * Holds the ballot data: the ballot itself and the texts for translations.
 */
public record BallotData(Ballot ballot, String ballotTextsJson) {

	/**
	 * Constructs the ballot data.
	 *
	 * @param ballot          the ballot. Must be non-null.
	 * @param ballotTextsJson zhe ballot texts. Must be non-null.
	 * @throws NullPointerException if any parameter is null.
	 */
	public BallotData {
		checkNotNull(ballot);
		checkNotNull(ballotTextsJson);
	}

}
