/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateNonBlankUCS;
import static com.google.common.base.Preconditions.checkNotNull;

import java.time.LocalDateTime;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "ELECTION_EVENT_CONTEXT")
public class ElectionEventContextEntity {

	@Id
	private String electionEventId;

	@MapsId
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ELECTION_EVENT_ID", referencedColumnName = "ELECTION_EVENT_ID")
	private ElectionEventEntity electionEventEntity;

	private LocalDateTime startTime;

	private LocalDateTime finishTime;

	private String electionEventAlias;

	private String electionEventDescription;

	@Version
	private Integer changeControlId;

	public ElectionEventContextEntity() {
	}

	public ElectionEventContextEntity(final ElectionEventEntity electionEventEntity, final LocalDateTime startTime, final LocalDateTime finishTime,
			final String electionEventAlias, final String electionEventDescription) {
		this.electionEventEntity = checkNotNull(electionEventEntity);
		this.startTime = checkNotNull(startTime);
		this.finishTime = checkNotNull(finishTime);
		this.electionEventAlias = validateNonBlankUCS(electionEventAlias);
		this.electionEventDescription = validateNonBlankUCS(electionEventDescription);
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public ElectionEventEntity getElectionEventEntity() {
		return electionEventEntity;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public LocalDateTime getFinishTime() {
		return finishTime;
	}

	public String getElectionEventAlias() {
		return electionEventAlias;
	}

	public String getElectionEventDescription() {
		return electionEventDescription;
	}
}
