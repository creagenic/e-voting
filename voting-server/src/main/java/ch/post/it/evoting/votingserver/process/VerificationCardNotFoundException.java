/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

public class VerificationCardNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1;

	public VerificationCardNotFoundException(final String message) {
		super(message);
	}

}