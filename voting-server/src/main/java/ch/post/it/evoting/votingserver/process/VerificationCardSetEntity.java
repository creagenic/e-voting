/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

import ch.post.it.evoting.domain.converters.BooleanConverter;
import ch.post.it.evoting.domain.converters.PrimesMappingTableConverter;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;

@Entity
@Table(name = "VERIFICATION_CARD_SET")
@SuppressWarnings("java:S1068")
public class VerificationCardSetEntity {

	@Id
	@Column(name = "VERIFICATION_CARD_SET_ID")
	private String verificationCardSetId;

	@ManyToOne
	@JoinColumn(name = "ELECTION_EVENT_FK_ID", referencedColumnName = "ELECTION_EVENT_ID")
	private ElectionEventEntity electionEventEntity;

	private String ballotBoxId;

	private int gracePeriod;

	@Convert(converter = PrimesMappingTableConverter.class)
	private PrimesMappingTable primesMappingTable;

	private LocalDateTime ballotBoxStartTime;

	private LocalDateTime ballotBoxFinishTime;

	@Convert(converter = BooleanConverter.class)
	private boolean testBallotBox;

	@Version
	private Integer changeControlId;

	public VerificationCardSetEntity() {
	}

	private VerificationCardSetEntity(final String verificationCardSetId, final ElectionEventEntity electionEventEntity, final String ballotBoxId,
			final int gracePeriod, final PrimesMappingTable primesMappingTable, final LocalDateTime ballotBoxStartTime,
			final LocalDateTime ballotBoxFinishTime, final boolean testBallotBox) {
		this.verificationCardSetId = validateUUID(verificationCardSetId);
		this.electionEventEntity = checkNotNull(electionEventEntity);
		this.ballotBoxId = validateUUID(ballotBoxId);

		checkArgument(gracePeriod >= 0, "The grace period must be positive.");
		this.gracePeriod = gracePeriod;

		checkArgument(ballotBoxStartTime.isBefore(ballotBoxFinishTime) || ballotBoxStartTime.equals(ballotBoxFinishTime),
				"The ballot box start time must not be after the ballot box finish time.");
		this.ballotBoxStartTime = ballotBoxStartTime;
		this.ballotBoxFinishTime = ballotBoxFinishTime;

		this.primesMappingTable = checkNotNull(primesMappingTable);
		this.testBallotBox = testBallotBox;
	}

	public String getVerificationCardSetId() {
		return verificationCardSetId;
	}

	public ElectionEventEntity getElectionEventEntity() {
		return electionEventEntity;
	}

	public String getBallotBoxId() {
		return ballotBoxId;
	}

	public int getGracePeriod() {
		return gracePeriod;
	}

	public LocalDateTime getBallotBoxStartTime() {
		return ballotBoxStartTime;
	}

	public LocalDateTime getBallotBoxFinishTime() {
		return ballotBoxFinishTime;
	}

	public PrimesMappingTable getPrimesMappingTable() {
		return primesMappingTable;
	}

	public static class Builder {
		private String verificationCardSetId;
		private ElectionEventEntity electionEventEntity;
		private String ballotBoxId;
		private int gracePeriod;
		private PrimesMappingTable primesMappingTable;
		private LocalDateTime ballotBoxStartTime;
		private LocalDateTime ballotBoxFinishTime;
		private boolean testBallotBox;

		public Builder setVerificationCardSetId(final String verificationCardSetId) {
			this.verificationCardSetId = verificationCardSetId;
			return this;
		}

		public Builder setElectionEventEntity(final ElectionEventEntity electionEventEntity) {
			this.electionEventEntity = electionEventEntity;
			return this;
		}

		public Builder setBallotBoxId(final String ballotBoxId) {
			this.ballotBoxId = ballotBoxId;
			return this;
		}

		public Builder setGracePeriod(final int gracePeriod) {
			this.gracePeriod = gracePeriod;
			return this;
		}

		public Builder setPrimesMappingTable(final PrimesMappingTable primesMappingTable) {
			this.primesMappingTable = primesMappingTable;
			return this;
		}

		public Builder setBallotBoxStartTime(final LocalDateTime ballotBoxStartTime) {
			this.ballotBoxStartTime = ballotBoxStartTime;
			return this;
		}

		public Builder setBallotBoxFinishTime(final LocalDateTime ballotBoxFinishTime) {
			this.ballotBoxFinishTime = ballotBoxFinishTime;
			return this;
		}

		public Builder setTestBallotBox(final boolean testBallotBox) {
			this.testBallotBox = testBallotBox;
			return this;
		}

		public VerificationCardSetEntity build() {
			return new VerificationCardSetEntity(verificationCardSetId, electionEventEntity, ballotBoxId, gracePeriod, primesMappingTable,
					ballotBoxStartTime, ballotBoxFinishTime, testBallotBox);
		}
	}
}
