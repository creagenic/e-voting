/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.election.VerificationCardSetContext;

@Service
public class VerificationCardSetService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerificationCardSetService.class);

	private final ElectionEventService electionEventService;
	private final VerificationCardSetRepository verificationCardSetRepository;

	public VerificationCardSetService(
			final ElectionEventService electionEventService,
			final VerificationCardSetRepository verificationCardSetRepository) {
		this.electionEventService = electionEventService;
		this.verificationCardSetRepository = verificationCardSetRepository;
	}

	@Transactional
	public void saveAllFromContext(final String electionEventId, final List<VerificationCardSetContext> verificationCardSetContexts) {
		validateUUID(electionEventId);
		checkNotNull(verificationCardSetContexts);

		final ElectionEventEntity electionEventEntity = electionEventService.retrieveElectionEventEntity(electionEventId);

		final List<VerificationCardSetEntity> verificationCardSetEntities = verificationCardSetContexts.stream()
				.map(verificationCardSetContext -> {
					final String verificationCardSetId = verificationCardSetContext.getVerificationCardSetId();
					final PrimesMappingTable primesMappingTable = verificationCardSetContext.getPrimesMappingTable();

					return new VerificationCardSetEntity.Builder()
							.setVerificationCardSetId(verificationCardSetId)
							.setElectionEventEntity(electionEventEntity)
							.setBallotBoxId(verificationCardSetContext.getBallotBoxId())
							.setGracePeriod(verificationCardSetContext.getGracePeriod())
							.setPrimesMappingTable(primesMappingTable)
							.setBallotBoxStartTime(verificationCardSetContext.getBallotBoxStartTime())
							.setBallotBoxFinishTime(verificationCardSetContext.getBallotBoxFinishTime())
							.setTestBallotBox(verificationCardSetContext.isTestBallotBox())
							.build();

				})
				.toList();

		verificationCardSetRepository.saveAll(verificationCardSetEntities);
		LOGGER.info("Successfully saved verification card sets. [electionEventId: {}]", electionEventId);
	}

	@Transactional
	public VerificationCardSetEntity getVerificationCardSetEntity(final String verificationCardSetId) {
		validateUUID(verificationCardSetId);

		return verificationCardSetRepository.findById(verificationCardSetId)
				.orElseThrow(() -> new IllegalStateException(
						String.format("Verification card set not found. [verificationCardSetId: %s]", verificationCardSetId)));
	}

}
