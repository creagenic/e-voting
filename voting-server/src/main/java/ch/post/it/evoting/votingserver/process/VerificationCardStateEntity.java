/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.time.LocalDateTime;
import java.util.List;

import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

import ch.post.it.evoting.domain.converters.ListConverter;

@Entity
@Table(name = "VERIFICATION_CARD_STATE")
public class VerificationCardStateEntity {

	@Id
	private String verificationCardId;

	@Convert(converter = ListConverter.class)
	private List<String> shortChoiceReturnCodes = List.of();

	private String shortVoteCastReturnCode;

	private VerificationCardState state = VerificationCardState.INITIAL;

	private int authenticationAttempts = 0;

	private long lastSuccessfulAuthenticationTimeStep = 0;

	@Convert(converter = SuccessfulAuthenticationAttemptsConverter.class)
	private SuccessfulAuthenticationAttempts successfulAuthenticationAttempts = new SuccessfulAuthenticationAttempts(List.of());

	private int confirmationAttempts = 0;

	private LocalDateTime stateDate = LocalDateTime.now();

	@Version
	private Integer changeControlId;

	public VerificationCardStateEntity() {
	}

	public VerificationCardStateEntity(final String verificationCardId) {
		this.verificationCardId = verificationCardId;
	}

	public String getVerificationCardId() {
		return verificationCardId;
	}

	public List<String> getShortChoiceReturnCodes() {
		return List.copyOf(shortChoiceReturnCodes);
	}

	public void setShortChoiceReturnCodes(final List<String> shortChoiceReturnCodes) {
		checkNotNull(shortChoiceReturnCodes);
		this.shortChoiceReturnCodes = List.copyOf(shortChoiceReturnCodes);
	}

	public String getShortVoteCastReturnCode() {
		return shortVoteCastReturnCode;
	}

	public void setShortVoteCastReturnCode(final String shortVoteCastReturnCode) {
		checkNotNull(shortVoteCastReturnCode);
		this.shortVoteCastReturnCode = shortVoteCastReturnCode;
	}

	public VerificationCardState getState() {
		return state;
	}

	public int getAuthenticationAttempts() {
		return authenticationAttempts;
	}

	public void setAuthenticationAttempts(final int authenticationAttempts) {
		checkArgument(authenticationAttempts > 0 && authenticationAttempts <= 5, "Authentication attempt must be in ]0,5].");
		this.authenticationAttempts = authenticationAttempts;
	}

	public long getLastSuccessfulAuthenticationTimeStep() {
		return lastSuccessfulAuthenticationTimeStep;
	}

	public void setLastSuccessfulAuthenticationTimeStep(final long lastTimeStep) {
		checkArgument(lastTimeStep >= 0, "Last time step must be positive.");
		this.lastSuccessfulAuthenticationTimeStep = lastTimeStep;
	}

	public SuccessfulAuthenticationAttempts getSuccessfulAuthenticationAttempts() {
		return successfulAuthenticationAttempts;
	}

	public void setSuccessfulAuthenticationAttempts(final SuccessfulAuthenticationAttempts lastSuccessfulAuthenticationChallenge) {
		this.successfulAuthenticationAttempts = lastSuccessfulAuthenticationChallenge;
	}

	public int getConfirmationAttempts() {
		return confirmationAttempts;
	}

	public void setConfirmationAttempts(final int confirmationAttempts) {
		checkArgument(confirmationAttempts > 0 && confirmationAttempts <= 5, "Confirmation attempt must be in ]0,5].");
		this.confirmationAttempts = confirmationAttempts;
	}

	public LocalDateTime getStateDate() {
		return stateDate;
	}

	/**
	 * Updates the state to the provided state. Subsequently, this method also updates the state date to {@code LocalDateTime.now()}.
	 *
	 * @param state the state to be set. Must be non-null.
	 * @throws NullPointerException if the provided state is null.
	 */
	public void updateState(final VerificationCardState state) {
		checkNotNull(state);
		setState(state);
		setStateDate(LocalDateTime.now());
	}

	private void setState(final VerificationCardState state) {
		checkNotNull(state);
		this.state = state;
	}

	private void setStateDate(final LocalDateTime stateDate) {
		checkNotNull(stateDate);
		this.stateDate = stateDate;
	}

}
