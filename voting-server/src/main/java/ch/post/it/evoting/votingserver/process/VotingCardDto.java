/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.time.LocalDateTime;

public record VotingCardDto(String votingCardId,
							VerificationCardState votingCardState,
							LocalDateTime votingCardStateDate) {

	public VotingCardDto {
		validateUUID(votingCardId);
		checkNotNull(votingCardState);
		checkNotNull(votingCardStateDate);
	}

}
