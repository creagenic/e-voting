/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "ENC_LONG_CODE_SHARE")
@SuppressWarnings("java:S1068")
class EncLongCodeShareEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ENC_LONG_CODE_SHARE_GENERATOR")
	@SequenceGenerator(name = "ENC_LONG_CODE_SHARE_GENERATOR", sequenceName = "ENC_LONG_CODE_SHARE_SEQ", allocationSize = 1)
	private Long id;

	private String electionEventId;

	private String verificationCardSetId;

	private int chunkId;

	private int nodeId;

	private byte[] encLongCodeShare;

	@Version
	private Integer changeControlId;

	public EncLongCodeShareEntity() {

	}

	public EncLongCodeShareEntity(final String electionEventId, final String verificationCardSetId, final int chunkId,
			int nodeId, final byte[] encLongCodeShare) {
		this.electionEventId = validateUUID(electionEventId);
		this.verificationCardSetId = validateUUID(verificationCardSetId);
		checkState(chunkId >= 0);
		this.chunkId = chunkId;
		checkState(NODE_IDS.contains(nodeId), "The node id must be part of the known node ids. [nodeId: %s]", nodeId);
		this.nodeId = nodeId;
		this.encLongCodeShare = checkNotNull(encLongCodeShare);
	}

	public byte[] getEncLongCodeShare() {
		return encLongCodeShare;
	}
}
