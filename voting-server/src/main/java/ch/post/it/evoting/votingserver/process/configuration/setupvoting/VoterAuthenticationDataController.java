/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

import java.security.SignatureException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.post.it.evoting.cryptoprimitives.hashing.Hashable;
import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.domain.InvalidPayloadSignatureException;
import ch.post.it.evoting.domain.configuration.SetupComponentVoterAuthenticationData;
import ch.post.it.evoting.domain.configuration.SetupComponentVoterAuthenticationDataPayload;
import ch.post.it.evoting.evotinglibraries.domain.common.ChannelSecurityContextData;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.votingserver.idempotence.IdempotenceContext;
import ch.post.it.evoting.votingserver.idempotence.IdempotenceService;
import ch.post.it.evoting.votingserver.process.Constants;

@RestController
@RequestMapping("api/v1/processor/configuration/setupvoting/voterauthenticationdata")
public class VoterAuthenticationDataController {

	private static final Logger LOGGER = LoggerFactory.getLogger(VoterAuthenticationDataController.class);

	private final SignatureKeystore<Alias> signatureKeystoreService;
	private final VoterAuthenticationDataService voterAuthenticationDataService;
	private final IdempotenceService<IdempotenceContext> idempotenceService;

	public VoterAuthenticationDataController(
			final SignatureKeystore<Alias> signatureKeystoreService,
			final VoterAuthenticationDataService voterAuthenticationDataService,
			final IdempotenceService<IdempotenceContext> idempotenceService) {
		this.signatureKeystoreService = signatureKeystoreService;
		this.voterAuthenticationDataService = voterAuthenticationDataService;
		this.idempotenceService = idempotenceService;
	}

	@PostMapping("electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}")
	public void saveVoterAuthenticationData(
			@PathVariable(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@PathVariable(Constants.PARAMETER_VALUE_VERIFICATION_CARD_SET_ID)
			final String verificationCardSetId,
			@RequestBody
			final SetupComponentVoterAuthenticationDataPayload setupComponentVoterAuthenticationDataPayload) {

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkArgument(electionEventId.equals(setupComponentVoterAuthenticationDataPayload.getElectionEventId()));
		checkArgument(verificationCardSetId.equals(setupComponentVoterAuthenticationDataPayload.getVerificationCardSetId()));

		LOGGER.debug("Verifying voter authentication data payload signature... [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
				verificationCardSetId);
		verifyPayloadSignature(setupComponentVoterAuthenticationDataPayload);

		LOGGER.debug("Saving all voter authentication data... [electionEventId: {}, verificationCardSetId: {}]", electionEventId,
				verificationCardSetId);
		idempotenceService.execute(IdempotenceContext.SAVE_VOTER_AUTHENTICATION_DATA, String.format("%s-%s", electionEventId, verificationCardSetId),
				() -> voterAuthenticationDataService.saveVoterAuthenticationData(setupComponentVoterAuthenticationDataPayload));

		LOGGER.info("Saved all voter authentication data. [electionEventId: {}, verificationCardSetId: {}]", electionEventId, verificationCardSetId);
	}

	@GetMapping("electionevent/{electionEventId}/credentialid/{credentialId}")
	public SetupComponentVoterAuthenticationData getVoterAuthenticationData(
			@PathVariable(Constants.PARAMETER_VALUE_ELECTION_EVENT_ID)
			final String electionEventId,
			@PathVariable(Constants.PARAMETER_VALUE_CREDENTIAL_ID)
			final String credentialId) {

		validateUUID(electionEventId);
		validateUUID(credentialId);

		LOGGER.debug("Retrieving voter authentication data... [electionEventId: {}, credentialId: {}]", electionEventId, credentialId);

		return voterAuthenticationDataService.getVoterAuthenticationData(electionEventId, credentialId);
	}

	private void verifyPayloadSignature(final SetupComponentVoterAuthenticationDataPayload setupComponentVoterAuthenticationDataPayload) {
		final String electionEventId = setupComponentVoterAuthenticationDataPayload.getElectionEventId();
		final String verificationCardSetId = setupComponentVoterAuthenticationDataPayload.getVerificationCardSetId();

		final CryptoPrimitivesSignature signature = setupComponentVoterAuthenticationDataPayload.getSignature();
		checkState(signature != null,
				"The signature of the setup component voter authentication data payload is null. [electionEventId: %s, verificationCardSetId: %s]",
				electionEventId, verificationCardSetId);

		final Hashable additionalContextData = ChannelSecurityContextData.setupComponentVoterAuthenticationData(electionEventId,
				verificationCardSetId);

		final boolean isSignatureValid;
		try {
			isSignatureValid = signatureKeystoreService.verifySignature(Alias.SDM_CONFIG, setupComponentVoterAuthenticationDataPayload,
					additionalContextData, signature.signatureContents());

		} catch (final SignatureException e) {
			throw new IllegalStateException(
					String.format(
							"Couldn't verify the signature of the setup component voter authentication data payload. [electionEventId: %s, verificationCardSetId: %s]",
							electionEventId, verificationCardSetId), e);
		}

		if (!isSignatureValid) {
			throw new InvalidPayloadSignatureException(SetupComponentVoterAuthenticationDataPayload.class,
					String.format("[electionEventId: %s, verificationCardSetId: %s]", electionEventId, verificationCardSetId));
		}
	}

}
