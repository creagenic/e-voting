/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process.tally.mixonline;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
interface ControlComponentBallotBoxPayloadRepository extends CrudRepository<ControlComponentBallotBoxPayloadEntity, Long> {

	List<ControlComponentBallotBoxPayloadEntity> findByElectionEventIdAndBallotBoxId(final String electionEventId, final String ballotBoxId);
}
