/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process.tally.mixonline;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Arrays;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import jakarta.persistence.Version;

@Entity
@Table(name = "CONTROL_COMPONENT_VOTES_HASH_PAYLOAD")
public class ControlComponentVotesHashPayloadEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "controlComponentVotesHashPayloadSeq")
	@SequenceGenerator(name = "controlComponentVotesHashPayloadSeq", sequenceName = "CONTROL_COMPONENT_VOTES_HASH_PAYLOAD_SEQ", allocationSize = 1)
	private Long id;

	private String electionEventId;

	private String ballotBoxId;

	private int nodeId;

	@Column(name = "PAYLOAD")
	private byte[] controlComponentVotesHashPayload;

	@Version
	private int changeControlId;

	protected ControlComponentVotesHashPayloadEntity() {
		// Intentionally left blank.
	}

	public ControlComponentVotesHashPayloadEntity(final String electionEventId, final String ballotBoxId, final int nodeId,
			final byte[] controlComponentVotesHashPayload) {
		this.electionEventId = validateUUID(electionEventId);
		this.ballotBoxId = validateUUID(ballotBoxId);
		this.nodeId = nodeId;
		this.controlComponentVotesHashPayload =  Arrays.copyOf(checkNotNull(controlComponentVotesHashPayload), controlComponentVotesHashPayload.length);

		checkArgument(NODE_IDS.contains(nodeId), "The node id must be part of the known node ids. [nodeId: %s]", nodeId);
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public String getBallotBoxId() {
		return ballotBoxId;
	}

	public int getNodeId() {
		return nodeId;
	}

	public byte[] getControlComponentVotesHashPayload() {
		return Arrays.copyOf(controlComponentVotesHashPayload, controlComponentVotesHashPayload.length);
	}

}
