/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process.tally.mixonline;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.domain.tally.MixDecryptOnlinePayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentShufflePayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ControlComponentVotesHashPayload;
import ch.post.it.evoting.evotinglibraries.domain.tally.ControlComponentBallotBoxPayload;

@Service
public class MixnetPayloadService {

	private final ObjectMapper objectMapper;
	private final ShufflePayloadRepository shufflePayloadRepository;
	private final ControlComponentBallotBoxPayloadRepository controlComponentBallotBoxPayloadRepository;
	private final ControlComponentVotesHashPayloadRepository controlComponentVotesHashPayloadRepository;

	public MixnetPayloadService(
			final ObjectMapper objectMapper,
			final ShufflePayloadRepository shufflePayloadRepository,
			final ControlComponentBallotBoxPayloadRepository controlComponentBallotBoxPayloadRepository,
			final ControlComponentVotesHashPayloadRepository controlComponentVotesHashPayloadRepository) {
		this.objectMapper = objectMapper;
		this.shufflePayloadRepository = shufflePayloadRepository;
		this.controlComponentBallotBoxPayloadRepository = controlComponentBallotBoxPayloadRepository;
		this.controlComponentVotesHashPayloadRepository = controlComponentVotesHashPayloadRepository;
	}

	@Transactional
	public void saveControlComponentBallotBoxPayload(final ControlComponentBallotBoxPayload controlComponentBallotBoxPayload) {
		checkNotNull(controlComponentBallotBoxPayload);

		final String electionEventId = controlComponentBallotBoxPayload.getElectionEventId();
		final String ballotBoxId = controlComponentBallotBoxPayload.getBallotBoxId();
		final int nodeId = controlComponentBallotBoxPayload.getNodeId();

		final byte[] payload;
		try {
			payload = objectMapper.writeValueAsBytes(controlComponentBallotBoxPayload);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException("Failed to serialize control component ballot box payload.", e);
		}

		final ControlComponentBallotBoxPayloadEntity controlComponentBallotBoxPayloadEntity = new ControlComponentBallotBoxPayloadEntity(
				electionEventId, ballotBoxId, nodeId, payload);

		controlComponentBallotBoxPayloadRepository.save(controlComponentBallotBoxPayloadEntity);
	}

	@Transactional
	public void saveControlComponentVotesHashPayload(final ControlComponentVotesHashPayload controlComponentVotesHashPayload) {
		checkNotNull(controlComponentVotesHashPayload);

		final String electionEventId = controlComponentVotesHashPayload.getElectionEventId();
		final String ballotBoxId = controlComponentVotesHashPayload.getBallotBoxId();
		final int nodeId = controlComponentVotesHashPayload.getNodeId();

		try {
			final byte[] payload = objectMapper.writeValueAsBytes(controlComponentVotesHashPayload);

			final ControlComponentVotesHashPayloadEntity controlComponentVotesHashPayloadEntity = new ControlComponentVotesHashPayloadEntity(
					electionEventId, ballotBoxId, nodeId, payload);

			controlComponentVotesHashPayloadRepository.save(controlComponentVotesHashPayloadEntity);
		} catch (final JsonProcessingException e) {
			throw new UncheckedIOException("Failed to save the Control Component Votes Hash Payload.", e);
		}
	}

	@Transactional
	public void saveShufflePayload(final ControlComponentShufflePayload mixnetShufflePayload) {
		checkNotNull(mixnetShufflePayload);

		final String electionEventId = mixnetShufflePayload.getElectionEventId();
		final String ballotBoxId = mixnetShufflePayload.getBallotBoxId();
		final int nodeId = mixnetShufflePayload.getNodeId();

		try {
			final byte[] payload = objectMapper.writeValueAsBytes(mixnetShufflePayload);

			final ShufflePayloadEntity shufflePayloadEntity = new ShufflePayloadEntity(electionEventId, ballotBoxId, nodeId, payload);

			shufflePayloadRepository.save(shufflePayloadEntity);
		} catch (final JsonProcessingException ex) {
			throw new UncheckedIOException("Failed to process the mixing DTO", ex);
		}
	}

	@Transactional
	public int countMixDecryptOnlinePayloads(final String electionEventId, final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		return shufflePayloadRepository.countByElectionEventIdAndBallotBoxId(electionEventId, ballotBoxId);
	}

	@Transactional
	public int getMaxNodeId(final String electionEventId, final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		return shufflePayloadRepository.maxNodeId(electionEventId, ballotBoxId);
	}

	@Transactional
	public Optional<MixDecryptOnlinePayload> getMixnetPayloads(final String electionEventId, final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		final List<ControlComponentBallotBoxPayloadEntity> controlComponentBallotBoxPayloadEntities =
				controlComponentBallotBoxPayloadRepository.findByElectionEventIdAndBallotBoxId(electionEventId, ballotBoxId).stream()
						.sorted(Comparator.comparingInt(ControlComponentBallotBoxPayloadEntity::getNodeId))
						.toList();

		if (controlComponentBallotBoxPayloadEntities.size() != 4) {
			return Optional.empty();
		}

		final List<ShufflePayloadEntity> shufflePayloadEntities =
				shufflePayloadRepository.findByElectionEventIdAndBallotBoxIdOrderByNodeId(electionEventId, ballotBoxId).stream()
						.toList();

		if (shufflePayloadEntities.size() != 4) {
			return Optional.empty();
		}

		return Optional.of(
				createMixDecryptOnlinePayload(electionEventId, ballotBoxId, controlComponentBallotBoxPayloadEntities, shufflePayloadEntities));
	}

	@Transactional
	public List<ControlComponentShufflePayload> getControlComponentShufflePayloadsOrderByNodeId(final String electionEventId,
			final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		return shufflePayloadRepository.findByElectionEventIdAndBallotBoxIdOrderByNodeId(electionEventId, ballotBoxId)
				.stream()
				.map(ShufflePayloadEntity::getShufflePayload)
				.map(bytes -> {
					try {
						return objectMapper.readValue(bytes, ControlComponentShufflePayload.class);
					} catch (final IOException e) {
						throw new UncheckedIOException("Couldn't deserialize a ControlComponentShufflePayload", e);
					}
				})
				.toList();
	}

	@Transactional
	public List<ControlComponentVotesHashPayload> getControlComponentVotesHashPayloads(final String electionEventId,
			final String ballotBoxId) {
		validateUUID(electionEventId);
		validateUUID(ballotBoxId);

		return controlComponentVotesHashPayloadRepository.findByElectionEventIdAndBallotBoxId(electionEventId, ballotBoxId)
				.stream()
				.map(ControlComponentVotesHashPayloadEntity::getControlComponentVotesHashPayload)
				.map(bytes -> {
					try {
						return objectMapper.readValue(bytes, ControlComponentVotesHashPayload.class);
					} catch (final IOException e) {
						throw new UncheckedIOException("Couldn't deserialize a ControlComponentVotesHashPayload", e);
					}
				})
				.toList();
	}

	private MixDecryptOnlinePayload createMixDecryptOnlinePayload(final String electionEventId, final String ballotBoxId,
			final List<ControlComponentBallotBoxPayloadEntity> controlComponentBallotBoxPayloadEntities,
			final List<ShufflePayloadEntity> shufflePayloadEntities) {

		final List<ControlComponentBallotBoxPayload> controlComponentBallotBoxPayloads = controlComponentBallotBoxPayloadEntities.stream()
				.map(controlComponentBallotBoxPayloadEntity -> {
					try {
						return objectMapper.readValue(controlComponentBallotBoxPayloadEntity.getControlComponentBallotBoxPayload(),
								ControlComponentBallotBoxPayload.class);
					} catch (final IOException e) {
						throw new UncheckedIOException("Error deserializing confirmed encrypted votes payload.", e);
					}
				})
				.toList();

		final List<ControlComponentShufflePayload> controlComponentShufflePayloads = shufflePayloadEntities.stream()
				.map(shufflePayloadEntity -> {
					try {
						return objectMapper.readValue(shufflePayloadEntity.getShufflePayload(), ControlComponentShufflePayload.class);
					} catch (final IOException e) {
						throw new UncheckedIOException("Error deserialiazing ControlComponentShufflePayload", e);
					}
				})
				.toList();

		return new MixDecryptOnlinePayload(electionEventId, ballotBoxId, controlComponentBallotBoxPayloads, controlComponentShufflePayloads);

	}

}
