/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process.voting.authenticatevoter;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;

import java.util.List;

import org.springframework.stereotype.Service;

import ch.post.it.evoting.domain.configuration.SetupComponentVoterAuthenticationData;
import ch.post.it.evoting.domain.configuration.VerificationCardKeystore;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.votingserver.process.BallotData;
import ch.post.it.evoting.votingserver.process.BallotDataService;
import ch.post.it.evoting.votingserver.process.SetupComponentPublicKeysService;
import ch.post.it.evoting.votingserver.process.SetupComponentVerificationCardKeystoreService;
import ch.post.it.evoting.votingserver.process.VerificationCardService;
import ch.post.it.evoting.votingserver.process.VerificationCardSetService;
import ch.post.it.evoting.votingserver.process.VerificationCardState;
import ch.post.it.evoting.votingserver.process.VotingClientPublicKeys;
import ch.post.it.evoting.votingserver.process.configuration.setupvoting.VoterAuthenticationDataService;
import ch.post.it.evoting.votingserver.process.voting.VoterAuthenticationData;

@Service
public class AuthenticateVoterService {

	private final BallotDataService ballotDataService;
	private final VerificationCardService verificationCardService;

	private final VerificationCardSetService verificationCardSetService;
	private final VoterAuthenticationDataService voterAuthenticationDataService;
	private final SetupComponentPublicKeysService setupComponentPublicKeysService;
	private final SetupComponentVerificationCardKeystoreService setupComponentVerificationCardKeystoreService;

	public AuthenticateVoterService(
			final BallotDataService ballotDataService,
			final VerificationCardService verificationCardService,
			final VoterAuthenticationDataService voterAuthenticationDataService,
			final SetupComponentPublicKeysService setupComponentPublicKeysService,
			final SetupComponentVerificationCardKeystoreService setupComponentVerificationCardKeystoreService,
			final VerificationCardSetService verificationCardSetService) {
		this.ballotDataService = ballotDataService;
		this.verificationCardService = verificationCardService;
		this.verificationCardSetService = verificationCardSetService;
		this.voterAuthenticationDataService = voterAuthenticationDataService;
		this.setupComponentPublicKeysService = setupComponentPublicKeysService;
		this.setupComponentVerificationCardKeystoreService = setupComponentVerificationCardKeystoreService;
	}

	/**
	 * Returns the {@code AuthenticateVoterResponsePayload} containing the necessary data.
	 *
	 * @param electionEventId the election event id. Must be non-null and a valid UUID.
	 * @param credentialId    the credential id.    Must be non-null and a valid UUID.
	 * @return the payload containing the necessary data. It's content varies depending on if a re-login is performed or not.
	 * @throws NullPointerException      if any parameter is null.
	 * @throws FailedValidationException if the {@code electionEventId} is invalid.
	 */
	public AuthenticateVoterResponsePayload retrieveAuthenticateVoterPayload(final String electionEventId, final String credentialId) {
		validateUUID(credentialId);
		validateUUID(electionEventId);

		// Construct response VoterAuthenticationData, which does not contain the baseAuthenticationChallenge.
		final SetupComponentVoterAuthenticationData setupComponentVoterAuthenticationData = voterAuthenticationDataService.getVoterAuthenticationData(
				electionEventId, credentialId);

		final String verificationCardSetId = setupComponentVoterAuthenticationData.verificationCardSetId();
		final String verificationCardId = setupComponentVoterAuthenticationData.verificationCardId();
		final String ballotId = setupComponentVoterAuthenticationData.ballotId();
		final String votingCardSetId = setupComponentVoterAuthenticationData.votingCardSetId();
		final String ballotBoxId = setupComponentVoterAuthenticationData.ballotBoxId();
		final String votingCardId = setupComponentVoterAuthenticationData.votingCardId();
		final VoterAuthenticationData voterAuthenticationData = new VoterAuthenticationData(electionEventId, verificationCardSetId, votingCardSetId,
				ballotBoxId, ballotId, verificationCardId, votingCardId, credentialId);

		// Construct authenticate voter response payload depending on the verification card state (re-login).
		final VerificationCardState verificationCardState = verificationCardService.getVerificationCardState(credentialId);

		return switch (verificationCardState) {
			case INITIAL -> {
				final BallotData ballotData = ballotDataService.getBallotData(electionEventId, ballotId);
				final VoterMaterial voterMaterial = new VoterMaterial(ballotData.ballot(), ballotData.ballotTextsJson());

				final VerificationCardKeystore verificationCardKeystore = setupComponentVerificationCardKeystoreService.loadVerificationCardKeystore(
						electionEventId, verificationCardSetId, verificationCardId);

				final VotingClientPublicKeys votingClientPublicKeys = setupComponentPublicKeysService.getVotingClientPublicKeys(electionEventId);

				final PrimesMappingTable primesMappingTable = verificationCardSetService.getVerificationCardSetEntity(verificationCardSetId)
						.getPrimesMappingTable();

				yield new AuthenticateVoterResponsePayload(verificationCardState, voterMaterial, voterAuthenticationData, verificationCardKeystore,
						votingClientPublicKeys, primesMappingTable);
			}
			case SENT -> {
				final BallotData ballotData = ballotDataService.getBallotData(electionEventId, ballotId);
				final List<String> shortChoiceReturnCodes = verificationCardService.getShortChoiceReturnCodes(credentialId);
				final VoterMaterial voterMaterial = new VoterMaterial(ballotData.ballot(), ballotData.ballotTextsJson(), shortChoiceReturnCodes);

				final VerificationCardKeystore verificationCardKeystore = setupComponentVerificationCardKeystoreService.loadVerificationCardKeystore(
						electionEventId, verificationCardSetId, verificationCardId);

				final VotingClientPublicKeys votingClientPublicKeys = setupComponentPublicKeysService.getVotingClientPublicKeys(electionEventId);

				final PrimesMappingTable primesMappingTable = verificationCardSetService.getVerificationCardSetEntity(verificationCardSetId)
						.getPrimesMappingTable();

				yield new AuthenticateVoterResponsePayload(verificationCardState, voterMaterial, voterAuthenticationData, verificationCardKeystore,
						votingClientPublicKeys, primesMappingTable);
			}
			case CONFIRMED -> {
				final VoterMaterial voterMaterial = new VoterMaterial(verificationCardService.getShortVoteCastReturnCode(credentialId));

				yield new AuthenticateVoterResponsePayload(verificationCardState, voterMaterial);
			}
			default -> throw new IllegalStateException(
					String.format("Invalid verification card state. [verificationCardId: %s, verificationCardState: %s]", verificationCardId,
							verificationCardState));
		};
	}

}
