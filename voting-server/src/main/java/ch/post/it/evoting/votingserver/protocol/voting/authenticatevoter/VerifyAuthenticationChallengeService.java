/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.protocol.voting.authenticatevoter;

import static ch.post.it.evoting.evotinglibraries.domain.validations.Validations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.domain.configuration.SetupComponentVoterAuthenticationData;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.votingserver.process.VerificationCardService;
import ch.post.it.evoting.votingserver.process.VerificationCardSetEntity;
import ch.post.it.evoting.votingserver.process.VerificationCardState;
import ch.post.it.evoting.votingserver.process.configuration.setupvoting.VoterAuthenticationDataService;
import ch.post.it.evoting.votingserver.process.voting.AuthenticationChallenge;
import ch.post.it.evoting.votingserver.process.voting.AuthenticationStep;
import ch.post.it.evoting.votingserver.process.voting.VerifyAuthenticationChallengeException;
import ch.post.it.evoting.votingserver.protocol.voting.authenticatevoter.VerifyAuthenticationChallengeOutput.VerifyAuthenticationChallengeStatus;

@Service
public class VerifyAuthenticationChallengeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VerifyAuthenticationChallengeService.class);

	private final VerificationCardService verificationCardService;
	private final VoterAuthenticationDataService voterAuthenticationDataService;
	private final VerifyAuthenticationChallengeAlgorithm verifyAuthenticationChallengeAlgorithm;

	public VerifyAuthenticationChallengeService(
			final VerificationCardService verificationCardService,
			final VoterAuthenticationDataService voterAuthenticationDataService,
			final VerifyAuthenticationChallengeAlgorithm verifyAuthenticationChallengeAlgorithm) {
		this.verificationCardService = verificationCardService;
		this.voterAuthenticationDataService = voterAuthenticationDataService;
		this.verifyAuthenticationChallengeAlgorithm = verifyAuthenticationChallengeAlgorithm;
	}

	/**
	 * Verifies the authentication challenge for the given {@code credentialId}. Invokes the algorithm VerifyAuthenticationChallenge.
	 *
	 * @param electionEventId         the election event id.
	 * @param authenticationStep      the {@link AuthenticationStep} for which to verify the challenge.
	 * @param authenticationChallenge the {@link AuthenticationChallenge} containing the credential id and derived authentication challenge.
	 * @throws NullPointerException                   if any parameter is null.
	 * @throws FailedValidationException              if {@code electionEventId} is invalid.
	 * @throws VerifyAuthenticationChallengeException if the verification of the authentication challenge is unsuccessful for the given
	 *                                                {@code authenticationChallenge}.
	 */
	public void verifyAuthenticationChallenge(final String electionEventId, final AuthenticationStep authenticationStep,
			final AuthenticationChallenge authenticationChallenge) {

		validateUUID(electionEventId);
		checkNotNull(authenticationStep);
		checkNotNull(authenticationChallenge);

		final String credentialId = authenticationChallenge.derivedVoterIdentifier();

		// Check that verification card is not blocked or in an incoherent state with respect to the current authenticationStep.
		final VerificationCardState verificationCardState = verificationCardService.getVerificationCardState(credentialId);
		VerificationCardState.validateVerificationCardState(authenticationStep, verificationCardState);

		// Check that the ballot box is still opened.
		validateBallotBoxTime(authenticationStep, credentialId);

		// Retrieve base authentication challenge.
		final SetupComponentVoterAuthenticationData voterAuthenticationData = voterAuthenticationDataService.getVoterAuthenticationData(
				electionEventId, credentialId);
		final String baseAuthenticationChallenge = voterAuthenticationData.baseAuthenticationChallenge();

		// Verify authentication challenge.
		final String derivedAuthenticationChallenge = authenticationChallenge.derivedAuthenticationChallenge();
		final BigInteger authenticationNonce = authenticationChallenge.authenticationNonce();

		final VerifyAuthenticationChallengeContext context = new VerifyAuthenticationChallengeContext(electionEventId, credentialId);
		final VerifyAuthenticationChallengeInput input = new VerifyAuthenticationChallengeInput(authenticationStep, derivedAuthenticationChallenge,
				baseAuthenticationChallenge, authenticationNonce);

		LOGGER.debug("Performing VerifyAuthenticationChallenge algorithm... [electionEventId: {}, credentialId: {}]", electionEventId, credentialId);

		final VerifyAuthenticationChallengeOutput output = verifyAuthenticationChallengeAlgorithm.verifyAuthenticationChallenge(context, input);

		final VerifyAuthenticationChallengeStatus status = output.getStatus();

		if (!VerifyAuthenticationChallengeStatus.SUCCESS.equals(status)) {
			final String errorMessage = String.format(
					"Verification of authentication challenge failed. [electionEventId: %s, credentialId: %s, reason: %s]", electionEventId,
					credentialId, output.getErrorMessage());
			throw new VerifyAuthenticationChallengeException(status, output.getAttemptsLeft(), errorMessage);
		}

		LOGGER.info("Successfully verified authentication challenge. [electionEventId: {}, credentialId: {}]", electionEventId, credentialId);
	}

	private void validateBallotBoxTime(final AuthenticationStep authenticationStep, final String credentialId) {
		final VerificationCardSetEntity verificationCardSetEntity = verificationCardService.getVerificationCardSetEntity(credentialId);

		final LocalDateTime now = LocalDateTime.now();
		final LocalDateTime ballotBoxStartTime = verificationCardSetEntity.getBallotBoxStartTime();
		// The voter can still vote and confirm during the grace period window after the ballot box is closed.
		LocalDateTime ballotBoxFinishTime = verificationCardSetEntity.getBallotBoxFinishTime();
		if (AuthenticationStep.SEND_VOTE.equals(authenticationStep) || AuthenticationStep.CONFIRM_VOTE.equals(authenticationStep)) {
			ballotBoxFinishTime = ballotBoxFinishTime.plusSeconds(verificationCardSetEntity.getGracePeriod());
		}

		if (now.isBefore(ballotBoxStartTime)) {
			final String errorMessage = String.format("The ballot box is not open yet. [step: %s, credentialId: %s, ballotBoxId: %s]",
					authenticationStep, credentialId, verificationCardSetEntity.getBallotBoxId());
			throw new VerifyAuthenticationChallengeException(VerifyAuthenticationChallengeStatus.BALLOT_BOX_NOT_STARTED, errorMessage);
		} else if (now.isAfter(ballotBoxFinishTime)) {
			final String errorMessage = String.format("The ballot box is closed. [step: %s, credentialId: %s, ballotBoxId: %s]", authenticationStep,
					credentialId, verificationCardSetEntity.getBallotBoxId());
			throw new VerifyAuthenticationChallengeException(VerifyAuthenticationChallengeStatus.BALLOT_BOX_ENDED, errorMessage);
		}
	}

}
