/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.shelf;

import static com.google.common.base.Preconditions.checkNotNull;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.post.it.evoting.votingserver.messaging.Serializer;

@Service
public class WorkflowShelfService {

	private final WorkflowShelfRepository repository;
	private final Serializer serializer;

	public WorkflowShelfService(WorkflowShelfRepository repository, Serializer serializer) {
		this.repository = repository;
		this.serializer = serializer;
	}

	/**
	 * Push a data to a shelf in database
	 *
	 * @param id the key identifying the data that will be shelf
	 * @param data          the data to be shelved.
	 * @param <T>           the type of the data
	 */
	@Transactional
	public <T> void pushToShelf(final String id, final T data) {
		checkNotNull(id);
		checkNotNull(data);

		byte[] dataAsBytes = serializer.serialize(data);

		WorkflowShelfEntity entity = new WorkflowShelfEntity(id, dataAsBytes);

		repository.save(entity);
	}

	/**
	 * Pull data from shelf in database
	 *
	 * @param id the key identifying the data to retrieve from shelf
	 * @param clazz         the clazz of the data to be pull
	 * @param <T>           the type of the data
	 * @return the data
	 */
	@Transactional
	public <T> T pullFromShelf(final String id, Class<T> clazz) {
		checkNotNull(id);
		checkNotNull(clazz);

		final WorkflowShelfEntity entity = repository.findById(id)
				.orElseThrow(() -> new IllegalStateException(String.format("No shelf with this correlationId. [id: %s]", id)));

		repository.deleteById(id);

		return serializer.deserialize(entity.getShelfData(), clazz);
	}
}
