/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base64Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.election.Ballot;
import ch.post.it.evoting.evotinglibraries.domain.election.Contest;
import ch.post.it.evoting.evotinglibraries.domain.election.Identifier;

@DisplayName("BallotData calling")
class BallotDataTest {

	private static final Random RANDOM = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final Alphabet base64Alphabet = Base64Alphabet.getInstance();

	private Ballot ballot;
	private String ballotTextJson;

	@BeforeEach
	void setup() {
		final String id = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String defaultTitle = String.join("_", "ballot", RANDOM.genRandomString(5, base64Alphabet));
		final String defaultDescription = String.join("_", "ballot", RANDOM.genRandomString(5, base64Alphabet));
		final String alias = RANDOM.genRandomString(5, base16Alphabet);
		final String status = RANDOM.genRandomString(5, base16Alphabet);
		final Identifier electionEvent = new Identifier(RANDOM.genRandomString(ID_LENGTH, base16Alphabet));
		final Contest contest = mock(Contest.class);
		doReturn(RANDOM.genRandomString(ID_LENGTH, base16Alphabet)).when(contest).id();
		doReturn(RANDOM.genRandomString(5, base64Alphabet)).when(contest).alias();
		final List<Contest> contests = List.of(contest);
		ballot = new Ballot(id, defaultTitle, defaultDescription, alias, status, electionEvent, contests);
		ballotTextJson = RANDOM.genRandomString(100, base64Alphabet);
	}

	@DisplayName("constructor with null arguments throws a NullPointerException")
	@Test
	void constructWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class, () -> new BallotData(null, ballotTextJson));
		assertThrows(NullPointerException.class, () -> new BallotData(ballot, null));
	}

	@DisplayName("constructor with valid arguments creates BallotData")
	@Test
	void constructWithValidArgumentsCreates() {
		final BallotData ballotData = assertDoesNotThrow(() -> new BallotData(ballot, ballotTextJson));
		assertEquals(ballot, ballotData.ballot());
		assertEquals(ballotTextJson, ballotData.ballotTextsJson());
	}
}
