/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.security.SignatureException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.cryptoprimitives.signing.SignatureKeystore;
import ch.post.it.evoting.evotinglibraries.domain.election.ElectionEventContext;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.ElectionEventContextPayload;
import ch.post.it.evoting.evotinglibraries.domain.mixnet.generators.ElectionEventContextPayloadGenerator;
import ch.post.it.evoting.evotinglibraries.domain.signature.Alias;

@Service
class ElectionEventContextServiceTest {

	private static ElectionEventContextService electionEventContextService;
	private static ElectionEventContextPayload electionEventContextPayload;

	@BeforeAll
	static void beforeAll() throws SignatureException {
		final ElectionEventContextPayloadGenerator electionEventContextPayloadGenerator = new ElectionEventContextPayloadGenerator();
		electionEventContextPayload = electionEventContextPayloadGenerator.generate();

		final ElectionEventContext electionEventContext = electionEventContextPayload.getElectionEventContext();
		final String electionEventId = electionEventContext.electionEventId();
		final ElectionEventEntity electionEventEntity = new ElectionEventEntity(electionEventId, electionEventContextPayload.getEncryptionGroup());

		final ElectionEventService electionEventService = mock(ElectionEventService.class);
		final VerificationCardSetService verificationCardSetService = mock(VerificationCardSetService.class);
		final ElectionEventContextRepository electionEventContextRepository = mock(ElectionEventContextRepository.class);

		final SignatureKeystore<Alias> signatureKeystore = mock(SignatureKeystore.class);

		when(electionEventService.retrieveElectionEventEntity(anyString())).thenReturn(electionEventEntity);
		when(electionEventContextRepository.save(any()))
				.thenReturn(new ElectionEventContextEntity(electionEventEntity, electionEventContext.startTime(), electionEventContext.finishTime(),
						electionEventContext.electionEventAlias(), electionEventContext.electionEventDescription()));

		electionEventContextService = spy(new ElectionEventContextService(electionEventService, verificationCardSetService,
				electionEventContextRepository, signatureKeystore));

		doReturn(true).when(signatureKeystore).verifySignature(any(), any(), any(), any());
	}

	@DisplayName("saveElectionEventContext with valid arguments does not throw")
	@Test
	void saveElectionEventContextWithValidArgumentsDoesNotThrow() {
		assertDoesNotThrow(() -> electionEventContextService.saveElectionEventContext(electionEventContextPayload));
	}

	@DisplayName("saveElectionEventContext with null argument throws a NullPointerException")
	@Test
	void saveElectionEventContextWithNullArgumentThrows() {
		assertThrows(NullPointerException.class, () -> electionEventContextService.saveElectionEventContext(null));
	}
}
