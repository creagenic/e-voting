/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.configuration.VerificationCardKeystore;

@DisplayName("SetupComponentVerificationCardKeystoreService")
@ExtendWith(MockitoExtension.class)
class SetupComponentVerificationCardKeystoreServiceTest {
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final String DUMMY_UUID = RandomFactory.createRandom().genRandomString(ID_LENGTH, base16Alphabet);

	private final ObjectMapper objectMapper = new ObjectMapper();
	private SetupComponentVerificationCardKeystoreRepository setupComponentVerificationCardKeystoreRepository;
	private ElectionEventContextService electionEventContextService;
	private SetupComponentVerificationCardKeystoreService setupComponentVerificationCardKeystoreService;
	private VerificationCardService verificationCardSetService;

	@BeforeEach
	void setUp() {
		setupComponentVerificationCardKeystoreRepository = Mockito.mock(SetupComponentVerificationCardKeystoreRepository.class);
		electionEventContextService = Mockito.mock(ElectionEventContextService.class);
		final ElectionEventService electionEventService = Mockito.mock(ElectionEventService.class);
		verificationCardSetService = mock(VerificationCardService.class);
		final IdentifierValidationService identifierValidationService = mock(IdentifierValidationService.class);

		doNothing().when(identifierValidationService).validateContextIds(any());

		setupComponentVerificationCardKeystoreService = new SetupComponentVerificationCardKeystoreService(objectMapper,
				verificationCardSetService, setupComponentVerificationCardKeystoreRepository, electionEventContextService, electionEventService,
				identifierValidationService);
	}

	@ParameterizedTest
	@MethodSource("loadVerificationCardKeystoreDoesNotThrowIfInValidPeriodProvider")
	void loadVerificationCardKeystoreDoesNotThrowIfInValidPeriod(final LocalDateTime now, final LocalDateTime start, final LocalDateTime finish)
			throws JsonProcessingException {
		setupCommonMocks(start, finish);

		final var setupComponentVerificationCardKeystoreEntity = new SetupComponentVerificationCardKeystoreEntity(
				new VerificationCardEntity(), objectMapper.writeValueAsBytes(new VerificationCardKeystore(DUMMY_UUID, DUMMY_UUID)));
		when(setupComponentVerificationCardKeystoreRepository.findById(any())).thenReturn(Optional.of(setupComponentVerificationCardKeystoreEntity));

		assertDoesNotThrow(
				() -> setupComponentVerificationCardKeystoreService.loadVerificationCardKeystore(DUMMY_UUID, DUMMY_UUID, DUMMY_UUID, () -> now));
	}

	public static Stream<Arguments> loadVerificationCardKeystoreDoesNotThrowIfInValidPeriodProvider() {
		final LocalDateTime now = LocalDateTime.of(2022, 1, 1, 15, 0);
		return Stream.of(
				Arguments.of(now, now.minusDays(5), now.plusDays(5)),
				Arguments.of(now, now, now)
		);
	}

	@ParameterizedTest
	@MethodSource("loadVerificationCardKeystoreThrowsIfInInvalidPeriodProvider")
	void loadVerificationCardKeystoreThrowsIfInInvalidPeriod(final LocalDateTime now, final LocalDateTime start, final LocalDateTime finish) {
		setupCommonMocks(start, finish);

		final IllegalStateException illegalStateException = assertThrows(IllegalStateException.class,
				() -> setupComponentVerificationCardKeystoreService.loadVerificationCardKeystore(DUMMY_UUID, DUMMY_UUID, DUMMY_UUID, () -> now));

		final String errorMessage = String.format(
				"Cannot load verification card keystore outside the opened election time window. [electionEventId: %s, verificationCardId: %s, startTime: %s, finishTime: %s]",
				DUMMY_UUID, DUMMY_UUID, start, finish);
		assertEquals(errorMessage, Throwables.getRootCause(illegalStateException).getMessage());
	}

	public static Stream<Arguments> loadVerificationCardKeystoreThrowsIfInInvalidPeriodProvider() {
		final LocalDateTime now = LocalDateTime.of(2022, 1, 1, 15, 0);
		return Stream.of(
				Arguments.of(now, now.minusDays(10), now.minusDays(5)),
				Arguments.of(now, now.plusSeconds(1), now.plusDays(10)),
				Arguments.of(now, now.minusDays(10), now.minusSeconds(1))
		);
	}

	private void setupCommonMocks(final LocalDateTime start, final LocalDateTime finish) {
		final var electionEventContextEntityMock = mock(ElectionEventContextEntity.class);
		when(electionEventContextEntityMock.getStartTime()).thenReturn(start);
		when(electionEventContextEntityMock.getFinishTime()).thenReturn(finish);
		when(electionEventContextService.getElectionEventContextEntity(any())).thenReturn(electionEventContextEntityMock);
	}
}
