/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.votingserver.cardmanagementapi.UsedVotingCardDTO;

class UsedVotingCardDTOTest {

	private static final Random RANDOM = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private String electionEventId;
	private String verificationCardSetId;
	private String verificationCardId;
	private String votingCardId;
	private VerificationCardState votingCardState;
	private LocalDateTime votingCardStateDate;

	@BeforeEach
	void setup() {
		electionEventId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		verificationCardSetId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		verificationCardId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		votingCardId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		votingCardState = VerificationCardState.INITIAL;
		votingCardStateDate = LocalDateTime.now();
	}

	@Test
	void constructWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class, () -> new UsedVotingCardDTO(null, verificationCardSetId, verificationCardId, votingCardId,
				votingCardState, votingCardStateDate));
		assertThrows(NullPointerException.class,
				() -> new UsedVotingCardDTO(electionEventId, null, verificationCardId, votingCardId, votingCardState, votingCardStateDate));
		assertThrows(NullPointerException.class, () -> new UsedVotingCardDTO(electionEventId, verificationCardSetId, null, votingCardId,
				votingCardState, votingCardStateDate));
		assertThrows(NullPointerException.class, () -> new UsedVotingCardDTO(electionEventId, verificationCardSetId, verificationCardId, null,
				votingCardState, votingCardStateDate));
		assertThrows(NullPointerException.class,
				() -> new UsedVotingCardDTO(electionEventId, verificationCardSetId, verificationCardId, votingCardId, null, votingCardStateDate));
		assertThrows(NullPointerException.class, () -> new UsedVotingCardDTO(electionEventId, verificationCardSetId, verificationCardId, votingCardId,
				votingCardState, null));
	}

	@Test
	void constructWithInvalidUuidsThrows() {
		assertThrows(FailedValidationException.class, () -> new UsedVotingCardDTO("badId", verificationCardSetId, verificationCardId, votingCardId,
				votingCardState, votingCardStateDate));
		assertThrows(FailedValidationException.class, () -> new UsedVotingCardDTO(electionEventId, "badId", verificationCardId, votingCardId,
				votingCardState, votingCardStateDate));
		assertThrows(FailedValidationException.class, () -> new UsedVotingCardDTO(electionEventId, verificationCardSetId, "badId", votingCardId,
				votingCardState, votingCardStateDate));
		assertThrows(FailedValidationException.class, () -> new UsedVotingCardDTO(electionEventId, verificationCardSetId, verificationCardId, "badId",
				votingCardState, votingCardStateDate));
	}

	@Test
	void constructWithValidArgumentsDoesNotThrow() {
		final UsedVotingCardDTO usedVotingCardDTO = assertDoesNotThrow(
				() -> new UsedVotingCardDTO(electionEventId, verificationCardSetId, verificationCardId, votingCardId, votingCardState,
						votingCardStateDate));
		assertEquals(electionEventId, usedVotingCardDTO.electionEventId());
		assertEquals(verificationCardSetId, usedVotingCardDTO.verificationCardSetId());
		assertEquals(verificationCardId, usedVotingCardDTO.verificationCardId());
		assertEquals(votingCardId, usedVotingCardDTO.votingCardId());
		assertEquals(votingCardState, usedVotingCardDTO.votingCardState());
		assertEquals(votingCardStateDate, usedVotingCardDTO.votingCardStateDate());
	}
}
