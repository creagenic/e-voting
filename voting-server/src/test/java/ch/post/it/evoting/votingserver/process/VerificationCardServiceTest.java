/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base64Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.configuration.SetupComponentVoterAuthenticationData;
import ch.post.it.evoting.votingserver.cardmanagementapi.TooManyVerificationCardsException;
import ch.post.it.evoting.votingserver.process.voting.VerifyAuthenticationChallengeException;
import ch.post.it.evoting.votingserver.protocol.voting.authenticatevoter.VerifyAuthenticationChallengeOutput;

@DisplayName("VerificationCardService calling")
class VerificationCardServiceTest {

	private static final Random RANDOM = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final Alphabet base64Alphabet = Base64Alphabet.getInstance();
	private static final int VOTING_CARD_LIMIT = 5;

	private static VerificationCardRepository verificationCardRepository;
	private static VerificationCardStateRepository verificationCardStateRepository;
	private static VerificationCardService verificationCardService;

	private String verificationCardId;
	private String credentialId;
	private String votingCardId;

	@BeforeAll
	static void setupAll() {
		verificationCardRepository = mock(VerificationCardRepository.class);
		verificationCardStateRepository = mock(VerificationCardStateRepository.class);
		final VerificationCardStateService verificationCardStateService = new VerificationCardStateService(verificationCardStateRepository);
		verificationCardService = new VerificationCardService(verificationCardRepository, verificationCardStateService, VOTING_CARD_LIMIT);
	}

	@BeforeEach
	void setup() {
		verificationCardId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final VerificationCardStateEntity verificationCardStateEntity = new VerificationCardStateEntity(verificationCardId);
		doReturn(Optional.of(verificationCardStateEntity)).when(verificationCardStateRepository).findById(verificationCardId);

		final String electionEventId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String verificationCardSetId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String votingCardSetId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String ballotBoxId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String ballotId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		credentialId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		votingCardId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final String baseAuthenticationChallenge = RANDOM.genRandomString(44, base64Alphabet);
		final VerificationCardSetEntity verificationCardSetEntity = new VerificationCardSetEntity();
		final SetupComponentVoterAuthenticationData voterAuthenticationData = new SetupComponentVoterAuthenticationData(electionEventId,
				verificationCardSetId, votingCardSetId, ballotBoxId, ballotId, verificationCardId, votingCardId, credentialId,
				baseAuthenticationChallenge);
		final VerificationCardEntity verificationCardEntity = new VerificationCardEntity(verificationCardId, verificationCardSetEntity, credentialId,
				votingCardId, voterAuthenticationData, verificationCardStateEntity);
		doReturn(Optional.of(verificationCardEntity)).when(verificationCardRepository).findById(verificationCardId);
		doReturn(Optional.of(verificationCardEntity)).when(verificationCardRepository).findByVotingCardId(votingCardId);
		doReturn(Optional.of(verificationCardEntity)).when(verificationCardRepository).findByCredentialId(credentialId);
		doReturn(List.of(verificationCardEntity)).when(verificationCardRepository).findAllByContainingPartialId(votingCardId.substring(0, 3));
	}

	@Nested
	@DisplayName("incrementConfirmationAttempts")
	class IncrementConfirmationAttemptsTest {

		@Test
		@DisplayName("when voting card state is AUTHENTICATION_ATTEMPTS_EXCEEDED then throws IllegalStateException")
		void incrementConfirmationAttemptsWhenStateAuthenticationAtemptsExceededThrows() {
			verificationCardService.incrementAuthenticationAttempts(credentialId);
			verificationCardService.incrementAuthenticationAttempts(credentialId);
			verificationCardService.incrementAuthenticationAttempts(credentialId);
			verificationCardService.incrementAuthenticationAttempts(credentialId);
			verificationCardService.incrementAuthenticationAttempts(credentialId);

			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> verificationCardService.incrementConfirmationAttempts(verificationCardId));
			final String expectedErrorMessage = String.format(
					"The current state does not allow to increment confirmation attempts. [verificationCardId: %s, verificationCardState: %s]",
					verificationCardId, VerificationCardState.AUTHENTICATION_ATTEMPTS_EXCEEDED);
			assertEquals(expectedErrorMessage, exception.getMessage());
		}

		@Test
		@DisplayName("when voting card state is INITIAL then throws IllegalStateException")
		void incrementConfirmationAttemptsWhenStateInitialThrows() {
			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> verificationCardService.incrementConfirmationAttempts(verificationCardId));
			final String expectedErrorMessage = String.format(
					"The current state does not allow to increment confirmation attempts. [verificationCardId: %s, verificationCardState: %s]",
					verificationCardId, VerificationCardState.INITIAL);
			assertEquals(expectedErrorMessage, exception.getMessage());
		}

		@Test
		@DisplayName("when voting card state is SENT then throws IllegalStateException")
		void incrementConfirmationAttemptsWhenStateSentThrows() {
			final List<String> shortChoiceReturnCodes = RANDOM.genUniqueDecimalStrings(4, 5);
			verificationCardService.saveSentState(verificationCardId, shortChoiceReturnCodes);
			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> verificationCardService.incrementConfirmationAttempts(verificationCardId));
			final String expectedErrorMessage = String.format(
					"The current state does not allow to increment confirmation attempts. [verificationCardId: %s, verificationCardState: %s]",
					verificationCardId, VerificationCardState.SENT);
			assertEquals(expectedErrorMessage, exception.getMessage());
		}

		@Test
		@DisplayName("when voting card state is CONFIRMING then increments confirmation attempts by 1")
		void incrementConfirmationAttemptsWhenStateConfirmingIncrements() {
			final List<String> shortChoiceReturnCodes = RANDOM.genUniqueDecimalStrings(4, 5);
			verificationCardService.saveSentState(verificationCardId, shortChoiceReturnCodes);
			verificationCardService.saveConfirmingState(verificationCardId);
			final Integer confirmationAttempts = assertDoesNotThrow(() -> verificationCardService.incrementConfirmationAttempts(verificationCardId));
			assertEquals(1, confirmationAttempts);
		}

		@Test
		@DisplayName("when voting card state is CONFIRMED then throws IllegalStateException")
		void incrementConfirmationAttemptsWhenStateConfirmedThrows() {
			final List<String> shortChoiceReturnCodes = RANDOM.genUniqueDecimalStrings(4, 5);
			verificationCardService.saveSentState(verificationCardId, shortChoiceReturnCodes);
			verificationCardService.saveConfirmingState(verificationCardId);
			final String shortVoteCastReturnCode = RANDOM.genUniqueDecimalStrings(8, 1).get(0);
			verificationCardService.saveConfirmedState(verificationCardId, shortVoteCastReturnCode);
			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> verificationCardService.incrementConfirmationAttempts(verificationCardId));
			final String expectedErrorMessage = String.format(
					"The current state does not allow to increment confirmation attempts. [verificationCardId: %s, verificationCardState: %s]",
					verificationCardId, VerificationCardState.CONFIRMED);
			assertEquals(expectedErrorMessage, exception.getMessage());
		}
	}

	@Nested
	@DisplayName("saveConfirmedState")
	class SaveConfirmedStateTest {

		@Test
		@DisplayName("with state AUTHENTICATION_ATTEMPTS_EXCEEDED throws a VerifyAuthenticationChallengeException")
		void saveConfirmedStateWhenCurrentStateIsAuthenticationAttemptsExceededThrows() {
			verificationCardService.incrementAuthenticationAttempts(credentialId);
			verificationCardService.incrementAuthenticationAttempts(credentialId);
			verificationCardService.incrementAuthenticationAttempts(credentialId);
			verificationCardService.incrementAuthenticationAttempts(credentialId);
			verificationCardService.incrementAuthenticationAttempts(credentialId);

			final String shortVoteCastReturnCode = RANDOM.genUniqueDecimalStrings(8, 1).get(0);
			final VerifyAuthenticationChallengeException exception = assertThrows(
					VerifyAuthenticationChallengeException.class,
					() -> verificationCardService.saveConfirmedState(verificationCardId, shortVoteCastReturnCode));
			assertEquals(VerifyAuthenticationChallengeOutput.VerifyAuthenticationChallengeStatus.VOTING_CARD_BLOCKED, exception.getErrorStatus());

			final VerificationCardState verificationCardState = verificationCardService.getVerificationCardEntity(verificationCardId)
					.getVerificationCardStateEntity().getState();
			assertEquals(VerificationCardState.AUTHENTICATION_ATTEMPTS_EXCEEDED, verificationCardState);
		}

		@Test
		@DisplayName("with state SENT saves CONFIRMED state")
		void saveConfirmedStateWhenCurrentStateIsSentChangesStateToConfirming() {
			final List<String> shortChoiceReturnCodes = RANDOM.genUniqueDecimalStrings(4, 5);
			verificationCardService.saveSentState(verificationCardId, shortChoiceReturnCodes);

			final String shortVoteCastReturnCode = RANDOM.genUniqueDecimalStrings(8, 1).get(0);
			assertDoesNotThrow(() -> verificationCardService.saveConfirmedState(verificationCardId, shortVoteCastReturnCode));

			final VerificationCardState verificationCardState = verificationCardService.getVerificationCardEntity(verificationCardId)
					.getVerificationCardStateEntity().getState();
			assertEquals(VerificationCardState.CONFIRMED, verificationCardState);
		}

		@Test
		@DisplayName("with state CONFIRMING saves CONFIRMED state")
		void saveConfirmedStateWhenCurrentStateIsConfirmingChangesStateToConfirmed() {
			final List<String> shortChoiceReturnCodes = RANDOM.genUniqueDecimalStrings(4, 5);
			verificationCardService.saveSentState(verificationCardId, shortChoiceReturnCodes);
			verificationCardService.saveConfirmingState(verificationCardId);

			final String shortVoteCastReturnCode = RANDOM.genUniqueDecimalStrings(8, 1).get(0);
			assertDoesNotThrow(() -> verificationCardService.saveConfirmedState(verificationCardId, shortVoteCastReturnCode));

			final VerificationCardState verificationCardState = verificationCardService.getVerificationCardEntity(verificationCardId)
					.getVerificationCardStateEntity().getState();
			assertEquals(VerificationCardState.CONFIRMED, verificationCardState);
		}

		@Test
		@DisplayName("with state CONFIRMED throws a VerifyAuthenticationChallengeException")
		void saveConfirmedStateWhenCurrentStateIsConfirmedThrows() {
			verificationCardService.blockVotingCard(votingCardId);

			final String shortVoteCastReturnCode = RANDOM.genUniqueDecimalStrings(8, 1).get(0);
			final VerifyAuthenticationChallengeException exception = assertThrows(VerifyAuthenticationChallengeException.class,
					() -> verificationCardService.saveConfirmedState(verificationCardId, shortVoteCastReturnCode));
			assertEquals(VerifyAuthenticationChallengeOutput.VerifyAuthenticationChallengeStatus.VOTING_CARD_BLOCKED, exception.getErrorStatus());

			final VerificationCardState verificationCardState = verificationCardService.getVerificationCardEntity(verificationCardId)
					.getVerificationCardStateEntity().getState();
			assertEquals(VerificationCardState.BLOCKED, verificationCardState);
		}

		@Test
		@DisplayName("with state BLOCKED throws a VerifyAuthenticationChallengeException")
		void saveConfirmedStateWhenCurrentStateIsBlockedThrows() {
			verificationCardService.blockVotingCard(votingCardId);

			final String shortVoteCastReturnCode = RANDOM.genUniqueDecimalStrings(8, 1).get(0);
			final VerifyAuthenticationChallengeException exception = assertThrows(VerifyAuthenticationChallengeException.class,
					() -> verificationCardService.saveConfirmedState(verificationCardId, shortVoteCastReturnCode));
			assertEquals(VerifyAuthenticationChallengeOutput.VerifyAuthenticationChallengeStatus.VOTING_CARD_BLOCKED, exception.getErrorStatus());

			final VerificationCardState verificationCardState = verificationCardService.getVerificationCardEntity(verificationCardId)
					.getVerificationCardStateEntity().getState();
			assertEquals(VerificationCardState.BLOCKED, verificationCardState);
		}

		@Test
		@DisplayName("with state CONFIRMATION_ATTEMPTS_EXCEEDED throws a VerifyAuthenticationChallengeException")
		void saveConfirmedStateWhenCurrentStateIsConfirmationAttemptsExceededThrows() {
			final List<String> shortChoiceReturnCodes = RANDOM.genUniqueDecimalStrings(4, 5);
			verificationCardService.saveSentState(verificationCardId, shortChoiceReturnCodes);

			for (int i = 0; i < 5; i++) {
				verificationCardService.saveConfirmingState(verificationCardId);
				verificationCardService.incrementConfirmationAttempts(verificationCardId);
			}

			final String shortVoteCastReturnCode = RANDOM.genUniqueDecimalStrings(8, 1).get(0);
			final VerifyAuthenticationChallengeException exception = assertThrows(VerifyAuthenticationChallengeException.class,
					() -> verificationCardService.saveConfirmedState(verificationCardId, shortVoteCastReturnCode));
			assertEquals(VerifyAuthenticationChallengeOutput.VerifyAuthenticationChallengeStatus.VOTING_CARD_BLOCKED, exception.getErrorStatus());

			final VerificationCardState verificationCardState = verificationCardService.getVerificationCardEntity(verificationCardId)
					.getVerificationCardStateEntity().getState();
			assertEquals(VerificationCardState.CONFIRMATION_ATTEMPTS_EXCEEDED, verificationCardState);
		}
	}

	@Nested
	@DisplayName("blockVotingCard")
	class BlockVotingCardTest {

		@Test
		@DisplayName("when VerificationCardEntity not found throws a VerificationCardNotFoundException ")
		void blockVotingCardWhenVerificationCardEntityNotFindThrows() {
			final String nonExistingVotingCardId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
			final VerificationCardNotFoundException exception = assertThrows(VerificationCardNotFoundException.class,
					() -> verificationCardService.blockVotingCard(nonExistingVotingCardId));
			final String expectedErrorMessage = String.format("Verification card not found. [votingCardIdSearched: %s]", nonExistingVotingCardId);
			assertEquals(expectedErrorMessage, exception.getMessage());
		}

		@Test
		@DisplayName("with state already BLOCKED throws an IllegalStateException")
		void blockVotingCardWhenStateIsBlockedThrows() {
			assertDoesNotThrow(() -> verificationCardService.blockVotingCard(votingCardId));

			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> verificationCardService.blockVotingCard(votingCardId));
			final String expectedErrorMessage = String.format(
					"Verification card not blocked. The current state does not allow to block it. [votingCardId: %s, verificationCardState: %s]",
					votingCardId, VerificationCardState.BLOCKED);
			assertEquals(expectedErrorMessage, exception.getMessage());
		}

		@Test
		@DisplayName("with state CONFIRMING throws an IllegalStateException")
		void blockVotingCardWhenStateIsConfirmingThrows() {
			final List<String> shortChoiceReturnCodes = RANDOM.genUniqueDecimalStrings(4, 5);
			verificationCardService.saveSentState(verificationCardId, shortChoiceReturnCodes);
			verificationCardService.saveConfirmingState(verificationCardId);

			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> verificationCardService.blockVotingCard(votingCardId));
			final String expectedErrorMessage = String.format(
					"Verification card not blocked. The current state does not allow to block it. [votingCardId: %s, verificationCardState: %s]",
					votingCardId, VerificationCardState.CONFIRMING);
			assertEquals(expectedErrorMessage, exception.getMessage());
		}

		@Test
		@DisplayName("with state CONFIRMED throws an IllegalStateException")
		void blockVotingCardWhenStateIsConfirmedThrows() {
			final List<String> shortChoiceReturnCodes = RANDOM.genUniqueDecimalStrings(4, 5);
			verificationCardService.saveSentState(verificationCardId, shortChoiceReturnCodes);
			final String shortVoteCastReturnCode = RANDOM.genUniqueDecimalStrings(8, 1).get(0);
			verificationCardService.saveConfirmedState(verificationCardId, shortVoteCastReturnCode);

			final IllegalStateException exception = assertThrows(IllegalStateException.class,
					() -> verificationCardService.blockVotingCard(votingCardId));
			final String expectedErrorMessage = String.format(
					"Verification card not blocked. The current state does not allow to block it. [votingCardId: %s, verificationCardState: %s]",
					votingCardId, VerificationCardState.CONFIRMED);
			assertEquals(expectedErrorMessage, exception.getMessage());
		}

		@Test
		@DisplayName("with state INITIAL blocks the voting card")
		void blockVotingCardWhenStateIsInitialBlocksTheVotingCard() {
			final VotingCardDto votingCardDto = assertDoesNotThrow(() -> verificationCardService.blockVotingCard(votingCardId));
			assertEquals(VerificationCardState.BLOCKED, votingCardDto.votingCardState());
		}

		@Test
		@DisplayName("with state SENT blocks the voting card")
		void blockVotingCardWhenStateIsSentBlocksTheVotingCard() {
			final List<String> shortChoiceReturnCodes = RANDOM.genUniqueDecimalStrings(4, 5);
			verificationCardService.saveSentState(verificationCardId, shortChoiceReturnCodes);

			final VotingCardDto votingCardDto = assertDoesNotThrow(() -> verificationCardService.blockVotingCard(votingCardId));
			assertEquals(VerificationCardState.BLOCKED, votingCardDto.votingCardState());
		}
	}

	@Nested
	@DisplayName("searchVotingCards")
	class SearchVotingCardsTest {

		@Test
		@DisplayName("when more matching verification cards than voting card limit throws a TooManyVerificationCardsException")
		void searchVotingCardsWhenMoreMatchingThanLimitThrows() {
			final String partialVotingCardId = votingCardId.substring(0, 3);
			doReturn(VOTING_CARD_LIMIT + 1L).when(verificationCardRepository).countAllByPartialVotingCardId(partialVotingCardId);
			final TooManyVerificationCardsException exception = assertThrows(TooManyVerificationCardsException.class,
					() -> verificationCardService.searchVotingCards(partialVotingCardId));
			final String expectedErrorMessage = String.format("Too many voting cards match the partialId. [limit: %s, found: %s, partialId: %s]",
					VOTING_CARD_LIMIT,
					VOTING_CARD_LIMIT + 1, partialVotingCardId);
			assertEquals(expectedErrorMessage, Throwables.getRootCause(exception).getMessage());
		}

		@Test
		@DisplayName("with valid argument returns list of matching voting cards")
		void searchVotingCardsWithValidArgumentReturns() {
			final String partialVotingCardId = votingCardId.substring(0, 3);
			doReturn(1L).when(verificationCardRepository).countAllByPartialVotingCardId(partialVotingCardId);
			final List<VotingCardDto> votingCardDtos = assertDoesNotThrow(() -> verificationCardService.searchVotingCards(partialVotingCardId));
			assertEquals(1, votingCardDtos.size());
			assertEquals(votingCardId, votingCardDtos.get(0).votingCardId());
		}
	}
}
