/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientCiphertext;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base64Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.math.ZqGroup;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ElGamalGenerator;
import ch.post.it.evoting.cryptoprimitives.test.tools.generator.ZqGroupGenerator;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.domain.configuration.setupvoting.ComputingStatus;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.ControlComponentCodeShare;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.SetupComponentVerificationData;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;
import ch.post.it.evoting.votingserver.messaging.MessageHandler;
import ch.post.it.evoting.votingserver.messaging.Serializer;

@DisplayName("EncryptedLongReturnCodeSharesService calling")
class EncryptedLongReturnCodeSharesServiceTest {

	private static final Random RANDOM = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();
	private static final Alphabet base64Alphabet = Base64Alphabet.getInstance();

	private static MessageHandler messageHandler;
	private static ObjectMapper objectMapper;
	private static EncLongCodeShareRepository encLongCodeShareRepository;
	private static EncryptedLongReturnCodeSharesService encryptedLongReturnCodeSharesService;

	private static QueuedComputeChunkIdsService queuedComputeChunkIdsService;

	private String electionEventId;
	private String verificationCardSetId;
	private int chunkId;
	private int chunkCount;
	private SetupComponentVerificationDataPayload setupComponentVerificationDataPayload;

	@BeforeAll
	static void setupAll() {
		messageHandler = mock(MessageHandler.class);
		objectMapper = DomainObjectMapper.getNewInstance();
		encLongCodeShareRepository = mock(EncLongCodeShareRepository.class);
		queuedComputeChunkIdsService = mock(QueuedComputeChunkIdsService.class);
		encryptedLongReturnCodeSharesService = new EncryptedLongReturnCodeSharesService(messageHandler, encLongCodeShareRepository,
				queuedComputeChunkIdsService, new Serializer(objectMapper));
	}

	@BeforeEach
	void setup() {
		reset(encLongCodeShareRepository);

		electionEventId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		verificationCardSetId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		chunkId = 0;
		chunkCount = 5;
		final List<String> partialChoiceReturnCodes = Stream.generate(() -> RANDOM.genRandomString(4, base64Alphabet)).limit(5).toList();
		final GqGroup gqGroup = GroupTestData.getGqGroup();
		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
		final String verificationCardId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final ElGamalMultiRecipientCiphertext encryptedHashedSquaredConfirmationKey = elGamalGenerator.genRandomCiphertext(1);
		final ElGamalMultiRecipientCiphertext encryptedHashedSquaredPartialChoiceReturnCodes = elGamalGenerator.genRandomCiphertext(5);
		final ElGamalMultiRecipientPublicKey verificationCardPublicKey = elGamalGenerator.genRandomPublicKey(1);
		final SetupComponentVerificationData setupComponentVerificationData = new SetupComponentVerificationData(verificationCardId,
				verificationCardPublicKey, encryptedHashedSquaredPartialChoiceReturnCodes, encryptedHashedSquaredConfirmationKey);
		setupComponentVerificationDataPayload = new SetupComponentVerificationDataPayload(electionEventId, verificationCardSetId,
				partialChoiceReturnCodes, chunkId, gqGroup, List.of(setupComponentVerificationData));
	}

	@DisplayName("onRequest with null arguments throws a NullPointerException")
	@Test
	void onRequestWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class, () -> encryptedLongReturnCodeSharesService.onRequest(null));
	}

	@DisplayName("onRequest with valid arguments does not throw")
	@Test
	void computeGenEncLongCodeSharesWithValidArgumentsDoesNotThrow() {
		assertDoesNotThrow(() -> encryptedLongReturnCodeSharesService.onRequest(setupComponentVerificationDataPayload));
		verify(messageHandler, times(1)).sendMessage(any());
	}

	@DisplayName("getEncLongCodeShares with null arguments throws a NullPointerException")
	@Test
	void getEncLongCodeSharesWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class,
				() -> encryptedLongReturnCodeSharesService.getEncLongCodeShares(null, verificationCardSetId, chunkId));
		assertThrows(NullPointerException.class, () -> encryptedLongReturnCodeSharesService.getEncLongCodeShares(electionEventId, null, chunkId));
	}

	@DisplayName("getEncLongCodeShares with non UUID arguments throws a FailedValidationException")
	@Test
	void getEncLongCodeSharesWithNonUuidArgumentsThrows() {
		assertThrows(FailedValidationException.class,
				() -> encryptedLongReturnCodeSharesService.getEncLongCodeShares("nonUUID", verificationCardSetId, chunkId));
		assertThrows(FailedValidationException.class,
				() -> encryptedLongReturnCodeSharesService.getEncLongCodeShares(electionEventId, "nonUUID", chunkId));
	}

	@DisplayName("getEncLongCodeShares with chunk id smaller than zero throws an IllegalArgumentException")
	@Test
	void getEncLongCodeSharesWithChunkIdSmallerZeroThrows() {
		assertThrows(IllegalArgumentException.class,
				() -> encryptedLongReturnCodeSharesService.getEncLongCodeShares(electionEventId, verificationCardSetId, -1));
	}

	@DisplayName("getEncLongCodeShares when the number of GenEncLongCodeShares is not a multiple of the number of control components throws an IllegalStateException")
	@Test
	void getEncLongCodeSharesWithBadNumberOfGenEncLongCodeSharesThrows() {
		final List<EncLongCodeShareEntity> encLongCodeShareEntities = List.of(Mockito.mock(EncLongCodeShareEntity.class));
		doReturn(encLongCodeShareEntities).when(encLongCodeShareRepository)
				.findByElectionEventIdAndVerificationCardSetIdAndChunkId(electionEventId, verificationCardSetId, chunkId);
		final IllegalStateException exception = assertThrows(IllegalStateException.class,
				() -> encryptedLongReturnCodeSharesService.getEncLongCodeShares(electionEventId, verificationCardSetId, chunkId));
		final String expectedErrorMessage = "The number of enc long code shares doesn't match the number of nodes. [numberOfEncLongCodeShares: 1, numberOfControlComponents: 4]";
		assertEquals(expectedErrorMessage, Throwables.getRootCause(exception).getMessage());
	}

	@DisplayName("getEncLongCodeShares with valid input returns list of ControlComponentCodeSharesPayloads")
	@Test
	void getEncLongCodeSharesWithValidInputReturnsControlComponentCodeSharesPayloads() throws IOException {
		final GqGroup gqGroup = GroupTestData.getGqGroup();
		final List<EncLongCodeShareEntity> encLongCodeShareEntities = IntStream.rangeClosed(1, 4)
				.mapToObj(nodeId -> {
					final List<ControlComponentCodeShare> controlComponentCodeShares = Stream.generate(() -> createControlComponentCodeShare(gqGroup))
							.limit(5)
							.toList();
					final CryptoPrimitivesSignature payloadSignature = new CryptoPrimitivesSignature(new byte[] { 0b0101010 });
					final ControlComponentCodeSharesPayload controlComponentCodeSharesPayload = new ControlComponentCodeSharesPayload(electionEventId,
							verificationCardSetId, chunkId, gqGroup, controlComponentCodeShares, nodeId, payloadSignature);
					try {
						return new EncLongCodeShareEntity(electionEventId, verificationCardSetId, chunkId, nodeId,
								objectMapper.writeValueAsBytes(controlComponentCodeSharesPayload));
					} catch (final JsonProcessingException e) {
						throw new RuntimeException(e);
					}
				})
				.toList();
		doReturn(encLongCodeShareEntities).when(encLongCodeShareRepository)
				.findByElectionEventIdAndVerificationCardSetIdAndChunkId(electionEventId, verificationCardSetId, chunkId);

		final List<ControlComponentCodeSharesPayload> controlComponentCodeSharesPayloads = assertDoesNotThrow(
				() -> encryptedLongReturnCodeSharesService.getEncLongCodeShares(electionEventId, verificationCardSetId, chunkId));
		assertEquals(encLongCodeShareEntities.size(), controlComponentCodeSharesPayloads.size());
		for (int i = 0; i < controlComponentCodeSharesPayloads.size(); i++) {
			final ControlComponentCodeSharesPayload expectedPayload = objectMapper.readValue(
					encLongCodeShareEntities.get(i).getEncLongCodeShare(), ControlComponentCodeSharesPayload.class);
			assertEquals(expectedPayload, controlComponentCodeSharesPayloads.get(i));
		}
	}

	@DisplayName("getEncLongCodeSharesComputingStatus with null arguments throws a NullPointerException")
	@Test
	void getEncLongCodeSharesComputingStatusWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class,
				() -> encryptedLongReturnCodeSharesService.getEncLongCodeSharesComputingStatus(null, verificationCardSetId, chunkCount));
		assertThrows(NullPointerException.class,
				() -> encryptedLongReturnCodeSharesService.getEncLongCodeSharesComputingStatus(electionEventId, null, chunkCount));
	}

	@DisplayName("getEncLongCodeSharesComputingStatus with non UUID arguments throws a FailedValidationException")
	@Test
	void getEncLongCodeSharesComputingStatusWithNonUuidArgumentsThrows() {
		assertThrows(FailedValidationException.class,
				() -> encryptedLongReturnCodeSharesService.getEncLongCodeSharesComputingStatus("nonUUID", verificationCardSetId, chunkCount));
		assertThrows(FailedValidationException.class,
				() -> encryptedLongReturnCodeSharesService.getEncLongCodeSharesComputingStatus(electionEventId, "nonUUID", chunkCount));
	}

	@DisplayName("getEncLongCodeSharesComputingStatus with chunk id smaller than zero throws an IllegalArgumentException")
	@Test
	void getEncLongCodeSharesComputingStatusWithChunkCountSmallerZeroThrows() {
		assertThrows(IllegalArgumentException.class,
				() -> encryptedLongReturnCodeSharesService.getEncLongCodeSharesComputingStatus(electionEventId, verificationCardSetId, -1));
	}

	@DisplayName("getEncLongCodeSharesComputingStatus when actual count is smaller than expected count returns COMPUTING")
	@Test
	void getEncLongCodeSharesComputingStatusWhenActualCountSmallerThanExpectedCountReturnsComputing() {
		doReturn(19L).when(encLongCodeShareRepository).countByElectionEventIdAndVerificationCardSetId(electionEventId, verificationCardSetId);
		final ComputingStatus computingStatus = assertDoesNotThrow(
				() -> encryptedLongReturnCodeSharesService.getEncLongCodeSharesComputingStatus(electionEventId, verificationCardSetId, chunkCount));
		assertEquals(ComputingStatus.COMPUTING, computingStatus);
	}

	@DisplayName("getEncLongCodeSharesComputingStatus when actual count is equal to expected count returns COMPUTED")
	@Test
	void getEncLongCodeSharesComputingStatusWhenActualCountEqualToExpectedCountReturnsComputed() {
		doReturn(20L).when(encLongCodeShareRepository).countByElectionEventIdAndVerificationCardSetId(electionEventId, verificationCardSetId);
		final ComputingStatus computingStatus = assertDoesNotThrow(
				() -> encryptedLongReturnCodeSharesService.getEncLongCodeSharesComputingStatus(electionEventId, verificationCardSetId, chunkCount));
		assertEquals(ComputingStatus.COMPUTED, computingStatus);
	}

	@DisplayName("getEncLongCodeSharesComputingStatus when actual count is greater than expected count returns COMPUTING_ERROR")
	@Test
	void getEncLongCodeSharesComputingStatusWhenActualCountGreaterThanExpectedCountReturnsComputingError() {
		doReturn(21L).when(encLongCodeShareRepository).countByElectionEventIdAndVerificationCardSetId(electionEventId, verificationCardSetId);
		final ComputingStatus computingStatus = assertDoesNotThrow(
				() -> encryptedLongReturnCodeSharesService.getEncLongCodeSharesComputingStatus(electionEventId, verificationCardSetId, chunkCount));
		assertEquals(ComputingStatus.COMPUTING_ERROR, computingStatus);
	}

	private ControlComponentCodeShare createControlComponentCodeShare(final GqGroup gqGroup) {
		final String verificationCardId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		final ZqGroup zqGroup = ZqGroup.sameOrderAs(gqGroup);
		final ZqGroupGenerator zqGroupGenerator = new ZqGroupGenerator(zqGroup);
		final ElGamalGenerator elGamalGenerator = new ElGamalGenerator(gqGroup);
		final ElGamalMultiRecipientPublicKey voterChoiceReturnCodeGenerationPublicKey = elGamalGenerator.genRandomPublicKey(1);
		final ElGamalMultiRecipientPublicKey voterVoteCastReturnCodeGenerationPublicKey = elGamalGenerator.genRandomPublicKey(1);
		final ElGamalMultiRecipientCiphertext exponentiatedEncryptedPartialChoiceReturnCodes = elGamalGenerator.genRandomCiphertext(1);
		final ExponentiationProof encryptedPartialChoiceReturnCodeExponentiationProof = new ExponentiationProof(
				zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementMember());
		final ElGamalMultiRecipientCiphertext exponentiatedEncryptedConfirmationKey = elGamalGenerator.genRandomCiphertext(1);
		final ExponentiationProof encryptedConfirmationKeyExponentiationProof = new ExponentiationProof(zqGroupGenerator.genRandomZqElementMember(),
				zqGroupGenerator.genRandomZqElementMember());
		return new ControlComponentCodeShare(verificationCardId, voterChoiceReturnCodeGenerationPublicKey, voterVoteCastReturnCodeGenerationPublicKey,
				exponentiatedEncryptedPartialChoiceReturnCodes, exponentiatedEncryptedConfirmationKey,
				encryptedPartialChoiceReturnCodeExponentiationProof, encryptedConfirmationKeyExponentiationProof);
	}
}
