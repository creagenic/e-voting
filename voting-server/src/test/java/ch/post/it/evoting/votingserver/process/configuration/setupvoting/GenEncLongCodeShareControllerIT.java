/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.domain.configuration.setupvoting.ComputingStatus;
import ch.post.it.evoting.domain.reactor.Box;
import ch.post.it.evoting.evotinglibraries.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.ControlComponentCodeSharesPayload;
import ch.post.it.evoting.evotinglibraries.domain.returncodes.SetupComponentVerificationDataPayload;
import ch.post.it.evoting.votingserver.ArtemisSupport;
import ch.post.it.evoting.votingserver.BroadcastIntegrationTestService;

import reactor.core.publisher.Flux;

@DisplayName("GenEncLongCodeShareController integration test")
class GenEncLongCodeShareControllerIT extends ArtemisSupport {

	private final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
	@Autowired
	private BroadcastIntegrationTestService broadcastIntegrationTestService;
	@Autowired
	private WebTestClient webTestClient;

	@Test
	@DisplayName("Simulate GenEncLongCodeShare Compute Request ")
	void firstTimeCommand() throws IOException, InterruptedException, TimeoutException {
		final CountDownLatch webClientCountDownLatch = new CountDownLatch(1);
		final Resource payloadsResource = new ClassPathResource(
				"/process/configuration/setupvoting/enclongcodeshares/setupComponentVerificationDataPayload.0.json");

		final SetupComponentVerificationDataPayload requestPayload = objectMapper.readValue(payloadsResource.getFile(),
				SetupComponentVerificationDataPayload.class);

		final ExecutorService executorService = Executors.newFixedThreadPool(1, new CustomizableThreadFactory("http-pool-"));

		final String electionEventId = requestPayload.getElectionEventId();
		final String verificationCardSetId = requestPayload.getVerificationCardSetId();

		//Send the HTTP request in a separate thread and wait for the results.
		executorService.execute(() -> {
			webTestClient
					.put()
					.uri(uriBuilder -> uriBuilder
							.path("/api/v1/configuration/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/computegenenclongcodeshares")
							.build(electionEventId, verificationCardSetId))
					.contentType(MediaType.APPLICATION_NDJSON)
					.accept(MediaType.APPLICATION_JSON)
					.body(Flux.just(requestPayload), SetupComponentVerificationDataPayload.class)
					.exchange()
					.expectStatus().isCreated();

			webClientCountDownLatch.countDown();
		});

		if (!webClientCountDownLatch.await(30, SECONDS)) {
			throw new TimeoutException("Timed out waiting for request to be sent. ");
		}

		final List<ControlComponentCodeSharesPayload> controlComponentCodeSharesPayloads = NODE_IDS.stream()
				.map(nodeId -> {
					try {
						return objectMapper.readValue((new ClassPathResource(
										"/process/configuration/setupvoting/enclongcodeshares/controlComponentCodeSharesPayloads.0.node." + nodeId
												+ ".json")).getFile(),
								ControlComponentCodeSharesPayload.class);
					} catch (final IOException e) {
						throw new UncheckedIOException(e);
					}
				}).toList();

		broadcastIntegrationTestService.respondWith(nodeId -> controlComponentCodeSharesPayloads.get(nodeId - 1));

		final String chunkCount = "1";

		// Wait till the status is COMPUTED
		final CompletableFuture<Boolean> future = new CompletableFuture<>();
		try (final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1)) {
			final ScheduledFuture<?> poller = scheduler.scheduleAtFixedRate(() -> {
				if (Objects.equals((webTestClient.get()
						.uri(uriBuilder -> uriBuilder
								.path("/api/v1/configuration/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/chunkcount/{chunkCount}/status")
								.build(electionEventId, verificationCardSetId, chunkCount))
						.accept(MediaType.APPLICATION_JSON)
						.exchange()
						.expectBody(ComputingStatus.class)
						.returnResult()
						.getResponseBody()
				), ComputingStatus.COMPUTED)) {
					future.complete(true);
				}
			}, 0, 100, TimeUnit.MILLISECONDS);

			try {
				future.orTimeout(30, TimeUnit.SECONDS).join();
			} finally {
				poller.cancel(true);
				scheduler.shutdown();
			}
		}

		final List<ControlComponentCodeSharesPayload> downloadedControlComponentCodeSharesPayloads = webTestClient
				.post()
				.uri(uriBuilder -> uriBuilder
						.path("/api/v1/configuration/electionevent/{electionEventId}/verificationcardset/{verificationCardSetId}/download")
						.build(electionEventId, verificationCardSetId))
				.contentType(MediaType.APPLICATION_NDJSON)
				.body(Flux.just(0), Integer.class)
				.accept(MediaType.APPLICATION_NDJSON)
				.exchange()
				.expectStatus().isOk()
				.returnResult(new ParameterizedTypeReference<Box<List<ControlComponentCodeSharesPayload>>>() {
				})
				.getResponseBody()
				.next()
				.map(Box::boxed)
				.block();

		assertEquals(controlComponentCodeSharesPayloads, downloadedControlComponentCodeSharesPayloads);

	}
}
