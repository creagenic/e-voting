/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process.configuration.setupvoting;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.util.List;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

@DisplayName("QueuedComputeChunkIdsService calling")
class QueuedComputeChunkIdsServiceTest {

	private static final Random RANDOM = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private static QueuedComputeChunkIdsRepository queuedComputeChunkIdsRepository;
	private static QueuedComputeChunkIdsService queuedComputeChunkIdsService;

	private String electionEventId;
	private String verificationCardSetId;

	@BeforeAll
	static void setupAll() {
		queuedComputeChunkIdsRepository = mock(QueuedComputeChunkIdsRepository.class);
		queuedComputeChunkIdsService = new QueuedComputeChunkIdsService(queuedComputeChunkIdsRepository);
	}

	@BeforeEach
	void setup() {
		electionEventId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		verificationCardSetId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
	}

	@DisplayName("getQueuedComputeChunksIds with null arguments throws a NullPointerException")
	@Test
	void getQueuedComputeChunksIdsWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class, () -> queuedComputeChunkIdsService.getQueuedComputeChunkIds(null, verificationCardSetId));
		assertThrows(NullPointerException.class, () -> queuedComputeChunkIdsService.getQueuedComputeChunkIds(electionEventId, null));
	}

	@DisplayName("getQueuedComputeChunksIds with non UUIDs throws a FailedValidationException")
	@Test
	void getQueuedComputeChunksIdsWithNonUuidsThrows() {
		assertThrows(FailedValidationException.class, () -> queuedComputeChunkIdsService.getQueuedComputeChunkIds("nonUUID", verificationCardSetId));
		assertThrows(FailedValidationException.class, () -> queuedComputeChunkIdsService.getQueuedComputeChunkIds(electionEventId, "nonUUID"));
	}

	@DisplayName("getQueuedComputeChunksIds with valid input returns list of chunk ids")
	@Test
	void getQueuedComputeChunksIdsWithValidArgumentsReturns() {
		final List<QueuedComputeChunkIdsEntity> chunks = IntStream.range(0, 20).mapToObj(i -> new QueuedComputeChunkIdsEntity(electionEventId, verificationCardSetId, i))
				.toList();
		doReturn(chunks).when(queuedComputeChunkIdsRepository)
				.findAllByElectionEventIdAndVerificationCardSetIdOrderByChunkId(electionEventId, verificationCardSetId);
		final List<Integer> chunkIds = IntStream.range(0, 20).boxed().toList();
		assertEquals(chunkIds, queuedComputeChunkIdsService.getQueuedComputeChunkIds(electionEventId, verificationCardSetId));
	}
}
