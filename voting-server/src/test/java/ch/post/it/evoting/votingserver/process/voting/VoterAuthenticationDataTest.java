/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.process.voting;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

class VoterAuthenticationDataTest {

	private static final Random RANDOM = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private String electionEventId;
	private String verificationCardSetId;
	private String votingCardSetId;
	private String ballotBoxId;
	private String ballotId;
	private String verificationCardId;
	private String votingCardId;
	private String credentialId;

	@BeforeEach
	void setup() {
		electionEventId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		verificationCardSetId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		votingCardSetId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		ballotBoxId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		ballotId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		verificationCardId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		votingCardId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		credentialId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
	}

	@Test
	void constructWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class,
				() -> new VoterAuthenticationData(null, verificationCardSetId, votingCardSetId, ballotBoxId, ballotId, verificationCardId,
						votingCardId, credentialId));
		assertThrows(NullPointerException.class,
				() -> new VoterAuthenticationData(electionEventId, null, votingCardSetId, ballotBoxId, ballotId, verificationCardId, votingCardId,
						credentialId));
		assertThrows(NullPointerException.class,
				() -> new VoterAuthenticationData(electionEventId, verificationCardSetId, null, ballotBoxId, ballotId, verificationCardId,
						votingCardId, credentialId));
		assertThrows(NullPointerException.class,
				() -> new VoterAuthenticationData(electionEventId, verificationCardSetId, votingCardSetId, null, ballotId, verificationCardId,
						votingCardId, credentialId));
		assertThrows(NullPointerException.class,
				() -> new VoterAuthenticationData(electionEventId, verificationCardSetId, votingCardSetId, ballotBoxId, null, verificationCardId,
						votingCardId, credentialId));
		assertThrows(NullPointerException.class,
				() -> new VoterAuthenticationData(electionEventId, verificationCardSetId, votingCardSetId, ballotBoxId, ballotId, null, votingCardId,
						credentialId));
		assertThrows(NullPointerException.class,
				() -> new VoterAuthenticationData(electionEventId, verificationCardSetId, votingCardSetId, ballotBoxId, ballotId, verificationCardId,
						null, credentialId));
		assertThrows(NullPointerException.class,
				() -> new VoterAuthenticationData(electionEventId, verificationCardSetId, votingCardSetId, ballotBoxId, ballotId, verificationCardId,
						votingCardId, null));
	}

	@Test
	void constructWithNonUuidArgumentsThrows() {
		final String nonUuid = "this is not a UUID";
		assertThrows(FailedValidationException.class,
				() -> new VoterAuthenticationData(nonUuid, verificationCardSetId, votingCardSetId, ballotBoxId, ballotId, verificationCardId,
						votingCardId, credentialId));
		assertThrows(FailedValidationException.class,
				() -> new VoterAuthenticationData(electionEventId, nonUuid, votingCardSetId, ballotBoxId, ballotId, verificationCardId, votingCardId,
						credentialId));
		assertThrows(FailedValidationException.class,
				() -> new VoterAuthenticationData(electionEventId, verificationCardSetId, nonUuid, ballotBoxId, ballotId, verificationCardId,
						votingCardId, credentialId));
		assertThrows(FailedValidationException.class,
				() -> new VoterAuthenticationData(electionEventId, verificationCardSetId, votingCardSetId, nonUuid, ballotId, verificationCardId,
						votingCardId, credentialId));
		assertThrows(FailedValidationException.class,
				() -> new VoterAuthenticationData(electionEventId, verificationCardSetId, votingCardSetId, ballotBoxId, nonUuid, verificationCardId,
						votingCardId, credentialId));
		assertThrows(FailedValidationException.class,
				() -> new VoterAuthenticationData(electionEventId, verificationCardSetId, votingCardSetId, ballotBoxId, ballotId, nonUuid,
						votingCardId, credentialId));
		assertThrows(FailedValidationException.class,
				() -> new VoterAuthenticationData(electionEventId, verificationCardSetId, votingCardSetId, ballotBoxId, ballotId, verificationCardId,
						nonUuid, credentialId));
		assertThrows(FailedValidationException.class,
				() -> new VoterAuthenticationData(electionEventId, verificationCardSetId, votingCardSetId, ballotBoxId, ballotId, verificationCardId,
						votingCardId, nonUuid));
	}

	@Test
	void constructWithValidInputInstantiatesObject() {
		final VoterAuthenticationData voterAuthenticationData = assertDoesNotThrow(
				() -> new VoterAuthenticationData(electionEventId, verificationCardSetId, votingCardSetId, ballotBoxId, ballotId, verificationCardId,
						votingCardId, credentialId));

		assertEquals(electionEventId, voterAuthenticationData.electionEventId());
		assertEquals(verificationCardSetId, voterAuthenticationData.verificationCardSetId());
		assertEquals(votingCardSetId, voterAuthenticationData.votingCardSetId());
		assertEquals(ballotBoxId, voterAuthenticationData.ballotBoxId());
		assertEquals(ballotId, voterAuthenticationData.ballotId());
		assertEquals(verificationCardId, voterAuthenticationData.verificationCardId());
		assertEquals(votingCardId, voterAuthenticationData.votingCardId());
		assertEquals(credentialId, voterAuthenticationData.credentialId());
	}
}
