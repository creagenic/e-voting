/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.protocol.voting.authenticatevoter;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

class VerifyAuthenticationChallengeContextTest {

	private static final Random RANDOM = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private String electionEventId;
	private String credentialId;

	@BeforeEach
	void setup() {
		electionEventId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
		credentialId = RANDOM.genRandomString(ID_LENGTH, base16Alphabet);
	}

	@Test
	void constructWithNullArgumentsThrows() {
		assertThrows(NullPointerException.class, () -> new VerifyAuthenticationChallengeContext(null, credentialId));
		assertThrows(NullPointerException.class, () -> new VerifyAuthenticationChallengeContext(electionEventId, null));
	}

	@Test
	void constructWithElectionEventIdNotValidUuidThrows() {
		final String tooLongElectionEventId = electionEventId + "1";
		assertThrows(FailedValidationException.class, () -> new VerifyAuthenticationChallengeContext(tooLongElectionEventId, credentialId));
		final String tooShortElectionEventId = electionEventId.substring(1);
		assertThrows(FailedValidationException.class, () -> new VerifyAuthenticationChallengeContext(tooShortElectionEventId, credentialId));
		final String electionEventIdBadCharacter = "$" + electionEventId.substring(1);
		assertThrows(FailedValidationException.class,
				() -> new VerifyAuthenticationChallengeContext(electionEventIdBadCharacter, credentialId));
	}

	@Test
	void constructWithCredentialIdNotValidUuidThrows() {
		final String tooShortCredentialId = credentialId.substring(1);
		assertThrows(FailedValidationException.class,
				() -> new VerifyAuthenticationChallengeContext(electionEventId, tooShortCredentialId));
		final String tooLongCredentialId = credentialId + "1";
		assertThrows(FailedValidationException.class,
				() -> new VerifyAuthenticationChallengeContext(electionEventId, tooLongCredentialId));
		final String credentialIdBadCharacter = "?" + credentialId;
		assertThrows(FailedValidationException.class,
				() -> new VerifyAuthenticationChallengeContext(electionEventId, credentialIdBadCharacter));
	}

	@Test
	void constructWithValidInputsDoesNotThrow() {
		assertDoesNotThrow(() -> new VerifyAuthenticationChallengeContext(electionEventId, credentialId));
	}
}
