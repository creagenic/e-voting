/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.protocol.voting.confirmvote;

import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

@DisplayName("ExtractVCCContext constructed with")
class ExtractVCCContextTest {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private String electionEventId;
	private String verificationCardId;
	private GqGroup encryptionGroup;

	@BeforeEach
	void setUp() {
		encryptionGroup = GroupTestData.getGqGroup();
		electionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);
		verificationCardId = random.genRandomString(ID_LENGTH, base16Alphabet);
	}

	@Test
	@DisplayName("any null parameter throws NullPointerException")
	void nullParamsThrows() {
		assertThrows(NullPointerException.class, () -> new ExtractVCCContext(null, electionEventId, verificationCardId));
		assertThrows(NullPointerException.class, () -> new ExtractVCCContext(encryptionGroup, null, verificationCardId));
		assertThrows(NullPointerException.class, () -> new ExtractVCCContext(encryptionGroup, electionEventId, null));
	}

	@Test
	@DisplayName("invalid election event id throws FailedValidationException")
	void invalidElectionEventIdThrows() {
		assertThrows(FailedValidationException.class, () -> new ExtractVCCContext(encryptionGroup, "invalid", verificationCardId));
	}

	@Test
	@DisplayName("invalid verification card id throws FailedValidationException")
	void invalidVerificationCardIdThrows() {
		assertThrows(FailedValidationException.class, () -> new ExtractVCCContext(encryptionGroup, electionEventId, "invalid"));
	}

	@Test
	@DisplayName("valid parameters does not throw")
	void validParamsDoesNotThrow() {
		assertDoesNotThrow(() -> new ExtractVCCContext(encryptionGroup, electionEventId, verificationCardId));
	}

}