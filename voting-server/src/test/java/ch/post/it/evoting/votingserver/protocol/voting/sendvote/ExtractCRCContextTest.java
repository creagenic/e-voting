/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.protocol.voting.sendvote;

import static ch.post.it.evoting.evotinglibraries.domain.VotingOptionsConstants.MAXIMUM_SUPPORTED_NUMBER_OF_SELECTIONS;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.cryptoprimitives.test.tools.data.GroupTestData;
import ch.post.it.evoting.domain.ElectionSetupUtils;
import ch.post.it.evoting.evotinglibraries.domain.validations.FailedValidationException;

@DisplayName("ExtractCRCContext constructed with")
class ExtractCRCContextTest {

	private static final Random random = RandomFactory.createRandom();
	private static final SecureRandom secureRandom = new SecureRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private String electionEventId;
	private String verificationCardId;
	private GqGroup encryptionGroup;
	private List<String> blankCorrectnessInformation;

	@BeforeEach
	void setUp() {
		encryptionGroup = GroupTestData.getGqGroup();
		electionEventId = random.genRandomString(ID_LENGTH, base16Alphabet);
		verificationCardId = random.genRandomString(ID_LENGTH, base16Alphabet);

		final int psi = secureRandom.nextInt(1, 5);
		blankCorrectnessInformation = ElectionSetupUtils.genBlankCorrectnessInformation(psi);
	}

	@Test
	@DisplayName("any null parameter throws NullPointerException")
	void nullParamsThrows() {
		assertThrows(NullPointerException.class, () -> new ExtractCRCContext(null, electionEventId, verificationCardId, blankCorrectnessInformation));
		assertThrows(NullPointerException.class, () -> new ExtractCRCContext(encryptionGroup, null, verificationCardId, blankCorrectnessInformation));
		assertThrows(NullPointerException.class, () -> new ExtractCRCContext(encryptionGroup, electionEventId, null, blankCorrectnessInformation));
		assertThrows(NullPointerException.class, () -> new ExtractCRCContext(encryptionGroup, electionEventId, verificationCardId, null));
	}

	@Test
	@DisplayName("invalid election event id throws FailedValidationException")
	void invalidElectionEventIdThrows() {
		assertThrows(FailedValidationException.class,
				() -> new ExtractCRCContext(encryptionGroup, "invalid", verificationCardId, blankCorrectnessInformation));
	}

	@Test
	@DisplayName("invalid verification card id throws FailedValidationException")
	void invalidVerificationCardIdThrows() {
		assertThrows(FailedValidationException.class,
				() -> new ExtractCRCContext(encryptionGroup, electionEventId, "invalid", blankCorrectnessInformation));
	}

	@Test
	@DisplayName("blank correctness information containing null throws NullPointerException")
	void blankCorrectnessInformationWithNullThrows() {
		final List<String> blankCorrectnessInformationWithNull = new ArrayList<>(blankCorrectnessInformation);
		blankCorrectnessInformationWithNull.set(0, null);

		assertThrows(NullPointerException.class, () -> new ExtractCRCContext(encryptionGroup, electionEventId, verificationCardId,
				blankCorrectnessInformationWithNull));
	}

	@Test
	@DisplayName("blank correctness information not in range throws IllegalArgumentException")
	void notRangeBlankCorrectnessInformationThrows() {
		final String errorMessage = String.format("The blank correctness information size must be in range [1, %s].",
				MAXIMUM_SUPPORTED_NUMBER_OF_SELECTIONS);

		// Too few.
		final List<String> emptyBlankCorrectnessInformation = List.of();

		final IllegalArgumentException tooFewException = assertThrows(IllegalArgumentException.class,
				() -> new ExtractCRCContext(encryptionGroup, electionEventId, verificationCardId, emptyBlankCorrectnessInformation));
		assertEquals(errorMessage, Throwables.getRootCause(tooFewException).getMessage());

		// Too many.
		final List<String> tooManyBlankCorrectnessInformation = ElectionSetupUtils.genBlankCorrectnessInformation(
				MAXIMUM_SUPPORTED_NUMBER_OF_SELECTIONS + 1);

		final IllegalArgumentException tooManyException = assertThrows(IllegalArgumentException.class,
				() -> new ExtractCRCContext(encryptionGroup, electionEventId, verificationCardId, tooManyBlankCorrectnessInformation));
		assertEquals(errorMessage, Throwables.getRootCause(tooManyException).getMessage());
	}

	@Test
	@DisplayName("valid parameters does not throw")
	void validParamsDoesNotThrow() {
		assertDoesNotThrow(() -> new ExtractCRCContext(encryptionGroup, electionEventId, verificationCardId, blankCorrectnessInformation));
	}

}