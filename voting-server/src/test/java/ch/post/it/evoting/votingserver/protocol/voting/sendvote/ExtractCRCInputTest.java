/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.protocol.voting.sendvote;

import static ch.post.it.evoting.evotinglibraries.domain.VotingOptionsConstants.MAXIMUM_SUPPORTED_NUMBER_OF_SELECTIONS;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.test.tools.TestGroupSetup;
import ch.post.it.evoting.evotinglibraries.domain.common.Constants;
import ch.post.it.evoting.votingserver.process.voting.ReturnCodesMappingTable;

@DisplayName("ExtractCRCInput constructed with")
class ExtractCRCInputTest extends TestGroupSetup {
	private static final SecureRandom secureRandom = new SecureRandom();

	private int psi;
	private ReturnCodesMappingTable returnCodesMappingTable;
	private List<GroupVector<GqElement, GqGroup>> longChoiceReturnCodeShares;

	@BeforeEach
	void setUp() {
		psi = secureRandom.nextInt(1, 5);
		longChoiceReturnCodeShares = IntStream.range(0, Constants.NUMBER_OF_CONTROL_COMPONENTS)
				.mapToObj(i -> gqGroupGenerator.genRandomGqElementVector(psi))
				.toList();
		returnCodesMappingTable = hashedLongReturnCode -> Optional.of("encryptedShortReturnCode");
	}

	@Test
	@DisplayName("any null parameter throws NullPointerException")
	void nullParamsThrows() {
		assertThrows(NullPointerException.class, () -> new ExtractCRCInput(null, returnCodesMappingTable));
		assertThrows(NullPointerException.class, () -> new ExtractCRCInput(longChoiceReturnCodeShares, null));
	}

	@Test
	@DisplayName("long Choice Return Code shares containing null throws NullPointerException")
	void longChoiceReturnCodeSharesWithNullThrows() {
		final List<GroupVector<GqElement, GqGroup>> mutableShares = new ArrayList<>(longChoiceReturnCodeShares);
		mutableShares.set(0, null);

		assertThrows(NullPointerException.class, () -> new ExtractCRCInput(mutableShares, returnCodesMappingTable));
	}

	@Test
	@DisplayName("wrong number of long Choice Return Code shares throws IllegalArgumentException")
	void wrongNumberOfLongChoiceReturnCodeSharesThrows() {
		// Too few.
		final List<GroupVector<GqElement, GqGroup>> emptyShares = List.of();
		assertThrows(IllegalArgumentException.class, () -> new ExtractCRCInput(emptyShares, returnCodesMappingTable));

		// Too many.
		final List<GroupVector<GqElement, GqGroup>> tooManyShares = new ArrayList<>(longChoiceReturnCodeShares);
		tooManyShares.add(gqGroupGenerator.genRandomGqElementVector(psi));

		assertThrows(IllegalArgumentException.class, () -> new ExtractCRCInput(tooManyShares, returnCodesMappingTable));
	}

	@Test
	@DisplayName("long Choice Return Code shares of different sizes throws IllegalArgumentException")
	void differentSizesLongChoiceReturnCodeShares() {
		final List<GroupVector<GqElement, GqGroup>> mutableShares = new ArrayList<>(longChoiceReturnCodeShares);
		mutableShares.set(0, gqGroupGenerator.genRandomGqElementVector(psi + 1));

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new ExtractCRCInput(mutableShares, returnCodesMappingTable));
		assertEquals("All long Choice Return Code Shares must have the same size.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("long Choice Return Code shares not in range throws IllegalArgumentException")
	void notInRangeLongChoiceReturnCodeShares() {
		final String errorMessage = String.format("The long Choice Return Code Shares size must be in range [1, %s].",
				MAXIMUM_SUPPORTED_NUMBER_OF_SELECTIONS);

		// Too few.
		final List<GroupVector<GqElement, GqGroup>> tooFewCRC = IntStream.range(0, Constants.NUMBER_OF_CONTROL_COMPONENTS)
				.mapToObj(i -> GroupVector.<GqElement, GqGroup>of())
				.toList();

		final IllegalArgumentException tooFewException = assertThrows(IllegalArgumentException.class,
				() -> new ExtractCRCInput(tooFewCRC, returnCodesMappingTable));
		assertEquals(errorMessage, Throwables.getRootCause(tooFewException).getMessage());

		// Too many.
		final List<GroupVector<GqElement, GqGroup>> tooManyCRC = IntStream.range(0, Constants.NUMBER_OF_CONTROL_COMPONENTS)
				.mapToObj(i -> gqGroupGenerator.genRandomGqElementVector(MAXIMUM_SUPPORTED_NUMBER_OF_SELECTIONS + 1))
				.toList();

		final IllegalArgumentException tooManyException = assertThrows(IllegalArgumentException.class,
				() -> new ExtractCRCInput(tooManyCRC, returnCodesMappingTable));
		assertEquals(errorMessage, Throwables.getRootCause(tooManyException).getMessage());
	}

	@Test
	@DisplayName("long Choice Return Code shares of different groups throws IllegalArgumentException")
	void differentGroupsLongChoiceReturnCodeShares() {
		final List<GroupVector<GqElement, GqGroup>> mutableShares = new ArrayList<>(longChoiceReturnCodeShares);
		mutableShares.set(0, otherGqGroupGenerator.genRandomGqElementVector(psi));

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new ExtractCRCInput(mutableShares, returnCodesMappingTable));
		assertEquals("All long Choice Return Code Shares must have the same Gq group.", Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("valid parameters does not throw")
	void validParamsDoesNotThrow() {
		assertDoesNotThrow(() -> new ExtractCRCInput(longChoiceReturnCodeShares, returnCodesMappingTable));
	}

}