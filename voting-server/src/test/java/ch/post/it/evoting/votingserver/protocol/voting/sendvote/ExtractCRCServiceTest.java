/*
 * (c) Copyright 2024 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.protocol.voting.sendvote;

import static ch.post.it.evoting.cryptoprimitives.hashing.HashFactory.createHash;
import static ch.post.it.evoting.evotinglibraries.domain.ControlComponentConstants.NODE_IDS;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.BASE64_ENCODED_HASH_OUTPUT_LENGTH;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.ID_LENGTH;
import static ch.post.it.evoting.evotinglibraries.domain.common.Constants.SHORT_CHOICE_RETURN_CODE_LENGTH;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.google.common.base.Throwables;

import ch.post.it.evoting.cryptoprimitives.hashing.Hash;
import ch.post.it.evoting.cryptoprimitives.math.Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base16Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.Base64Alphabet;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.Random;
import ch.post.it.evoting.cryptoprimitives.math.RandomFactory;
import ch.post.it.evoting.domain.voting.sendvote.ControlComponentLCCSharePayload;
import ch.post.it.evoting.domain.voting.sendvote.LongChoiceReturnCodesShare;
import ch.post.it.evoting.evotinglibraries.domain.SerializationUtils;
import ch.post.it.evoting.evotinglibraries.domain.common.ContextIds;
import ch.post.it.evoting.evotinglibraries.domain.election.PrimesMappingTable;
import ch.post.it.evoting.evotinglibraries.domain.election.generators.PrimesMappingTableGenerator;
import ch.post.it.evoting.evotinglibraries.domain.signature.CryptoPrimitivesSignature;
import ch.post.it.evoting.evotinglibraries.protocol.algorithms.preliminaries.votingoptions.PrimesMappingTableAlgorithms;
import ch.post.it.evoting.votingserver.process.ElectionEventService;
import ch.post.it.evoting.votingserver.process.IdentifierValidationService;
import ch.post.it.evoting.votingserver.process.VerificationCardService;
import ch.post.it.evoting.votingserver.process.voting.ReturnCodesMappingTable;
import ch.post.it.evoting.votingserver.process.voting.ReturnCodesMappingTableSupplier;

@DisplayName("extractCRC called with")
class ExtractCRCServiceTest {

	private static final Random random = RandomFactory.createRandom();
	private static final Alphabet base16Alphabet = Base16Alphabet.getInstance();

	private static ExtractCRCService extractCRCService;
	private static ContextIds contextIds;
	private static List<ControlComponentLCCSharePayload> controlComponentLCCSharePayloads;

	@BeforeAll
	static void setUpAll() {
		final ExtractCRCAlgorithm extractCRCAlgorithm = mock(ExtractCRCAlgorithm.class);
		final ElectionEventService electionEventService = mock(ElectionEventService.class);
		final VerificationCardService verificationCardService = mock(VerificationCardService.class);
		final IdentifierValidationService identifierValidationService = mock(IdentifierValidationService.class);
		final PrimesMappingTableAlgorithms primesMappingTableAlgorithms = new PrimesMappingTableAlgorithms();
		final ReturnCodesMappingTableSupplier returnCodesMappingTableSupplier = mock(ReturnCodesMappingTableSupplier.class);
		extractCRCService = new ExtractCRCService(extractCRCAlgorithm, electionEventService, verificationCardService, identifierValidationService,
				primesMappingTableAlgorithms, returnCodesMappingTableSupplier);

		contextIds = new ContextIds(
				random.genRandomString(ID_LENGTH, base16Alphabet),
				random.genRandomString(ID_LENGTH, base16Alphabet),
				random.genRandomString(ID_LENGTH, base16Alphabet)
		);
		final PrimesMappingTableGenerator primesMappingTableGenerator = new PrimesMappingTableGenerator();
		final PrimesMappingTable primesMappingTable = primesMappingTableGenerator.generate(1);
		final int numberOfSelections = primesMappingTableAlgorithms.getPsi(primesMappingTable);
		controlComponentLCCSharePayloads = NODE_IDS.stream()
				.map(nodeId -> {
					final GroupVector<GqElement, GqGroup> longChoiceCodes = SerializationUtils.getLongChoiceCodes(numberOfSelections);
					final LongChoiceReturnCodesShare longChoiceReturnCodesShare = new LongChoiceReturnCodesShare(contextIds.electionEventId(),
							contextIds.verificationCardSetId(), contextIds.verificationCardId(), nodeId, longChoiceCodes);
					final ControlComponentLCCSharePayload controlComponentLCCSharePayload = new ControlComponentLCCSharePayload(
							longChoiceCodes.getGroup(), longChoiceReturnCodesShare);

					final Hash hash = createHash();
					controlComponentLCCSharePayload.setSignature(new CryptoPrimitivesSignature(hash.recursiveHash(controlComponentLCCSharePayload)));

					return controlComponentLCCSharePayload;
				})
				.toList();

		doNothing().when(identifierValidationService).validateContextIds(contextIds);

		final GqGroup encryptionGroup = controlComponentLCCSharePayloads.getFirst().getEncryptionGroup();
		when(electionEventService.getEncryptionGroup(contextIds.electionEventId())).thenReturn(encryptionGroup);

		when(verificationCardService.getPrimesMappingTable(contextIds.verificationCardId())).thenReturn(primesMappingTable);

		final ReturnCodesMappingTable returnCodesMappingTable = hashedLongReturnCode -> Optional.of(
				random.genRandomString(BASE64_ENCODED_HASH_OUTPUT_LENGTH, Base64Alphabet.getInstance()));
		when(returnCodesMappingTableSupplier.get(contextIds.verificationCardSetId())).thenReturn(returnCodesMappingTable);

		when(extractCRCAlgorithm.extractCRC(any(), any()))
				.thenReturn(new ExtractCRCOutput(random.genUniqueDecimalStrings(SHORT_CHOICE_RETURN_CODE_LENGTH, 2)));
	}

	private static Stream<Arguments> provideNullParameters() {
		final List<ControlComponentLCCSharePayload> controlComponentLCCSharePayloadsWithNull = new ArrayList<>(controlComponentLCCSharePayloads);
		controlComponentLCCSharePayloadsWithNull.add(null);

		return Stream.of(
				Arguments.of(null, controlComponentLCCSharePayloads),
				Arguments.of(contextIds, null),
				Arguments.of(contextIds, controlComponentLCCSharePayloadsWithNull)
		);
	}

	@ParameterizedTest
	@MethodSource("provideNullParameters")
	@DisplayName("null parameters throws NullPointerException")
	void extractCRCWithNullParametersThrows(final ContextIds contextIds,
			final List<ControlComponentLCCSharePayload> controlComponentLCCSharePayloads) {
		assertThrows(NullPointerException.class,
				() -> extractCRCService.extractCRC(contextIds, controlComponentLCCSharePayloads));
	}

	@Test
	@DisplayName("too few payloads throws IllegalArgumentException")
	void extractCRCWithTooFewControlComponentLCCSharePayloadsThrows() {
		final List<ControlComponentLCCSharePayload> tooFewControlComponentLCCSharePayloads = List.of();

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> extractCRCService.extractCRC(contextIds, tooFewControlComponentLCCSharePayloads));

		final String expected = "There is the wrong number of Control Component LCC Share payloads.";
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("too many payloads throws IllegalArgumentException")
	void extractCRCWithTooManyControlComponentLCCSharePayloadsThrows() {
		final List<ControlComponentLCCSharePayload> tooManyControlComponentLCCSharePayloads = new ArrayList<>(controlComponentLCCSharePayloads);
		tooManyControlComponentLCCSharePayloads.add(controlComponentLCCSharePayloads.get(0));

		final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> extractCRCService.extractCRC(contextIds, tooManyControlComponentLCCSharePayloads));

		final String expected = "There is the wrong number of Control Component LCC Share payloads.";
		assertEquals(expected, Throwables.getRootCause(exception).getMessage());
	}

	@Test
	@DisplayName("valid parameters does not throw")
	void extractCRCWithValidParametersDoesNotThrow() {
		assertDoesNotThrow(() -> extractCRCService.extractCRC(contextIds, controlComponentLCCSharePayloads));
	}

}
